<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRmaShipmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rma_shipment', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('rma_id')->default(0);
            $table->string('track_no', 191)->unique();
            $table->integer('mode_id')->default(0);
            $table->integer('agent_id')->default(0);
            $table->text('track_link');
            $table->boolean('status')->default(1);
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('rma_shipment_item', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('parent_id')->unsigned()->index('rma_shipment_item_parent_id_foreign');
            $table->bigInteger('catalog_id')->unsigned()->index('rma_shipment_item_catalog_id_foreign');
            $table->boolean('qty')->default(1);
            $table->boolean('status')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rma_shipment');
    }
}
