<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCatalogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('catalogs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('brand_id')->default(0);
            $table->integer('model_id')->default(0);
            $table->integer('series_id')->default(0);
            $table->string('name', 191);
            $table->string('image', 191)->nullable();
            $table->string('focus_code', 191)->nullable();
            $table->string('part_no', 191)->nullable();
            $table->text('description', 65535)->nullable();
            $table->tinyInteger('status')->default(0)->comment('0 => InActive, 1 => Published, 2 => Draft');
            $table->integer('created_by')->unsigned();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('catalogs');
    }
}
