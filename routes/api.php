<?php

use Illuminate\Http\Request;

Route::group(['namespace' => 'Api\V1', 'prefix' => 'v1', 'as' => 'v1.'], function () {
    Route::post('login', 'LoginController@login');
    Route::group(['middleware' => ['auth:api']], function () {
        Route::group(['prefix' => 'auth'], function () {
            Route::get('logout', 'LoginController@logout');
            Route::get('me', 'LoginController@me');
            Route::post('change-password', 'LoginController@changePassword');
            Route::patch('profile/update', 'LoginController@update');  
            Route::get('user/list', 'LoginController@getUser');
        });
        Route::post('password/create', 'ForgotPasswordController@create');
        Route::group(['prefix' => 'dashbord'], function () {
            Route::get('pricecalender', 'DashboardController@fetchPrice');
            Route::apiResource('/', 'DashboardController');
        });
        //product
        Route::apiResource('product', 'ProductController');
        Route::apiResource('vendor', 'VendorController');

        // stocks
        Route::apiResource('stocks', 'StocksController');
        // price
        Route::apiResource('price', 'PriceController');
       
        Route::group(['prefix' => 'cart'], function () {
             Route::post('item/update/{id}', 'CartController@itemUpdate');
        });    

       Route::group(['prefix' => 'enquiry'], function () {
            Route::post('send', 'EnquiryController@send');
            Route::get('list', 'EnquiryController@index');
            Route::get('view/{id}','EnquiryController@show');
            Route::post('convert/{id}','EnquiryController@convert');
            Route::post('create/{id}', 'EnquiryController@submit');
            Route::post('store', 'EnquiryController@store');
        });

        Route::group(['prefix' => 'quotation'], function () {
            Route::post('create/{id}', 'QuotationController@submit');
            Route::post('convert/{id}','QuotationController@convert');
            Route::post('item/update/{id}', 'QuotationController@itemUpdate');
            Route::post('store', 'QuotationController@store');
        });
        
        Route::group(['prefix' => 'order'], function () {
            Route::post('create/{id}', 'OrderController@submit');
            Route::post('convert/{id}','OrderController@convert');
            Route::post('item/update/{id}', 'OrderController@itemUpdate');
            Route::post('store', 'OrderController@store');
        });
        
        Route::group(['prefix' => 'invoice'], function () {
            Route::post('create/{id}', 'InvoiceController@submit');
            Route::post('store', 'InvoiceController@store');
            Route::post('item/update/{id}', 'InvoiceController@itemUpdate');
            Route::post('convert/{id}','InvoiceController@convert');
        });
        
        Route::group(['prefix' => 'shipment'], function () {
            Route::get('status/{id}', 'ShipmentController@update');
        });
        
         Route::group(['prefix' => 'notification'], function () {
            Route::post('read', 'NotificationController@read');
        });

        // cart
        Route::apiResource('cart', 'CartController');
        // quotation
        Route::apiResource('quotation', 'QuotationController');
        // order
        Route::apiResource('order', 'OrderController');
        // enquiry
        Route::apiResource('enquiry', 'EnquiryController');
        // invoice
        Route::apiResource('invoice', 'InvoiceController');
        
        Route::apiResource('shipment', 'ShipmentController');
        
        Route::apiResource('rma', 'RMAController');
        
        // chat
        Route::apiResource('chat', 'ChatController');
        
        // notification
        Route::apiResource('notification', 'NotificationController');

    });
});
