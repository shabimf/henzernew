<?php

use App\Http\Controllers\LanguageController;

Route::get('lang/{lang}', [LanguageController::class, 'swap']);

Route::group(['namespace' => 'Frontend', 'as' => 'frontend.'], function () {
    include_route_files(__DIR__.'/frontend/');
});

Route::group(['namespace' => 'Backend', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'access.routeNeedsPermission:view-backend'], function () {
    
    Route::redirect('/', '/admin/dashboard', 301);
    
    Route::group([
        'prefix' => 'auth', 
        'as' => 'auth.',
        'namespace' => 'Auth'
    ], function () {
        // User Management
        Route::group(['namespace' => 'User'], function () {
            // For DataTables
            Route::post('user/get', 'UserTableController')->name('user.get');
    
            // User Status'
            Route::get('user/deactivated', 'UserStatusController@getDeactivated')->name('user.deactivated');
            Route::get('user/deleted', 'UserStatusController@getDeleted')->name('user.deleted');
    
            // User CRUD
            Route::get('user', 'UserController@index')->name('user.index');
            Route::get('user/create', 'UserController@create')->name('user.create');
            Route::post('user',  'UserController@store')->name('user.store');
    
            // Specific User
            Route::group(['prefix' => 'user/{user}'], function () {
                // User
                Route::get('/', 'UserController@show')->name('user.show');
                Route::get('edit', 'UserController@edit')->name('user.edit');
                Route::patch('/', 'UserController@update')->name('user.update');
                Route::delete('/', 'UserController@destroy')->name('user.destroy');
    
                // Account
                Route::get('account/confirm/resend', 'UserConfirmationController@sendConfirmationEmail')->name('user.account.confirm.resend');
    
                // Status
                Route::get('mark/{status}', 'UserStatusController@mark')->name('user.mark')->where(['status' => '[0,1]']);
    
                // Social
                Route::delete('social/{social}/unlink', 'UserSocialController@unlink')->name('user.social.unlink');
    
                // Confirmation
                Route::get('confirm', 'UserConfirmationController@confirm')->name('user.confirm');
                Route::get('unconfirm', 'UserConfirmationController@unconfirm')->name('user.unconfirm');
    
                // Password
                Route::get('password/change', 'UserPasswordController@edit')->name('user.change-password');
                Route::patch('password/change', 'UserPasswordController@update')->name('user.change-password.post');
    
                // log in as
                Route::get('login-as', 'UserAccessController@loginAs')->name('user.login-as');
    
                // Session
                Route::get('clear-session', 'UserSessionController@clearSession')->name('user.clear-session');
    
                // Deleted
                Route::delete('delete-permanently', 'UserStatusController@delete')->name('user.delete-permanently');
                Route::post('restore', 'UserStatusController@restore')->name('user.restore');
            });
        });
    
        // Role Management
        Route::group(['namespace' => 'Role'], function () {
            Route::get('role', 'RoleController@index')->name('role.index');
            Route::get('role/create', 'RoleController@create')->name('role.create');
            Route::post('role', 'RoleController@store')->name('role.store');
    
            Route::group(['prefix' => 'role/{role}'], function () {
                Route::get('edit', 'RoleController@edit')->name('role.edit');
                Route::patch('/', 'RoleController@update')->name('role.update');
                Route::delete('/', 'RoleController@destroy')->name('role.destroy');
            });
    
            // For DataTables
            Route::post('role/get', 'RoleTableController')->name('role.get');
        });
    
        // Permission Management
        Route::group(['namespace' => 'Permission'], function () {
            Route::resource('permission', 'PermissionsController', ['except' => ['show']]);
    
            //For DataTables
           Route::post('permission/get', 'PermissionTableController')->name('permission.get');
        });
    });
    

    Route::get('dashboard', 'DashboardController@index')->name('dashboard');
    Route::post('get-permission', 'DashboardController@getPermissionByRole')->name('get.permission');

    //For settings
    Route::group(['prefix' => 'settings'], function () {
        Route::group(['namespace' => 'Countries'], function () {
            $country_path = 'countries';  
            Route::resource($country_path, 'CountriesController', ['except' => ['show']]);
            Route::post($country_path.'/get', 'CountriesTableController')->name('countries.get');
        });
        Route::group(['namespace' => 'Cities'], function () {
            $city_path = 'cities';  
            Route::resource($city_path, 'CitiesController', ['except' => ['show']]);
            Route::post($city_path.'/get', 'CitiesTableController')->name('cities.get');
        });
        Route::group(['namespace' => 'Branches'], function () {
            $branch_path = 'branches';  
            Route::resource($branch_path, 'BranchesController', ['except' => ['show']]);
            Route::post($branch_path.'/get', 'BranchesTableController')->name('branches.get');
        });
        Route::group(['namespace' => 'Vendors'], function () {
            $vendor_path = 'vendors';  
            Route::resource($vendor_path, 'VendorsController', ['except' => ['show']]);
            Route::post($vendor_path.'/get',  'VendorsTableController')->name('vendors.get');
        });
        Route::group(['namespace' => 'Subvendors'], function () {
            $subvendor_path = 'subvendors';  
            Route::resource($subvendor_path, 'SubVendorsController', ['except' => ['show']]);
            // Route::post($subvendor_path.'/get', 'SubVendorsTableController')->name('subvendors.get');
        });
        Route::group(['namespace' => 'Currencies'], function () {
            $currencies_path = 'currencies';  
            Route::resource($currencies_path, 'CurrenciesController', ['except' => ['show']]);
            Route::post($currencies_path.'/get', 'CurrenciesTableController')->name('currencies.get');
        });
       
    });
    
    // For catalogs
    Route::group(['prefix' => 'catalogs'], function () {

        Route::group(['namespace' => 'Brands'], function () {
            $brand_path = 'brands';  
            Route::resource($brand_path, 'BrandsController', ['except' => ['show']]);
            Route::post($brand_path.'/get', 'BrandsTableController')->name('brands.get');            
        });
        Route::group(['namespace' => 'Models'], function () {
            $model_path = 'models';  
            Route::resource($model_path, 'ModelsController', ['except' => ['show']]);
            Route::post($model_path.'/get', 'ModelsTableController')->name('models.get');
        });
        Route::group(['namespace' => 'Series'], function () {
            $series_path = 'series';  
            Route::resource($series_path, 'SeriesController', ['except' => ['show']]);
            Route::post($series_path.'/get',  'SeriesTableController')->name('series.get');
        });
        Route::group(['namespace' => 'Attributes'], function () {
            $attributes_path = 'attributes';  
            Route::resource($attributes_path, 'AttributesController', ['except' => ['show']]);
            Route::post($attributes_path.'/get',  'AttributesTableController')->name('attributes.get');
        });

        
    });

    
    Route::group(['namespace' => 'Catalogs'], function () {
        $catalog_path = 'catalogs';  
        Route::resource($catalog_path, 'CatalogsController', ['except' => ['show']]);
        Route::post($catalog_path.'/get', 'CatalogsTableController')->name('catalogs.get');            
    });

    //For shipping
    Route::group(['prefix' => 'shipping'], function () {

        Route::group(['namespace' => 'Cargo'], function () {
            $cargo_path = 'cargo';  
            Route::resource($cargo_path, 'CargoController', ['except' => ['show']]);
            Route::post($cargo_path.'/get', 'CargoTableController')->name('cargo.get');
        });

        Route::group(['namespace' => 'Agent'], function () {
            $agent_path = 'agent';  
            Route::resource($agent_path, 'AgentController', ['except' => ['show']]);
            Route::post($agent_path.'/get', 'AgentTableController')->name('agent.get');
        });
    });    
    Route::group(['prefix' => 'ajax'], function () {
        Route::group(['namespace' => 'Common'], function () {
            Route::post('get-model', 'CommonController@index');
            Route::post('get-city', 'CommonController@getCity');
            Route::post('get-series', 'CommonController@getSerie');
            Route::get('get-attribute', 'CommonController@getAttribute');
            Route::post('get-branch', 'CommonController@getBranch');
        });
       

    });
    
    

    

    
    
    
    
    
});
