<?php

Breadcrumbs::for('admin.auth.user.index', function ($trail) {
    $trail->push(__('labels.backend.access.users.management'), route('admin.auth.user.index'));
});

Breadcrumbs::for('admin.auth.user.deactivated', function ($trail) {
    $trail->parent('admin.auth.user.index');
    $trail->push(__('menus.backend.access.users.deactivated'), route('admin.auth.user.deactivated'));
});

Breadcrumbs::for('admin.auth.user.deleted', function ($trail) {
    $trail->parent('admin.auth.user.index');
    $trail->push(__('menus.backend.access.users.deleted'), route('admin.auth.user.deleted'));
});

Breadcrumbs::for('admin.auth.user.create', function ($trail) {
    $trail->parent('admin.auth.user.index');
    $trail->push(__('labels.backend.access.users.create'), route('admin.auth.user.create'));
});

Breadcrumbs::for('admin.auth.user.show', function ($trail, $id) {
    $trail->parent('admin.auth.user.index');
    $trail->push(__('menus.backend.access.users.view'), route('admin.auth.user.show', $id));
});

Breadcrumbs::for('admin.auth.user.edit', function ($trail, $id) {
    $trail->parent('admin.auth.user.index');
    $trail->push(__('menus.backend.access.users.edit'), route('admin.auth.user.edit', $id));
});

Breadcrumbs::for('admin.auth.user.change-password', function ($trail, $id) {
    $trail->parent('admin.auth.user.index');
    $trail->push(__('menus.backend.access.users.change-password'), route('admin.auth.user.change-password', $id));
});
Breadcrumbs::for('admin.dashboard', function ($trail) {
    $trail->push(__('strings.backend.dashboard.title'), route('admin.dashboard'));
});


Breadcrumbs::for('admin.auth.role.index', function ($trail) {
    $trail->push(__('menus.backend.access.roles.management'), route('admin.auth.role.index'));
});

Breadcrumbs::for('admin.auth.role.create', function ($trail) {
    $trail->parent('admin.auth.role.index');
    $trail->push(__('menus.backend.access.roles.create'), route('admin.auth.role.create'));
});

Breadcrumbs::for('admin.auth.role.edit', function ($trail, $id) {
    $trail->parent('admin.auth.role.index');
    $trail->push(__('menus.backend.access.roles.edit'), route('admin.auth.role.edit', $id));
});
Breadcrumbs::for('admin.auth.permission.index', function ($trail) {
    $trail->push(__('menus.backend.access.permissions.management'), route('admin.auth.permission.index'));
});

Breadcrumbs::for('admin.auth.permission.create', function ($trail) {
    $trail->parent('admin.auth.permission.index');
    $trail->push(__('menus.backend.access.permissions.create'), route('admin.auth.permission.create'));
});

Breadcrumbs::for('admin.auth.permission.edit', function ($trail, $id) {
    $trail->parent('admin.auth.permission.index');
    $trail->push(__('menus.backend.access.permissions.edit'), route('admin.auth.permission.edit', $id));
});


Breadcrumbs::for('admin.countries.index', function ($trail) {
    $trail->push(__('labels.backend.access.country.management'), route('admin.countries.index'));
});

Breadcrumbs::for('admin.countries.create', function ($trail) {
    $trail->parent('admin.countries.index');
    $trail->push(__('labels.backend.access.country.create'), route('admin.countries.create'));
});

Breadcrumbs::for('admin.countries.edit', function ($trail, $id) {
    $trail->parent('admin.countries.index');
    $trail->push(__('labels.backend.access.country.edit'), route('admin.countries.edit', $id));
});


Breadcrumbs::for('admin.brands.index', function ($trail) {
    $trail->push(__('Brands'), route('admin.brands.index'));
});

Breadcrumbs::for('admin.brands.create', function ($trail) {
    $trail->parent('admin.brands.index');
    $trail->push(__('Create Brand'), route('admin.brands.create'));
});

Breadcrumbs::for('admin.brands.edit', function ($trail, $id) {
    $trail->parent('admin.brands.index');
    $trail->push(__('Edit Brand'), route('admin.brands.edit', $id));
});

Breadcrumbs::for('admin.models.index', function ($trail) {
    $trail->push(__('Models'), route('admin.models.index'));
});

Breadcrumbs::for('admin.models.create', function ($trail) {
    $trail->parent('admin.models.index');
    $trail->push(__('Create Model'), route('admin.models.create'));
});

Breadcrumbs::for('admin.models.edit', function ($trail, $id) {
    $trail->parent('admin.models.index');
    $trail->push(__('Edit Model'), route('admin.models.edit', $id));
});

Breadcrumbs::for('admin.cities.index', function ($trail) {
    $trail->push(__('Cities'), route('admin.cities.index'));
});

Breadcrumbs::for('admin.cities.create', function ($trail) {
    $trail->parent('admin.cities.index');
    $trail->push(__('Create Cities'), route('admin.cities.create'));
});

Breadcrumbs::for('admin.cities.edit', function ($trail, $id) {
    $trail->parent('admin.cities.index');
    $trail->push(__('Edit Cities'), route('admin.cities.edit', $id));
});


Breadcrumbs::for('admin.branches.index', function ($trail) {
    $trail->push(__('Branches'), route('admin.branches.index'));
});

Breadcrumbs::for('admin.branches.create', function ($trail) {
    $trail->parent('admin.branches.index');
    $trail->push(__('Create Branches'), route('admin.branches.create'));
});

Breadcrumbs::for('admin.branches.edit', function ($trail, $id) {
    $trail->parent('admin.branches.index');
    $trail->push(__('Edit Branches'), route('admin.branches.edit', $id));
});

Breadcrumbs::for('admin.series.index', function ($trail) {
    $trail->push(__('Series'), route('admin.series.index'));
});

Breadcrumbs::for('admin.series.create', function ($trail) {
    $trail->parent('admin.series.index');
    $trail->push(__('Create Series'), route('admin.series.create'));
});

Breadcrumbs::for('admin.series.edit', function ($trail, $id) {
    $trail->parent('admin.series.index');
    $trail->push(__('Edit Series'), route('admin.series.edit', $id));
});

Breadcrumbs::for('admin.vendors.index', function ($trail) {
    $trail->push(__('Vendors'), route('admin.vendors.index'));
});

Breadcrumbs::for('admin.vendors.create', function ($trail) {
    $trail->parent('admin.vendors.index');
    $trail->push(__('Create Vendors'), route('admin.vendors.create'));
});

Breadcrumbs::for('admin.vendors.edit', function ($trail, $id) {
    $trail->parent('admin.vendors.index');
    $trail->push(__('Edit Vendors'), route('admin.vendors.edit', $id));
});

Breadcrumbs::for('admin.subvendors.index', function ($trail) {
    $trail->push(__('Subvendors'), route('admin.subvendors.index'));
});

Breadcrumbs::for('admin.subvendors.create', function ($trail) {
    $trail->parent('admin.subvendors.index');
    $trail->push(__('Create Subvendors'), route('admin.subvendors.create'));
});

Breadcrumbs::for('admin.subvendors.edit', function ($trail, $id) {
    $trail->parent('admin.subvendors.index');
    $trail->push(__('Edit Subvendors'), route('admin.subvendors.edit', $id));
});


Breadcrumbs::for('admin.catalogs.index', function ($trail) {
    $trail->push(__('Catalogs'), route('admin.catalogs.index'));
});

Breadcrumbs::for('admin.catalogs.create', function ($trail) {
    $trail->parent('admin.catalogs.index');
    $trail->push(__('Create Catalogs'), route('admin.catalogs.create'));
});

Breadcrumbs::for('admin.catalogs.edit', function ($trail, $id) {
    $trail->parent('admin.catalogs.index');
    $trail->push(__('Edit Catalogs'), route('admin.catalogs.edit', $id));
});

Breadcrumbs::for('admin.attributes.index', function ($trail) {
    $trail->push(__('Attributes'), route('admin.attributes.index'));
});

Breadcrumbs::for('admin.attributes.create', function ($trail) {
    $trail->parent('admin.attributes.index');
    $trail->push(__('Create Attributes'), route('admin.attributes.create'));
});

Breadcrumbs::for('admin.attributes.edit', function ($trail, $id) {
    $trail->parent('admin.attributes.index');
    $trail->push(__('Edit Attributes'), route('admin.attributes.edit', $id));
});



Breadcrumbs::for('admin.currencies.index', function ($trail) {
    $trail->push(__('labels.backend.access.currencies.management'), route('admin.currencies.index'));
});

Breadcrumbs::for('admin.currencies.create', function ($trail) {
    $trail->parent('admin.currencies.index');
    $trail->push(__('labels.backend.access.currencies.create'), route('admin.currencies.create'));
});

Breadcrumbs::for('admin.currencies.edit', function ($trail, $id) {
    $trail->parent('admin.currencies.index');
    $trail->push(__('labels.backend.access.currencies.edit'), route('admin.currencies.edit', $id));
});


Breadcrumbs::for('admin.cargo.index', function ($trail) {
    $trail->push(__('labels.backend.access.cargo.management'), route('admin.cargo.index'));
});

Breadcrumbs::for('admin.cargo.create', function ($trail) {
    $trail->parent('admin.cargo.index');
    $trail->push(__('labels.backend.access.cargo.create'), route('admin.cargo.create'));
});

Breadcrumbs::for('admin.cargo.edit', function ($trail, $id) {
    $trail->parent('admin.cargo.index');
    $trail->push(__('labels.backend.access.cargo.edit'), route('admin.cargo.edit', $id));
});


Breadcrumbs::for('admin.agent.index', function ($trail) {
    $trail->push(__('labels.backend.access.agent.management'), route('admin.agent.index'));
});

Breadcrumbs::for('admin.agent.create', function ($trail) {
    $trail->parent('admin.agent.index');
    $trail->push(__('labels.backend.access.agent.create'), route('admin.agent.create'));
});

Breadcrumbs::for('admin.agent.edit', function ($trail, $id) {
    $trail->parent('admin.agent.index');
    $trail->push(__('labels.backend.access.agent.edit'), route('admin.agent.edit', $id));
});