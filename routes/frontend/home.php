<?php

use App\Http\Controllers\Frontend\HomeController;
use App\Http\Controllers\Frontend\User\AccountController;
use App\Http\Controllers\Frontend\User\DashboardController;
use App\Http\Controllers\Frontend\User\ProfileController;
use App\Http\Controllers\Frontend\Auth\UpdatePasswordController;
//use App\Http\Controllers\Frontend\Auth\LoginController;

Route::get('/', [HomeController::class, 'index'])->name('index');
// Route::get('login', [LoginController::class, 'showLoginForm'])->name('login');
// Route::post('login', [LoginController::class, 'login'])->name('login.post');

Route::group(['middleware' => ['auth']], function () {
    
    Route::patch('password/update', 'Auth\UpdatePasswordController@update');
    Route::group(['namespace' => 'User', 'as' => 'user.'], function () {
        Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');
        Route::post('dashboard/fetch_data', [DashboardController::class, 'fetch_data'])->name('dashboard.fetch_data');
        Route::get('account', [AccountController::class, 'index'])->name('account'); 
        Route::patch('profile/update', [ProfileController::class, 'update'])->name('profile.update');  
        Route::patch('pricegraph/update', [ProfileController::class, 'priceGraphUpdate']);
        Route::patch('notification/update', [ProfileController::class, 'notificationUpdate']);
    });
   
    Route::group(['namespace' => 'Enquiry', 'as' => 'enquiry.'], function () {
        $enquiry_url = 'enquiry';
        Route::post($enquiry_url.'/store','EnquiryController@store')->name('enquiry.store');
        Route::delete($enquiry_url.'/remove','EnquiryController@remove')->name('enquiry.remove'); 
        Route::post($enquiry_url.'/update/{id}','EnquiryController@update')->name('enquiry.update');
        Route::get($enquiry_url.'/view/{id}','EnquiryController@view')->name('enquiry.view');
        Route::post($enquiry_url.'/submit/{id}','EnquiryController@submit')->name('enquiry.submit');
        Route::get($enquiry_url.'/add/{id}','EnquiryController@add')->name('enquiry.add');
        Route::resource($enquiry_url, 'EnquiryController'); 

    });

    Route::group(['namespace' => 'Quotation', 'as' => 'quotation.'], function () {   
      Route::get('quotation/view/{id}','QuotationController@view');
      Route::delete('quotation/remove','QuotationController@remove');
      Route::post('quotation/store','QuotationController@store')->name('quotation.store');
      Route::post('quotation/submit/{id}','QuotationController@submit')->name('quotation.submit');
      Route::get('quotation/add/{id}','QuotationController@add')->name('quotation.add');
      Route::post('quotation/update/{id}','QuotationController@update')->name('quotation.update');
      Route::resource('quotation', 'QuotationController');
    });

    Route::group(['namespace' => 'Order', 'as' => 'order.'], function () {   
        Route::get('order/view/{id}','OrderController@view');
        Route::delete('order/remove','OrderController@remove');
        Route::post('order/submit/{id}','OrderController@submit');
        Route::post('order/update/{id}','OrderController@update');
        Route::post('order/store','OrderController@store')->name('order.store');
        Route::get('order/add/{id}','OrderController@add');
        Route::resource('order', 'OrderController');
    });

    Route::group(['namespace' => 'Invoice', 'as' => 'invoice.'], function () {   
        Route::get('invoice/view/{id}','InvoiceController@view');
        Route::delete('invoice/remove','InvoiceController@remove');
        Route::post('invoice/submit/{id}','InvoiceController@submit');
        Route::post('invoice/update/{id}','InvoiceController@update');
        Route::get('invoice/download/{id}','InvoiceController@pdf');
        Route::post('invoice/store','InvoiceController@store');
        Route::get('invoice/add/{id}','InvoiceController@add');
        Route::get('invoice/rma/{id}','InvoiceController@rma');
        Route::resource('invoice', 'InvoiceController');

    });

    Route::group(['namespace' => 'Stock', 'as' => 'stock.'], function () {
        $stock_url = 'stock';
        Route::post($stock_url.'/store','StockController@store')->name('stock.store');
        Route::post($stock_url.'/import', 'StockController@import');
        Route::get($stock_url.'/export', 'StockController@export');
        Route::resource($stock_url, 'StockController'); 
    });

    Route::group(['namespace' => 'Price', 'as' => 'price.'], function () {   
        $price_url = 'price';
        Route::post($price_url.'/store','PriceController@store')->name('price.store');
        Route::post($price_url.'/import', 'PriceController@import');
        Route::get($price_url.'/export', 'PriceController@export');
        Route::resource($price_url, 'PriceController'); 
    });
    Route::group(['namespace' => 'Subvendor', 'as' => 'subvendor.'], function () {
        Route::resource('subvendor', 'SubvendorController'); 
    });

    Route::group(['namespace' => 'Markup', 'as' => 'markup.'], function () {
        Route::resource('markup', 'MarkupController'); 
        Route::post('markup/store','MarkupController@store');
    });


    Route::group(['namespace' => 'RMA', 'as' => 'rma.'], function () {
        Route::post('rma/store','RMAController@store');
        Route::get('rma/add/{id}','RMAController@add');
        Route::post('rma/submit/{id}','RMAController@submit');
        Route::delete('rma/remove','RMAController@remove');
        Route::post('rma/update/{id}','RMAController@update');
        Route::post('rma/note/{id}','RMAController@updateNote');
        Route::get('rma/view/{id}','RMAController@view');
        Route::post('rma/status/{id}','RMAController@changeStatus');
        Route::get('rma/shipment','RMAController@shipmentList');
        Route::post('rma/shipment/store','RMAController@storeRMAShipment');
        Route::post('rma/qtyupdate/{id}','RMAController@qtyupdate');
        Route::resource('rma', 'RMAController'); 
    });

    Route::group(['namespace' => 'Shipment', 'as' => 'shipment.'], function () {
        Route::post('shipment/store','ShipmentController@store');
        Route::get('shipment/view/{id}','ShipmentController@view');
        Route::get('shipment/upload_data/{id}','ShipmentController@upload_data');
        Route::post('shipment/save_upload_data/{id}','ShipmentController@save_upload_data');
        Route::post('shipment/update/{id}','ShipmentController@update');
        Route::get('shipment/select2-autocomplete-ajax', 'ShipmentController@dataAjax');
        Route::get('shipment/search','ShipmentController@search');
        Route::resource('shipment', 'ShipmentController', ['except' => ['show']]); 
    });
   
    
    Route::group(['prefix' => 'ajax'], function () {
        Route::get('/set-leftnav/{number?}', function ($number = 1) {
            if($number == 1) session(['csh_nav' => 0, 'set_nav' => 1]); 
            if($number == 0) session(['csh_nav' => 1, 'set_nav' => false]);
        });
        Route::get('/close-leftnav', function () {
            session(['close_nav' => 1]); 
        });
        Route::get('/set-mode/{id}', function ($id) {
            session(['is_source' => $id,'is_source_set' => 1]); 
            if ($id==0) session(['is_source_set' => 0]);
        });
        Route::get('/autocomplete-search', 'Ajax\AjaxController@autocompleteSearch');
        Route::post('/stock_available/check', 'Ajax\AjaxController@stockAvailableCheck');
        Route::get('/notification', 'Ajax\AjaxController@getNotification');
        Route::post('/mark-as-read', 'Ajax\AjaxController@markNotification');
        Route::get('/users', 'Ajax\AjaxController@getUser');
        Route::get('/search', 'Ajax\AjaxController@searchUser');
        Route::get('/userinfo', 'Ajax\AjaxController@getUserInfo');
        Route::get('/get-chat', 'Ajax\AjaxController@getChat');
        Route::get('/insert-chat', 'Ajax\AjaxController@insertChat');
        Route::get('/graph', 'Ajax\AjaxController@getGraphData');
    });
});
