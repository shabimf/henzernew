@extends('frontend.layouts.app')
@section('title', app_name() . ' | ' . __('navs.general.home'))
@section('content') 
@include('common.alert') 
<div class="main">
@include('frontend.includes.shipment-tab')
    <div class=" search_bar minus-margin" >
    <h5><i class="glyphicon glyphicon-upload"></i> Package Listing File Upload</h5> 
        <form method="post" action="{{url('shipment/save_upload_data/'.$parent_id)}}" enctype="multipart/form-data">
        {{csrf_field()}}
            <input type="hidden" name="type_id" value="1">
            <div class="input-group control-group increment" > 
                <input type="file" name="filename[]" class="form-control">
                <div class="input-group-btn"> 
                    <button class="btn btn-success package" type="button"><i class="glyphicon glyphicon-plus"></i>Add</button>
                </div>
            </div>
            <div class="clone hide">
                <div class="control-group input-group" style="margin-top:10px"> 
                    <input type="file" name="filename[]" class="form-control">
                    <div class="input-group-btn"> 
                        <button class="btn btn-danger" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button>
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-info" style="margin-top:12px"><i class="glyphicon glyphicon-check"></i> Submit</button>
        </form>
       
        @if (count($package_list) > 0 )
        <hr>
        <table class="table  table-bordered  table-dark list_table fixed_head_">
            <thead>
            <tr>
                <th>#</th>
                <th>File</th>
            </tr>
            </thead>
            <tbody>
            @foreach($package_list as $key => $file)
                <tr>
                    <td>{{$key+1}}</td>
                    <td><img src="{{asset('files/'.$file)}}" width="100"/></td>
                </tr>
            @endforeach
            </tbody>
       </table>
       @endif
    </div>
    <div class=" search_bar minus-margin" >
    <h5>Custom Clearance File Upload</h5> 
        <form method="post" action="{{url('shipment/save_upload_data/'.$parent_id)}}" enctype="multipart/form-data">
        {{csrf_field()}}
            <input type="hidden" name="type_id" value="2">
            <div class="input-group control-group increment1" > 
                <input type="file" name="filename[]" class="form-control">
                <div class="input-group-btn"> 
                    <button class="btn btn-success custom" type="button"><i class="glyphicon glyphicon-plus"></i>Add</button>
                </div>
            </div>
            <div class="clone1 hide">
                <div class="control-group input-group" style="margin-top:10px"> 
                    <input type="file" name="filename[]" class="form-control">
                    <div class="input-group-btn"> 
                        <button class="btn btn-danger" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button>
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-info" style="margin-top:12px"><i class="glyphicon glyphicon-check"></i> Submit</button>
        </form>
        @if (count($clearance_list) > 0 )
        <hr>
        <table class="table  table-bordered  table-dark list_table fixed_head_">
            <thead>
            <tr>
                <th>#</th>
                <th>File</th>
            </tr>
            </thead>
            <tbody>
            @foreach($clearance_list as $key => $file)
                <tr>
                    <td>{{$key+1}}</td>
                    <td><img src="{{asset('files/'.$file)}}" width="100"/></td>
                </tr>
            @endforeach
            </tbody>
       </table>
       @endif
    </div>
    <div class=" search_bar minus-margin" >
    <h5>Shipment Invoice File Upload</h5> 
        <form method="post" action="{{url('shipment/save_upload_data/'.$parent_id)}}" enctype="multipart/form-data">
        {{csrf_field()}}
        <input type="hidden" name="type_id" value="3">
            <div class="input-group control-group increment2" > 
                <input type="file" name="filename[]" class="form-control">
                <div class="input-group-btn"> 
                    <button class="btn btn-success shipment" type="button"><i class="glyphicon glyphicon-plus"></i>Add</button>
                </div>
            </div>
            <div class="clone2 hide">
                <div class="control-group input-group" style="margin-top:10px"> 
                    <input type="file" name="filename[]" class="form-control">
                    <div class="input-group-btn"> 
                        <button class="btn btn-danger" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button>
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-info" style="margin-top:12px"><i class="glyphicon glyphicon-check"></i> Submit</button>
        </form>
        @if (count($invoice_list) > 0 )
        <hr>
        <table class="table  table-bordered  table-dark list_table fixed_head_">
            <thead>
            <tr>
                <th>#</th>
                <th>File</th>
            </tr>
            </thead>
            <tbody>
            @foreach($invoice_list as $key => $file)
                <tr>
                    <td>{{$key+1}}</td>
                    <td><img src="{{asset('files/'.$file)}}" width="100"/></td>
                </tr>
            @endforeach
            </tbody>
       </table>
       @endif
    </div>
</div>  
@endsection
@push('after-scripts')
<script type="text/javascript">
    $(document).ready(function() {
      $(".package").click(function(){ 
          var html = $(".clone").html();
          $(".increment").after(html);
      });
      $(".custom").click(function(){ 
          var html = $(".clone1").html();
          $(".increment1").after(html);
      });
      $(".shipment").click(function(){ 
          var html = $(".clone2").html();
          $(".increment2").after(html);
      });
      $("body").on("click",".btn-danger",function(){ 
          $(this).parents(".control-group").remove();
      });
    });
</script>
@endpush