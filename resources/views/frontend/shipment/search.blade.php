<form method="post" action="{{ url('/shipment/store') }}">
    <div class="search_bar minus-margin">
      <div class="clearfix"></div>
        <div class="row ">                
            <div class="col-sm-3">
                <div class="form-group">
                    <label>Track Number/AWB</label>
                    <input class="full_box_dark_sm" type="text" name="track_no" value="">
                    <input class="full_box_dark_sm" type="hidden" name="serial_no" value="">
                </div>
            </div>          
            <div class="col-sm-3">
                <div class="form-group">
                    <label>Cargo Mode</label>
                    {{ Form::select('mode_id', [null=>'SELECT MODE'] + $list_mode, request('mode_id'), ['class' => 'full_box_dark_sm ']) }}
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <label>Agent</label>
                    {{ Form::select('agent_id', [null=>'SELECT AGENT'] + $list_agent, request('agent_id'), ['class' => 'full_box_dark_sm ']) }}
                </div>
            </div>          
            <div class="col-sm-3">
                <div class="form-group">
                    <label>Tracking Link</label>
                    <input class="full_box_dark_sm" type="text" name="track_link" value="{{request('track_link')}}">
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <label>Carton</label>
                    <input class="full_box_dark_sm" type="number" name="carton" value="{{request('carton')}}">
                </div>
            </div>  
        </div>   
    </div>
    
    <div class="row">
    
    <div class="clearfix"></div>
        <hr>
        @php
            $total = 0;
            $bal_qty = 0;
        @endphp
        <div class="table-responsive">
            <table class="table table-hover table-bordered  table-dark fixed_head_ list_table">
                <thead>
                    <tr>
                        <th></th>
                        <th>Invoice No</th>
                        <th>Product</th>
                        <th>Brand</th>
                        <th>Model</th>
                        <th>Series</th>
                        <th>Total Qty</th> 
                        <th>Balance Qty</th> 
                        <th>Shipped Qty</th>                      
                    </tr>
                </thead>
     
                <tbody>   
                @foreach($list as $data)
                    @php
                  
                    $qty  = checkShipmentItemCount($data->parent_id, $data->catalog_id);
                    $bal_qty = ($data->qty - checkShipmentItemCount($data->parent_id, $data->catalog_id));
                    $total += $bal_qty;
                    @endphp
                    <tr>  
                        <td>
                            <input type="hidden" name="invoice_id[{{ $data->id }}]" value="{{ $data->parent_id }}"/>
                            <input type="hidden" name="catalog_id[{{ $data->id }}]" value="{{ $data->catalog_id }}"/>
                            <input type="checkbox" name="item_id[]" value="{{ $data->id }}" checked @if($bal_qty==0) disabled @endif/>
                        </td>
                        <td>{{ $data->serial_no }}</td>
                        <td>{{ $data->product_name }}</td>
                        <td>{{ $data->brand_name }}</td>
                        <td>{{ $data->model_name }}</td>
                        <td>{{ $data->series_name }}</td>
                        <td>{{ $data->qty }}</td>
                        <td>
                           {{ checkShipmentItemCount($data->parent_id, $data->catalog_id)}}
                            <input type="hidden" value="{{$bal_qty}}"  id="total_qty_{{ $data->id }}"/></td>
                        </td>
                        <td><input type="text" value="{{ $data->qty - checkShipmentItemCount($data->parent_id, $data->catalog_id)}}"  name="qty[{{ $data->id }}]" id="qty_{{ $data->id }}" @if($bal_qty==0) readonly @endif  onkeyup = "qtyupdate(this.value , {{$data->id}})"/></td>
                    </tr>  
                @endforeach 
                    
                    @if (count($list) > 0 && $total>0) 
                    <tfoot>
                    <tr class="text-right">
                        <td colspan="9"><input class="btn btn-success btn-sm pull-right" type="submit" value="SUBMIT"></td>
                    </tr>
                    </tfoot>
                    @endif
                </tbody>
            </table>
            {{ csrf_field() }}
        </div>  
    </div>
</form>