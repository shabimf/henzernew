@extends('frontend.layouts.app')
@section('title', app_name() . ' | ' . __('navs.general.home'))
@section('content')  
@include('common.alert')

<div class="main">
    @include('frontend.includes.shipment-tab')
        <div class="col">
            @include('frontend.includes.status')

            <div class="row">
                <div class="col-2">
                <ul class="nav flex-column tab-style" id="tab-style" style="margin-top: 15px; text-align: center;">
                    <li class="tab-table" onclick="return toggle('enquiries')"><a data-toggle="tab" href="#enquiries">Enquiries</a></li>
                    <li class="tab-table" onclick="return toggle('quotation')"><a data-toggle="tab" href="#quotation">Quotations</a></li>
                    <li class="tab-table" onclick="return toggle('order')"><a data-toggle="tab" href="#order">Orders</a></li>
                    <li class="tab-table" onclick="return toggle('invoice')"><a data-toggle="tab" href="#invoice">Invoices</a></li>
                    <li class="active tab-table" onclick="return toggle('shipment')"><a data-toggle="tab" href="#shipment">Shipment</a></li>
                </ul>
                </div>
                <div class="col-10">
                    <div class="tab-content" style="margin-top:15px">
                        <div id="enquiries" class="tab-pane fade">
                        <div class="mt-3">
                    <div class="table-responsive">
                        <table class="table  table-bordered  table-dark list_table fixed_head_">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>ENQ NO.</th>
                                    <th>From</th>
                                    <th>To</th>
                                    <th>Product</th>
                                    <th>Brand</th>
                                    <th>Model</th>
                                    <th>Series</th>
                                    <th>Qty</th> 
                                    <th>Price</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>   
                                @php
                                    $total = 0;
                                @endphp
                                @foreach($enquiry_details as $data)
                                <tr  data-id="{{ $data->id }}">  
                                    <td>{{ $data->date }}</td>
                                    <td><a href="/enquiry/view/{{ $data->enquiry_id }}" target="_blank">{{ $data->enquiry_number }}</a></td>
                                    <td>{{ $data->brnch_from }}</td>
                                    <td>{{ $data->brnch_to }}</td>
                                    <td>{{ $data->product_name }}</td>
                                    <td>{{ $data->brand_name }}</td>
                                    <td>{{ $data->model_name }}</td>
                                    <td>{{ $data->series_name }}</td>
                                    <td>{{ $data->enquiry_qty }}</td>
                                    <td><span>$</span> {{ $data->price }}</td>
                                    <td>{{ $data->enquiry_status }}</td>
                                </tr>
                                @endforeach 
                            </tbody>
                        </table>
                        {{ csrf_field() }}
                    </div>
                </div>                
                        </div>
                        <div id="quotation" class="tab-pane fade">
                        <div class="mt-3">
                    <div class="table-responsive">
                        <table class="table  table-bordered  table-dark list_table fixed_head_">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>QUOT NO.</th>
                                    <th>From</th>
                                    <th>To</th>
                                    <th>Product</th>
                                    <th>Brand</th>
                                    <th>Model</th>
                                    <th>Series</th>
                                    <th>Qty</th> 
                                    <th>Price</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>   
                                @php
                                    $total = 0;
                                @endphp
                                @foreach($quotation_details as $data)
                                <tr  data-id="{{ $data->id }}">  
                                    <td>{{ $data->date }}</td>
                                    <td><a href="/quotation/view/{{ $data->quotation_id }}" target="_blank">{{ $data->quotation_number }}</a></td>
                                    <td>{{ $data->brnch_from }}</td>
                                    <td>{{ $data->brnch_to }}</td>
                                    <td>{{ $data->product_name }}</td>
                                    <td>{{ $data->brand_name }}</td>
                                    <td>{{ $data->model_name }}</td>
                                    <td>{{ $data->series_name }}</td>
                                    <td>{{ $data->quotation_qty }}</td>
                                    <td><span>$</span> {{ $data->price }}</td>
                                    <td>{{ $data->quotation_status }}</td>
                                </tr>
                                @endforeach 
                            </tbody>
                        </table>
                        {{ csrf_field() }}
                    </div>
                </div>
                        </div>
                        <div id="order" class="tab-pane fade">
                        <div class="mt-3">
                    <div class="table-responsive">
                        <table class="table  table-bordered  table-dark list_table fixed_head_">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>ORD NO.</th>
                                    <th>From</th>
                                    <th>To</th>
                                    <th>Product</th>
                                    <th>Brand</th>
                                    <th>Model</th>
                                    <th>Series</th>
                                    <th>Qty</th> 
                                    <th>Price</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>   
                                @php
                                    $total = 0;
                                @endphp
                                @foreach($order_details as $data)
                                <tr data-id="{{ $data->id }}">  
                                    <td>{{ $data->date }}</td>
                                    <td><a href="/order/view/{{ $data->order_id }}" target="_blank">{{ $data->order_number }}</a></td>
                                    <td>{{ $data->brnch_from }}</td>
                                    <td>{{ $data->brnch_to }}</td>
                                    <td>{{ $data->product_name }}</td>
                                    <td>{{ $data->brand_name }}</td>
                                    <td>{{ $data->model_name }}</td>
                                    <td>{{ $data->series_name }}</td>
                                    <td>{{ $data->order_qty }}</td>
                                    <td><span>$</span> {{ $data->price }}</td>
                                    <td>{{ $data->order_status }}</td>
                                </tr>
                                @endforeach 
                            </tbody>
                        </table>
                        {{ csrf_field() }}
                    </div>
                </div>
                        </div>
                        <div id="invoice" class="tab-pane fade">
                        <div class="mt-3">
                    <div class="table-responsive">
                        <table class="table  table-bordered  table-dark list_table fixed_head_">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>INV NO.</th>
                                    <th>From</th>
                                    <th>To</th>
                                    <th>Product</th>
                                    <th>Brand</th>
                                    <th>Model</th>
                                    <th>Series</th>
                                    <th>Qty</th> 
                                    <th>Price</th>
                                   
                                </tr>
                            </thead>
                            <tbody>   
                                @php
                                    $total = 0;
                                @endphp
                                @foreach($invoice_details as $data)
                                <tr data-id="{{ $data->id }}">  
                                    <td>{{ $data->date }}</td>
                                    <td><a href="/invoice/view/{{ $data->invoice_id }}" target="_blank">{{ $data->invoice_number }}</a></td>
                                    <td>{{ $data->brnch_from }}</td>
                                    <td>{{ $data->brnch_to }}</td>
                                    <td>{{ $data->product_name }}</td>
                                    <td>{{ $data->brand_name }}</td>
                                    <td>{{ $data->model_name }}</td>
                                    <td>{{ $data->series_name }}</td>
                                    <td>{{ $data->qty }}</td>
                                    <td><span>$</span> {{ $data->price }}</td>                                  
                                </tr>
                                @endforeach 
                            </tbody>
                        </table>
                        {{ csrf_field() }}
                    </div>
                </div>
                        </div>
                        <div id="shipment" class="tab-pane fade  active show">
                        
                        <h3></h3>
                        <table class="text-white table-sm table-bordered col-12 table">
                        <tbody>
                            <tr>
                                <th class="bg-th">TRACK NO</th> 
                                <td> {{ $common_details->track_no ? $common_details->track_no : '---' }} </td>  
                                <th class="bg-th">TRACK LINK</th> 
                                <td> {{ $common_details->track_link ? $common_details->track_link : '---' }} </td>     
                                <th class="bg-th">MODE NAME</th> 
                                <td>{{ $common_details->cargo_name ? $common_details->cargo_name : '---' }}</td>    
                            </tr>
                            <tr>
                                <th class="bg-th">AGENT NAME</th> 
                                <td> {{ $common_details->agent_name ? $common_details->agent_name : '---' }}</td>
                                <th class="bg-th">STATUS</th> 
                                <td>@if($common_details->status =='1') Transit @else Received @endif</td> 
                            </tr>
                            
                        </tbody>
                        </table>
                    <hr>
                    <div class="mt-3">
                    <div class="table-responsive">
                        <table class="table  table-bordered  table-dark list_table fixed_head_">
                            <thead>
                                <tr>
                                    <th>Invoice Number</th>
                                    <th>Product</th>
                                    <th>Brand</th>
                                    <th>Model</th>
                                    <th>Series</th>
                                    <th>Qty</th> 
                                </tr>
                            </thead>
                            <tbody>   
                                @php
                                    $total = 0;
                                @endphp
                                @foreach($details as $data)
                                <tr data-id="{{ $data->id }}">  
                                    <td><a href="/invoice/view/{{ $data->invoice_id }}" target="_blank">{{ $data->invoice_number }}</a></td>
                                    <td>{{ $data->product_name }}</td>
                                    <td>{{ $data->brand_name }}</td>
                                    <td>{{ $data->model_name }}</td>
                                    <td>{{ $data->series_name }}</td>
                                    <td>{{ $data->qty }}</td>
                                </tr>
                                @endforeach 
                            </tbody>
                        </table>
                        {{ csrf_field() }}
                    </div>
                </div>


                        </div>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <!-- <hr>
            <table class="text-white table-sm table-bordered col-12 table">
                <tbody>
                    <tr>
                        <th class="bg-th">TRACK NO</th> 
                        <td> {{ $common_details->track_no ? $common_details->track_no : '---' }} </td>  
                        <th class="bg-th">DATE</th> 
                        <td> {{ $common_details->created_at ? date("d-M-Y h:i:a" ,strtotime($common_details->created_at)) : '---' }}</td>        
                    </tr>
                    <tr>
                        <th class="bg-th">CARGO MODE</th> 
                        <td>{{ $common_details->cargo_name ? $common_details->cargo_name : '---' }}</td>
                        <th class="bg-th">AGENT</th> 
                        <td> {{ $common_details->agent_name ? $common_details->agent_name : '---' }}</td>
                    </tr>
                    
                </tbody>
            </table>
            <hr> -->
                <div class="mt-3">
                    <div class="table-responsive">
                        <!-- <table class="table  table-bordered  table-dark list_table fixed_head_">
                            <thead>
                                <tr>
                                    <th>Product</th>
                                    <th>Brand</th>
                                    <th>Model</th>
                                    <th>Series</th>
                                    <th>Qty</th> 
                                </tr>
                            </thead>
                            <tbody>   
                                @php
                                    $total = 0;
                                @endphp
                                @foreach($details as $data)
                                <tr  data-id="{{ $data->id }}">  
                                    <td>{{ $data->product_name }}</td>
                                    <td>{{ $data->brand_name }}</td>
                                    <td>{{ $data->model_name }}</td>
                                    <td>{{ $data->series_name }}</td>
                                    <td>{{ $data->qty }}</td>
                                </tr>
                                @endforeach 
                            </tbody>
                        </table> -->
                        {{ csrf_field() }}
                    </div>
                </div>
                
            </div>
        </div>
    
</div>
@endsection

@push('after-scripts')
<script>
$("#tab-style").on('click','li',function(){
    // remove classname 'active' from all li who already has classname 'active'
    $("#tab-style li.active").removeClass("active"); 
    // adding classname 'active' to current click li 
    $(this).addClass("active"); 

});
function toggle(val) {
  $(".tab-pane").removeClass("active show"); 
  $("#"+val).addClass("active show"); 
}
</script>
@endpush


