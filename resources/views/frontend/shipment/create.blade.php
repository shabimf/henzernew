@extends('frontend.layouts.app')
@section('title', app_name() . ' | ' . __('navs.general.home'))
@section('content') 
@include('common.alert') 
<div class="main">
    @include('frontend.includes.shipment-tab') 
    <div class=" search_bar minus-margin" >
        <div class="row ">    
            <div class="col-sm-4">
                <div class="form-group">
                    <label>SERIAL.Num</label>
                    <select class="serial_num full_box_dark_sm" type="text" id="serial_num" name="serial_num[]" multiple></select>                   
                </div>
            </div>           
            <div class="col-sm-2 pt-25">
                <button class="btn btn-search btn-block Searchitem">SEARCH</button>
            </div>
            <div class="col-sm-1 pt-25">
                <a href="{{ url('/shipment') }}" class="btn btn-secondary ls1 br0 btn-block">RESET</a>
            </div>              
        </div>
    </div>
    <div id="itemsList">
    </div>
</div>  
@endsection
@push('after-scripts')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script type="text/javascript">
  function qtyupdate(qty, catalog_id)
  {
    var total_qty = $("#total_qty_"+catalog_id).val();
    if (total_qty<=qty) {
        $("#qty_"+catalog_id).val(total_qty);
    } 
  }
  $('#serial_num').select2({
    placeholder: "Select an Invoice",
    minimumInputLength: 2,
    ajax: {
        url: '/shipment/select2-autocomplete-ajax',
        dataType: 'json',
        data: function (params) {
            return {
                q: $.trim(params.term)
            };
        },
        processResults: function (data) {
            return {
                results: data
            };
        },
        cache: true
    }
});
$(document).ready(function(){
    $(".Searchitem").click(function(e){
        e.preventDefault();
        $.ajax({
            type: "GET",
            url: "/shipment/search",
            data: { serial_num: $("#serial_num").val()},
            success:function(result){
            $("#itemsList").html(result);
        }});
    });
});
</script>
@endpush




