@extends('frontend.layouts.app')
@section('title', app_name() . ' | ' . __('navs.general.home'))
@section('content') 
@include('common.alert') 
<div class="main">

    @include('frontend.includes.shipment-tab')
    <div class="col text-right text-white  pt-25 box-padd">
        <mark> <b>{{number_format($list->total())}}</b> Shipment  </mark>
    </div>
    <div class=" search_bar minus-margin" >
        {{ Form::open(['method'=>'GET']) }}  

            <div class="row ">            
                <div class="@if(isset($role) && $role != "Sub Vendor") col-sm-2 @else col-sm-4 @endif">
                    <div class="form-group">
                        <label>SERIAL.Num</label>
                        <input class="full_box_dark_sm" type="text" name="serial_num" value="{{request('serial_num')}}">
                    </div>
                </div>
               
                @if(isset($role) && $role == "Vendor" && Session::get('is_source_set') == 1)
                <div class="col-sm-2">
                    <div class="form-group">
                        <label>SOURCING TEAM</label>
                        {{ Form::select('branch_from', [null=>'SELECT '] + $sources, old('branch_from',(isset($branch_to)) ? $branch_to:''), ['class' => 'full_box_dark_sm ', (isset(Auth::user()->branch_id)) ? 'readonly':'']) }}
                    </div>
                </div>
               <div class="col-sm-2">
                    <div class="form-group">
                        <label>VENDOR</label>
                        {{ Form::select('branch_to', [null=>'SELECT '] + $vendors, old('branch_to',(isset(Auth::user()->branch_id)) ? Auth::user()->branch_id:''), ['class' => 'full_box_dark_sm ', (isset(Auth::user()->branch_id)) ? 'readonly':'']) }}
                    </div>
                </div>
                @endif
                @if(isset($role) && $role == "Vendor" && Session::get('is_source_set') == 0)
                <div class="col-sm-2">
                    <div class="form-group">
                        <label>VENDOR</label>
                        {{ Form::select('branch_from', [null=>'SELECT '] + $vendors, old('branch_from',(isset(Auth::user()->branch_id)) ? Auth::user()->branch_id:''), ['class' => 'full_box_dark_sm ', (isset(Auth::user()->branch_id)) ? 'readonly':'']) }}
                    </div>
                </div>
               <div class="col-sm-2">
                    <div class="form-group">
                        <label>SUB VENDOR</label>
                        {{ Form::select('branch_to', [null=>'SELECT '] + $subvendors, request('branch_to'), ['class' => 'full_box_dark_sm ']) }}
                    </div>
                </div>
                @endif
                @if(isset($role) && $role == "Sourcing Team" && Session::get('is_source_set') == 0)
                <div class="col-sm-2">
                    <div class="form-group">
                        <label>SOURCING TEAM</label>   
                        {{ Form::select('branch_from', [null=>'SELECT '] + $sources, old('branch_from',(isset(Auth::user()->branch_id)) ? Auth::user()->branch_id:''), ['class' => 'full_box_dark_sm ', (isset(Auth::user()->branch_id)) ? 'readonly':'']) }}
                    </div>
                </div>
               <div class="col-sm-2">
                    <div class="form-group">
                        <label>VENDOR</label>
                        {{ Form::select('branch_to', [null=>'SELECT '] + $subvendors, request('branch_to'), ['class' => 'full_box_dark_sm ']) }}
                    </div>
                </div>
                @endif 
                @if(isset($role) && $role == "Administrator" && Session::get('is_source_set') == 0)
                <div class="col-sm-2">
                    <div class="form-group">
                        <label>FROM BRANCH</label>
                        {{ Form::select('branch_from', [null=>'SELECT '] + $branches, request('branch_from'), ['class' => 'full_box_dark_sm ']) }}
                    </div>
                </div>
               <div class="col-sm-2">
                    <div class="form-group">
                        <label>TO BRANCH</label>
                        {{ Form::select('branch_to', [null=>'SELECT '] + $branches, request('branch_to') , ['class' => 'full_box_dark_sm ', (isset(Auth::user()->branch_id)) ? 'readonly':'']) }}
                    </div>
                </div>
                @endif 
               
                <div class="@if(isset($role) && $role != "Sub Vendor") col-sm-3 @else col-sm-4 @endif">
                    <div class="form-group">
                        <label>DATE.FROM</label>
                        <input class="full_box_dark_sm" type="date" name="d_from" value="{{request('d_from')}}">
                    </div>
                </div>
                <div class="@if(isset($role) && $role != "Sub Vendor") col-sm-3 @else col-sm-4 @endif">
                    <div class="form-group">
                        <label>DATE.TO</label>
                        <input class="full_box_dark_sm" type="date" name="d_to" value="{{request('d_to')}}">
                    </div>
                </div>

               

            </div>

            <div class="row ">
                
                <div class="col-sm-2">
                    <div class="form-group">
                        <label>&nbsp;</label>
                        <select id="filter_date" name="filter_date" class="full_box_dark_sm">
                        <option value="0">--Select--</option>
                            <option value="10" {{ request('filter_date') == 10 ? 'selected' : '' }}>Last 10 days</option>
                            <option value="30" {{ request('filter_date') == 30 ? 'selected' : '' }}>Last 30 days</option>
                            <option value="3" {{ request('filter_date') == 3 ? 'selected' : '' }}>3 months</option>
                       </select>
                    </div>
                </div>
                <div class="col-sm-2 pt-25">
                    <button class="btn btn-search btn-block">SEARCH</button>
                </div>

                <div class="col-sm-1 pt-25">
                    <a href="{{ url('/shipment') }}"
                        class="btn btn-secondary ls1 br0 btn-block">RESET</a>
                </div>

                

            </div>

        {{ Form::close() }}
    </div>
    
    <div class="row">

        
            <div class="table-responsive">
                <table class="table table-hover table-bordered  table-dark fixed_head_ list_table">
                    <thead>
                        <tr class="bg-th">
                            <th>SL</th>
                            <th>DATE</th>
                            <th>TRACK No.</th>
                            <th>CARGO MODE</th>
                            <th>AGENT</th>
                            <th>TRACK LINK</th>
                            <th>CREATED BY</th>
                            <th>STATUS</th>
                            <th width="80" align="middle" >ACTION</th>
                        </tr>
                    </thead>
                    @foreach($list as $key => $row)

                    <tr class="bg-tr tr-pointer-{{$row->id}}"  @if($row->status == 0) data-href="{{ url('/shipment/add/'.$row->id) }}" @else data-href="{{ url('/shipment/view/'.$row->id) }}"  @endif>
                        <td class="td-pointer" value="{{$row->id}}">{{ ($list->currentpage()-1) * $list->perpage() + $key + 1 }}</td>
                        <td class="td-pointer" value="{{$row->id}}"> 
                            {{date("d-M-Y" ,strtotime($row->created_at))}}
                            <sub>{{date("h:i:a" ,strtotime($row->created_at))}}</sub>
                        </td>
                        <td class="td-pointer" value="{{$row->id}}">{{$row->track_no}}</td>
                        
                        <td class="td-pointer" value="{{$row->id}}">{{$row->cargo_name}}</td>
                        <td class="td-pointer" value="{{$row->id}}">{{$row->agent_name}}</td>
                        <td class="td-pointer" value="{{$row->id}}">{{$row->track_link}}</td>
                        <td class="td-pointer" value="{{$row->id}}">{{$row->created_by}}</td>
                        <td value="{{$row->id}}" class="td-pointer @if($row->status == 1) text-white  bg-primary @elseif($row->status == 2) text-white  bg-success  @else text-black bg-warning @endif">
                            @if($row->status == 1) <span>TRANSIT</span>
                            @elseif($row->status == 3) <span>HOLD</span>
                            @elseif($row->status == 4) <span>IN CLEARANCE</span>
                            @else <span>RECEIVED</span>
                            @endif
                        </td>
                        <td class="text-center action_td">                           
                            <a href="{{ url('/shipment/view/'.$row->id) }}" >
                                <i title="View" class="far fa-eye text-primary cp "></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </table>

            </div>
            <nav>
                <ul class="pagination justify-content-end">
                    {{$list->appends(\Request::except('_token'))->links('pagination::bootstrap-4')}}
                </ul>
            </nav>

        </div>
    

</div>  
@include('frontend.includes.comment')
@endsection



