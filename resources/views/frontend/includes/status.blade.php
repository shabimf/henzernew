@if(count($details) > 0)
    <a href="{{ url($module) }}"><button class="btn btn-danger back_btn" type="button"> BACK <<</button></a>
    @if($module=='enquiry')
        <div class="dropdown float-right">
            <div class="row">
              
                <div class="col-md-5">
                @if($common_details->status == 0 )
                    <button class="btn btn-primary" type="button" > DRAFT </button>
                @elseif($common_details->status == 1 && Auth::user()->hasPermission('convert-to-quotation'))
                    <button class="btn btn-warning" type="button"> PENDING </button>
                @elseif($common_details->status == 2)
                    <button class="btn btn-success" type="button"> QUOTED </button>
                {{-- @elseif($common_details->status == 5)
                    <button class="btn btn-danger dropdown-toggle" type="button" data-toggle="dropdown"> REJECTED </button>--}}
                @endif 
                </div>
            </div>
            
            <div class="dropdown-menu dropdown-menu-right">  
                @if($common_details->status == 0 )
                    <a class="dropdown-item status"  data-value="enquiry">SUBMIT</a>  
                @elseif($common_details->status == 1 && Auth::user()->hasPermission('convert-to-quotation'))
                    <a class="dropdown-item status" data-value="quotation">CONVERT TO QUOTATION</a>
                {{-- @elseif($common_details->status == 2 )
                    <a class="dropdown-item cp rma_main_approval" data-id="{{$parent_id}}" >APPROVE</a>
                    <a class="dropdown-item" href="">REJECT</a>
                @elseif($common_details->status == 3 )
                    <a class="dropdown-item" href="">RECEIVED</a>--}}
                @endif 
            </div>
        </div>
    @elseif($module=='quotation')
        <div class="dropdown float-right">
            @if($common_details->status == 0 )
                <button class="btn btn-primary" type="button"> DRAFT </button>
            @elseif($common_details->status == 1  && Auth::user()->hasPermission('convert-to-order'))
                <button class="btn btn-warning" type="button"> PENDING </button>
            @elseif($common_details->status == 2)
                <button class="btn btn-success" type="button"> ORDER </button>
            {{-- @elseif($common_details->status == 5)
                <button class="btn btn-danger dropdown-toggle" type="button" data-toggle="dropdown"> REJECTED </button>--}}
            @endif 
            
            <div class="dropdown-menu dropdown-menu-right">  
                @if($common_details->status == 0 )
                    <a class="dropdown-item status"  data-value="quotation">SUBMIT</a>  
                @elseif($common_details->status == 1 && Auth::user()->hasPermission('convert-to-order'))
                    <a class="dropdown-item status" data-value="order">CONVERT TO ORDER</a>
                {{-- @elseif($common_details->status == 2 )
                    <a class="dropdown-item cp rma_main_approval" data-id="{{$parent_id}}" >APPROVE</a>
                    <a class="dropdown-item" href="">REJECT</a>
                @elseif($common_details->status == 3 )
                    <a class="dropdown-item" href="">RECEIVED</a>--}}
                @endif 
            </div>
        </div>
   
    @elseif($module=='order')
        <div class="dropdown float-right">
            @if($common_details->status == 0 )
                <button class="btn btn-primary" type="button" > DRAFT </button>
            @elseif($common_details->status == 1 && Auth::user()->hasPermission('convert-to-invoice'))
                <button class="btn btn-warning" type="button"> PENDING </button>
            @elseif($common_details->status == 2)
                <button class="btn btn-success" type="button"> INVOICED </button>
            {{-- @elseif($common_details->status == 5)
                <button class="btn btn-danger dropdown-toggle" type="button" data-toggle="dropdown"> REJECTED </button>--}}
            @endif 
            
            <div class="dropdown-menu dropdown-menu-right">  
                @if($common_details->status == 0 )
                    <a class="dropdown-item status"  data-value="order">SUBMIT</a>  
                @elseif($common_details->status == 1 && Auth::user()->hasPermission('convert-to-invoice'))
                    <a class="dropdown-item status" data-value="invoice">CONVERT TO INVOICE</a>
                {{-- @elseif($common_details->status == 2 )
                    <a class="dropdown-item cp rma_main_approval" data-id="{{$parent_id}}" >APPROVE</a>
                    <a class="dropdown-item" href="">REJECT</a>
                @elseif($common_details->status == 3 )
                    <a class="dropdown-item" href="">RECEIVED</a>--}}
                @endif 
            </div>
        </div>

    @elseif($module=='invoice')
        <div class="dropdown float-right">
            @if($common_details->status == 0 && Auth::user()->hasPermission('convert-to-invoice'))
                <button class="btn btn-primary" type="button" > DRAFT </button>
            @elseif($common_details->status == 1)
                <button class="btn btn-warning" type="button"> PENDING </button>
            @elseif($common_details->status == 2)
                <button class="btn btn-success" type="button"> INVOICED </button>
            {{-- @elseif($common_details->status == 5)
                <button class="btn btn-danger dropdown-toggle" type="button" data-toggle="dropdown"> REJECTED </button>--}}
            @endif 
            
            <div class="dropdown-menu dropdown-menu-right">  
                @if($common_details->status == 0 && Auth::user()->hasPermission('convert-to-invoice'))
                    <a class="dropdown-item status"  data-value="invoice">SUBMIT</a>  
                {{-- @elseif($common_details->status == 1 )
                    <a class="dropdown-item status" data-value="invoice">CONVERT TO INVOICE</a>
                @elseif($common_details->status == 2 )
                    <a class="dropdown-item cp rma_main_approval" data-id="{{$parent_id}}" >APPROVE</a>
                    <a class="dropdown-item" href="">REJECT</a>
                @elseif($common_details->status == 3 )
                    <a class="dropdown-item" href="">RECEIVED</a>--}}
                @endif 
            </div>
        </div>
    @elseif($module=='shipment' && Auth::user()->hasPermission('convert-to-shipment-received') && Session::get('is_source_set') == 1 && $role == "Vendor" )
        <div class="dropdown float-right">
            @if($common_details->status == 1 )
                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> IN TRANSIT </button>
            @elseif($common_details->status == 2)
                <button class="btn btn-success" type="button"> RECEIVED </button>
            @elseif($common_details->status == 3)
                <button class="btn btn-warning" type="button"> HOLD </button>
            @elseif($common_details->status == 4)
                <button class="btn btn-danger" type="button"> IN CLEARANCE </button>
            @endif 
            <div class="dropdown-menu dropdown-menu-right">  
                @if($common_details->status == 1 )
                    <a class="dropdown-item status"  data-value="2" data-id="{{$parent_id}}" > RECEIVED</a>  
                    <a class="dropdown-item status"  data-value="3" data-id="{{$parent_id}}" > HOLD</a>  
                    <a class="dropdown-item status"  data-value="4" data-id="{{$parent_id}}" > IN CLEARANCE</a>  
                @endif
            </div>
        </div>
    @elseif($module=='shipment' && Auth::user()->hasPermission('convert-to-shipment-received') && Session::get('is_source_set') == 0 && $role == "Sub Vendor" )
        <div class="dropdown float-right">
            @if($common_details->status == 1 )
                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> IN TRANSIT </button>
            @elseif($common_details->status == 2)
                <button class="btn btn-success" type="button"> RECEIVED </button>
            @elseif($common_details->status == 3)
                <button class="btn btn-warning" type="button"> HOLD </button>
            @elseif($common_details->status == 4)
                <button class="btn btn-danger" type="button"> IN CLEARANCE </button>
            @endif 
            <div class="dropdown-menu dropdown-menu-right">  
                @if($common_details->status == 1 )
                    <a class="dropdown-item status"  data-value="2" data-id="{{$parent_id}}" > RECEIVED</a>  
                    <a class="dropdown-item status"  data-value="3" data-id="{{$parent_id}}" > HOLD</a>  
                    <a class="dropdown-item status"  data-value="4" data-id="{{$parent_id}}" > IN CLEARANCE</a>  
                @endif
            </div>
        </div>
    @endif
    
@endif