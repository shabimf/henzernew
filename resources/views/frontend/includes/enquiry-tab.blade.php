<div class="d-flex justify-content-center">
    <div class="col-sm-8 col-xs-12 no-padding float-left">
        <ul id="tabsJustified" class="nav nav-tabs posbos_nav_tabs">
            @if (Auth::user()->hasPermission('create-enquiry') && Session::get('is_source') == 1)
            <li class="nav-item col-6 no-padding text-center">
                <a href="{{ url('enquiry/create') }}"
                    class=" nav-link small text-uppercase text-light  {{ isset($active_list) ? 'inactive_tab' : 'active_tab_enquiry' }}">
                    CREATE ENQUIRY <i class="fas fa-plus-circle"></i>
                </a>
            </li>
            @endif
           
            <li class="nav-item  @if (Auth::user()->hasPermission('create-enquiry') && Session::get('is_source') == 1) col-6 @else col-12 @endif no-padding text-center">
                <a href="{{ url('enquiry') }}"
                    class="nav-link nav-link_1 @if (!Auth::user()->hasPermission('create-enquiry') || Session::get('is_source') == 0) nav-link_2 @endif small text-uppercase text-light {{ isset($active_list) ? 'active_tab_enquiry' :  'inactive_tab' }}  ">
                    ENQUIRY LIST
                </a>
            </li>
        </ul>
    </div>
</div>

<br/>