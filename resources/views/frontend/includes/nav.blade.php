<div class="sidebar-nav" >
    <ul class="nav_ nav-list" style="margin-top: 8vh">
      <li ><a href="javascript:void(0)" class="menu_ menu_label menu_inactive">MENU</a> </li>
      <li>
        <a  class="{{ ($module=='dashbord') ? 'active_tab_'.$module : 'menu_list_box' }} " title="Dashbord" href="{{ url('/') }}">
          DASHBORD
        </a>
      </li>
      @if (Auth::user()->hasPermission('view-enquiry'))
      <li>
        <a  class="{{ ($module=='enquiry') ? 'active_tab_'.$module : 'menu_list_box' }} " title="Enquiry" href="{{ url('enquiry') }}">
          ENQUIRY 
        </a>
      </li>
      @endif
      <li>
        <a  class=" {{ ($module=='quotation') ? 'active_tab_'.$module : 'menu_list_box' }}" title="Quotation" href="{{ url('quotation') }}">
          QUOTATION 
        </a>
      </li>
      <li>
        <a  class="{{ ($module=='order') ? 'active_tab_'.$module : 'menu_list_box' }}" title="Order" href="{{ url('order') }}">
          ORDER 
        </a>
      </li>
      <li>
        <a  class="{{ ($module=='invoice') ? 'active_tab_'.$module : 'menu_list_box' }}" title="Invoice" href="{{ url('invoice') }}">
          INVOICE 
        </a>
      </li>
      <li>
        <a  class="{{ ($module=='shipment') ? 'active_tab_'.$module : 'menu_list_box' }}" title="shipment" href="{{ url('shipment') }}">
         SHIPMENT 
        </a>
      </li>
     
      <!-- <li>
        <a  class="{{ ($module=='rma_shipment') ? 'active_tab_'.$module : 'menu_list_box' }}" title="rmashipment" href="{{ url('rma/shipment') }}">
        RMA SHIPMENT 
        </a>
      </li> -->
      <li>
        <a  class="{{ ($module=='rma') ? 'active_tab_'.$module : 'menu_list_box' }}" title="RMA" href="{{ url('rma') }}">
          RMA 
        </a>
      </li>
      @if (Auth::user()->hasPermission('view-subvendor-management'))
      <li>
        <a  class="{{ ($module=='subvendor') ? 'active_tab_'.$module : 'menu_list_box' }}" title="Subvendor List" href="{{ url('subvendor') }}">
          SUBVENDOR MANAGE
        </a>
      </li>
      @endif
      @if (Auth::user()->hasPermission('view-stock-management'))
      <li>
        <a   class="{{ ($module=='stock') ? 'active_tab_'.$module : 'menu_list_box' }}" title="Stock List" href="{{ url('stock') }}">
          STOCK MANAGE
        </a>
      </li>
      @endif
      @if (Auth::user()->hasPermission('view-price-management'))
      <li>
        <a class="{{ ($module=='price') ? 'active_tab_'.$module : 'menu_list_box' }}" title="Price List" href="{{ url('price') }}">
          PRICE MANAGE
        </a>
      </li>
      @endif
      <li>
        <a  class="{{ ($module=='markup') ? 'active_tab_'.$module : 'web-enquiry' }}" title="Enquiries" href="#">
         WEB ENQUIRY  <span class="float-right"><i class="fas fa-envelope-open"></i></span>
        </a>
      </li>
      <li>
        <a  class="{{ ($module=='markup') ? 'active_tab_'.$module : 'chats' }}" title="Messages" href="#" onclick="openForm()">
         CHATS <span class="float-right"><i class="fas fa-comments"></i></span>
        </a>
      </li>
    </ul>
  </div>
  <div class="menu_toggle" id="toggle_menu" data-expand="{{ session()->get('csh_nav') }}">
    @if(session()->get('csh_nav') == 1 )
      <img src="/images/arrow_close.png" border="0" width="90%"> 
    @else
      <img src="/images/arrow_open.png" border="0" width="90%"> 
    @endif
  </div>
  <div class="wrapper-chat">
    
    <section class="users chat-popup" id="myForm">
    <div class="top-bar">
      <div class="col-8">
        <h5>Chat</h5>
      </div> 
      <div class="col-4" style="float: right;text-align: end; margin-top: -28px; color: #fff;">
          <a id="chat-minus" onclick="chatMinus()"><span class="chat-minus"><i aria-hidden="true" class="fa fa-minus"></i></span></a>
          <a id="chat-close" onclick="chatClose()"><span class="chat-close"><i aria-hidden="true" class="fa fa-times"></i></span></a>
          <input type="hidden" name="hidden-chatvalue" id="hidden-chatvalue" value="0">
      </div> 
    </div>
    <div class="search"><span class="text">Select an user to start chat</span> <input type="text" placeholder="Enter name to search..." class="show"> </div>
      <div class="users-list">
      </div>
    
    </section>
    <section class="chat-area chat-popup" id="chat-area">
      
    </section>
  </div>
  @push('after-scripts')
<script src="/js/frondend/users.js"></script>
@endpush