<div class="d-flex justify-content-center">
    <div class="col-sm-8 col-xs-12 no-padding float-left">
        <ul id="tabsJustified" class="nav nav-tabs posbos_nav_tabs">
            <li class="nav-item col-4 no-padding text-center">
                <a href="{{ url('order/create') }}"
                    class=" nav-link  small text-uppercase text-light  {{ isset($active_profile_list) ? 'active_tab_account' : 'inactive_tab' }}">
                    PROFILE
                </a>
            </li>
            <li class="nav-item col-4 no-padding text-center">
                <a href="{{ url('order/create') }}"
                    class="nav-link nav-ink_centre small text-uppercase text-light  {{ isset($active_profile_list) ? 'active_tab_account' : 'inactive_tab' }}">
                    UPDATE INFORMATION
                </a>
            </li>
            <li class="nav-item  col-4 no-padding text-center">
                <a href="{{ url('account/password') }}"
                    class="nav-link nav-link_1  small text-uppercase text-light {{ isset($active_password_list) ? 'active_tab_account' :  'inactive_tab' }}  ">
                    CHANGE PASSWORD
                </a>
            </li>
        </ul>
    </div>
</div>

<br/>