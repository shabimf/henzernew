<div class="row text-white">
    <div class="col">
        <div class="form-group">
            <span class="form-label">Product</span>
            <input class="full_box_dark add_field" type="text" id="search" placeholder="Name"  name="product_name"autocomplete="off"  value="{{ old('product_name', null) === null ? old('product_name') : ''}}" required>
            <input class="form-control" type="hidden" id="catalog_id" name="catalog_id" value="{{ old('catalog_id', null) === null ? old('catalog_id') : ''}}">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <span class="form-label">Quantity</span>
            <input class="full_box_dark add_field" type="text" autocomplete="off"  id="qty" name="qty"  value="{{ (old() ? '' :'') }}" required>
        </div>
    </div> 
    <div class="col-md-2">
        <div class="form-btn1 add_btn">
            <button class="btn btn-success  pull-right">Add</button>
        </div>
    </div>
</div>  