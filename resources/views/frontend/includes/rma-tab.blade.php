@if(isset($role) && $role == "Vendor") 
<div class="d-flex justify-content-center">
    <div class="col-sm-8 col-xs-12 no-padding float-left">
        <ul id="tabsJustified" class="nav nav-tabs posbos_nav_tabs">
            @if (Auth::user()->hasPermission('create-rma') && Session::get('is_source_set') == 1)
            <li class="nav-item col-6 no-padding text-center">
                <a href="{{ url('rma/create') }}"
                    class=" nav-link small text-uppercase text-light  {{ isset($active_list) ? 'inactive_tab' : 'active_tab_rma' }}">
                    CREATE RMA <i class="fas fa-plus-circle"></i>
                </a>
            </li>
           @endif
            <li class="nav-item @if (Auth::user()->hasPermission('create-rma') && Session::get('is_source_set') == 1) col-6 @else col-12 @endif no-padding text-center">
                <a href="{{ url('rma') }}"
                    class="nav-link nav-link_1 @if (!Auth::user()->hasPermission('create-rma') || Session::get('is_source_set') == 0) nav-link_2 @endif small text-uppercase text-light {{ isset($active_list) ? 'active_tab_rma' :  'inactive_tab' }}  ">
                    RMA LIST
                </a>
            </li>
        </ul>
    </div>
</div>
@elseif(isset($role) && $role == "Sub Vendor") 
<div class="d-flex justify-content-center">
    <div class="col-sm-8 col-xs-12 no-padding float-left">
        <ul id="tabsJustified" class="nav nav-tabs posbos_nav_tabs">
            @if (Auth::user()->hasPermission('create-rma') && Session::get('is_source_set') == 0)
            <li class="nav-item col-6 no-padding text-center">
                <a href="{{ url('rma/create') }}"
                    class=" nav-link small text-uppercase text-light  {{ isset($active_list) ? 'inactive_tab' : 'active_tab_rma' }}">
                    CREATE RMA <i class="fas fa-plus-circle"></i>
                </a>
            </li>
           @endif
            <li class="nav-item @if (Auth::user()->hasPermission('create-rma') && Session::get('is_source_set') == 0) col-6 @else col-12 @endif no-padding text-center">
                <a href="{{ url('rma') }}"
                    class="nav-link nav-link_1 @if (!Auth::user()->hasPermission('create-rma') || Session::get('is_source_set') == 1) nav-link_2 @endif small text-uppercase text-light {{ isset($active_list) ? 'active_tab_rma' :  'inactive_tab' }}  ">
                    RMA LIST
                </a>
            </li>
        </ul>
    </div>
</div>
@endif
<br/>