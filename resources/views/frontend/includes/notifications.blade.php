<div class="notifiscroll" @if (UnreadNotificationCount() > 3) style="height: 254px" @else style="height: auto !important;" @endif>
@forelse(UnreadNotificationFetch() as $notification)
    @php
        $module = $notification->data['module'];
        $serial_no = DB::table($module)->select('serial_no')->where('id',$notification->data['parent_id'])->first()->serial_no;     
    @endphp
   
    <a href="/{{ $module }}?serial_num={{$serial_no}}" class="dropdown-item mark-as-read" onclick="return sendMarkRequest('{{ $notification->id }}');">
        <!-- Message Start -->
        <div class="media">
        <div class="media-body">
            <h3 class="dropdown-item-title">
                {{ \App\Models\Auth\User::where('id',$notification->data['created_by'])->first()->first_name }}
            </h3> 
            
            <p class="text-sm">{{ \Illuminate\Support\Str::limit($notification->data['comment'], 40, $end='...') }}</p>
            <p class="text-sm text-muted"><i class="far fa-clock"></i> {{ Carbon\Carbon::parse($notification->created_at)->diffForHumans() }}</p>
        </div>
        </div>
        <!-- Message End -->
    </a>
    <div class="dropdown-divider"></div>
    
@empty
    <div class="notiempty">There are no new notifications</div>
@endforelse
</div>
@if( UnreadNotificationCount() > 0)
    <a href="javascript:void(0)" class="dropdown-item dropdown-footer" onclick="return sendMarkRequest();" >
      Mark all as read 
    </a>
@endif
