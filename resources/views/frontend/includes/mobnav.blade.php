
  <nav class="main-header navbar navbar-expand navbar-dark">
    <a class="navbar-brand" href="{{ route('frontend.index') }}" tabindex="-1" style="color:#fff">
      <img src="{{asset('img/backend/brand/logo_home.svg')}}" tabindex="-1"  height="40px" style="margin-right:15px">
    </a>
    <!-- Left navbar links -->
    <!-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" tabindex="-1" >
      <span class="navbar-toggler-icon"></span>
    </button> -->

    <ul class="navbar-nav mobilemenu">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" tabindex="-1" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      

      <!-- Messages Dropdown Menu -->
      <!-- <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="far fa-comments"></i>
          <span class="badge badge-danger navbar-badge">3</span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <a href="#" class="dropdown-item">
            <div class="media">
              <img src="{{asset('images/user1-128x128.jpg')}}" alt="User Avatar" class="img-size-50 mr-3 img-circle">
              <div class="media-body">
                <h3 class="dropdown-item-title">
                  Brad Diesel
                  <span class="float-right text-sm text-danger"><i class="fas fa-star"></i></span>
                </h3>
                <p class="text-sm">Call me whenever you can...</p>
                <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
              </div>
            </div>
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <div class="media">
              <img src="{{asset('images/user1-128x128.jpg')}}" alt="User Avatar" class="img-size-50 img-circle mr-3">
              <div class="media-body">
                <h3 class="dropdown-item-title">
                  John Pierce
                  <span class="float-right text-sm text-muted"><i class="fas fa-star"></i></span>
                </h3>
                <p class="text-sm">I got your message bro</p>
                <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
              </div>
            </div>
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <div class="media">
              <img src="{{asset('images/user1-128x128.jpg')}}" alt="User Avatar" class="img-size-50 img-circle mr-3">
              <div class="media-body">
                <h3 class="dropdown-item-title">
                  Nora Silvester
                  <span class="float-right text-sm text-warning"><i class="fas fa-star"></i></span>
                </h3>
                <p class="text-sm">The subject goes here</p>
                <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
              </div>
            </div>

          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item dropdown-footer">See All Messages</a>
        </div>
      </li> -->

      <!-- Notifications Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link notificls" data-toggle="dropdown" href="#">
          <i class="far fa-bell"></i>
          @if (UnreadNotificationCount() > 0)<span class="badge badge-warning navbar-badge">{{UnreadNotificationCount()}}</span> @endif
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right notifiresult">
          <div class="notifiscroll">
        
            @forelse(UnreadNotificationFetch() as $notification)
              @php
                  $module = $notification->data['module'];
                  $serial_no = DB::table($module)->select('serial_no')->where('id',$notification->data['parent_id'])->first()->serial_no;     
              @endphp
              <a href="/{{ $module }}?serial_num={{$serial_no}}" class="dropdown-item mark-as-read"  onclick="return sendMarkRequest('{{ $notification->id }}');">
                  <!-- Message Start -->
                  <div class="media">
                  <div class="media-body">
                      <h3 class="dropdown-item-title">
                          {{ \App\Models\Auth\User::where('id',$notification->data['created_by'])->first()->first_name }}
                      </h3> 
                      
                      <p class="text-sm">{{ \Illuminate\Support\Str::limit($notification->data['comment'], 40, $end='...') }}</p>
                      <p class="text-sm text-muted"><i class="far fa-clock"></i> {{ Carbon\Carbon::parse($notification->created_at)->diffForHumans() }}</p>
                  </div>
                  </div>
                  <!-- Message End -->
              </a>
              <div class="dropdown-divider"></div>
            
          @empty
            There are no new notifications
          @endforelse
    
        </div>
        @if( UnreadNotificationCount() > 0)
            <a href="javascript:void(0)" class="dropdown-item dropdown-footer"  onclick="return sendMarkRequest();" >
            Mark all as read 
        </a>
        @endif
      </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-widget="fullscreen" href="#" role="button">
          <i class="fas fa-expand-arrows-alt"></i>
        </a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="true">
          <img src="https://www.gravatar.com/avatar/64e1b8d34f425d19e1ee2ea7236d3028.jpg?s=80&amp;d=mm&amp;r=g" class="img-avatar" alt="admin@admin.com">
          {{-- <span class="d-md-down-none">Admin User</span> --}}
        </a>
        <div class="dropdown-menu dropdown-menu-right">
          {{-- <div class="dropdown-header text-center">
            <strong>Account</strong>
          </div> --}}
          <a class="dropdown-item" href="{{ url('account') }}">
            <i class="fas fa-cog"></i> <span class="left">Settings</span>
          </a>
          <a class="dropdown-item" href="{{ route('frontend.auth.logout') }}">
            <i class="fas fa-lock"></i> <span class="left">Logout</span>
          </a>
        </div>
      </li>
    </ul>
  </nav>
  @if($role == "Vendor" )
      
  <div class="btn-group mode-btn switch-mode">
    <!-- <a class="btn btn-info @if(Session::get('is_source') == 1) active @endif btn-mode source" id="source" href="javascript:void(0)" data-filter="1">SOURCING MODE</a>
    <a class="btn btn-info @if(Session::get('is_source') == 0) active @endif btn-mode vendor" id="vendor" href="javascript:void(0)" data-filter="0">VENDOR MODE</a>   -->
    <a class="btn btn-info @if(Session::get('is_source') == 1) active @endif btn-mode source" href="javascript:void(0)" data-filter="1">@if(Session::get('is_source') != 1) SWITCH TO @endif SOURCING MODE @if(Session::get('is_source') == 1) ON<i class="fa fa-circle btn-mode" aria-hidden="true"></i> @endif </a>
    <a class="btn btn-info @if(Session::get('is_source') == 0) active @endif btn-mode vendor" href="javascript:void(0)" data-filter="0">@if(Session::get('is_source') != 0) SWITCH TO @endif  VENDOR MODE  @if(Session::get('is_source') == 0) ON<i class="fa fa-circle btn-mode" aria-hidden="true"></i> @endif</a>  
  </div>
      
  @endif
  <div class="collapse navbar-collapse " id="navbarNavDropdown" >
          <ul class="nav navbar-nav ml-auto">
            <li class="nav-item active">
              <a  href="javascript:void(0)" class="nav-link" >MENU</a>
            </li>
            <li class="nav-item">
              <a  class="nav-link " title="Enquiry" href="{{ url('/') }}">
                DASHBORD
              </a>
            </li>
            @if (Auth::user()->hasPermission('view-enquiry'))
            <li class="nav-item">
              <a  class="nav-link" title="Enquiry" href="{{ url('enquiry') }}">
                ENQUIRY
              </a>
            </li>
            @endif
            <li class="nav-item">
              <a  class="nav-link" title="Quotation" href="{{ url('quotation') }}">
                QUOTATION
              </a>
            </li>
            <li class="nav-item">
              <a  class="nav-link" title="Order" href="{{ url('order') }}">
                ORDER
              </a>
            </li>
            <li class="nav-item">
              <a  class="nav-link" title="Invoice" href="{{ url('invoice') }}">
                INVOICE
              </a>
            </li>
            
            @if (Auth::user()->hasPermission('view-subvendor-management'))
            <li class="nav-item">
              <a  class="nav-link" title="Subvendor List" href="{{ url('subvendor') }}">
                SUBVENDOR MANAGE
              </a>
            </li>
            @endif
            @if (Auth::user()->hasPermission('view-stock-management'))
            <li class="nav-item">
              <a   class="nav-link" title="Stock List" href="{{ url('stock') }}">
                STOCK MANAGE
              </a>
            </li>
            @endif
            @if (Auth::user()->hasPermission('view-price-management'))
            <li class="nav-item">
              <a class="nav-link" title="Price List" href="{{ url('price') }}">
                PRICE MANAGE
              </a>
            </li>
            @endif
            {{-- <li class="nav-item">
              <a  class="nav-link" title="My Account" href="{{ url('account') }}">
                MY ACCOUNT
              </a>
            </li> --}}
            <li class="nav-item">
              <a class="nav-link" href="{{ route('frontend.auth.logout') }}">LOGOUT</a>
            </li>
            
           </ul>
     </div>

  <nav class="sidebar_right @if(session()->has('close_nav')) @else open @endif price"> 
    <a href="" id="menuToggle" title="show menu"> <span class="navClosed"><i></i></span> </a> 
    <div class="card dark mob-nav">
    <div class="card-header border-0">
      <h6 class="price_update">PRICE UPDATES.</h6>
      <h3 class="date">14th </br><span style="font-size:16px;">Oct 2021</span></h3>
    </div>

    <div class="card-body p-0">
      <table class="table dash m-0">
        <thead>
        <tr class="head_row">
          <th>Product</th>
          <th>USD</th>
          <!--<th>RMB</th>-->
          <th>Change</th>
        </tr>
        </thead>
        <tbody>   
        @foreach($price_array as $key => $value)
        <tr class="rows tab" id="tab" data-id="#content{{$key}}">
          <td>
            {{$value['name']}}
          </td>
          <td class="orgprice @if($value['price']>$value['old_price']) green @else red @endif ">${{$value['price']}}</td>
          <!--<td class="orgprice @if($value['price']>$value['old_price']) green @else red @endif ">${{$value['price']}}</td>-->
          <td>
            @if($value['difference'] != 0)
            <small class="badge @if($value['price']>$value['old_price']) badge-success @else badge-danger @endif mr-1">
              <i class="fas @if($value['price']>$value['old_price']) fa-arrow-up @else fa-arrow-down @endif"></i>
              {!! str_replace('-', ' ', $value['difference']) !!}% 
            </small>
            @endif
           
          </td>
        

        </tr>
        <!-- <tr class="content collapse{{$key}}" id="content{{$key}}" style="background-color: #0d1d2c;display:none">
            <td colspan="5">
            <h6 style="color: rgb(77 107 140) !important;float:right;font-weight: 700;">October 2021</h5>
                <div><canvas id="Chart" style="width:100%;"></canvas></div>
            </td>
        </tr>        -->
                 @endforeach
        
        </tbody>
      </table>
    </div>
  </div> 
  <div class="closee" style="color: rgb(255, 255, 255); padding: 8px; background-color: #803333;">CLOSE <span style="float: right;margin-top:11px;"><a id="menuToggle" class="closee" title="show menu"> <i class="fas fa-times"></i></a></span></div>
</nav>




  @push('after-scripts')
  <script>
$(document).ready(function() {
  
  $(".tab").click(function() {
        var contentId =this.dataset.id;
        $(contentId).slideToggle("collapse"+contentId);
        $('#content'+contentId).css('display', 'block');
        
    });

    
});


var xValues = [100,200,300,400,500,600,700,800,900,1000];

new Chart("Chart", {
  type: "line",
  
  data: {
    labels: xValues,
    datasets: [{ 
      data: [300,700,2000,5000,6000,4000,2000,1000,200,100],
      borderColor: "#17a2b8",
      fill: false
    }]
  },
  options: {
   
    legend: {display: false},
    scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true,
                    fontColor: '#4d6b8c'
                },
            }],
          xAxes: [{
                ticks: {
                    fontColor: '#4d6b8c'
                },
            }]
        } ,
        elements: {
                    point:{
                        radius: 0
                    }
                }
  }
});




</script>


@endpush
 