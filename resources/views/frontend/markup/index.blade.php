@extends('frontend.layouts.app')
@section('title', app_name() . ' | ' . __('navs.general.home'))
@section('content')  
@include('frontend.includes.title')
@include('common.alert')
<div class="row text-white">
    <div class="col "> 
        <form method="post" action="{{ url('/markup/store') }}"  >
            @csrf
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        {{-- <span class="form-label">Product Name</span>  --}}
                        {{ Form::select('branch_id', [null=>'SELECT '] + $subvendors, request('branch_id'), ['class' => 'full_box_dark_sm ']) }}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {{-- <span class="form-label">Price</span> --}}
                        <input class="full_box_dark_sm" type="number" placeholder="Markup Value" id="markup_value" name="markup_value"  value="{{old('markup_value')}}" required>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-btn">
                        <button class="btn btn-success pull-right">Save</button>
                    </div>
                </div>
               
            </div>    
        </form>
        <div class="table-responsive">
            <table class="table table-hover table-bordered  table-dark fixed_head_ list_table">
                <thead>
                    <tr class="bg-th">
                        <th>Sl No</th>
                        <th>Created At</th>
                        <th>Branch Name</th>
                        <th>Markup Value(%)</th>
                    </tr>
                </thead>
                @foreach($list as $key => $row)

                <tr class="bg-tr">
                    <td>{{ ($list->currentpage()-1) * $list->perpage() + $key + 1 }}</td>
                    <td>{{date("d-M-Y" ,strtotime($row->created_at))}}
                        <sub>{{date("h:i:a" ,strtotime($row->created_at))}}</sub>
                    </td>
                    <td>{{ $row->branch_name }}</td>
                    <td>{{ $row->markup_value }}</td>       
                </tr>
                @endforeach
            </table>

        </div>
        <nav>
            <ul class="pagination justify-content-end">
                {{$list->appends(\Request::except('_token'))->links('pagination::bootstrap-4')}}
            </ul>
        </nav>
      </div>
  </div>  
  
@endsection



