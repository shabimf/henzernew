@extends('frontend.layouts.app')
@section('title', app_name() . ' | ' . __('navs.general.home'))
@section('content')  
@include('frontend.includes.title')
@include('common.alert')
<div class="row text-white">
    <div class="col list_tab"> 
        
        <div class="table-responsive">
            <table class="table table-hover table-bordered  table-dark fixed_head_ list_table">
                <thead>
                    <tr class="bg-th">
                        <th width="4%">Sl No</th>
                        <th>Created At</th>
                        <th>Comany Name</th>
                        <th>Contact Person</th>
                        <th>Email</th>
                        <th>Phone No</th>
                    </tr>
                </thead>
                @foreach($list as $key => $row)

                <tr class="bg-tr">
                    <td>{{ ($list->currentpage()-1) * $list->perpage() + $key + 1 }}</td>
                    <td>{{date("d-M-Y" ,strtotime($row->created_at))}}
                        <sub>{{date("h:i:a" ,strtotime($row->created_at))}}</sub>
                    </td>
                    <td>{{ $row->company_name }}</td>
                    <td>{{ $row->contact_person }}</td>
                    <td>{{ $row->email }}</td>     
                    <td>{{ $row->phone_number  }}</td>            
                </tr>
                @endforeach
            </table>

        </div>
        <nav>
            <ul class="pagination justify-content-end">
                {{$list->appends(\Request::except('_token'))->links('pagination::bootstrap-4')}}
            </ul>
        </nav>
      </div>
  </div>  
@endsection
@push('after-scripts')
<link href="css/select2.min.css" rel="stylesheet" />
<script>
$(document).ready(function() {
    $('.select2').select2({
        placeholder: "Select Product",
        closeOnSelect: true
    });
});
</script>
@endpush


