@extends('frontend.layouts.app')
@section('title', app_name() . ' | ' . __('navs.general.home'))
@section('content') 
@include('common.alert') 
<div class="main">

    @include('frontend.includes.enquiry-tab')

        <div class="">

            
            <div class="card">
                <div class="card-body">
                  <form method="post" action="{{ url('/enquiry/store') }}"  >
                  @csrf
                  @if(isset($role) && $role == "Administrator" && Session::get('is_source_set') == 0)
                    <div class="row">
                        <div class="col">
                            <label>FROM BRANCH</label>
                            {{ Form::select('branch_from', [null=>'SELECT '] + $branches, request('branch_from'), ['class' => 'full_box_dark_sm ']) }}
                        </div>
                        <div class="col">
                            <label>TO BRANCH</label>
                            {{ Form::select('branch_to', [null=>'SELECT '] + $branches, request('branch_to') , ['class' => 'full_box_dark_sm ', (isset(Auth::user()->branch_id)) ? 'readonly':'']) }}
                        </div>
                    </div>
                  @else
                  <div class="row">
                      <div class="col">
                          <label>
                              @if(isset($role) && $role == "Vendor" && Session::get('is_source_set') == 1) 
                              VENDOR 
                              @elseif(isset($role) && $role == "Sub Vendor" && Session::get('is_source_set') == 0)
                              SUB VENDOR
                              @endif
                            </label>
                          {{ Form::select('branch_from', [null => 'SELECT TYPE'] + $vendors, old('branch_from',(isset(Auth::user()->branch_id)) ? Auth::user()->branch_id:''), ['class' => 'full_box_dark', (isset(Auth::user()->branch_id)) ? 'readonly':'']  ) }}
                      </div>            
                      <div class="col">
                          <label>
                              @if(isset($role) && $role == "Vendor" && Session::get('is_source_set') == 1) 
                              SOURCING TEAM 
                              @elseif(isset($role) && $role == "Sub Vendor" && Session::get('is_source_set') == 0)
                              VENDOR
                              @endif
                            </label>
                          {{ Form::select('branch_to', [null => 'SELECT TYPE'] + $sources, old('branch_to',(isset($branch_to)) ? $branch_to:''), ['class' => 'full_box_dark ', (isset(Auth::user()->branch_id)) ? 'readonly':'']) }}
                      </div>
                  </div>
                  @endif 
                  <div class="row">
                      <div class="col">
                          <button type="submit" class="btn btn-primary float-right">CREATE</button>
                      </div>
                  </div>
                  </form>       
                </div>
              </div>

        </div>

</div>  

@endsection



