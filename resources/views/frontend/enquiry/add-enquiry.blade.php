@extends('frontend.layouts.app')
@section('title', app_name() . ' | ' . __('navs.general.home'))
@section('content')  
@include('common.alert')

<div class="main">
    @include('frontend.includes.enquiry-tab')
        <div class="col">
            @include('frontend.includes.status')
            <div class="clearfix"></div>
            <hr>
           
            <table class="text-white table-sm table-bordered col-12 table">
                <tbody><tr>
                    <th class="bg-th">SERIAL NUM  </th> 
                    <td> {{ $common_details->serial_no ? $common_details->serial_no : '---' }}</td>
                    
                    <th class="bg-th">DATE  </th> 
                    <td> {{ $common_details->created_at ? date("d-M-Y h:i:a" ,strtotime($common_details->created_at)) : '---' }}</td>        
                </tr>
                <tr>
                    <th class="bg-th">FROM  </th> 
                    <td>{{ $common_details->branch_from_name ? $common_details->branch_from_name : '---' }}</td>
                    <th class="bg-th">TO  </th> 
                    <td> {{ $common_details->branch_to_name ? $common_details->branch_to_name : '---' }}</td>
                </tr>
            </tbody></table>
            <hr>
            @if($common_details->status == 0)
            <form method="post"  action="{{ url('/enquiry/submit/'.$parent_id) }}" class="form_add">
                @csrf
                @include('frontend.includes.create-form')    
            </form>
            @endif
                @if(count($details) > 0) 
                    <form method="post" id="update-form"  action="{{ url('/enquiry/update/'.$parent_id) }}">
                    <input type="hidden" value="enquiry" name="status"/>
                @endif
                <div class="mt-3">
                    <div class="table-responsive">
                        <table class="table  table-bordered  table-dark list_table fixed_head_" id="Tablesample">
                            <thead>
                                <tr>
                                    <th>Product</th>
                                    <th>Brand</th>
                                    <th>Model</th>
                                    <th>Series</th>
                                    <th>Qty</th> 
                                    <th>Price</th>  
                                    <th width="100"></th>
                                </tr>
                            </thead>
                            <tbody>   
                                @foreach($details as $data)
                                <tr  data-id="{{ $data->id }}">  
                                    <td>{{ $data->product_name }}</td>
                                    <td>{{ $data->brand_name }}</td>
                                    <td>{{ $data->model_name }}</td>
                                    <td>{{ $data->series_name }}</td>
                                    <td><input type="text" name="qty[{{ $data->id }}]" value="{{ $data->qty }}" class="full_box" ></td>
                                    <td class="keyvalue"><span>$</span> {{ $data->price }}</td>
                                    <td align="middle">  
                                        @if($common_details->status)
                                            <i class="fab fa-bitbucket float-left  get-value" title="Check Availability" data-value="{{ $data->catalog_id }}"></i>
                                            <span id="error_stock{{ $data->catalog_id }}"></span>
                                        @endif
                                        <i class="fa fa-times remove  text-danger pointer" aria-hidden="true" title="delete"  data-value="{{ $data->id }}"></i>
                                    </td>    
                                </tr>
                                @endforeach 
                            </tbody>
                            @if(count($details) > 0 && $common_details->status == 0) 
                            <tfoot>
                                <tr class="text-right">
                                    <td colspan="7">
                                      <input class="btn btn-success btn-sm pull-right" data-value="enquiry" data-id="{{ $parent_id }}" type="submit" value="Submit">
                                    </td>
                                </tr>
                            </tfoot>
                            @endif
                        </table>
                    </div>
                </div>
               
                {{ csrf_field() }}
                </form>
            </div>
        </div>
    
</div>
@endsection



