@extends('frontend.layouts.app')
@section('title', app_name() . ' | ' . __('navs.general.home'))
@section('content')  
@include('common.alert')

<div class="main">
    @include('frontend.includes.rma-tab')
        <div class="col">
            @include('frontend.includes.status')
            <div class="clearfix"></div>
            <hr>
            <table class="text-white table-sm table-bordered col-12 table">
                <tbody>
                    <tr>
                        <th class="bg-th">SERIAL NUM  </th> 
                        <td> {{ $common_details->serial_no ? $common_details->serial_no : '---' }}</td>
                        
                        <th class="bg-th">DATE  </th> 
                        <td> {{ $common_details->created_at ? date("d-M-Y h:i:a" ,strtotime($common_details->created_at)) : '---' }}</td>        
                    </tr>
                    <tr>
                        <th class="bg-th">FROM  </th> 
                        <td>{{ $common_details->branch_from_name ? $common_details->branch_from_name : '---' }}</td>
                        <th class="bg-th">TO  </th> 
                        <td> {{ $common_details->branch_to_name ? $common_details->branch_to_name : '---' }}</td>
                    </tr>
                </tbody>
            </table>
            <hr>
                <div class="mt-3">
                    <div class="table-responsive">
                        <table class="table  table-bordered  table-dark list_table fixed_head_">
                            <thead>
                                <tr>
                                    <th>Product</th>
                                    <th>Brand</th>
                                    <th>Model</th>
                                    <th>Series</th>
                                    <th>Qty</th> 
                                    <!-- <th>Price</th> 
                                    <th>Subtotal</th>    -->
                                    @if (Auth::user()->hasPermission('convert-status'))   
                                    <th>Actions</th>   
                                    @else
                                    <th>Accepted</th> 
                                    <th>Rejected</th>   
                                    <th>Balanced</th>     
                                    @endif
                                </tr>
                            </thead>
                            <tbody>   
                                @php
                                    $total = 0;
                                @endphp

                                @foreach($details as $data)
                                <tr  data-id="{{ $data->id }}">  
                                    <td>{{ $data->product_name }}</td>
                                    <td>{{ $data->brand_name }}</td>
                                    <td>{{ $data->model_name }}</td>
                                    <td>{{ $data->series_name }}</td>
                                    <td>{{ $data->qty }}</td>
                                    <!-- <td>AED {{ number_format($data->price,2) }}</td>
                                    <td>AED {{ number_format($data->price*$data->qty,2) }}</td> -->
                                    
                                    @if (Auth::user()->hasPermission('convert-status'))   
                                        <td >
                                        @if ( $data->status == 1)
                                        <button class="badge btn-success accecls" data-status="2">
                                        Accept
                                        </button>
                                        <button class="badge btn-danger rejcls" data-status="3">
                                            Reject 
                                        </button>
                                        @elseif ( $data->status == 2)
                                        <label class="badge badge-success">Accepted</label>
                                        @elseif ( $data->status == 3)
                                        <label class="badge badge-danger">Rejected</label>
                                        @endif
                                        </td>
                                    @else
                                    <td>{{checkRmaItemCount(2, $data->catalog_id, $parent_id)}}</td>
                                    <td>{{checkRmaItemCount(3, $data->catalog_id, $parent_id)}}</td>
                                    <td>{{checkRmaItemCount(1, $data->catalog_id, $parent_id)}}</td>
                                    @endif
                                </tr>
                                @php
                                    $total += $data->price*$data->qty;
                                @endphp
                                @endforeach 
                                <!-- <tr>
                                    <td colspan="6" align="right" class="total">Total</td>
                                    <td colspan="6" class="total">AED {{ number_format($total,2) }}</td>
                                </tr> -->
                            </tbody>
                           
                        </table>
                        {{ csrf_field() }}
                        
                    </div>
                </div>
                
            </div>
        </div>
    
</div>
@endsection


