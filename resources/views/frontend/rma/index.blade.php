@extends('frontend.layouts.app')
@section('title', app_name() . ' | ' . __('navs.general.home'))
@section('content') 
@include('common.alert') 
<div class="main">

    @include('frontend.includes.rma-tab')
    <div class="col text-right text-white pt-25 box-padd">
        <mark> <b>{{number_format($list->total())}}</b> RMA  </mark>
    </div>
    <div class=" search_bar minus-margin" >
        {{ Form::open(['method'=>'GET']) }}  

            <div class="row ">            
                <div class="@if(isset($role) && $role != "Sub Vendor") col-sm-2 @else col-sm-4 @endif">
                    <div class="form-group">
                        <label>RMA.Num</label>
                        <input class="full_box_dark_sm" type="text" name="serial_num" value="{{request('serial_num')}}">
                    </div>
                </div>
               
                @if(isset($role) && $role == "Vendor" && Session::get('is_source_set') == 1)
                <div class="col-sm-2">
                    <div class="form-group">
                        <label>VENDOR</label>
                        {{ Form::select('branch_from', [null=>'SELECT '] + $vendors, old('branch_from',(isset(Auth::user()->branch_id)) ? Auth::user()->branch_id:''), ['class' => 'full_box_dark_sm', (isset(Auth::user()->branch_id)) ? 'readonly':'']) }}
                    </div>
                </div>
               <div class="col-sm-2">
                    <div class="form-group">
                        <label>SOURCING TEAM</label>
                        {{ Form::select('branch_to', [null=>'SELECT '] + $sources, old('branch_to',(isset($branch_to)) ? $branch_to:''), ['class' => 'full_box_dark_sm ', (isset(Auth::user()->branch_id)) ? 'readonly':'']) }}
                    </div>
                </div>
                @endif
                @if(isset($role) && $role == "Vendor" && Session::get('is_source_set') == 0)
                <div class="col-sm-2">
                    <div class="form-group">
                        <label>SUB VENDOR</label>
                        {{ Form::select('branch_from', [null=>'SELECT '] + $subvendors, request('branch_from'), ['class' => 'full_box_dark_sm']) }}
                    </div>
                </div>
               <div class="col-sm-2">
                    <div class="form-group">
                        <label>VENDOR</label>
                        {{ Form::select('branch_to', [null=>'SELECT '] + $vendors, old('branch_to',(isset(Auth::user()->branch_id)) ? Auth::user()->branch_id:''), ['class' => 'full_box_dark_sm ', (isset(Auth::user()->branch_id)) ? 'readonly':'']) }}
                    </div>
                </div>
                @endif
                @if(isset($role) && $role == "Sourcing Team" && Session::get('is_source_set') == 0)
                <div class="col-sm-2">
                    <div class="form-group">
                        <label>VENDOR</label>
                        {{ Form::select('branch_from', [null=>'SELECT '] + $subvendors, request('branch_from'), ['class' => 'full_box_dark_sm']) }}
                    </div>
                </div>
               <div class="col-sm-2">
                    <div class="form-group">
                        <label>SOURCING TEAM</label>
                        {{ Form::select('branch_to', [null=>'SELECT '] + $sources, old('branch_to',(isset(Auth::user()->branch_id)) ? Auth::user()->branch_id:''), ['class' => 'full_box_dark_sm ', (isset(Auth::user()->branch_id)) ? 'readonly':'']) }}
                    </div>
                </div>
                @endif 
                @if(isset($role) && $role == "Administrator" && Session::get('is_source_set') == 0)
                <div class="col-sm-2">
                    <div class="form-group">
                        <label>FROM BRANCH</label>
                        {{ Form::select('branch_from', [null=>'SELECT '] + $branches, request('branch_from'), ['class' => 'full_box_dark_sm']) }}
                    </div>
                </div>
               <div class="col-sm-2">
                    <div class="form-group">
                        <label>TO BRANCH</label>
                        {{ Form::select('branch_to', [null=>'SELECT '] + $branches, request('branch_to') , ['class' => 'full_box_dark_sm ', (isset(Auth::user()->branch_id)) ? 'readonly':'']) }}
                    </div>
                </div>
                @endif 
               
                <div class="@if(isset($role) && $role != "Sub Vendor") col-sm-3 @else col-sm-4 @endif">
                    <div class="form-group">
                        <label>DATE.FROM</label>
                        <input class="full_box_dark_sm" type="date" name="d_from" value="{{request('d_from')}}">
                    </div>
                </div>
                <div class="@if(isset($role) && $role != "Sub Vendor") col-sm-3 @else col-sm-4 @endif">
                    <div class="form-group">
                        <label>DATE.TO</label>
                        <input class="full_box_dark_sm" type="date" name="d_to" value="{{request('d_to')}}">
                    </div>
                </div>

               

            </div>

            <div class="row ">
                <div class="col-sm-2">
                    <div class="form-group">
                        <label>STATUS</label>
                        {{ Form::select('status', [null=>'SELECT '] + $status_array, request('status'), ['class' => 'full_box_dark_sm ']) }}
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label>&nbsp;</label>
                        <select id="filter_date" name="filter_date" class="full_box_dark_sm">
                        <option value="0">--Select--</option>
                            <option value="10" {{ request('filter_date') == 10 ? 'selected' : '' }}>Last 10 days</option>
                            <option value="30" {{ request('filter_date') == 30 ? 'selected' : '' }}>Last 30 days</option>
                            <option value="3" {{ request('filter_date') == 3 ? 'selected' : '' }}>3 months</option>
                       </select>
                    </div>
                </div>

                <div class="col-sm-2 pt-25">
                    <button class="btn btn-search btn-block">SEARCH</button>
                </div>

                <div class="col-sm-1 pt-25">
                    <a href="{{ url('/rma') }}"
                        class="btn btn-secondary ls1 br0 btn-block">RESET</a>
                </div>

               

            </div>

        {{ Form::close() }}
    </div>
    
    
        <div class="row">

        
            <div class="table-responsive">
                <table class="table table-hover table-bordered  table-dark fixed_head_ list_table">
                    <thead>
                        <tr class="bg-th">
                            <th>SL</th>
                            <th>DATE</th>
                            <th>RMA No.</th>
                            <th>FROM</th>
                            <th>TO</th>
                           
                            <th>CREATED BY</th>
                            <th>STATUS</th>
                            <th width="80" align="middle" >ACTION</th>
                        </tr>
                    </thead>
                    @foreach($list as $key => $row)

                    <tr class="bg-tr tr-pointer-{{$row->id}}" data-href="{{ url('/rma/view/'.$row->id) }}" >
                        <td class="td-pointer" value="{{$row->id}}">{{ ($list->currentpage()-1) * $list->perpage() + $key + 1 }}</td>
                        <td class="td-pointer" value="{{$row->id}}">
                            {{date("d-M-Y" ,strtotime($row->created_at))}}
                            <sub>{{date("h:i:a" ,strtotime($row->created_at))}}</sub>
                        </td>
                        <td class="td-pointer" value="{{$row->id}}">{{$row->serial_no}}</td>
                        <td class="td-pointer" value="{{$row->id}}">{{$row->branch_from_name}}</td>
                        <td class="td-pointer" value="{{$row->id}}">{{$row->branch_to_name}}</td>
                      
                        <td class="td-pointer" value="{{$row->id}}">{{$row->created_by}}</td>
                        <td value="{{$row->id}}" class="td-pointer @if(checkRmaCount(2, $row->id) == 0 && checkRmaCount(3, $row->id) == 0) text-black bg-warning  @elseif(checkRmaCount(1, $row->id) == 0) text-white  bg-success @else text-white  bg-primary @endif">
                            @if(checkRmaCount(2, $row->id) == 0 && checkRmaCount(3, $row->id) == 0) <span> PENDING</span>
                            @elseif(checkRmaCount(1, $row->id) == 0) <span> FINISHED </span>
                            @else <span> PROCESSING </span>                           
                            @endif
                        </td>
                        <td class="text-center action_td">
                            @if($row->status == 0)
                              <a href="{{ url('/rma/add/'.$row->id) }}" >
                                <i title="View" class="far fa-eye text-primary cp "></i>
                              </a>
                            @else
                                <a href="{{ url('/rma/view/'.$row->id) }}" >
                                  <i title="View" class="far fa-eye text-primary cp "></i>
                                </a>
                            @endif
                            
                            <!-- <a href="#" data-toggle="modal" data-target="#comment-box" data-id="{{$row->id}}"  class="openPopup">
                                <i title="Chat" class="far fa-comment text-primary cp "></i>
                            </a> -->

                            <a href="#" data-toggle="modal" data-target="#credit-box" data-id="{{$row->id}}" data-note="{{$row->note}}">
                                <i title="Chat" class="far  fa-address-book text-primary cp "></i>
                            </a>

                        </td>
                    </tr>
                    @endforeach
                </table>

            </div>
            <nav>
                <ul class="pagination justify-content-end">
                    {{$list->appends(\Request::except('_token'))->links('pagination::bootstrap-4')}}
                </ul>
            </nav>

        </div>

</div>  
@include('frontend.includes.comment')
@endsection



