<!DOCTYPE html>
@langrtl
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="rtl">
@else
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@endlangrtl
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>@yield('title', app_name())</title>
        <meta name="description" content="@yield('meta_description', 'Laravel Starter')">
        <meta name="author" content="@yield('meta_author', 'FasTrax Infotech')">
        @yield('meta')

        {{-- See https://laravel.com/docs/5.5/blade#stacks for usage --}}
        @stack('before-styles')

        <!-- Check if the language is set to RTL, so apply the RTL layouts -->
        <!-- Otherwise apply the normal LTR layouts -->        
        {{ style(mix('css/vendor.css')) }} 
        {{ style(mix('css/font.css')) }} 
        {{ style(mix('css/style.css')) }}
        @isset($css)
        @foreach($css as $c)
        {!! style(asset('css/'. $c. '.css')) !!}
        @endforeach
        @endif
        @stack('after-styles')
    </head>
    <body class="dark-mode">
        <div id="app"> 
            @if (auth()->check())
            @include('frontend.includes.mobnav')
            @endif   
            <div class="container-fluid body_bg">
                @include('includes.partials.messages') 
                <div class="d-flex flex-row">
                    @if (auth()->check())
                    <div class="col-sm-2 d-none d-sm-block left_nav {{ session()->get('nav') }}"  id="left_nav" >
                        @include('frontend.includes.nav')
                    </div> 
                    @endif   
                    
                    <div class="@auth col-sm-10 @else col-sm-12 @endauth  col-xs-12 content_box no-padding-lr {{ session()->get('container') }}" id="content_box"  data-simplebar>
                        <div class="col-sm-12 col-xs-12 inner-pad" >  
                        {{-- @include('frontend.includes.title')   --}}
                        @yield('content') 
                        </div>
                        <button onclick="topFunction()" id="myBtn" title="Go to top">&#9651;</button>
                    </div>
                </div>
            </div><!-- container -->
        </div><!-- #app -->

        <!-- Scripts -->
        @stack('before-scripts')
        {!! script(mix('js/manifest.js')) !!}
        {!! script(mix('js/vendor.js')) !!}
        {!! script(mix('js/frontend.js')) !!}
        {!! script(mix('js/menu.js')) !!}
        {!! script(mix('js/adminlte.js')) !!}
        
        @isset($js)
        @foreach($js as $j)
        {!! script(asset('js/frondend/'. $j. '.js')) !!}
        @endforeach
        @endif
        @stack('after-scripts')
        @include('includes.partials.ga')
    </body>
</html>
