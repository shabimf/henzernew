<!DOCTYPE html>
<html>
	<head>
    <meta charset="utf-8" />
    <title>Invoice - {!! $common_details->serial_no !!}</title>

    <style>
       /*
        Common invoice styles. These styles will work in a browser or using the HTML
        to PDF anvil endpoint.
        */

        body {
        font-size: 16px;
        }

        table {
        width: 100%;
        border-collapse: collapse;
        }

        table tr td {
        padding: 0;
        }

        table tr td:last-child {
        text-align: right;
        }

        .bold {
        font-weight: bold;
        }

        .right {
        text-align: right;
        }

        .large {
        font-size: 1.75em;
        }

        .total {
        font-weight: bold;
        color: #fb7578;
        }

        .logo-container {
        margin: 20px 0 70px 0;
        }

        .invoice-info-container {
        font-size: 0.875em;
        }
        .invoice-info-container td {
        padding: 4px 0;
        }

        .client-name {
        font-size: 1.5em;
        vertical-align: top;
        }

        .line-items-container {
        margin: 70px 0;
        font-size: 0.875em;
        }

        .line-items-container th {
        text-align: left;
        color: #212529;
        border-bottom: 2px solid #ddd;
        padding: 10px 0 15px 0;
        font-size: 0.75em;
        text-transform: uppercase;
        }

        .line-items-container th:last-child {
        text-align: right;
        }

        .line-items-container td {
        padding: 15px 0;
        }

        .line-items-container tbody tr:first-child td {
        padding-top: 25px;
        }

        .line-items-container.has-bottom-border tbody tr:last-child td {
        padding-bottom: 25px;
        border-bottom: 2px solid #ddd;
        }

        .line-items-container.has-bottom-border {
        margin-bottom: 0;
        }
        .heading {
            color: #212529;
        }
       
        .line-items-container th.heading-quantity {
        width: 50px;
        }
        .line-items-container th.heading-price {
        text-align: right;
        width: 100px;
        }
        .line-items-container th.heading-subtotal {
        width: 100px;
        }

        .payment-info {
        width: 38%;
        font-size: 0.75em;
        line-height: 1.5;
        }

        .footer {
        margin-top: 100px;
        }

        .footer-thanks {
        font-size: 1.125em;
        }

        .footer-thanks img {
        display: inline-block;
        position: relative;
        top: 1px;
        width: 16px;
        margin-right: 4px;
        }

        .footer-info {
        float: right;
        margin-top: 5px;
        font-size: 0.75em;
        color: #ccc;
        }

        .footer-info span {
        padding: 0 5px;
        color: black;
        }

        .footer-info span:last-child {
        padding-right: 0;
        }

        .page-container {
        display: none;
        }
        .footer {
        margin-top: 30px;
        }

        .footer-info {
        float: none;
        position: running(footer);
        margin-top: -25px;
        }

        .page-container {
        display: block;
        position: running(pageContainer);
        margin-top: -25px;
        font-size: 12px;
        text-align: right;
        color: #999;
        }

        .page-container .page::after {
        content: counter(page);
        }

        .page-container .pages::after {
        content: counter(pages);
        }


        @page {
        @bottom-right {
            content: element(pageContainer);
        }
        @bottom-left {
            content: element(footer);
        }
        }
	</style>
	</head>

	<body>
        <div class="page-container">
            Page
            <span class="page"></span>
            of
            <span class="pages"></span>
          </div>
		<div class="logo-container">
            <img style="height: 18px" src="images/logo.png">
        </div>
          
        <table class="invoice-info-container">
            <tr>
                <td rowspan="2" >
                </td>
                <td class="client-name">
                    From: {!! $common_details->branch_from_name !!}  
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                Invoice Date: <strong>{!! date('M d, Y', strtotime($common_details->created_at)) !!}</strong>
                </td>
                <td>
                To: {!! $common_details->branch_to_name !!} 
                </td>
            </tr>
            <tr>
                <td>
                Invoice No: <strong>{!! $common_details->serial_no !!}</strong>
                </td>
                <td>
                
                </td>
            </tr>
        </table>
          
          
        <table class="line-items-container">
            <thead>
                <tr>
                <th class="heading-description">ITEM</th>
                <th class="heading-quantity">QTY</th>
                <th class="heading-price">PRICE</th>
                <th class="heading-subtotal">SUBTOTAL</th>
                </tr>
            </thead>
            <tbody>
            @php($total=0)
            @foreach($details as $data)
            @php($total+=$data->qty*$data->price)

                <tr>
                <td>{{ $data->product_name }}</td>
                <td>{{ $data->qty }}</td>
                <td class="right">AED {{ number_format($data->price,2) }}</td>
                <td class="bold">AED {{ number_format($data->qty*$data->price,2) }}</td>
                </tr>
                
            @endforeach
            <tr>
                <td class="right" colspan="3">Total amount</td>
                <td class="right total-amount">AED {{ number_format($total,2) }}</td>
            </tr>
            </tbody>
        </table>
	</body>
</html>

