@extends('frontend.layouts.app')
@section('title', app_name() . ' | ' . __('navs.general.home'))
@section('content')  
@include('common.alert')

<div class="main">
    @include('frontend.includes.invoice-tab')
        <div class="col">
            @include('frontend.includes.status')
            <div class="clearfix"></div>
            <hr>
            <table class="text-white table-sm table-bordered col-12 table">
                <tbody>
                    <tr>
                        <th class="bg-th">SERIAL NUM  </th> 
                        <td> {{ $common_details->serial_no ? $common_details->serial_no : '---' }}</td>
                        
                        <th class="bg-th">DATE  </th> 
                        <td> {{ $common_details->created_at ? date("d-M-Y h:i:a" ,strtotime($common_details->created_at)) : '---' }}</td>        
                    </tr>
                    <tr>
                        <th class="bg-th">FROM  </th> 
                        <td>{{ $common_details->branch_from_name ? $common_details->branch_from_name : '---' }}</td>
                        <th class="bg-th">TO  </th> 
                        <td> {{ $common_details->branch_to_name ? $common_details->branch_to_name : '---' }}</td>
                    </tr>
                </tbody>
            </table>
            <hr>
                <div class="mt-3">
                    <div class="table-responsive">
                        <table class="table  table-bordered  table-dark list_table fixed_head_">
                            <thead>
                                <tr>
                                    <th>Product</th>
                                    <th>Brand</th>
                                    <th>Model</th>
                                    <th>Series</th>
                                    <th>Is Warranty</th> 
                                    <th>Qty</th> 
                                    <th>Price</th> 
                                    <th>Subtotal</th>   
                                </tr>
                            </thead>
                            <tbody>   
                                @php
                                    $total = 0;
                                @endphp

                                @foreach($details as $data)
                                <tr  data-id="{{ $data->id }}">  
                                    <td>{{ $data->product_name }}</td>
                                    <td>{{ $data->brand_name }}</td>
                                    <td>{{ $data->model_name }}</td>
                                    <td>{{ $data->series_name }}</td>
                                    <td>{{ $data->is_warranty ? 'yes' : 'no' }}</td>
                                    <td>{{ $data->qty }}</td>
                                    <td><span>$</span> {{ number_format($data->price,2) }}</td>
                                    <td><span>$</span> {{ number_format($data->price*$data->qty,2) }}</td>
                                </tr>
                                @php
                                    $total += $data->price*$data->qty;
                                @endphp
                                @endforeach 
                                <tr>
                                    <td colspan="7" align="right"  class="total" >Total</td>
                                    <td  class="total"><span>$</span> {{ number_format($total,2) }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                
            </div>
        </div>
    
</div>
@endsection

