
@extends('frontend.layouts.app')
@section('title', app_name() . ' | ' . __('navs.general.home'))
@section('content')  
@include('common.alert')

<div class="main">
    @include('frontend.includes.invoice-tab')
        <div class="col">
            @include('frontend.includes.status')
            <div class="clearfix"></div>
            <hr>
            <table class="text-white table-sm table-bordered col-12 table">
                <tbody>
                    <tr>
                        <th class="bg-th">SERIAL NUM  </th> 
                        <td> {{ $common_details->serial_no ? $common_details->serial_no : '---' }}</td>
                        
                        <th class="bg-th">DATE  </th>
                        <td> {{ $common_details->created_at ? date("d-M-Y h:i:a" ,strtotime($common_details->created_at)) : '---' }}</td>        
                    </tr>
                    <tr>
                        <th class="bg-th">FROM  </th> 
                        <td>{{ $common_details->branch_from_name ? $common_details->branch_from_name : '---' }}</td>
                        <th class="bg-th">TO  </th> 
                        <td> {{ $common_details->branch_to_name ? $common_details->branch_to_name : '---' }}</td>
                    </tr>
                </tbody>
            </table>
            <hr>

                <div class="mt-3">
                <form method="post" action="{{ url('/rma/qtyupdate/'.$parent_id) }}" >
                  @csrf
                    <div class="table-responsive">
                        <table class="table  table-bordered  table-dark list_table fixed_head_">
                            <thead>                                                                     
                                <tr>
                                    <th>#</th>                                   
                                    <th>Product</th>
                                    <th>Brand</th>
                                    <th>Model</th>
                                    <th>Series</th>
                                    <th>Is Warranty</th> 
                                    <th>Qty</th> 
                                    <th class="return">Return Qty</th> 
                                     
                                </tr>
                            </thead>
                            <tbody>   
                                @php
                                    $total = 0;
                                @endphp

                                @foreach($details as $data)
                                <tr  data-id="{{ $data->id }}">  
                                    <td><input type="checkbox" name="check[]" id="check" data-id="{{ $data->id }}" value="{{ $data->catalog_id }}"></td>
                                    <td>{{ $data->product_name }}</td>
                                    <td>{{ $data->brand_name }}</td> 
                                    <td>{{ $data->model_name }}</td>
                                    <td>{{ $data->series_name }}</td>
                                    <td>{{ $data->is_warranty ? 'yes' : 'no' }}</td>
                                    <td>{{ $data->qty }}</td>
                                    <td class="return"><input type="text" name="returnqty[{{ $data->catalog_id }}]" id="returnqty_{{ $data->id }}" class="hide" value="{{ $data->qty }}"></td>
                                  
                                </tr>
                            
                                @endforeach 
                                <tr>

                                    <td colspan="12" align="right"><button type="submit" class="btn btn-primary"> SUBMIT </button></td>
                                    
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    </form> 
                </div>
                
            </div>
        </div>
    
</div>
@endsection
@push('after-scripts')
<script>
     $(document).ready(function() {
          $('input[type="checkbox"]').click(function() {
            var id = $(this).data('id');
            var checked = $("input[name='check[]']:checked").length;
              if(checked>0) {
                 $('.return').show();
              } else {
                $('.return').hide();
              }
              if($(this).prop("checked") == true) {             
                $('#returnqty_'+id).show();
              } else {
                $('#returnqty_'+id).hide();
              }
 
            });
        });

</script>

@endpush