@extends('frontend.layouts.app')
@section('title', app_name() . ' | ' . __('navs.general.home'))
@section('content')  
@include('frontend.includes.title')
@include('common.alert')
<div class="row text-white">
    <div class="col list_tab"> 
        <form method="post" class="search_barr" action="{{ url('/price/store') }}"  >
            @csrf
            <div class="row padd">
                <div class="col">
                    <div class="form-group">
                        {{-- <span class="form-label">Product Name</span>  --}}
                        {{ Form::select('catalog_id', $list_catalog, old('catalog_id'), ['class' => 'full_box_dark select2 add_fieldd', 'placeholder' => trans('--- Select Product ---'), 'required' => 'required']) }}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {{-- <span class="form-label">Price</span> --}}
                        <input class="full_box_dark add_fieldd" type="text" placeholder="USD Price" id="price" name="price"  value="{{old('price')}}" required>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-btn stock-add">
                        <button class="btn btn-success pull-right">Add</button>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-btn text-right">
                        <a class="btn btn-primary btn-sm pull-right text-white buttons" 
                            style="cursor: pointer" 
                            data-toggle="modal" 
                            data-target="#importModal">
                            Import Price
                        </a>
                    </div>
                </div>
            </div>  
        </form>
        <div class="table-responsive">
            <table class="table table-hover table-bordered  table-dark fixed_head_ list_table">
                <thead>
                    <tr class="bg-th">
                        <th width="4%">Sl No</th>
                        <th>Created At</th>
                        <th>Product Name</th>
                        <th>Price</th>
                    </tr>
                </thead>
                @foreach($list as $key => $row)

                <tr class="bg-tr">
                    <td>{{ ($list->currentpage()-1) * $list->perpage() + $key + 1 }}</td>
                    <td>{{date("d-M-Y" ,strtotime($row->created_at))}}
                        <sub>{{date("h:i:a" ,strtotime($row->created_at))}}</sub>
                    </td>
                    <td>{{ $row->catalog_id }}</td>
                    <td><span>$</span> {{ $row->price }}</td>       
                </tr>
                @endforeach
            </table>

        </div>
        <nav>
            <ul class="pagination justify-content-end">
                {{$list->appends(\Request::except('_token'))->links('pagination::bootstrap-4')}}
            </ul>
        </nav>
      </div>
  </div>  
  <div class="modal fade" id="importModal" tabindex="-1" role="dialog" aria-labelledby="importModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="importModal">Import Catalog Price</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ url('/price/import') }}" method="POST" name="importform" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group row">
                        <label for="file" class="col-md-3 col-form-label text-md-right">File</label>

                        <div class="col-md-6">
                            <input id="file" type="file" name="file" class="form-control">
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-8 offset-md-4">
                            <button type="submit" class="btn btn-info">
                                Submit
                            </button>
                            <a class="btn btn-link" href="{{ url('/price/export') }}">
                                Export File
                            </a>
                            
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@push('after-scripts')
<link href="css/select2.min.css" rel="stylesheet" />
<script>
$(document).ready(function() {
    $('.select2').select2({
        placeholder: "Select Product",
        closeOnSelect: true
    });
});
</script>
@endpush


