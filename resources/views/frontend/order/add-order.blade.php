@extends('frontend.layouts.app')
@section('title', app_name() . ' | ' . __('navs.general.home'))
@section('content')  
@include('common.alert')

<div class="main">
    @include('frontend.includes.order-tab')
        <div class="col">
            @include('frontend.includes.status')
            <div class="clearfix"></div>
            <hr>
           
            <table class="text-white table-sm table-bordered col-12 table">
                <tbody><tr>
                    <th class="bg-th">SERIAL NUM  </th> 
                    <td> {{ $common_details->serial_no ? $common_details->serial_no : '---' }}</td>
                    
                    <th class="bg-th">DATE  </th> 
                    <td> {{ $common_details->created_at ? date("d-M-Y h:i:a" ,strtotime($common_details->created_at)) : '---' }}</td>        
                </tr>
                <tr>
                    <th class="bg-th">FROM  </th> 
                    <td>{{ $common_details->branch_from_name ? $common_details->branch_from_name : '---' }}</td>
                    <th class="bg-th">TO  </th> 
                    <td> {{ $common_details->branch_to_name ? $common_details->branch_to_name : '---' }}</td>
                </tr>
            </tbody></table>
            <hr>
            @if($common_details->status == 0 && $common_details->quotation_id == 0)
            <form method="post" class="form_add"  action="{{ url('/order/submit/'.$parent_id) }}">
                @csrf
                @include('frontend.includes.create-form')    
            </form>
            @endif
                @if(count($details) > 0)         
                     <form method="post" id="update-form"  action="{{ url('/order/update/'.$parent_id) }}">
                    <input type="hidden" value="order" name="status"/>
                @endif
                <div class="mt-3">
                    <div class="table-responsive">
                        <table class="table  table-bordered  table-dark list_table fixed_head_">
                            <thead>
                                <tr>
                                    <th>Product</th>
                                    <th>Brand</th>
                                    <th>Model</th>
                                    <th>Series</th>
                                    <th align="left">Is Warranty</th> 
                                    <th>Qty</th> 
                                    <th>Price</th>  
                                    <th width="100"></th>
                                </tr>
                            </thead>
                            <tbody>   
                                @foreach($details as $data)
                                <tr  data-id="{{ $data->id }}">  
                                    <td>{{ $data->product_name }}</td>
                                    <td>{{ $data->brand_name }}</td>
                                    <td>{{ $data->model_name }}</td>
                                    <td>{{ $data->series_name }}</td>
                                    <td>
                                        <div class="form-check form-check-inline">
                                        @if($common_details->status == 0) 
                                          <input  type="hidden" name="is_warranty[{{ $data->id }}]"  value="{{ $data->is_warranty ? '1' : '0' }}" 
                                          />
                                          {{ $data->is_warranty ? 'yes' : 'no' }}
                                          
                                          @else 
                                          <input class="form-check-input" type="checkbox" name="is_warranty[{{ $data->id }}]" id="inlineCheckbox1" value="1" 
                                          {{ $data->is_warranty ? 'checked' : '' }} />
                                          @endif
                                        </div>
                                      </td>
                                    <td><input type="hidden" name="qty[{{ $data->id }}]" value="{{ $data->qty }}" class="full_box" >{{ $data->qty }}</td>
                                    <td> 
                                        @if($common_details->status == 0) 
                                        <span>$</span> <input type="hidden" name="price[{{ $data->id }}]" value="{{ $data->price }}" class="full_box" >
                                          {{ $data->price }}
                                        @else
                                        <span>$</span> <input type="text" name="price[{{ $data->id }}]" value="{{ $data->price }}" class="full_box" >
                                        @endif
                                    </td>
                                    <td align="middle">                                       
                                        @if($common_details->status)
                                            <i class="fab fa-bitbucket float-left  get-value" title="Check Availability" data-value="{{ $data->catalog_id }}"></i>
                                            <span id="error_stock{{ $data->catalog_id }}"></span>
                                        @endif
                                        <i class="fa fa-times remove  text-danger" aria-hidden="true" title="delete"  data-value="{{ $data->id }}"></i>
                                    </td>    
                                </tr>
                                @endforeach 
                            </tbody>
                            @if(count($details) > 0 && $common_details->status == 0) 
                            <tfoot>
                                <tr class="text-right">
                                    <td colspan="8">
                                      <input class="btn btn-success btn-sm pull-right" data-value="order" data-id="{{$parent_id}}" type="submit" value="Submit">
                                    </td>
                                </tr>
                            </tfoot>
                            @endif
                        </table>
                    </div>
                </div>
               
                {{ csrf_field() }}
                </form>
            </div>
        </div>
    
</div>
@endsection


