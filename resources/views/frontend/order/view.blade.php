@extends('frontend.layouts.app')
@section('title', app_name() . ' | ' . __('navs.general.home'))
@section('content')  
@include('common.alert')

<div class="main">
    @include('frontend.includes.order-tab')
        <div class="col">
            @include('frontend.includes.status')
            <div class="clearfix"></div>
            <hr>
            <table class="text-white table-sm table-bordered col-12 table">
                <tbody>
                    <tr>
                        <th class="bg-th">SERIAL NUM  </th> 
                        <td> {{ $common_details->serial_no ? $common_details->serial_no : '---' }}</td>
                        
                        <th class="bg-th">DATE  </th> 
                        <td> {{ $common_details->created_at ? date("d-M-Y h:i:a" ,strtotime($common_details->created_at)) : '---' }}</td>        
                    </tr>
                    <tr>
                        <th class="bg-th">FROM  </th> 
                        <td>{{ $common_details->branch_from_name ? $common_details->branch_from_name : '---' }}</td>
                        <th class="bg-th">TO  </th> 
                        <td> {{ $common_details->branch_to_name ? $common_details->branch_to_name : '---' }}</td>
                    </tr>
                </tbody>
            </table>
            <hr>
           
                <div class="mt-3">
                    <div class="table-responsive">
                    @if($common_details->status > 0)
                      <form method="post"  action="{{ url('/order/update/'.$parent_id) }}">
                    @else 
                    
                    @endif
                        <table class="table  table-bordered  table-dark list_table fixed_head_">
                            <thead>
                                <tr>
                                @if (Auth::user()->hasPermission('convert-to-invoice') && $common_details->status == 3 || $common_details->status == 1)
                                    @if(Session::get('is_source_set') == 0 && $role == "Vendor" || $role == "Sourcing Team")<th><input type="checkbox" id="select_all"/></th>@endif
                                @endif
                                    <th>Product</th>
                                    <th>Brand</th>
                                    <th>Model</th>
                                    <th>Series</th>
                                    <th>Is Warranty</th> 
                                    <th>Qty</th> 
                                    @if ($common_details->status == 3)
                                    <th>Total Qty</th> 
                                    <th>Used Qty</th>
                                    @endif
                                    <th>Price</th> 
                                </tr>
                            </thead>
                            <tbody>   
                                @php
                                    $total = 0;
                                    $total_qty = 0;
                                @endphp
                                @foreach($details as $data)
                                @php
                                    $total += $data->price*$data->qty;
                                    $total_qty +=  $data->qty - checkInvoiceItemCount($parent_id, $data->catalog_id);
                                @endphp
                                <tr  data-id="{{ $data->id }}"> 
                                    @if ($total_qty > 0 && Auth::user()->hasPermission('convert-to-invoice')  && $common_details->status == 3 || $common_details->status == 1)
                                      @if(Session::get('is_source_set') == 0 && $role == "Vendor" || $role == "Sourcing Team")
                                        <td>
                                        @if(checkInvoiceItemCount($parent_id, $data->catalog_id) != $data->qty) <input  class="checkbox" type="checkbox" value="{{$data->id}}" name="selector[]" @if($data->status == 1) disabled  @endif/>  @endif
                                        </td>
                                      @endif 
                                    @endif 
                                    <td>{{ $data->product_name }}</td>
                                    <td>{{ $data->brand_name }}</td>
                                    <td>{{ $data->model_name }}</td>
                                    <td>{{ $data->series_name }}</td>
                                    <td>{{ $data->is_warranty ? 'yes' : 'no' }}</td>
                                    <td>
                                    @if ($total_qty > 0 && Auth::user()->hasPermission('convert-to-invoice')  && $common_details->status == 3)
                                    @if(Session::get('is_source_set') == 0 && $role == "Vendor" || $role == "Sourcing Team")
                                    <input type="text" value="{{ $data->qty - checkInvoiceItemCount($parent_id, $data->catalog_id)}}" name="qty_{{$data->id}}" @if(checkInvoiceItemCount($parent_id, $data->catalog_id) == $data->qty) disabled  @endif /> 
                                    @else {{ $data->qty }} @endif
                                    @else {{ $data->qty }} @endif </td>
                                    @if ($common_details->status == 3)
                                   
                                    <td> {{$data->qty}} </td>
                                    <td> {{ checkInvoiceItemCount($parent_id, $data->catalog_id)}} </td>
                              
                                    @endif
                                    <td>@if($common_details->status == 1 && Auth::user()->hasPermission('convert-to-sales-order') && Session::get('is_source_set') == 0) <span>$</span> <input type="hidden" value="{{ $data->price }}" name="price_{{$data->id}}"/> {{ $data->price }}  @else <span>$</span> {{ number_format($data->price,2) }} @endif</td>
                                </tr>  
                                @endforeach 
                                <!-- <tr>
                                    <td colspan="@if($common_details->status == 4 || $common_details->status == 2) 9 @elseif($common_details->status == 1) 6 @else 8 @endif" align="right" class="total">Total </td>
                                    <td  class="total">{{ number_format($total,2) }} </td>
                                </tr> -->
                            </tbody>
                            @if(count($details) > 0  && $common_details->status == 1 || $common_details->status == 3 || $common_details->status == 2 || $common_details->status == 4) 
                            <tfoot>
                               
                                <tr class="text-right"> 
                                    <!-- @if($common_details->status == 3 &&  Auth::user()->hasPermission('convert-status') && Session::get('is_source_set') == 1 && $role == "Vendor")   
                                    <td colspan="10">
                                        <input class="btn btn-success btn-sm pull-right btn-app" data-id="approve" type="submit" value="APPROVE">
                                        <input class="btn btn-danger btn-sm pull-right btn-app" data-id="reject" type="submit" value="REJECT">
                                    </td> 
                                    @endif
                                     @if($common_details->status == 3 &&  Auth::user()->hasPermission('convert-status') && Session::get('is_source_set') == 0 && $role == "Sub Vendor")   
                                    <td colspan="10">
                                        <input class="btn btn-success btn-sm pull-right btn-app" data-id="approve" type="submit" value="APPROVE">
                                        <input class="btn btn-danger btn-sm pull-right btn-app" data-id="reject" type="submit" value="REJECT">
                                    </td> 
                                    @endif   -->
                                    @if($common_details->status == 1)
                                        @if (Auth::user()->hasPermission('convert-to-sales-order') && Session::get('is_source_set') == 0 && $role == "Vendor")
                                            <td colspan="10">
                                                <input class="btn btn-success btn-sm pull-right btn-submit" disabled data-id="{{$parent_id}}" type="submit" value="CONVERT TO SALESORDER">
                                            </td>
                                        @endif
                                        @if (Auth::user()->hasPermission('convert-to-sales-order') && Session::get('is_source_set') == 0 && $role == "Sourcing Team")
                                            <td colspan="10">
                                                <input class="btn btn-success btn-sm pull-right btn-submit" disabled data-id="{{$parent_id}}" type="submit" value="CONVERT TO SALESORDER">
                                            </td>
                                        @endif
                                        @else
                                        @if (Auth::user()->hasPermission('convert-to-invoice')  && $common_details->status == 3)
                                          @if($total_qty > 0 && Session::get('is_source_set') == 0 && $role == "Vendor" || $role == "Sourcing Team")
                                            <td colspan="@if($common_details->status == 3 || $common_details->status == 2) 11 @else 10 @endif">
                                                <input class="btn btn-success btn-sm pull-right btn-submit" type="submit" value="CONVERT TO INVOICE">
                                            </td>
                                          @endif
                                        @endif
                                    @endif
                                </tr>
                            </tfoot>
                            @endif
                        </table>
                        <input type="hidden" name="status" value="@if($common_details->status == 3) invoice  @else salesorder @endif" >
                        {{ csrf_field() }}
                        </form>
                    </div>
                </div>     
            </div>
        </div>
</div>
@endsection


