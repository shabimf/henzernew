
{{ html()->form('PATCH', url('password/update'))->class('form-horizontal')->open() }}
<div class="form-group row">
    <label for="inputName" class="col-sm-2 col-form-label"> {{ html()->label(__('validation.attributes.frontend.old_password'))->for('old_password') }}</label>
    <div class="col-sm-10">
        {{ html()->password('old_password')
        ->class('form-control')
        ->placeholder(__('validation.attributes.frontend.old_password'))
        ->autofocus()
        ->required() }}
    </div>
  </div>
  <div class="form-group row">
    <label for="inputEmail" class="col-sm-2 col-form-label">{{ html()->label(__('validation.attributes.frontend.password'))->for('password') }}</label>
    <div class="col-sm-10">
        {{ html()->password('password')
        ->class('form-control')
        ->placeholder(__('validation.attributes.frontend.password'))
        ->required() }}
    </div>
  </div>
  <div class="form-group row">
    <label for="inputEmail" class="col-sm-2 col-form-label"> {{ html()->label(__('validation.attributes.frontend.password_confirmation'))->for('password_confirmation') }}</label>
    <div class="col-sm-10">

        {{ html()->password('password_confirmation')
            ->class('form-control')
            ->placeholder(__('validation.attributes.frontend.password_confirmation'))
            ->required() }}
    </div>
  </div>

    <div class="form-group row">
        <div class="offset-sm-2 col-sm-10">
           
                {{ form_submit(__('labels.general.buttons.update') . ' ' . __('validation.attributes.frontend.password')) }}

        </div><!--col-->
    </div><!--row-->
{{ html()->form()->close() }}
