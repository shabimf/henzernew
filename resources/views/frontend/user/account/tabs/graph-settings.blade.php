
{{ html()->form('PATCH', url('pricegraph/update'))->class('form-horizontal')->open() }}
<div class="form-group row">
    <label for="inputName" class="col-sm-3 col-form-label"> Price calender graph settings for</label>
    <div class="col-sm-2">
    @php 
    $selected = '1';
    if( $list_price_graph_type ) {
        $selected = $list_price_graph_type->type_id;
    }
    @endphp
    {{ Form::select('graph', [1=>'1 Month', 2=>'1 Week'] , $selected , ['class' => 'form-control']) }}
    </div>
  </div>
  <div class="form-group row">
  </div>

    <div class="form-group row">
        <div class="offset-sm-2 col-sm-10">         
            {{ form_submit(__('submit')) }}
        </div>
    </div>

{{ html()->form()->close() }}
