
{{ html()->form('PATCH', url('notification/update'))->class('form-horizontal')->open() }}
@foreach($list_notification as $key => $value)
<div class="form-group row">

    <div class="col-sm-2">
        {{ html()->checkbox('chk_noti[]')
          ->class('checks')
          ->value($key) 
          ->checked(in_array($key, json_decode($list_user_notification->notification_id) )?true:false) }}
    </div>
    <label for="inputName" class="col-sm-10 col-form-label"> {{ html()->label(__($value))->for('chk_noti') }}</label>
  </div>
  @endforeach
  
    <div class="form-group row">
        <div class="offset-sm-2 col-sm-10">
           
        {{ form_submit(__('SUBMIT')) }}

        </div><!--col-->
    </div><!--row-->
{{ html()->form()->close() }}

