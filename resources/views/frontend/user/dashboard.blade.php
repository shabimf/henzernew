@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.general.home'))

@section('content')  



<div class="row mb-3 mt-3">
    <div class="col-sm-8 no-padding flaot-left " >
    </div>
    <div class="col-sm-4" >
        @if($role != "Sourcing Team")
        <a href="{{ url('enquiry/create') }}">
          <div class="create_enq @if(Session::get('is_source') == 1) source-enq @endif"> Create Enquiry <span class="plus-icon">+</span>
          </div>
        </a> 
         @endif
        @if(Session::get('is_source') == 0)  
        <select class="form-control custom-select mode" id="mode" class="mode" name="mode">
          <option value="">Main Vendor Mode</option>
          @foreach ($subvendors as $key=>$val)
            <option value="{{$key}}">{{$val}}</option>
          @endforeach
          <option value="0">Report Mode</option>
        </select>
       
        @endif
    </div>
    <!-- <div class="col-sm-2" >

       
       
    </div> -->
</div>


<div class="row  padding">
  <div class="col-12 col-sm-6 col-md-3">
    <p class="pcls">Date From: <input type="date" name="d_from" id="d_from" value="" class="full_box_dark_sm" onchange="fetch_data()"></p>
  </div>
  <div class="col-12 col-sm-6 col-md-3">
    <p class="pcls">Date To: <input type="date" name="d_to" id="d_to" value="" class="full_box_dark_sm" onchange="fetch_data()"></p>
  </div>
  {{-- @if(isset($role) && $role == "Administrator") --}}
  <div class="col-12 col-sm-6 col-md-3 @if(isset($role) && $role != "Administrator") hide @endif ">
  <p class="pcls">From Branch: 
    {{ Form::select('branch_from', [null=>'SELECT '] + $list_branches, request('branch_from'), ['class' => 'full_box_dark_sm ','id' => 'branch_from' ,'onchange' => 'fetch_data()']) }}

  </p>
  </div>
  <div class="col-12 col-sm-6 col-md-3  @if(isset($role) && $role != "Administrator") hide @endif ">
    <p class="pcls">To Branch:
      {{ Form::select('branch_to', [null=>'SELECT '] + $list_branches, request('branch_to'), ['class' => 'full_box_dark_sm ','id' => 'branch_to','onchange' => 'fetch_data()']) }}
      </select>
    </p>
  </div>
  {{-- @endif --}}
</div>
<div class="row ">

    <div class="col padding">
        <div class="col-6"  style="float:left;padding-left:0px;">
              <div class="info-box item-boxes">
                <!-- <span class="info-box-icon bg-info elevation-1"><i class="fas fa-comment"></i></span> -->
                <img src="images/enq.png" alt="enquiries">
        
                <div class="info-box-content enquiry">
                  <a href="{{ url('enquiry?status=1') }}">
                    <span class="info-box-number enquiry_count_cls">
                      {{$enquiry_count}}
                    </span>
                  </a>
                  <span class="info-box-text">Enquiries.</span>
                </div>
              
              </div>
              <div class="info-box mb-3  item-boxes">
                  <!-- <span class="info-box-icon bg-success elevation-1"><i class="fas fa-shopping-cart"></i></span> -->
                  <img src="images/ord.png" alt="enquiries">
                  <div class="info-box-content order">
                    <a href="{{ url('order?status=1') }}">
                      <span class="info-box-number order_count_cls">
                        {{$order_count}}
                      </span>
                    </a>
                    <span class="info-box-text">Orders.</span>
                  </div>
              </div>


        </div>
        <div class="col-6" style="float:right;padding-right:0px;">
                  <div class="info-box mb-3 item-boxes">
                    <!-- <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-quote-left"></i></span>            -->
                    <img src="images/quot.png" alt="Quotation">
                    <div class="info-box-content quotation">
                      <a href="{{ url('quotation?status=1') }}">
                        <span class="info-box-number quotations_count_cls">
                          {{$quotation_count}}
                        </span>
                      </a>
                      <span class="info-box-text">Quotations.</span>
                    </div>
                  </div>
                  <div class="info-box mb-3 item-boxes">
                    <!-- <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-money-bill-wave-alt"></i></span> -->
                    <img src="images/inv.png" alt="enquiries">
                    <div class="info-box-content invoice">
                    <a href="{{ url('invoice?status=1') }}">
                    <span class="info-box-number invoice_count_cls">{{$invoice_count}}</span>
                    </a>
                    <span class="info-box-text">Invoices.</span>
                    </div>
                  </div>
      
        </div>
        
  <div class="row">
    <div class="col-md-12">
      <div class="card prgrs">
              <div class="progress-group gray">
                <span class="blue">Enquiries.</span>
                <span class="float-right">
                <a href="{{ url('enquiry?status=1') }}"><b class="enquiry_count_cls blue">0</b></a>/
                <a href="{{ url('enquiry') }}"><span  class="enquiry_total_cls totalcnt">0</span></a>
                </span>
                <div class="progress progress-sm">
                  <div class="progress-bar bg-primary enquiry_per_cls"></div>
                </div>
              </div>
              <div class="progress-group gray">
              <span class="red">Quotations.</span>
                <span class="float-right">
                <a href="{{ url('quotation?status=1') }}"><b class="quotations_count_cls red">0</b></a>/
                <a href="{{ url('quotation') }}">  <span class="quotations_total_cls totalcnt">0</span></a>
                </span>
                <div class="progress progress-sm">
                  <div class="progress-bar bg-danger quotation_per_cls" ></div>
                </div>
              </div>

              <div class="progress-group gray">
                <span class="green">Orders.</span>
                <span class="float-right">
                <a href="{{ url('order?status=1') }}"> <b class="order_count_cls green">0</b></a>/
                <a href="{{ url('order') }}">  <span class="order_total_cls totalcnt">0</span></a>
                </span>
                <div class="progress progress-sm">
                  <div class="progress-bar bg-success order_per_cls"></div>
                </div>
              </div>

              <div class="progress-group gray">
              <span class="yellow">Invoices.</span>
                <span class="float-right">
                <a href="{{ url('invoice?status=1') }}"><b class="invoice_count_cls yellow">0</b></a>/
                <a href="{{ url('invoice') }}"><span class="invoice_total_cls totalcnt">0</span></a>
                </span>
                <div class="progress progress-sm">
                  <div class="progress-bar bg-warning invoice_per_cls"></div>
                </div>
              </div>

      </div>
    </div>
  </div>
    </div>
    <div class="col padding">

       
    <div class="col-12 graph padding">
        <!-- <h5 class="graph_title" style="color: #535151 !important;">Monthly Activity Graph</h5> -->
        <div id="chartContainer" style="height: 370px; width: 100%;"></div>


    </div>
    <div class="col-12 sale_price padding">
        
        <div class="col-6 total_sale">
            <span class="date" style="color: #777777; float: none!important;">{{ date('jS F Y') }}</span>   
            <h1 class="font-wght totalsalesclas"> 0.00</h1>
            <span class="sale_title">Total Sale of this Month</span>
        </div>
        <div class="col-6" style="float:right;text-align: right;padding-top:5%;padding-bottom:5%;">
            <h1 class="yellow font-wght todaysale">  0.00</h1>
            <span  class="yellow font-size">Todays Sale</span>
            <hr color="#ffc107">
            <h1 class="font-wght prevsale" style="color:#868686;"> 0.00</h1>
            <span class="font-size" style="color:#868686;">Daily average sale of Last Month</span>
        </div>


    </div>


    </div>
      

</div>


  {{ csrf_field() }}
  {{-- <div class="row">
    <!-- Left col -->
    <div class="col-md-8">
     
      <!-- /.row -->

      <!-- TABLE: LATEST ORDERS -->
      <div class="card">
        <div class="card-header border-transparent">
          <h3 class="card-title">Latest Orders</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse">
              <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove">
              <i class="fas fa-times"></i>
            </button>
          </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body p-0">
          <div class="table-responsive">
            <table class="table dash m-0">
              <thead>
              <tr>
                <th>Order ID</th>
                <th>From</th>
                <th>To</th>
                <th>Status</th>
              </tr>
              </thead>
              <tbody>
              <tr>
                <td><a href="pages/examples/invoice.html">INV9842</a></td>
                <td>SOURCING 1</td>
                <td>VENDOR 1</td>
                <td><span class="badge badge-success">Shipped</span></td>
              </tr>
              <tr>
                <td><a href="pages/examples/invoice.html">INV1848</a></td>
                <td>SOURCING 1</td>
                <td>VENDOR 1</td>
                <td><span class="badge badge-warning">Pending</span></td>
                
              </tr>
              <tr>
                <td><a href="pages/examples/invoice.html">INV7429</a></td>
                <td>SOURCING 1</td>
                <td>VENDOR 1</td>
                <td><span class="badge badge-danger">Delivered</span></td>
                
              </tr>
              <tr>
                <td><a href="pages/examples/invoice.html">INV7429</a></td>
                <td>SOURCING 1</td>
                <td>VENDOR 1</td>
                <td><span class="badge badge-info">Processing</span></td>
                
              </tr>
              <tr>
                <td><a href="pages/examples/invoice.html">INV1848</a></td>
                <td>SOURCING 1</td>
                <td>VENDOR 1</td>
                <td><span class="badge badge-warning">Pending</span></td>
                
              </tr>
              <tr>
                <td><a href="pages/examples/invoice.html">INV7429</a></td>
                <td>SOURCING 1</td>
                <td>VENDOR 1</td>
                <td><span class="badge badge-danger">Delivered</span></td>
                
              </tr>
              <tr>
                <td><a href="pages/examples/invoice.html">INV9842</a></td>
                <td>SOURCING 1</td>
                <td>VENDOR 1</td>
                <td><span class="badge badge-success">Shipped</span></td>
                
              </tr>
              </tbody>
            </table>
          </div>
          <!-- /.table-responsive -->
        </div>
        <!-- /.card-body -->
        <div class="card-footer clearfix">
          <a href="javascript:void(0)" class="btn btn-sm btn-secondary float-right">View All Orders</a>
        </div>
        <!-- /.card-footer -->
      </div>
      <!-- /.card -->
    </div> 
    <!-- /.col -->

    <div class="col-md-4">
      
      <!-- PRODUCT LIST -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Recently Added Products</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse">
              <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove">
              <i class="fas fa-times"></i>
            </button>
          </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body p-0">
          <ul class="products-list product-list-in-card pl-2 pr-2">
            <li class="item">
              <div class="product-img">
                <img src="{{asset('images/default-150x150.png')}}" alt="Product Image" class="img-size-50">
              </div>
              <div class="product-info">
                <a href="javascript:void(0)" class="product-title">Samsung TV
                  <span class="badge badge-warning float-right">$1800</span></a>
                <span class="product-description">
                  Samsung 32" 1080p 60Hz LED Smart HDTV.
                </span>
              </div>
            </li>
            <!-- /.item -->
            <li class="item">
              <div class="product-img">
                <img src="{{asset('images/default-150x150.png')}}" alt="Product Image" class="img-size-50">
              </div>
              <div class="product-info">
                <a href="javascript:void(0)" class="product-title">Bicycle
                  <span class="badge badge-info float-right">$700</span></a>
                <span class="product-description">
                  26" Mongoose Dolomite Men's 7-speed, Navy Blue.
                </span>
              </div>
            </li>
            <!-- /.item -->
            <li class="item">
              <div class="product-img">
                <img src="{{asset('images/default-150x150.png')}}" alt="Product Image" class="img-size-50">
              </div>
              <div class="product-info">
                <a href="javascript:void(0)" class="product-title">
                  Xbox One <span class="badge badge-danger float-right">
                  $350
                </span>
                </a>
                <span class="product-description">
                  Xbox One Console Bundle with Halo Master Chief Collection.
                </span>
              </div>
            </li>
            <!-- /.item -->
            <li class="item">
              <div class="product-img">
                <img src="{{asset('images/default-150x150.png')}}" alt="Product Image" class="img-size-50">
              </div>
              <div class="product-info">
                <a href="javascript:void(0)" class="product-title">PlayStation 4
                  <span class="badge badge-success float-right">$399</span></a>
                <span class="product-description">
                  PlayStation 4 500GB Console (PS4)
                </span>
              </div>
            </li>
            <!-- /.item -->
          </ul>
        </div>
        <!-- /.card-body -->
        <div class="card-footer text-center">
          <a href="javascript:void(0)" class="uppercase">View All Products</a>
        </div>
        <!-- /.card-footer -->
      </div>
      <!-- /.card -->
    </div>
    <!-- /.col -->
  </div>--}}
  {{-- <div class="row">
    <div class="col-md-6">
      <!-- DIRECT CHAT -->
      <div class="card direct-chat direct-chat-warning">
        <div class="card-header">
          <h3 class="card-title">Direct Chat</h3>

          <div class="card-tools">
            <span title="3 New Messages" class="badge badge-warning">3</span>
            <button type="button" class="btn btn-tool" data-card-widget="collapse">
              <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" title="Contacts" data-widget="chat-pane-toggle">
              <i class="fas fa-comments"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove">
              <i class="fas fa-times"></i>
            </button>
          </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          <!-- Conversations are loaded here -->
          <div class="direct-chat-messages">
            <!-- Message. Default to the left -->
            <div class="direct-chat-msg">
              <div class="direct-chat-infos clearfix">
                <span class="direct-chat-name float-left">Alexander Pierce</span>
                <span class="direct-chat-timestamp float-right">23 Jan 2:00 pm</span>
              </div>
              <!-- /.direct-chat-infos -->
              <img class="direct-chat-img" src="{{asset('images/user1-128x128.jpg')}}" alt="message user image">
              <!-- /.direct-chat-img -->
              <div class="direct-chat-text">
                Is this template really for free? That's unbelievable!
              </div>
              <!-- /.direct-chat-text -->
            </div>
            <!-- /.direct-chat-msg -->

            <!-- Message to the right -->
            <div class="direct-chat-msg right">
              <div class="direct-chat-infos clearfix">
                <span class="direct-chat-name float-right">Sarah Bullock</span>
                <span class="direct-chat-timestamp float-left">23 Jan 2:05 pm</span>
              </div>
              <!-- /.direct-chat-infos -->
              <img class="direct-chat-img" src="{{asset('images/user1-128x128.jpg')}}" alt="message user image">
              <!-- /.direct-chat-img -->
              <div class="direct-chat-text">
                You better believe it!
              </div>
              <!-- /.direct-chat-text -->
            </div>
            <!-- /.direct-chat-msg -->

            <!-- Message. Default to the left -->
            <div class="direct-chat-msg">
              <div class="direct-chat-infos clearfix">
                <span class="direct-chat-name float-left">Alexander Pierce</span>
                <span class="direct-chat-timestamp float-right">23 Jan 5:37 pm</span>
              </div>
              <!-- /.direct-chat-infos -->
              <img class="direct-chat-img" src="{{asset('images/user1-128x128.jpg')}}" alt="message user image">
              <!-- /.direct-chat-img -->
              <div class="direct-chat-text">
                Working with AdminLTE on a great new app! Wanna join?
              </div>
              <!-- /.direct-chat-text -->
            </div>
            <!-- /.direct-chat-msg -->

            <!-- Message to the right -->
            <div class="direct-chat-msg right">
              <div class="direct-chat-infos clearfix">
                <span class="direct-chat-name float-right">Sarah Bullock</span>
                <span class="direct-chat-timestamp float-left">23 Jan 6:10 pm</span>
              </div>
              <!-- /.direct-chat-infos -->
              <img class="direct-chat-img" src="{{asset('images/user1-128x128.jpg')}}" alt="message user image">
              <!-- /.direct-chat-img -->
              <div class="direct-chat-text">
                I would love to.
              </div>
              <!-- /.direct-chat-text -->
            </div>
            <!-- /.direct-chat-msg -->

          </div>
          <!--/.direct-chat-messages-->

          <!-- Contacts are loaded here -->
          <div class="direct-chat-contacts">
            <ul class="contacts-list">
              <li>
                <a href="#">
                  <img class="contacts-list-img" src="{{asset('images/user1-128x128.jpg')}}" alt="User Avatar">

                  <div class="contacts-list-info">
                    <span class="contacts-list-name">
                      Count Dracula
                      <small class="contacts-list-date float-right">2/28/2015</small>
                    </span>
                    <span class="contacts-list-msg">How have you been? I was...</span>
                  </div>
                  <!-- /.contacts-list-info -->
                </a>
              </li>
              <!-- End Contact Item -->
              <li>
                <a href="#">
                  <img class="contacts-list-img" src="{{asset('images/user1-128x128.jpg')}}" alt="User Avatar">

                  <div class="contacts-list-info">
                    <span class="contacts-list-name">
                      Sarah Doe
                      <small class="contacts-list-date float-right">2/23/2015</small>
                    </span>
                    <span class="contacts-list-msg">I will be waiting for...</span>
                  </div>
                  <!-- /.contacts-list-info -->
                </a>
              </li>
              <!-- End Contact Item -->
              <li>
                <a href="#">
                  <img class="contacts-list-img" src="{{asset('images/user1-128x128.jpg')}}" alt="User Avatar">

                  <div class="contacts-list-info">
                    <span class="contacts-list-name">
                      Nadia Jolie
                      <small class="contacts-list-date float-right">2/20/2015</small>
                    </span>
                    <span class="contacts-list-msg">I'll call you back at...</span>
                  </div>
                  <!-- /.contacts-list-info -->
                </a>
              </li>
              <!-- End Contact Item -->
              <li>
                <a href="#">
                  <img class="contacts-list-img" src="{{asset('images/user1-128x128.jpg')}}" alt="User Avatar">

                  <div class="contacts-list-info">
                    <span class="contacts-list-name">
                      Nora S. Vans
                      <small class="contacts-list-date float-right">2/10/2015</small>
                    </span>
                    <span class="contacts-list-msg">Where is your new...</span>
                  </div>
                  <!-- /.contacts-list-info -->
                </a>
              </li>
              <!-- End Contact Item -->
              <li>
                <a href="#">
                  <img class="contacts-list-img" src="{{asset('images/user1-128x128.jpg')}}" alt="User Avatar">

                  <div class="contacts-list-info">
                    <span class="contacts-list-name">
                      John K.
                      <small class="contacts-list-date float-right">1/27/2015</small>
                    </span>
                    <span class="contacts-list-msg">Can I take a look at...</span>
                  </div>
                  <!-- /.contacts-list-info -->
                </a>
              </li>
              <!-- End Contact Item -->
              <li>
                <a href="#">
                  <img class="contacts-list-img" src="{{asset('images/user1-128x128.jpg')}}" alt="User Avatar">

                  <div class="contacts-list-info">
                    <span class="contacts-list-name">
                      Kenneth M.
                      <small class="contacts-list-date float-right">1/4/2015</small>
                    </span>
                    <span class="contacts-list-msg">Never mind I found...</span>
                  </div>
                  <!-- /.contacts-list-info -->
                </a>
              </li>
              <!-- End Contact Item -->
            </ul>
            <!-- /.contacts-list -->
          </div>
          <!-- /.direct-chat-pane -->
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
          <form action="#" method="post">
            <div class="input-group">
              <input type="text" name="message" placeholder="Type Message ..." class="form-control">
              <span class="input-group-append">
                <button type="button" class="btn btn-warning">Send</button>
              </span>
            </div>
          </form>
        </div>
        <!-- /.card-footer-->
      </div>
      <!--/.direct-chat -->
    </div>
    <!-- /.col -->

    
  </div> --}}
  
@endsection
@push('after-scripts')

<!-- <script src="js/frondend/chat.js"></script> -->
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
<script>
fetch_data();

function fetch_data() {
    var from_date = $('#d_from').val() ?? 0;
    var to_date = $('#d_to').val() ?? 0;
    var branch_from = $('#branch_from').val() ?? 0;
    var branch_to = $('#branch_to').val() ?? 0;
    var _token = $('input[name="_token"]').val();

    $.ajax({
        url: "{{ url('dashboard/fetch_data') }}",
        method: "POST",
        data: {
            from_date: from_date,
            to_date: to_date,
            branch_from: branch_from,
            branch_to: branch_to,
            _token: _token
        },
        dataType: "json",
        success: function(data) {
            var enquiry_count = ((data.enquiry_count.toString().length) == 1) ? "0" + data.enquiry_count : data.enquiry_count;
            var quotation_count = ((data.quotation_count.toString().length) == 1) ? "0" + data.quotation_count : data.quotation_count;
            var order_count = ((data.order_count.toString().length) == 1) ? "0" + data.order_count : data.order_count;
            var invoice_count = ((data.invoice_count.toString().length) == 1) ? "0" + data.invoice_count : data.invoice_count;

            var enquiry_total_count = ((data.enquiry_total_count.toString().length) == 1) ? "0" + data.enquiry_total_count : data.enquiry_total_count;
            var quotation_total_count = ((data.quotation_total_count.toString().length) == 1) ? "0" + data.quotation_total_count : data.quotation_total_count;
            var order_total_count = ((data.order_total_count.toString().length) == 1) ? "0" + data.order_total_count : data.order_total_count;
            var invoice_total_count = ((data.invoice_total_count.toString().length) == 1) ? "0" + data.invoice_total_count : data.invoice_total_count;


            $(".enquiry_count_cls").html(enquiry_count);
            $(".enquiry_total_cls").html(enquiry_total_count);
            $(".quotations_count_cls").html(quotation_count);
            $(".quotations_total_cls").html(quotation_total_count);
            $(".order_count_cls").html(order_count);
            $(".order_total_cls").html(order_total_count);
            $(".invoice_count_cls").html(invoice_count);
            $(".invoice_total_cls").html(invoice_total_count);
            $(".totalsalesclas").html(data.total_sale);
            $(".todaysale").html(data.today_sale);
            $(".prevsale").html(data.prev_sale);


            var enquiry_per = (parseInt(data.enquiry_count) / parseInt(data.enquiry_total_count) * 100);
            var quotation_per = (parseInt(data.quotation_count) / parseInt(data.quotation_total_count) * 100);
            var order_per = (parseInt(data.order_count) / parseInt(data.order_total_count) * 100);
            var invoice_per = (parseInt(data.invoice_count) / parseInt(data.invoice_total_count) * 100);

            $('.enquiry_per_cls').css('width', enquiry_per.toFixed(2) + '%');
            $('.quotation_per_cls').css('width', quotation_per.toFixed(2) + '%');
            $('.order_per_cls').css('width', order_per.toFixed(2) + '%');
            $(".invoice_per_cls").css('width', invoice_per.toFixed(2) + '%');

        }
    })
}

window.onload = function() {
    
    var dataPoints = [];
    var dataPoints1 = [];
    var dataPoints2 = [];
    var dataPoints3 = [];
    
    var chart;
    
    var graphdata1 = {
        name: "Enquiry",
        type: "spline",
        showInLegend: true,
        color: "#dc3545",
        dataPoints: dataPoints,
        };
    var graphdata2 = {
        name: "Quotation",
        type: "spline",
        showInLegend: true,
        color: "#28a745",
        dataPoints: dataPoints1
    };
    var graphdata3 = {
        name: "Order",
        type: "spline",
        showInLegend: true,
        color: "#ffc107",
        dataPoints: dataPoints2
    };
    var graphdata4 = {
        name: "Invoice",
        type: "spline",
        showInLegend: true,
        color: "#17a2b8",
        dataPoints: dataPoints3
    };
    var chart = new CanvasJS.Chart("chartContainer", {
        backgroundColor: "black",
        animationEnabled: true,
        title: {
            text: "Monthly Activity Graph",
        },
        axisX: {
            valueFormatString: "DD MMM,YY",
            labelFontColor: "white",
        },
        axisY: {
            labelFontColor: "white"
        },

        legend: {
            cursor: "pointer",
            fontSize: 16,
            itemclick: toggleDataSeries
        },
        toolTip: {
            shared: true
        },
        data: [graphdata1, graphdata2, graphdata3, graphdata4]
    });
    chart.render();
    $.getJSON("/ajax/graph?tbl=enquiry&type=json&status=1", function(data) {  
        var objects = JSON.parse(JSON.stringify(data));
         $(jQuery.parseJSON(JSON.stringify(data))).each(function() {  
         var date = this.date;
         var Arr = date.split("-");
        dataPoints.push({x: new Date(Arr[0], Arr[1], Arr[2]), y: parseInt(this.count)});
        
     });

        chart.render();
    }); 
    
    $.getJSON("/ajax/graph?tbl=quotation&type=json&status=1", function(data) {  
        var objects = JSON.parse(JSON.stringify(data));
         $(jQuery.parseJSON(JSON.stringify(data))).each(function() {  
         var date = this.date;
         var Arr = date.split("-");
        dataPoints1.push({x: new Date(Arr[0], Arr[1], Arr[2]), y: parseInt(this.count)});
        
     });

        chart.render();
    }); 
    
     $.getJSON("/ajax/graph?tbl=order&type=json&status=1", function(data) {  
        var objects = JSON.parse(JSON.stringify(data));
         $(jQuery.parseJSON(JSON.stringify(data))).each(function() {  
         var date = this.date;
         var Arr = date.split("-");
        dataPoints2.push({x: new Date(Arr[0], Arr[1], Arr[2]), y: parseInt(this.count)});
        
     });

        chart.render();
    });  
    $.getJSON("/ajax/graph?tbl=invoice&type=json&status=2", function(data) {  
        var objects = JSON.parse(JSON.stringify(data));
         $(jQuery.parseJSON(JSON.stringify(data))).each(function() {  
         var date = this.date;
         var Arr = date.split("-");
        dataPoints3.push({x: new Date(Arr[0], Arr[1], Arr[2]), y: parseInt(this.count)});
        
     });

        chart.render();
    });  

    function toggleDataSeries(e) {
        if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
            e.dataSeries.visible = false;
        } else {
            e.dataSeries.visible = true;
        }
        chart.render();
    }


}


</script>
@endpush

