@extends('frontend.layouts.app')

@section('content')
    <div class="main">
      @include('common.alert')
        <div class="row">
         
            <!-- /.col -->
            <div class="col-md-12">
              <div class="card">
                <div class="card-header p-2">
                  <ul class="nav nav-pills">
                    <li class="nav-item"><a class="nav-link active btn-tab" href="#profile" data-toggle="tab">Profile</a></li>
                    <li class="nav-item"><a class="nav-link btn-tab" href="#editprofile" data-toggle="tab">Edit Profile</a></li>
                    <li class="nav-item"><a class="nav-link btn-tab" href="#settings" data-toggle="tab">Change Password</a></li>
                    <li class="nav-item"><a class="nav-link btn-tab" href="#notification" data-toggle="tab">Notification Settings</a></li>
                    <li class="nav-item"><a class="nav-link btn-tab" href="#graph" data-toggle="tab">Graph Settings</a></li>
                  </ul>
                </div><!-- /.card-header -->
                <div class="card-body">
                  <div class="tab-content">
                    <div class="tab-pane active" id="profile">
                        @include('frontend.user.account.tabs.profile')
                      
                    </div>
                    <div class="tab-pane" id="editprofile">
                        @include('frontend.user.account.tabs.edit')
                    </div>

                    <div class="tab-pane" id="settings">
                        @include('frontend.user.account.tabs.change-password')
                     
                    </div>
                    <div class="tab-pane" id="notification">
                    @include('frontend.user.account.tabs.notification')
                     
                    </div>
                    <div class="tab-pane" id="graph">
                    @include('frontend.user.account.tabs.graph-settings')
                     
                    </div>
                
                  </div>
                  <!-- /.tab-content -->
                </div><!-- /.card-body -->
              </div>
              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
    </div><!-- row -->
@endsection