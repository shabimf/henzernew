@extends('backend.layouts.app')

@section('title',' SubVendors | Create SubVendors')

@section('breadcrumb-links')
    @include('backend.subvendors.includes.breadcrumb-links')
@endsection

@section('content')

@include('common.alert')


    @if(isset($edit_id))
        {{ Form::model($edit_id, ['route' => ['admin.subvendors.update', $edit_id], 'method' => 'patch']) }}
    @else
        {{ Form::open(['route' => 'admin.subvendors.store']) }}
    @endif

    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        SubVendors
                        @if(isset($edit_id))
                        <small class="text-muted">Edit SubVendors</small>
                        @else
                        <small class="text-muted">Create SubVendors</small>
                        @endif
                    </h4>
                </div><!--col-->
            </div><!--row-->
        
            <hr>
        
            <div class="row mt-4 mb-4">
                <div class="col">
                    <div class="form-group row">
                        {{ Form::label('vendor_id', trans('Vendor'), ['class' => 'col-md-2 from-control-label required']) }}
        
                        <div class="col-md-10">
                            {{ Form::select('vendor_id', $list_vendor, old('vendor_id',(isset($edit_vendor)) ? $edit_vendor->vendor_id:''), ['class' => 'form-control select2 status box-size', 'placeholder' => trans('--- Select Vendor ---'), 'required' => 'required']) }}
                        </div>
                        <!--col-->
                    </div>
                    <div class="form-group row">
                        {{ Form::label('company_name', trans('Company Name'), ['class' => 'col-md-2 from-control-label required']) }}
                        
                        <div class="col-md-10">
                            {{ Form::text('company_name', old('company_name',(isset($edit_vendor)) ? $edit_vendor->company_name:''),['class' => 'form-control' , 'placeholder' => trans('Company Name')]) }}
                        </div><!--col-->
                    </div><!--form-group-->
                    <div class="form-group row">
                        {{ Form::label('contact_person', trans('Contact Person'), ['class' => 'col-md-2 from-control-label required']) }}
                        
                        <div class="col-md-10">
                            {{ Form::text('contact_person', old('contact_person',(isset($edit_vendor)) ? $edit_vendor->contact_person:''),['class' => 'form-control' , 'placeholder' => trans('Contact Person Name')]) }}
                        </div><!--col-->
                    </div><!--form-group-->
                    <div class="form-group row">
                        {{ Form::label('email', trans('Email Id'), ['class' => 'col-md-2 from-control-label required']) }}
                        
                        <div class="col-md-10">
                            {{ Form::text('email', old('email',(isset($edit_vendor)) ? $edit_vendor->email:''),['class' => 'form-control' , 'placeholder' => trans('Email Id')]) }}
                        </div><!--col-->
                    </div><!--form-group-->
                    <div class="form-group row">
                        {{ Form::label('phone_number', trans('Phone'), ['class' => 'col-md-2 from-control-label required']) }}
                        
                        <div class="col-md-10">
                            {{ Form::text('phone_number', old('phone_number',(isset($edit_vendor)) ? $edit_vendor->phone_number:''),['class' => 'form-control' , 'placeholder' => trans('Phoneno')]) }}
                        </div><!--col-->
                    </div><!--form-group-->
                    <div class="form-group row">
                        {{ Form::label('address', trans('Address'), ['class' => 'col-md-2 from-control-label required']) }}
        
                        <div class="col-md-10">
                            {{ Form::textarea('address',old('address',(isset($edit_vendor)) ? $edit_vendor->address:''), ['class' => 'form-control', 'placeholder' => trans('Address')]) }}
                        </div>
                        <!--col-->
                    </div>
                    <!--form-group-->
        
                    <div class="form-group row">
                        {{ Form::label('status', trans('Status'), ['class' => 'col-md-2 from-control-label required']) }}
        
                        
                        <div class="col-md-10">
                            <div class="checkbox d-flex align-items-center">
                                <label class="switch switch-label switch-pill switch-primary mr-2" for="role-1">
                                    <input class="switch-input" type="checkbox" name="status" id="role-1" value="1" 
                                        {{ (!isset($edit_vendor->status) ||(isset($edit_vendor->status) && $edit_vendor->status == 1)) ? "checked" : "" }}>
        
                                        <span class="switch-slider" data-checked="on" data-unchecked="off"></span>
                                </label>
                            </div>
                        </div><!--col-->
                    </div><!--form-group-->
                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->
        
        <div class="card-footer">
            <div class="row">
                <div class="col">
                    <a href="{{route('admin.subvendors.index')}}" class="btn btn-danger btn-sm">Cancel</a>
                </div><!--col-->
        
                <div class="col text-right">
                    @if(isset($edit_id))
                    <input class="btn btn-success btn-sm pull-right" type="submit" value="UPDATE">
                    @else
                    <input class="btn btn-success btn-sm pull-right" type="submit" value="Create">
                    @endif
                </div><!--row-->
            </div><!--row-->
        </div>
    </div><!--card-->
    {{ Form::close() }}
@endsection
@section('pagescript')
<script type="text/javascript">
    FTX.Utils.documentReady(function() {
        FTX.vendors.edit.init("{{ config('locale.languages.' . app()->getLocale())[1] }}");
    });
</script>
@stop