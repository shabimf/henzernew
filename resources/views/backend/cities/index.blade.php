@extends('backend.layouts.app')

@section('title', app_name() . ' | Cities')

@section('breadcrumb-links')
@include('backend.cities.includes.breadcrumb-links')
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    Cities <small class="text-muted">City List</small>
                </h4>
            </div>
            <!--col-->
        </div>
        <!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table id="cities-table" class="table" data-ajax_url="{{ route("admin.cities.get") }}">
                        <thead>
                            <tr>
                                <th>Country Name</th>
                                <th>City Name</th>
                                <th>Status</th>
                                <th>Created By</th>
                                <th>Created At</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <!--col-->
        </div>
        <!--row-->

    </div>
    <!--card-body-->
</div>
<!--card-->
@endsection

@section('pagescript')
<script>
    FTX.Utils.documentReady(function() {
        FTX.Cities.list.init();
    });
</script>
@stop