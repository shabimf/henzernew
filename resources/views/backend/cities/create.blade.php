@extends('backend.layouts.app')

@section('title',' Cities | Create Cities')

@section('breadcrumb-links')
    @include('backend.cities.includes.breadcrumb-links')
@endsection

@section('content')

@include('common.alert')


    @if(isset($edit_id))
        {{ Form::model($edit_id, ['route' => ['admin.cities.update', $edit_id], 'method' => 'patch']) }}
    @else
        {{ Form::open(['route' => 'admin.cities.store']) }}
    @endif

    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        Cities
                        @if(isset($edit_id))
                        <small class="text-muted">Edit Cities</small>
                        @else
                        <small class="text-muted">Create Cities</small>
                        @endif
                    </h4>
                </div><!--col-->
            </div><!--row-->
        
            <hr>
        
            <div class="row mt-4 mb-4">
                <div class="col">
                    <div class="form-group row">
                        {{ Form::label('country_id', trans('Country Name'), ['class' => 'col-md-2 from-control-label required']) }}
        
                        <div class="col-md-10">
                            {{ Form::select('country_id', $list_country, old('country_id',(isset($edit_city)) ? $edit_city->country_id:''), ['class' => 'form-control select2 status box-size', 'placeholder' => trans('Select Country'), 'required' => 'required']) }}
                        </div>
                        <!--col-->
                    </div>
                    <div class="form-group row">
                        {{ Form::label('name', trans('City Name'), ['class' => 'col-md-2 from-control-label required']) }}
                        
                        <div class="col-md-10">
                            {{ Form::text('name', old('name',(isset($edit_city)) ? $edit_city->name:''),['class' => 'form-control' , 'placeholder' => trans('City Name'), 'required']) }}
                        </div><!--col-->
                    </div><!--form-group-->
        
                    <div class="form-group row">
                        {{ Form::label('status', trans('Status'), ['class' => 'col-md-2 from-control-label required']) }}
        
                        
                        <div class="col-md-10">
                            <div class="checkbox d-flex align-items-center">
                                <label class="switch switch-label switch-pill switch-primary mr-2" for="role-1">
                                    <input class="switch-input" type="checkbox" name="status" id="role-1" value="1" 
                                        {{ (!isset($edit_city->status) ||(isset($edit_city->status) && $edit_city->status == 1)) ? "checked" : "" }}>
        
                                        <span class="switch-slider" data-checked="on" data-unchecked="off"></span>
                                </label>
                            </div>
                        </div><!--col-->
                    </div><!--form-group-->
                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->
        
        <div class="card-footer">
            <div class="row">
                <div class="col">
                    <a href="{{route('admin.models.index')}}" class="btn btn-danger btn-sm">Cancel</a>
                </div><!--col-->
        
                <div class="col text-right">
                    @if(isset($edit_id))
                    <input class="btn btn-success btn-sm pull-right" type="submit" value="UPDATE">
                    @else
                    <input class="btn btn-success btn-sm pull-right" type="submit" value="Create">
                    @endif
                </div><!--row-->
            </div><!--row-->
        </div>
    </div><!--card-->
    {{ Form::close() }}
@endsection
@section('pagescript')
<script type="text/javascript">
    FTX.Utils.documentReady(function() {
        FTX.Cities.edit.init("{{ config('locale.languages.' . app()->getLocale())[1] }}");
    });
</script>
@stop