@extends('backend.layouts.app')

@section('title',' Agents | Create Agent')

@section('breadcrumb-links')
    @include('backend.agent.includes.breadcrumb-links')
@endsection

@section('content')

@include('common.alert')


    @if(isset($edit_id))
        {{ Form::model($edit_id, ['route' => ['admin.agent.update', $edit_id], 'method' => 'patch']) }}
    @else
        {{ Form::open(['route' => 'admin.agent.store']) }}
    @endif

    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        Agent
                        @if(isset($edit_id))
                        <small class="text-muted">Edit Agent</small>
                        @else
                        <small class="text-muted">Create Agent</small>
                        @endif
                    </h4>
                </div><!--col-->
            </div><!--row-->
        
            <hr>
        
            <div class="row mt-4 mb-4">
                <div class="col">
                    <div class="form-group row">
                        {{ Form::label('name', trans('Agent Name'), ['class' => 'col-md-2 from-control-label required']) }}
                        
                        <div class="col-md-10">
                            {{ Form::text('name', old('name',(isset($edit_agent)) ? $edit_agent->name:''),['class' => 'form-control' , 'placeholder' => trans('Agent Name')]) }}
                        </div><!--col-->
                    </div><!--form-group-->
                    <div class="form-group row">
                        {{ Form::label('email', trans('Email Id'), ['class' => 'col-md-2 from-control-label required']) }}
                        
                        <div class="col-md-10">
                            {{ Form::text('email', old('email',(isset($edit_agent)) ? $edit_agent->email:''),['class' => 'form-control' , 'placeholder' => trans('Email Id')]) }}
                        </div><!--col-->
                    </div><!--form-group-->
                    <div class="form-group row">
                        {{ Form::label('phone', trans('Phone'), ['class' => 'col-md-2 from-control-label required']) }}
                        
                        <div class="col-md-10">
                            {{ Form::text('phone', old('phone',(isset($edit_agent)) ? $edit_agent->phone:''),['class' => 'form-control' , 'placeholder' => trans('Phoneno')]) }}
                        </div><!--col-->
                    </div><!--form-group-->
                    <div class="form-group row">
                        {{ Form::label('address', trans('Address'), ['class' => 'col-md-2 from-control-label required']) }}
        
                        <div class="col-md-10">
                            {{ Form::textarea('address',old('address',(isset($edit_agent)) ? $edit_agent->address:''), ['class' => 'form-control', 'placeholder' => trans('Address')]) }}
                        </div>
                        <!--col-->
                    </div>
                    <!--form-group-->
        
                    <div class="form-group row">
                        {{ Form::label('status', trans('Status'), ['class' => 'col-md-2 from-control-label required']) }}
        
                        
                        <div class="col-md-10">
                            <div class="checkbox d-flex align-items-center">
                                <label class="switch switch-label switch-pill switch-primary mr-2" for="role-1">
                                    <input class="switch-input" type="checkbox" name="status" id="role-1" value="1" 
                                        {{ (!isset($edit_agent->status) ||(isset($edit_agent->status) && $edit_agent->status == 1)) ? "checked" : "" }}>
        
                                        <span class="switch-slider" data-checked="on" data-unchecked="off"></span>
                                </label>
                            </div>
                        </div><!--col-->
                    </div><!--form-group-->
                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->
        
        <div class="card-footer">
            <div class="row">
                <div class="col">
                    <a href="{{route('admin.agent.index')}}" class="btn btn-danger btn-sm">Cancel</a>
                </div><!--col-->
        
                <div class="col text-right">
                    @if(isset($edit_id))
                    <input class="btn btn-success btn-sm pull-right" type="submit" value="UPDATE">
                    @else
                    <input class="btn btn-success btn-sm pull-right" type="submit" value="Create">
                    @endif
                </div><!--row-->
            </div><!--row-->
        </div>
    </div><!--card-->
    {{ Form::close() }}
@endsection
