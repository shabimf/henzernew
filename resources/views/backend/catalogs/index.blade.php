@extends('backend.layouts.app')

@section('title', app_name() . ' | Catalogs')

@section('breadcrumb-links')
@include('backend.catalogs.includes.breadcrumb-links')
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    Catalogs <small class="text-muted">Catalogs List</small>
                </h4>
            </div>
            <!--col-->
        </div>
        <!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table id="series-table" class="table" data-ajax_url="{{ route("admin.catalogs.get") }}">
                        <thead>
                            <tr>
                                <th>Brand Name</th>
                                <th>Model Name</th>
                                <th>Series Name</th>
                                <th>Product Name</th>
                                <th>Focus Code</th>
                                <th>Status</th>
                                <th>Created By</th>
                                <th>Created At</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <!--col-->
        </div>
        <!--row-->

    </div>
    <!--card-body-->
</div>
<!--card-->
@endsection

@section('pagescript')
<script>
    FTX.Utils.documentReady(function() {
        FTX.Series.list.init();
    });
</script>
@stop