@extends('backend.layouts.app')

@section('title',' Catalogs | Create Catalogs')

@section('breadcrumb-links')
    @include('backend.catalogs.includes.breadcrumb-links')
@endsection

@section('content')

@include('common.alert')


    @if(isset($edit_id))
        {{ Form::model($edit_id, ['route' => ['admin.catalogs.update', $edit_id], 'method' => 'patch','files' => true]) }}
    @else
        {{ Form::open(['route' => 'admin.catalogs.store', 'files' => true]) }}
    @endif

    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        Catalogs
                        @if(isset($edit_id))
                        <small class="text-muted">Edit Catalogs</small>
                        @else
                        <small class="text-muted">Create Catalogs</small>
                        @endif
                    </h4>
                </div><!--col-->
            </div><!--row-->
        
            <hr>

            <div class="row mt-4 mb-4">
                <div class="col">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#overview" role="tab" aria-controls="overview" aria-expanded="true">General</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#overview1" role="tab" aria-controls="overview1" aria-expanded="true">Specification</a>
                        </li>
                    </ul>
    
                    <div class="tab-content">
                        <div class="tab-pane active" id="overview" role="tabpanel" aria-expanded="true">
                            <div class="row mt-4 mb-4">
                                <div class="col">
                                    <div class="form-group row">
                                        {{ Form::label('brand_id', trans('Brand'), ['class' => 'col-md-2 from-control-label required']) }}
                        
                                        <div class="col-md-10">
                                            {{ Form::select('brand_id', $list_brand, old('brand_id',(isset($edit_series)) ? $edit_series->brand_id:''), ['class' => 'form-control select2 status box-size', 'placeholder' => trans('--- Select Brand ---'), 'required' => 'required']) }}
                                        </div>
                                        <!--col-->
                                    </div>
                                    <div class="form-group row">
                                        {{ Form::label('model_id', trans('Model'), ['class' => 'col-md-2 from-control-label  required']) }}                   
                        
                                        <div class="col-md-10">
                                            {{ Form::select('model_id', $list_model, old('model_id',(isset($edit_series)) ? $edit_series->model_id:''), ['class' => 'form-control select2 status box-size', 'placeholder' => trans('--- Select Model ---'), 'required' => 'required']) }}
                                        </div>
                                        <!--col-->
                                    </div>
                                    <div class="form-group row">
                                        {{ Form::label('series_id', trans('Series'), ['class' => 'col-md-2 from-control-label  required']) }}                   
                        
                                        <div class="col-md-10">
                                            {{ Form::select('series_id',$list_series, old('series_id',(isset($edit_series)) ? $edit_series->series_id:''), ['class' => 'form-control select2 status box-size', 'placeholder' => trans('--- Select Series ---'), 'required' => 'required']) }}
                                        </div>
                                        <!--col-->
                                    </div>
                                    <div class="form-group row">
                                        {{ Form::label('name', trans('Product Name'), ['class' => 'col-md-2 from-control-label required']) }}
                                        
                                        <div class="col-md-10">
                                            {{ Form::text('name', old('name',(isset($edit_series)) ? $edit_series->name:''),['class' => 'form-control' , 'placeholder' => trans('Product Name'), 'required']) }}
                                        </div><!--col-->
                                    </div><!--form-group-->
                                    <div class="form-group row">
                                        {{ Form::label('focus_code', trans('Focus Code'), ['class' => 'col-md-2 from-control-label required']) }}
                                        
                                        <div class="col-md-10">
                                            {{ Form::text('focus_code', old('focus_code',(isset($edit_series)) ? $edit_series->focus_code:''),['class' => 'form-control' , 'placeholder' => trans('Focus Code'), 'required']) }}
                                        </div><!--col-->
                                    </div><!--form-group-->
                                    <div class="form-group row">
                                        {{ Form::label('part_no', trans('Part Number'), ['class' => 'col-md-2 from-control-label required']) }}
                                        
                                        <div class="col-md-10">
                                            {{ Form::text('part_no', old('part_no',(isset($edit_series)) ? $edit_series->part_no:''),['class' => 'form-control' , 'placeholder' => trans('Part Number'), 'required']) }}
                                        </div><!--col-->
                                    </div><!--form-group-->
                
                                    <!--form-group-->
                
                                    <div class="form-group row">
                                        {{ Form::label('image', trans('Image'), ['class' => 'col-md-2 from-control-label required']) }}
                                        
                                        @if(!empty($edit_series->image))
                                        <div class="col-lg-1">
                                            <img src="{{ asset('storage'.$edit_series->image) }}" height="80" width="80">
                                        </div>
                                        <div class="col-lg-5">
                                            {{ Form::file('image', ['id' => 'image']) }}
                                        </div>
                                        @else
                                        <div class="col-lg-5">
                                            {{ Form::file('image', ['id' => 'image']) }}
                                        </div>
                                        @endif
                                    </div>
                                    <!--form-group-->
                
                                    <div class="form-group row">
                                        {{ Form::label('description', trans('Description'), ['class' => 'col-md-2 from-control-label required']) }}
                
                                        <div class="col-md-10">
                                            {{ Form::textarea('description',  old('description',(isset($edit_series)) ? $edit_series->description:''), ['class' => 'form-control', 'placeholder' => trans('Description')]) }}
                                        </div>
                                        <!--col-->
                                    </div>
                
                                    <div class="form-group row">
                                        {{ Form::label('status', trans('Status'), ['class' => 'col-md-2 from-control-label required']) }}
                        
                                        
                                        <div class="col-md-10">
                                            <div class="checkbox d-flex align-items-center">
                                                <label class="switch switch-label switch-pill switch-primary mr-2" for="role-1">
                                                    <input class="switch-input" type="checkbox" name="status" id="role-1" value="1" 
                                                        {{ (!isset($edit_series->status) ||(isset($edit_series->status) && $edit_series->status == 1)) ? "checked" : "" }}>
                        
                                                        <span class="switch-slider" data-checked="on" data-unchecked="off"></span>
                                                </label>
                                            </div>
                                        </div><!--col-->
                                    </div><!--form-group-->
                                    
                                    <!--form-group-->
                                </div><!--col-->
                            </div>
                        </div><!--tab-->
                        <div class="tab-pane" id="overview1" role="tabpanel">
                            <div class="row">
                                
                                <div class="col">
                                    <div class="text-right" role="toolbar" aria-label="@lang('labels.general.toolbar_btn_groups')" style="margin-bottom: 10px;">
                                        <button type="button" name="add" data-toggle="tooltip" title="@lang('labels.general.create_new')" id="add" class="btn btn-success ml-1"><i class="fas fa-plus-circle"></i> Add Attribute</button>
                                    </div>
                                    
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Attribute</th>
                                                <th>Text</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody id="dynamic_field">
                                            @if(isset($list_cat_attr))
                                                @foreach($list_cat_attr as $key => $value)
                                                <tr id="row{{$key}}">
                                                    <td>{{ Form::select('attr_id[]', $list_attribute, old('attr_id[]',(isset($list_cat_attr)) ? $value['attr_id']:''), ['class' => 'form-control select2 status box-size', 'placeholder' => trans('--- Select Attribute ---'), 'required' => 'required']) }}</td>
                                                    <td>
                                                        {{ Form::text('attr_dec[]', old('attr_dec[]',(isset($list_cat_attr)) ? $value['attr_dec']:''),['class' => 'form-control' , 'placeholder' => trans('Part Number'), 'required']) }}
                                                    </td>
                                                    <td>
                                                        <button type="button" name="remove" id="{{$key}}" class="btn btn-danger btn_remove">X</button>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            @endif
                                        </tbody>
                                    </table> 
                                </div>
                            </div>
                        </div><!--tab-->
                    </div><!--tab-content-->
                </div><!--col-->
            </div><!--row-->
        
            <!--row-->
        </div><!--card-body-->
        {{ Form::hidden('cat_att_id', old('cat_att_id',(isset($key)) ? $key+1 :'0'),['class' => 'form-control' , 'placeholder' => trans('Part Number'), 'required', 'id' =>'cat_att_id']) }}

        <div class="card-footer">
            <div class="row">
                <div class="col">
                    <a href="{{route('admin.catalogs.index')}}" class="btn btn-danger btn-sm">Cancel</a>
                </div><!--col-->
        
                <div class="col text-right">
                    @if(isset($edit_id))
                    <input class="btn btn-success btn-sm pull-right" type="submit" value="UPDATE">
                    @else
                    <input class="btn btn-success btn-sm pull-right" type="submit" value="Create">
                    @endif
                </div><!--row-->
            </div><!--row-->
        </div>
    </div><!--card-->
    {{ Form::close() }}
@endsection
@section('pagescript')
<script type="text/javascript">
    FTX.Utils.documentReady(function() {
        var url = "{{asset('admin/ajax')}}"; 
        FTX.Series.edit.init("{{ config('locale.languages.' . app()->getLocale())[1] }}", url);
    });
    $(document).ready(function(){
        var i= $('#cat_att_id').val();  
        $('#add').click(function(){  
            i++;   
            $('#dynamic_field').append('<tr id="row'+i+'" class="dynamic-added"><td width="50%">{{ Form::select('attr_id[]', $list_attribute, null , ['class' => 'form-control select2 status box-size', 'placeholder' => trans('--- Select Attribute ---'), 'required' => 'required']) }}</td><td><input type="text" name="attr_dec[]"  placeholder="Enter your description" class="form-control" /></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></tr>');  
            $('.select2').select2();
        });
        $(document).on('click', '.btn_remove', function(){  
           var button_id = $(this).attr("id");   
           $('#row'+button_id+'').remove();  
        }); 
    });  
</script>
@stop