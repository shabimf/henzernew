<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            <li class="nav-title">
                @lang('menus.backend.sidebar.general')
            </li>
            <li class="nav-item">
                <a class="nav-link {{
                    active_class(Route::is('admin/dashboard'))
                }}" href="{{ route('admin.dashboard') }}">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                    @lang('menus.backend.sidebar.dashboard')
                </a>
            </li>

            @if ($logged_in_user->isAdmin())
                <li class="nav-title">
                    @lang('menus.backend.sidebar.system')
                </li>

                <li class="nav-item nav-dropdown {{
                    active_class(Route::is('admin/auth*'), 'open')
                }}">
                    <a class="nav-link nav-dropdown-toggle {{
                        active_class(Route::is('admin/auth*'))
                    }}" href="#">
                        <i class="nav-icon far fa-user"></i>
                        @lang('menus.backend.access.title')

                        @if ($pending_approval > 0)
                            <span class="badge badge-danger">{{ $pending_approval }}</span>
                        @endif
                    </a>

                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a class="nav-link {{
                                active_class(Route::is('admin/auth/user*'))
                            }}" href="{{ route('admin.auth.user.index') }}">
                                @lang('labels.backend.access.users.management')

                                @if ($pending_approval > 0)
                                    <span class="badge badge-danger">{{ $pending_approval }}</span>
                                @endif
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{
                                active_class(Route::is('admin/auth/role*'))
                            }}" href="{{ route('admin.auth.role.index') }}">
                                @lang('labels.backend.access.roles.management')
                            </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link {{
                                active_class(Route::is('admin/auth/permission*'))
                            }}" href="{{ route('admin.auth.permission.index') }}">
                                @lang('labels.backend.access.permissions.management')
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="divider"></li>

                <li class="nav-item nav-dropdown {{ active_class(Route::is('admin/catalogs'), 'open') }}">
                    <a class="nav-link nav-dropdown-toggle {{ active_class(Route::is('admin/catalogs*')) }}" href="#">
                        <i class="nav-icon fas fa-tags"></i> @lang('menus.backend.sidebar.Catalogs')
                    </a>

                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a class="nav-link {{ active_class(Route::is('admin/catalogs/brands*')) }}" href="{{ route('admin.brands.index') }}">
                                @lang('labels.backend.access.brand-category.management')
                            </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link {{ active_class(Route::is('admin/catalogs/models*'))}}" href="{{ route('admin.models.index') }}">
                                @lang('labels.backend.access.model-tag.management')
                            </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link {{ active_class(Route::is('admin/catalogs/series*')) }}" 
                                href="{{ route('admin.series.index') }}">
                                @lang('labels.backend.access.series.management')
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ active_class(Route::is('admin/catalogs/attributes*')) }}" 
                                href="{{ route('admin.attributes.index') }}">
                                @lang('labels.backend.access.attribute.management')
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ active_class(Route::is('admin/catalogs*')) }}" 
                                href="{{ route('admin.catalogs.index') }}">
                                @lang('labels.backend.access.catalogs.management')
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="divider"></li>

                <li class="nav-item nav-dropdown {{ active_class(Route::is('admin/shipping'), 'open') }}">
                    <a class="nav-link nav-dropdown-toggle {{ active_class(Route::is('admin/shipping*')) }}" href="#">
                        <i class="nav-icon fas fa-ship"></i> @lang('menus.backend.sidebar.shipping')
                    </a>

                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a class="nav-link {{ active_class(Route::is('admin/shipping/cargo*'))}}" href="{{ route('admin.cargo.index') }}">
                                @lang('menus.backend.setting.cargo')
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ active_class(Route::is('admin/shipping/agent*'))}}" href="{{ route('admin.agent.index') }}">
                                @lang('menus.backend.setting.agent')
                            </a>
                        </li>

                        
                    </ul>
                </li>

                <li class="divider"></li>

                <li class="nav-item nav-dropdown {{ active_class(Route::is('admin/settings*'), 'open')}}">
                    <a class="nav-link nav-dropdown-toggle {{ active_class(Route::is('admin/settings*')) }}" href="#">
                        <i class="nav-icon fas fa-cogs"></i> @lang('menus.backend.setting.main')
                    </a>

                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a class="nav-link {{
                            active_class(Route::is('admin/settings/countries*'))
                        }}" href="{{ route('admin.countries.index') }}">
                                @lang('menus.backend.setting.country')
                            </a>
                        </li>
                        
                        <li class="nav-item">
                            <a class="nav-link {{
                            active_class(Route::is('admin/settings/cities*'))
                        }}" href="{{ route('admin.cities.index') }}">
                                @lang('menus.backend.setting.city')
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ active_class(Route::is('admin/settings/branches*'))}}" href="{{ route('admin.branches.index') }}">
                                @lang('menus.backend.setting.branch')
                            </a>
                        </li>
                        
                        {{-- <li class="nav-item">
                            <a class="nav-link {{ active_class(Route::is('admin/settings/currencies*'))}}" href="{{ route('admin.currencies.index') }}">
                                @lang('menus.backend.setting.currencies')
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ active_class(Route::is('admin/settings/vendor*'))}}" href="{{ route('admin.vendors.index') }}">
                                @lang('menus.backend.setting.vendor')
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ active_class(Route::is('admin/settings/subvendors*')) }}" href="{{ route('admin.subvendors.index') }}">
                                @lang('menus.backend.setting.subvendor')
                            </a>
                        </li> --}}
                    </ul>
                </li>
            @endif
        </ul>
    </nav>

    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div><!--sidebar-->
