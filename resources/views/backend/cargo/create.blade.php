@extends('backend.layouts.app')

@section('title', __('labels.backend.access.cargo.management') . ' | ' . __('labels.backend.access.cargo.create'))

@section('breadcrumb-links')
    @include('backend.cargo.includes.breadcrumb-links')
@endsection

@section('content')

@include('common.alert')

    @if(isset($edit_id))
        {{ Form::model($edit_id, ['route' => ['admin.cargo.update', $edit_id], 'method' => 'patch']) }}
    @else
        {{ Form::open(['route' => 'admin.cargo.store']) }}
    @endif

    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        {{ __('labels.backend.access.cargo.management') }}
                        @if(isset($edit_id))
                        <small class="text-muted">{{ __('labels.backend.access.cargo.edit') }}</small>
                        @else
                        <small class="text-muted">{{ __('labels.backend.access.cargo.create') }}</small>
                        @endif
                    </h4>
                </div><!--col-->
            </div><!--row-->
        
            <hr>
        
            <div class="row mt-4 mb-4">
                
                <div class="col">
                    <div class="form-group row">
                        {{ Form::label('name', trans('validation.attributes.backend.access.cargo.name'), ['class' => 'col-md-2 from-control-label required']) }}
                        
                        <div class="col-md-10">
                            {{ Form::text('name', old('name',(isset($edit_cargo)) ? $edit_cargo->name:''),['class' => 'form-control' , 'placeholder' => trans('validation.attributes.backend.access.cargo.name')]) }}
                        </div><!--col-->
                    </div><!--form-group-->
                    
                    
        
                    <div class="form-group row">
                        {{ Form::label('status', trans('validation.attributes.backend.access.cargo.status'), ['class' => 'col-md-2 from-control-label required']) }}
        
                        
                        <div class="col-md-10">
                            <div class="checkbox d-flex align-items-center">
                                <label class="switch switch-label switch-pill switch-primary mr-2" for="role-1">
                                    <input class="switch-input" type="checkbox" name="status" id="role-1" value="1" 
                                        {{ (!isset($edit_cargo->status) ||(isset($edit_cargo->status) && $edit_cargo->status == 1)) ? "checked" : "" }}>
        
                                        <span class="switch-slider" data-checked="on" data-unchecked="off"></span>
                                </label>
                            </div>
                        </div><!--col-->
                    </div><!--form-group-->
                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->
        
        <div class="card-footer">
            <div class="row">
                <div class="col">
                    <a href="{{route('admin.cargo.index')}}" class="btn btn-danger btn-sm">Cancel</a>
                </div><!--col-->
        
                <div class="col text-right">
                    @if(isset($edit_id))
                    <input class="btn btn-success btn-sm pull-right" type="submit" value="UPDATE">
                    @else
                    <input class="btn btn-success btn-sm pull-right" type="submit" value="Create">
                    @endif
                </div><!--row-->
            </div><!--row-->
        </div>
    </div><!--card-->
    {{ Form::close() }}
@endsection