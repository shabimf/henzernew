@extends('backend.layouts.app')

@section('title', __('labels.backend.access.roles.management') . ' | ' . __('labels.backend.access.roles.create'))

@section('breadcrumb-links')
@include('backend.auth.roles.includes.breadcrumb-links')
@endsection

@section('content')
@include('common.alert')
@if(isset($edit_id))
{{ Form::model($edit_id, ['route' => ['admin.auth.role.update', $role], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH', 'id' => 'edit-role']) }}

@else
    {{ Form::open(['route' => 'admin.auth.role.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'id' => 'create-role']) }}
@endif
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    @lang('labels.backend.access.roles.management')
                    <small class="text-muted">
                        @if(isset($edit_id)) 
                        @lang('labels.backend.access.roles.edit')
                        @else 
                        @lang('labels.backend.access.roles.create')
                        @endif
                    </small>
                </h4>
            </div>
            <!--col-->
        </div>
        <!--row-->

        <hr>

        <div class="row mt-4">
            <div class="col">
                <div class="form-group row">
                    {{ Form::label('name', __('validation.attributes.backend.access.roles.name'), [ 'class'=>'col-md-2 form-control-label']) }}

                    <div class="col-md-10">
                        {{ Form::text('name', old('name',(isset($role)) ? $role->name:''), ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.access.roles.name'), 'required' => 'required']) }}
                    </div>
                    <!--col-->
                </div>
                <!--form-group-->
                
                
                <div class="form-group row">
                    {{ Form::label('associated_permissions', __('validation.attributes.backend.access.roles.associated_permissions'), [ 'class'=>'col-md-2 form-control-label']) }}
                    
                    <div class="col-md-10 search-permission" style="min-height: unset;">
                        {{ Form::select('associated_permissions', ['all' => 'All', 'custom' => 'Custom'],  old('associated_permissions',(isset($role) && $role->all == "0") ? 'custom':'all'), ['class' => 'form-control select2']) }}

                        <div id="available-permissions" style="margin-top: 20px;" class="hidden">
                            <div>
                                <input type="text" class="form-control search-button" placeholder="Search..." />
                            </div>
                            <div class="get-available-permissions">
                                @if (isset($permissions))
                                @foreach ($permissions as $k=>$v)
                                <div>
                                    <input type="checkbox" name="permissions[{{ $k }}]" value="{{ $k }}" id="perm_{{ $k }}" {{ is_array(old('permissions')) && in_array($k, old('permissions')) ? 'checked' : (in_array($k, $rolePermissions) ? 'checked' : '')}} /> <label style="margin-left:20px;" for="perm_{{ $k }}">{{ $v }}</label>
                                </div>
                                @endforeach
                                @else
                                <p>There are no available permissions.</p>
                                @endif
                                <!--col-lg-6-->
                            </div>
                            <!--row-->
                        </div>
                        <!--available permissions-->
                    </div>
                    <!--col-->
                </div>
                <!--form-group-->

                <div class="form-group row">
                    {{ Form::label('sort', trans('validation.attributes.backend.access.roles.sort'), ['class' => 'col-md-2 control-label']) }}

                    <div class="col-md-10">
                        {{ Form::text('sort', old('sort',(isset($role)) ? $role->sort:$roleCount+1), ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.access.roles.sort')]) }}
                    </div>
                    <!--col-lg-10-->
                </div>
                <!--form control-->
                @if(!isset($role))

                <div class="form-group row">
                    {{ Form::label('status', trans('validation.attributes.backend.access.users.active'), ['class' => 'col-md-2 control-label']) }}

                    <div class="col-md-10">
                        {{ Form::checkbox('status', 1, true) }}
                    </div>
                    <!--col-lg-3-->
                </div>
                @endif
                <!--form control-->
            </div>
            <!--col-->
        </div>
        <!--row-->
    </div>
    <!--card-body-->

    @include('backend.components.footer-buttons', [ 'cancelRoute' => 'admin.auth.role.index' ])
</div>
<!--card-->
{{ Form::close() }}
@endsection

@section('pagescript')
<script>
    FTX.Utils.documentReady(function() {
        FTX.Roles.edit.init();
    });
</script>
@endsection