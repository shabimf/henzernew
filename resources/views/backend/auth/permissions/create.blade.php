@extends ('backend.layouts.app')
@section ('title', trans('labels.backend.access.permissions.management') . ' | ' . trans('labels.backend.access.permissions.create'))
@section('breadcrumb-links')
@include('backend.auth.permissions.includes.breadcrumb-links')
@endsection
@section('content')
@include('common.alert')
@if(isset($edit_id))
{{ Form::model($permission, ['route' => ['admin.auth.permission.update', $edit_id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH', 'id' => 'create-permission']) }}
@else
{{ Form::open(['route' => 'admin.auth.permission.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'id' => 'create-permission']) }}
@endif

    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        {{ __('labels.backend.access.permissions.management') }}
                        @if(isset($edit_id))
                        <small>{{ trans('labels.backend.access.permissions.edit') }}
                        @else
                        <small class="text-muted">{{ __('labels.backend.access.permissions.create') }}
                        @endif
                        </small>
                    </h4>
                </div><!--col-->
            </div><!--row-->
            <hr>
        
            <div class="row mt-4 mb-4">
                <div class="col">
                    <div class="form-group row">
                        {{ Form::label('name', trans('validation.attributes.backend.access.permissions.name'), ['class' => 'col-lg-2 control-label required']) }}
                        
                        <div class="col-md-10">
                            {{ Form::text('name', null, ['class' => 'form-control box-size', 'placeholder' => trans('validation.attributes.backend.access.permissions.name'), 'required' => 'required']) }}
                        </div><!--col-->
                    </div><!--form-group-->
        
                    <div class="form-group row">
                        {{ Form::label('display_name', trans('validation.attributes.backend.access.permissions.display_name'), ['class' => 'col-lg-2 control-label required']) }}
                        
                        <div class="col-md-10">
                            {{ Form::text('display_name', null,['class' => 'form-control box-size', 'placeholder' => trans('validation.attributes.backend.access.permissions.display_name'), 'required' => 'required']) }}
                        </div><!--col-->
                    </div><!--form-group-->
        
                    <div class="form-group row">
                        {{ Form::label('sort', trans('labels.backend.access.permissions.table.sort'), ['class' => 'col-lg-2 control-label']) }}
                        
                        <div class="col-md-10">
                            {{ Form::text('sort', null, ['class' => 'form-control box-size', 'placeholder' => trans('labels.backend.access.permissions.table.sort')]) }}
                        </div><!--col-->
                    </div><!--form-group-->
        
                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->
        <div class="card-footer">
            <div class="row">
                <div class="col">
                    <a href="{{route('admin.auth.permission.index')}}" class="btn btn-danger btn-sm">Cancel</a>
                </div><!--col-->
        
                <div class="col text-right">
                    @if(isset($edit_id))
                    <input class="btn btn-success btn-sm pull-right" type="submit" value="UPDATE">
                    @else
                    <input class="btn btn-success btn-sm pull-right" type="submit" value="Create">
                    @endif
                </div><!--row-->
            </div><!--row-->
        </div>
    </div><!--card-->
    
    {{ Form::close() }}
@endsection
