@extends('backend.layouts.app')

@section('title', __('labels.backend.access.country.management') . ' | ' . __('labels.backend.access.country.create'))

@section('breadcrumb-links')
    @include('backend.countries.includes.breadcrumb-links')
@endsection

@section('content')

@include('common.alert')

{{-- {{ Form::open(['route' => 'admin.countries.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'id' => 'create-permission', 'files' => true]) }} --}}


        @if(isset($edit_id))
            {{ Form::model($edit_id, ['route' => ['admin.countries.update', $edit_id], 'method' => 'patch']) }}
        @else
            {{ Form::open(['route' => 'admin.countries.store']) }}
        @endif

    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        {{ __('labels.backend.access.country.management') }}
                        @if(isset($edit_id))
                        <small class="text-muted">{{ __('labels.backend.access.country.edit') }}</small>
                        @else
                        <small class="text-muted">{{ __('labels.backend.access.country.create') }}</small>
                        @endif
                    </h4>
                </div><!--col-->
            </div><!--row-->
        
            <hr>
        
            <div class="row mt-4 mb-4">
                
                <div class="col">
                    <div class="form-group row">
                        {{ Form::label('name', trans('validation.attributes.backend.access.countries.name'), ['class' => 'col-md-2 from-control-label required']) }}
                        
                        <div class="col-md-10">
                            {{ Form::text('name', old('name',(isset($edit_country)) ? $edit_country->name:''),['class' => 'form-control' , 'placeholder' => trans('validation.attributes.backend.access.countries.name'), 'required' => 'required']) }}
                        </div><!--col-->
                    </div><!--form-group-->
                    
                    <div class="form-group row">
                        {{ Form::label('currency_id', trans('Currency'), ['class' => 'col-md-2 from-control-label required']) }}
        
                        <div class="col-md-10">
                            {{ Form::select('currency_id', $list_currency, old('currency_id',(isset($edit_country)) ? $edit_country->currency_id:''), ['class' => 'form-control select2 status box-size', 'placeholder' => trans('--- Select Currency ---'), 'required' => 'required']) }}
                        </div>
                        <!--col-->
                    </div>
                    <div class="form-group row">
                        {{ Form::label('rate', trans('validation.attributes.backend.access.countries.rate'), ['class' => 'col-md-2 from-control-label required']) }}
                        
                        <div class="col-md-10">
                            {{ Form::text('exchange_rate', old('name',(isset($edit_country)) ? $edit_country->exchange_rate:''),['class' => 'form-control' , 'placeholder' => trans('validation.attributes.backend.access.countries.rate'), 'required' => 'required']) }}
                        </div><!--col-->
                    </div><!--form-group-->
                    <div class="form-group row">
                        {{ Form::label('warranty', trans('Warranty Year'), ['class' => 'col-md-2 from-control-label required']) }}
        
                        <div class="col-md-10">

                            {{ Form::select('warranty',['1' => '1 Year', '2' => '2 Year', '3' => '3 Year', '4' => '4 Year', '5' => '5 Year', '6' => '6 Year', '7' => '7 Year'], old('warranty',(isset($edit_country)) ? $edit_country->warranty:''), ['class' => 'form-control select2 status box-size', 'placeholder' => trans('--- Select Warranty Year ---'), 'required' => 'required']) }}
                        </div>
                        <!--col-->
                    </div>
        
                    <div class="form-group row">
                        {{ Form::label('status', trans('validation.attributes.backend.access.countries.status'), ['class' => 'col-md-2 from-control-label required']) }}
        
                        
                        <div class="col-md-10">
                            <div class="checkbox d-flex align-items-center">
                                <label class="switch switch-label switch-pill switch-primary mr-2" for="role-1">
                                    <input class="switch-input" type="checkbox" name="status" id="role-1" value="1" 
                                        {{ (!isset($edit_country->status) ||(isset($edit_country->status) && $edit_country->status == 1)) ? "checked" : "" }}>
        
                                        <span class="switch-slider" data-checked="on" data-unchecked="off"></span>
                                </label>
                            </div>
                        </div><!--col-->
                    </div><!--form-group-->
                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->
        
        <div class="card-footer">
            <div class="row">
                <div class="col">
                    <a href="{{route('admin.countries.index')}}" class="btn btn-danger btn-sm">Cancel</a>
                </div><!--col-->
        
                <div class="col text-right">
                    @if(isset($edit_id))
                    <input class="btn btn-success btn-sm pull-right" type="submit" value="UPDATE">
                    @else
                    <input class="btn btn-success btn-sm pull-right" type="submit" value="Create">
                    @endif
                </div><!--row-->
            </div><!--row-->
        </div>
    </div><!--card-->
    {{ Form::close() }}
@endsection