@extends('backend.layouts.app')

@section('title', __('labels.backend.access.country.management') . ' | ' . __('labels.backend.access.country.create'))

@section('breadcrumb-links')
    @include('backend.currencies.includes.breadcrumb-links')
@endsection

@section('content')

@include('common.alert')

    @if(isset($edit_id))
        {{ Form::model($edit_id, ['route' => ['admin.currencies.update', $edit_id], 'method' => 'patch']) }}
    @else
        {{ Form::open(['route' => 'admin.currencies.store']) }}
    @endif

    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        {{ __('labels.backend.access.currencies.management') }}
                        @if(isset($edit_id))
                        <small class="text-muted">{{ __('labels.backend.access.currencies.edit') }}</small>
                        @else
                        <small class="text-muted">{{ __('labels.backend.access.currencies.create') }}</small>
                        @endif
                    </h4>
                </div><!--col-->
            </div><!--row-->
        
            <hr>
        
            <div class="row mt-4 mb-4">
                
                <div class="col">
                    <div class="form-group row">
                        {{ Form::label('currency_id', trans('validation.attributes.backend.access.currencies.currency_id'), ['class' => 'col-md-2 from-control-label required']) }}
        
                        <div class="col-md-10">
                            {{ Form::select('currency_id', $list_currency, old('currency_id',(isset($edit_country)) ? $edit_country->currency_id:''), ['class' => 'form-control select2 status box-size', 'placeholder' => trans('--- Select Currency ---'), 'required' => 'required']) }}
                        </div>
                        <!--col-->
                    </div>
                    <div class="form-group row">
                        {{ Form::label('name', trans('validation.attributes.backend.access.currencies.name'), ['class' => 'col-md-2 from-control-label required']) }}
                        
                        <div class="col-md-10">
                            {{ Form::text('name', old('name',(isset($edit_country)) ? $edit_country->name:''),['class' => 'form-control' , 'placeholder' => trans('validation.attributes.backend.access.currencies.name')]) }}
                        </div><!--col-->
                    </div><!--form-group-->
                    
                    <div class="form-group row">
                        {{ Form::label('symbol', trans('validation.attributes.backend.access.currencies.symbol'), ['class' => 'col-md-2 from-control-label required']) }}
                        
                        <div class="col-md-10">
                            {{ Form::text('symbol', old('symbol',(isset($edit_country)) ? $edit_country->name:''),['class' => 'form-control' , 'placeholder' => trans('validation.attributes.backend.access.currencies.symbol')]) }}
                        </div><!--col-->
                    </div><!--form-group-->
        
                    <div class="form-group row">
                        {{ Form::label('status', trans('validation.attributes.backend.access.countries.status'), ['class' => 'col-md-2 from-control-label required']) }}
        
                        
                        <div class="col-md-10">
                            <div class="checkbox d-flex align-items-center">
                                <label class="switch switch-label switch-pill switch-primary mr-2" for="role-1">
                                    <input class="switch-input" type="checkbox" name="status" id="role-1" value="1" 
                                        {{ (!isset($edit_country->status) ||(isset($edit_country->status) && $edit_country->status == 1)) ? "checked" : "" }}>
        
                                        <span class="switch-slider" data-checked="on" data-unchecked="off"></span>
                                </label>
                            </div>
                        </div><!--col-->
                    </div><!--form-group-->
                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->
        
        <div class="card-footer">
            <div class="row">
                <div class="col">
                    <a href="{{route('admin.currencies.index')}}" class="btn btn-danger btn-sm">Cancel</a>
                </div><!--col-->
        
                <div class="col text-right">
                    @if(isset($edit_id))
                    <input class="btn btn-success btn-sm pull-right" type="submit" value="UPDATE">
                    @else
                    <input class="btn btn-success btn-sm pull-right" type="submit" value="Create">
                    @endif
                </div><!--row-->
            </div><!--row-->
        </div>
    </div><!--card-->
    {{ Form::close() }}
@endsection