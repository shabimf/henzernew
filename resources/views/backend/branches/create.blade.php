@extends('backend.layouts.app')

@section('title',' Branches | Create Branches')

@section('breadcrumb-links')
    @include('backend.branches.includes.breadcrumb-links')
@endsection

@section('content')

@include('common.alert')


    @if(isset($edit_id))
        {{ Form::model($edit_id, ['route' => ['admin.branches.update', $edit_id], 'method' => 'patch']) }}
    @else
        {{ Form::open(['route' => 'admin.branches.store']) }}
    @endif

    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        Branches
                        @if(isset($edit_id))
                        <small class="text-muted">Edit Branches</small>
                        @else
                        <small class="text-muted">Create Branches</small>
                        @endif
                    </h4>
                </div><!--col-->
            </div><!--row-->
        
            <hr>
        
            <div class="row mt-4 mb-4">
                <div class="col">
                    <div class="form-group row">
                        {{ Form::label('country_id', trans('Country'), ['class' => 'col-md-2 from-control-label required']) }}
        
                        <div class="col-md-10">
                            {{ Form::select('country_id', $list_country, old('country_id',(isset($edit_branches)) ? $edit_branches->country_id:''), ['class' => 'form-control select2 status box-size', 'placeholder' => trans('--- Select Country ---'), 'required' => 'required']) }}
                        </div>
                        <!--col-->
                    </div>
                    <div class="form-group row">
                        {{ Form::label('city_id', trans('City'), ['class' => 'col-md-2 from-control-label  required']) }}                   
        
                        <div class="col-md-10">
                            {{ Form::select('city_id', $list_city, old('model_id',(isset($edit_branches)) ? $edit_branches->city_id:''), ['class' => 'form-control select2 status box-size', 'placeholder' => trans('--- Select City ---'), 'required' => 'required']) }}
                        </div>
                        <!--col-->
                    </div>
                    <div class="form-group row">
                        {{ Form::label('type', trans('Type'), ['class' => 'col-md-2 control-label required']) }}
                        <div class="col-md-8">
                            <label for="type-1" class="control">
                                <input type="radio" value="1" name="type"  id="type-1" class="typecls" @if(isset($edit_branches)) {{ old('type',$edit_branches->type) == '1' ? 'checked' : '' }} @endif /> &nbsp;&nbsp;Sourcing Team
                            </label>
                            <label for="type-2" class="control">
                                <input type="radio" value="2" name="type"  id="type-2" class="typecls" @if(isset($edit_branches)) {{ (old('type', $edit_branches->type) == '2' || old('type', $edit_branches->type) == '3') ? 'checked' : '' }} @endif/> &nbsp;&nbsp;Vendor
                            </label>
                            {{-- <label for="type-3" class="control">
                                <input type="radio" value="3" name="type"  id="type-3" @if(isset($edit_branches)) {{ old('type', $edit_branches->type) == '3' ? 'checked' : '' }} @endif/> &nbsp;&nbsp;Subvendor
                            </label> --}}
                           
                        </div>
                    </div>
                    <div class="form-group row ctypecls @if(isset($edit_branches)) @if($edit_branches->type == 2 || $edit_branches->type == 3) @else  hidden @endif @else  hidden @endif">
                        {{ Form::label('ctype', trans('Communication With'), ['class' => 'col-md-2 control-label required']) }}
                        <div class="col-md-8">
                            <label for="ctype-1" class="control">
                                <input type="radio" value="1" name="ctype"  id="ctype-1" class="ctype" @if(isset($edit_branches)) {{ old('ctype',$edit_branches->type) == '2' ? 'checked' : '' }} @endif /> &nbsp;&nbsp;Sourcing Team
                            </label>
                            <label for="ctype-2" class="control">
                                <input type="radio" value="2" name="ctype"  id="ctype-2"  class="ctype" @if(isset($edit_branches)) {{ old('ctype', $edit_branches->type) == '3' ? 'checked' : '' }} @endif/> &nbsp;&nbsp;Vendor
                            </label>

                        </div>
                    </div>
                    <div class="form-group row ctypecls @if(isset($edit_branches)) @if($edit_branches->type == 2 || $edit_branches->type == 3) @else  hidden @endif @else  hidden @endif">
                        <label for="branch" class="col-md-2 from-control-label required">Branch</label>
                        <div class="col-md-10">
                        <select class="form-control" id="branch-dropdown" name="branch_id">
                            @if(isset($edit_branches))
                              @if($edit_branches->type == 2)
                                @foreach ($sources as $key=>$val)
                                    <option value="{{$key}}" @if($parent_details->branch_id == $key) selected @endif>{{$val}}</option> 
                                @endforeach
                              @elseif($edit_branches->type == 3)
                                @foreach ($vendors as $key=>$val)
                                <option value="{{$key}}"  @if($parent_details->branch_id == $key) selected @endif>{{$val}}</option> 
                                @endforeach
                              @endif
                            @endif
                        </select>
                        </div>
                    </div>   
                    <div class="form-group row">
                        {{ Form::label('name', trans('Branch Name'), ['class' => 'col-md-2 from-control-label required']) }}
                        
                        <div class="col-md-10">
                            {{ Form::text('name', old('name',(isset($edit_branches)) ? $edit_branches->name:''),['class' => 'form-control' , 'placeholder' => trans('Branch Name'), 'required']) }}
                        </div><!--col-->
                    </div><!--form-group-->
                    <div class="form-group row">
                        {{ Form::label('code', trans('Branch Code'), ['class' => 'col-md-2 from-control-label required']) }}
                        
                        <div class="col-md-10">
                            {{ Form::text('code', old('code',(isset($edit_branches)) ? $edit_branches->code:''),['class' => 'form-control' , 'placeholder' => trans('Branch Code'), 'required']) }}
                        </div><!--col-->
                    </div>
                    <div class="form-group row">
                        {{ Form::label('company_name', trans('Company Name'), ['class' => 'col-md-2 from-control-label required']) }}
                        
                        <div class="col-md-10">
                            {{ Form::text('company_name', old('company_name',(isset($parent_details)) ? $parent_details->company_name:''),['class' => 'form-control' , 'placeholder' => trans('Company Name')]) }}
                        </div><!--col-->
                    </div><!--form-group-->
                    <div class="form-group row">
                        {{ Form::label('contact_person', trans('Contact Person'), ['class' => 'col-md-2 from-control-label required']) }}
                        
                        <div class="col-md-10">
                            {{ Form::text('contact_person', old('contact_person',(isset($parent_details)) ? $parent_details->contact_person:''),['class' => 'form-control' , 'placeholder' => trans('Contact Person Name')]) }}
                        </div><!--col-->
                    </div><!--form-group-->
                    <div class="form-group row">
                        {{ Form::label('email', trans('Email Id'), ['class' => 'col-md-2 from-control-label required']) }}
                        
                        <div class="col-md-10">
                            {{ Form::text('email', old('email',(isset($parent_details)) ? $parent_details->email:''),['class' => 'form-control' , 'placeholder' => trans('Email Id')]) }}
                        </div><!--col-->
                    </div><!--form-group-->
                    <div class="form-group row">
                        {{ Form::label('phone_number', trans('Phone'), ['class' => 'col-md-2 from-control-label required']) }}
                        
                        <div class="col-md-10">
                            {{ Form::text('phone_number', old('phone_number',(isset($parent_details)) ? $parent_details->phone_number:''),['class' => 'form-control' , 'placeholder' => trans('Phoneno')]) }}
                        </div><!--col-->
                    </div><!--form-group-->
                    <div class="form-group row">
                        {{ Form::label('address', trans('Address'), ['class' => 'col-md-2 from-control-label required']) }}
        
                        <div class="col-md-10">
                            {{ Form::textarea('address',old('address',(isset($parent_details)) ? $parent_details->address:''), ['class' => 'form-control', 'placeholder' => trans('Address')]) }}
                        </div>
                        <!--col-->
                    </div>
        
                    <div class="form-group row">
                        {{ Form::label('status', trans('Status'), ['class' => 'col-md-2 from-control-label required']) }}
        
                        
                        <div class="col-md-10">
                            <div class="checkbox d-flex align-items-center">
                                <label class="switch switch-label switch-pill switch-primary mr-2" for="role-1">
                                    <input class="switch-input" type="checkbox" name="status" id="role-1" value="1" 
                                        {{ (!isset($edit_branches->status) ||(isset($edit_branches->status) && $edit_branches->status == 1)) ? "checked" : "" }}>
        
                                        <span class="switch-slider" data-checked="on" data-unchecked="off"></span>
                                </label>
                            </div>
                        </div><!--col-->
                    </div><!--form-group-->
                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->
        
        <div class="card-footer">
            <div class="row">
                <div class="col">
                    <a href="{{route('admin.branches.index')}}" class="btn btn-danger btn-sm">Cancel</a>
                </div><!--col-->
        
                <div class="col text-right">
                    @if(isset($edit_id))
                    <input class="btn btn-success btn-sm pull-right" type="submit" value="UPDATE">
                    @else
                    <input class="btn btn-success btn-sm pull-right" type="submit" value="Create">
                    @endif
                </div><!--row-->
            </div><!--row-->
        </div>
    </div><!--card-->
    {{ Form::close() }}
@endsection
@section('pagescript')
<script type="text/javascript">
    FTX.Utils.documentReady(function() {
        var url = "{{asset('admin/ajax/get-city')}}";
        FTX.Branches.edit.init("{{ config('locale.languages.' . app()->getLocale())[1] }}", url);
    });
    $(".typecls") // select the radio by its id
    .change(function(){ // bind a function to the change event
        if( $(this).is(":checked") ){ // check if the radio is checked
            var val = $(this).val(); // retrieve the value
            if(val == 1) {
                $('.ctypecls').addClass("hidden");
            } else {
                $('.ctypecls').removeClass("hidden");
            }
        }
    });
    $(document).ready(function() {
    $('.ctype').on('change', function() {
    var type = this.value;
    //alert(type);
    $("#branch-dropdown").html('');
    $.ajax({
    url:"{{url('admin/ajax/get-branch')}}",
    type: "POST",
    data: {
    type: type,
    _token: '{{csrf_token()}}' 
    },
    dataType : 'json',
    success: function(result){
    $('#branch-dropdown').html('<option value="">Select branch</option>'); 
    $.each(JSON.parse(JSON.stringify(result)),function(key,value){
    $("#branch-dropdown").append('<option value="'+key+'">'+value+'</option>');
    });
    }
    });
    });    
   });
</script>
@stop