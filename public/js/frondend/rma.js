$(document).ready(function() {
    


    $(".accecls").click(function (e) {
        e.preventDefault(); 
        var ele = $(this);
        var id = ele.parents("tr").attr("data-id");
        var status = $(this).data('status');
        if(confirm("Are you sure want to accept?")) {
            $.ajax({
                url: "/rma/status/"+id,
                method: "POST",
                data: {
                    _token: $('[name="_token"]').val(), 
                    id: id,
                    status : status
                },
                success: function (response) { 
                    window.location.reload();
                }
            });
        }
       
    });  
    $(".rejcls").click(function (e) {
        e.preventDefault(); 
        var ele = $(this);
        var id = ele.parents("tr").attr("data-id");
        var status = $(this).data('status');
        if(confirm("Are you sure want to reject?")) {
            $.ajax({
                url: "/rma/status/"+id,
                method: "POST",
                data: {
                    _token: $('[name="_token"]').val(), 
                    id: id,
                    status : status
                },
                success: function (response) { 
                    window.location.reload();
                }
            });
        }
       
    });  
    $(".remove").click(function (e) {
        e.preventDefault(); 
        var ele = $(this);
        if(confirm("Are you sure want to remove?")) {
            $.ajax({
                url: "/rma/remove",
                method: "DELETE",
                data: {
                    _token: $('[name="_token"]').val(), 
                    id: ele.parents("tr").attr("data-id")
                },
                success: function (response) { 
                    window.location.reload();
                }
            });
        }
    }); 
    $('#credit-box').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var note = button.data('note');
        var modal = $(this);
        modal.find('#note').val(note);
        modal.find('#rma_id').val(button.data('id'));
    });

    $(".notebtn").click(function (e) {
        var id = $('#rma_id').val();
        var newData =   {
            "_token":$('[name="_token"]').val(),
            "note":$('[name="note"]').val()
            };

            var dataJson = JSON.stringify(newData);

            $.ajax({
            type: 'POST',
            url: "http://127.0.0.1:8000/rma/note/"+id,
            data: dataJson,
            error: function(e) {
            console.log(e);
            },
            dataType: "json",
            contentType: "application/json"
            });
        // $.ajax({
        //     url: "/rma/note/"+id,
        //     method: "POST",
        //     data: {
        //         "_token": $('[name="_token"]').val(), 
        //         "note": $('[name="note"]').val()
        //     },
        //     success: function (response) { 
        //         window.location.reload();
        //     }
        // });
        // $.ajax({
        //     url: "http://127.0.0.1:8000/rma/note/"+id,
        //     method: "POST",
        //     data: {
        //         _token: $('[name="_token"]').val(), 
        //         note: $('[name="note"]').val()
        //     },
        //     success: function (response) { 
        //         alert(response);
        //         //window.location.reload();
        //     }
        // });
    });
    
});

