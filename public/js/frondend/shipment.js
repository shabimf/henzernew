$(document).on('click', '.status', function(){
    var id = $(this).data('id');
    var _token = $('[name="_token"]').val();
    var formData = 'status='+$(this).data('value')+'& _token='+_token;
        $.ajax({
            url: "/shipment/update/"+id,
            method: "POST",
            data: formData,
            success: function (response) { 
                console.log(response);
                if (response > 0) {
                    window.location.assign("/shipment/view/"+response);
                }
            }
        });
    });