    var page = 1; //track user scroll as page number, right now page number is 1
    load_more(page); //initial content load
    $(document).ready(function(){
        $('#results').bind('scroll',chk_scroll);
    });

    function chk_scroll(e)
    {
        var elem = $(e.currentTarget);
        if (elem[0].scrollHeight - elem.scrollTop() == elem.outerHeight())
        {
            
            page++;
            load_more(page);
        }

    }   

    function getID(id) {
        window.location = $('.tr-pointer-'+id).data('href');
        return false;
    }
    function load_more(page){
        var serial_num =  $("input[name='serial_num']").val();
        var d_from =  $("input[name='d_from']").val();
        var d_to =  $("input[name='d_to']").val();
        var status =  $("select[name='status']").val();
        var branch_from =  $("select[name='branch_from']").val();
        var branch_to =  $("select[name='branch_to']").val();
        $.ajax({
           url: SITEURL + "?page=" + page +'&serial_num='+serial_num+'&d_from='+d_from+'&d_to='+d_to+'&status='+status+'&branch_from='+branch_from+'&branch_to='+branch_to,
           type: "get",
           datatype: "html",
           beforeSend: function()
           {
           }
        })
        .done(function(data)
        {      
            if (data.length == 0) {
                console.log(data.length);
                return; 
            }
            $("#results").append(data); //append data into #results element      
            var totalcount = $('.bg-tr').data('count');
            var count = $('#results').children('tr').length;   
            if (count<=11) {
                $(".scroll tbody").css("height", "auto");
            }
            $(".totalcount").html(totalcount);       
       })
       .fail(function(jqXHR, ajaxOptions, thrownError)
       {
          alert('No response from server');
       });
    }