const searchBar = document.querySelector(".search input"),
    // searchIcon = document.querySelector(".search button"),
    usersList = document.querySelector(".users-list"),
    form = document.querySelector(".typing-area");


$(".chat-area").hide();
// searchIcon.onclick = () => {
//   searchBar.classList.toggle("show");
//   searchIcon.classList.toggle("active");
//   searchBar.focus();
//   if (searchBar.classList.contains("active")) {
//     searchBar.value = "";
//     searchBar.classList.remove("active");
//   }
// }

searchBar.onkeyup = () => {
  let searchTerm = searchBar.value;

  if (searchTerm != "") {
    searchBar.classList.add("active");
  } else {
    searchBar.classList.remove("active");
  }
  var formData = 'searchTerm=' + searchTerm;
  $.ajax({
      url: "/ajax/search",
      method: "GET",
      data: formData,
      success: function(data) {
        usersList.innerHTML = data;
      }
  });
}

// setInterval(() => {
    let xhr = new XMLHttpRequest();
    xhr.open("GET", "/ajax/users", true);
    xhr.onload = () => {
        if (xhr.readyState === XMLHttpRequest.DONE) {
            if (xhr.status === 200) {
                let data = xhr.response;
                //if (!searchBar.classList.contains("active")) {
                  usersList.innerHTML = data;
                //}
            }
        }
    }
    xhr.send();
// }, 500);

$(document).on('click', '.chatid', function() {
    $(".users").hide();
    $(".chat-area").show();
    var id = $(this).data('id');
    var formData = 'id=' + id;
    $.ajax({
        url: "/ajax/userinfo",
        method: "GET",
        data: formData,
        success: function(data) {
          $('.chat-area').html(data);
          getchat(id);
        }
    });
});

function getchat(id) {
  if (id == 0) id = $(".incoming_id").val();
  var formData = 'incoming_id=' + id;
  $.ajax({
    url: "/ajax/get-chat",
    method: "GET",
    data: formData,
    success: function(data) {
      $(".chat-box").html(data);
      chatBox = document.querySelector(".chat-box");
      if (!chatBox.classList.contains("active")) {
        scrollToBottom();
      }
    }
  });
}

setInterval(() => {
  if ($('.chat-box').length > 0) {
    chatBox = document.querySelector(".chat-box");
    chatBox.onmouseenter = () => {
      chatBox.classList.add("active");
    }

    chatBox.onmouseleave = () => {
      chatBox.classList.remove("active");
    }
    getchat(0)
    $(".input-field").focus();
    $('.input-field').on('keyup', function(evt) {
      if (this.value != "") {
        $(".btn-chat").addClass("active");
      } else {
        $(".btn-chat").removeClass("active");
      }
    });
  }
}, 500);


function scrollToBottom() {
  var chatBox = document.querySelector(".chat-box");
  chatBox.scrollTop = chatBox.scrollHeight;
}

function insertChat() {
  var message = $("#message").val();
  var incoming_id = $(".incoming_id").val();
  var formData = 'msg=' + message + '&incoming_msg_id=' + incoming_id;
  $.ajax({
    url: "/ajax/insert-chat",
    method: "get",
    data: formData,
    success: function(data) {
      if (data > 0) {
        scrollToBottom();
        $("#message").val('');
      }
    }
  });
}

function clickPress(e) {
  if (event.keyCode == 13) {
    insertChat();
  }
}