$(document).ready(function() {
    $(document).on('click', '.get-value', function(){
        var catalog_id = $(this).data('value');
        var _token = $('input[name="_token"]').val();
        $.ajax({
            url:"ajax/stock_available/check",
            method:"POST",
            data:{catalog_id:catalog_id, _token:_token},
            success:function(result)
            {
               $('#error_stock'+catalog_id).html('<label class="text-success">'+result+' Stock Available</label>');
            }
        })
    });
    $(document).on('click', '.quotation-submit', function(){
        var id = $(this).data('id');
        var formData = $('#update-form').serialize()+'&status='+$(this).data('value');
        $.ajax({
            url: "/quotation/update/"+id,
            method: "POST",
            data: formData,
            success: function (response) { 
                console.log(response);
                window.location.assign("/quotation/view/"+id);
            }
        });
    }); 
    $(document).on('click', '.order-submit', function(){
        var id = $(this).data('id');
        var _token = $('[name="_token"]').val();
        var ele=[];
        $("input:checkbox[name=chk]:checked").each(function(){
            ele.push($(this).val());
        });
        var formData = 'status='+$(this).data('value')+'& _token='+_token+'&chk='+ele;
        $.ajax({
            url: "/quotation/update/"+id,
            method: "POST",
            data: formData,
            success: function (response) { 
                console.log(response);
                window.location.assign("/order/view/"+response);
            }
        });
    });
    $(".remove").click(function (e) {
        e.preventDefault(); 
        var ele = $(this);
        if(confirm("Are you sure want to remove?")) {
            $.ajax({
                url: "/quotation/remove",
                method: "DELETE",
                data: {
                    _token: $('[name="_token"]').val(), 
                    id: ele.parents("tr").attr("data-id")
                },
                success: function (response) { 
                    window.location.reload();
                }
            });
        }
    });     
});

 