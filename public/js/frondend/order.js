$(document).ready(function() {
    
    $(document).on('click', '.get-value', function(){
        var catalog_id = $(this).data('value');
        var _token = $('input[name="_token"]').val();
        $.ajax({
            url:"ajax/stock_available/check",
            method:"POST",
            data:{catalog_id:catalog_id, _token:_token},
            success:function(result)
            {
               $('#error_stock'+catalog_id).html('<label class="text-success">'+result+' Stock Available</label>');
            }
        })
    });
    $(document).on('click', '.order-submit', function(){
        var id = $(this).data('id');
       
        var formData = $('#update-form').serialize()+'&status='+$(this).data('value');
        $.ajax({
            url: "/order/update/"+id,
            method: "POST",
            data: formData,
            success: function (response) { 
                console.log(response);
                window.location.assign("/order/view/"+id);
            }
        });
    }); 
    $(document).on('click', '.invoice-submit', function(){
        var id = $(this).data('id');
        var _token = $('[name="_token"]').val();
        var order_ids = new Array();
        $("input[name='selector']:checked").each(function() {
            order_ids.push($(this).val());
        });
        
        var formData = 'status='+$(this).data('value')+'& _token='+_token+'&order_ids='+order_ids;
        alert(formData);
            $.ajax({
                url: "/order/update/"+id,
                method: "POST",
                data: formData,
                success: function (response) { 
                    console.log(response);
                    if (response == 1) {
                        window.location.assign("/invoice/view/"+response);
                    } else {
                        window.location.assign("/order/view/"+id);
                    }
                }
            });
    });    
    $(".remove").click(function (e) {
        e.preventDefault(); 
        var ele = $(this);
        if(confirm("Are you sure want to remove?")) {
            $.ajax({
                url: "/order/remove",
                method: "DELETE",
                data: {
                    _token: $('[name="_token"]').val(), 
                    id: ele.parents("tr").attr("data-id")
                },
                success: function (response) { 
                    window.location.reload();
                }
            });
        }
    });    
    $(document).on('click', '.btn-app', function(){
        var id = $(this).data('id');
        $('[name="status"]').val(id);
    });    
});

