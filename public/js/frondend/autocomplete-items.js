$( "#search" ).autocomplete({
    source: function(request, response) {
        $.ajax({
        url: "/ajax/autocomplete-search",
        data: {
            query : request.term
         },
        dataType: "json",
        success: function(data){
            var resp = $.map(data,function(obj){
                    return {
                                value:obj.name,
                                id:obj.id
                            
                            }
            }); 

            response(resp);
            }
        });
    },
    minLength: 2,
    select:function(event,ui){
        $('#catalog_id').val(ui.item.id);
    }
});