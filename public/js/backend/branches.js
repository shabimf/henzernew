(function () {

    FTX.Branches = {

        list: {

            selectors: {
                branches_table: $('#branch-table'),
            },

            init: function () {

                this.selectors.branches_table.dataTable({

                    processing: false,
                    serverSide: true,
                    ajax: {
                        url: this.selectors.branches_table.data('ajax_url'),
                        type: 'post',
                    },
                    columns: [
                        { data: 'country_name', name: 'country_name' },
                        { data: 'city_name', name: 'city_name' },
                        { data: 'type', name: 'type' },
                        { data: 'name', name: 'name' },
                        { data: 'code', name: 'code' },
                        { data: 'status', name: 'status' },
                        { data: 'created_by', name: 'created_by' },
                        { data: 'created_at', name: 'created_at' },
                        { data: 'actions', name: 'actions', searchable: false, sortable: false }

                    ],
                    order: [[3, "asc"]],
                    searchDelay: 500,
                    "createdRow": function (row, data, dataIndex) {
                        FTX.Utils.dtAnchorToForm(row);
                    }
                });
            }
        },

        edit: {
            selectors: {
              
                status: jQuery(".status"),
                type: document.querySelectorAll(".typecls"),
                country: document.querySelector("select[name='country_id']"),
            },

            init: function (locale, url) {
                this.addHandlers(locale, url);
            },

            addHandlers: function (locale, url) {
                var country = this.selectors.country;
                var type =  this.selectors.type;
                this.selectors.status.select2({
                    width: '100%'
                }); 
                type.onclick = function (event) {
                    alert(event);
                };
  
                country.onchange = function (event) {
                    var token = $("input[name='_token']").val();
                    $.ajax({
                        url: url,
                        method: 'POST',
                        data: {id_country:country.value, _token:token},
                        success: function(data) {
                            $("select[name='city_id'").html('');
                            $("select[name='city_id'").append('<option value="">---Select City--</option>');
                            $.each(JSON.parse(JSON.stringify(data)), function(i, item) {
                                $("select[name='city_id'").append('<option value="'+i+'">'+item+'</option>');
                            });
                         
                        }
                    });
                    
                };
            },
        },
    }
})();

