(function () {

    FTX.Series = {

        list: {

            selectors: {
                series_table: $('#series-table'),
            },

            init: function () {

                this.selectors.series_table.dataTable({

                    processing: false,
                    serverSide: true,
                    ajax: {
                        url: this.selectors.series_table.data('ajax_url'),
                        type: 'post',
                    },
                    columns: [
                        { data: 'brand_name', name: 'brand_name' },
                        { data: 'model_name', name: 'model_name' },
                        { data: 'series_name', name: 'series_name' },
                        { data: 'name', name: 'name' },
                        { data: 'focus_code', name: 'focus_code' },
                        { data: 'status', name: 'status' },
                        { data: 'created_by', name: 'created_by' },
                        { data: 'created_at', name: 'created_at' },
                        { data: 'actions', name: 'actions', searchable: false, sortable: false }

                    ],
                    order: [[3, "asc"]],
                    searchDelay: 500,
                    "createdRow": function (row, data, dataIndex) {
                        FTX.Utils.dtAnchorToForm(row);
                    }
                });
            }
        },

        edit: {
            selectors: {
              
                status: jQuery(".status"),
                brand: document.querySelector("select[name='brand_id']"),
                model: document.querySelector("select[name='model_id']"),
            },

            init: function (locale, url) {
                this.addHandlers(locale, url);
                FTX.tinyMCE.init(locale);
            },

            addHandlers: function (locale, url) {
                var brand = this.selectors.brand;
                var model = this.selectors.model;
                this.selectors.status.select2({
                    width: '100%'
                }); 

                brand.onchange = function (event) {
                    var token = $("input[name='_token']").val();
                    $.ajax({
                        url: url+"/get-model",
                        method: 'POST',
                        data: {id_brand:brand.value, _token:token},
                        success: function(data) {
                            $("select[name='model_id'").html('');
                            $("select[name='model_id'").append('<option value="">---Select Model--</option>');
                            $.each(JSON.parse(JSON.stringify(data)), function(i, item) {
                                $("select[name='model_id'").append('<option value="'+i+'">'+item+'</option>');
                            });
                         
                        }
                    });
                    
                };
                model.onchange = function (event) {
                    var token = $("input[name='_token']").val();
                    $.ajax({
                        url: url+"/get-series",
                        method: 'POST',
                        data: {id_model:model.value, _token:token},
                        success: function(data) {
                            $("select[name='series_id'").html('');
                            $("select[name='series_id'").append('<option value="">---Select Series--</option>');
                            $.each(JSON.parse(JSON.stringify(data)), function(i, item) {
                                $("select[name='series_id'").append('<option value="'+i+'">'+item+'</option>');
                            });
                         
                        }
                    });
                    
                };
            },
        },
    }
})();

