(function () {

    FTX.vendors = {

        list: {

            selectors: {
                vendor_table: $('#vendor'),
            },

            init: function () {

                this.selectors.vendor_table.dataTable({

                    processing: false,
                    serverSide: true,
                    ajax: {
                        url: this.selectors.vendor_table.data('ajax_url'),
                        type: 'post',
                    },
                    columns: [

                       
                        { data: 'company_name', name: 'company_name' },
                        { data: 'contact_person', name: 'contact_person' },
                        { data: 'vendor_name', name: 'vendor_name' },
                        { data: 'email', name: 'email' },
                        { data: 'phone_number', name: 'phone_number' },
                        { data: 'status', name: 'status' },
                        { data: 'created_by', name: 'created_by' },
                        { data: 'created_at', name: 'created_at' },
                        { data: 'actions', name: 'actions', searchable: false, sortable: false }

                    ],
                    order: [[3, "asc"]],
                    searchDelay: 500,
                    "createdRow": function (row, data, dataIndex) {
                        FTX.Utils.dtAnchorToForm(row);
                    }
                });
            }
        },
        edit: {
            selectors: {
              
                status: jQuery(".status"),
            },

            init: function (locale) {
                this.addHandlers(locale);
            },

            addHandlers: function (locale) {
                this.selectors.status.select2({
                    width: '100%'
                }); 

                
            },
        },
    }
})();