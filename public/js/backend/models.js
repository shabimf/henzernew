(function () {

    FTX.Models = {

        list: {

            selectors: {
                models_table: $('#models-table'),
            },

            init: function () {

                this.selectors.models_table.dataTable({

                    processing: false,
                    serverSide: true,
                    ajax: {
                        url: this.selectors.models_table.data('ajax_url'),
                        type: 'post',
                    },
                    columns: [
                        { data: 'brand_name', name: 'brand_name' },
                        { data: 'name', name: 'name' },
                        { data: 'status', name: 'status' },
                        { data: 'created_by', name: 'created_by' },
                        { data: 'created_at', name: 'created_at' },
                        { data: 'actions', name: 'actions', searchable: false, sortable: false }

                    ],
                    order: [[3, "asc"]],
                    searchDelay: 500,
                    "createdRow": function (row, data, dataIndex) {
                        FTX.Utils.dtAnchorToForm(row);
                    }
                });
            }
        },

        edit: {
            selectors: {
              
                status: jQuery(".status"),
            },

            init: function (locale) {
                this.addHandlers(locale);
                FTX.tinyMCE.init(locale);
            },

            addHandlers: function (locale) {

               
                this.selectors.status.select2({
                    width: '100%'
                });

            },
        },
    }
})();