$( document ).ready(function() {
    $("#toggle_menu").click(function() {
        var expand = $(this).attr("data-expand");
        if(expand==1){
            $(".content_box").css({'-ms-flex':'0 0 97%', 'flex':'0 0 97%'});
            $("#left_nav").animate({marginLeft:'-13.5%'}, { duration: 300, queue: true });
            $(".content_box").animate({'max-width':'97%'}, { duration: 1000, queue: true });
            $(this).attr("data-expand",0);
            $(this).html('<img src="images/arrow_open.png" border="0" width="90%">');
            
        }
        else{
            
            $(this).attr("data-expand",1);
            $(".content_box").css({'-ms-flex':'0 0 83.333333%', 'flex':'0 0 83.333333%'});
            $(".content_box").animate({'max-width':'83.33333333%'});
            $("#left_nav").animate({marginLeft:'0'});
            $(this).html('<img src="images/arrow_close.png" border="0" width="90%">');  
            
        }
        
     $.ajax({
          type : "get",
          url: "/ajax/set-leftnav/"+expand,
          success: function(response){
          }
     });
             
    });	
    $("#menuToggle").click(function() {
          
    });	   
    $(".simplebar-content-wrapper").scroll(function() {
         if($(this).scrollTop()>50){
              document.getElementById("myBtn").style.display = "block";
         }else{
              document.getElementById("myBtn").style.display = "none";
         }
    });
    
    $('#menuToggle').click(function(e){
     var $parent = $(this).parent('nav');
     $parent.toggleClass("open");
     var navState = $parent.hasClass('open') ? "hide" : "show";
     $(this).attr("title", navState + " navigation");
     e.preventDefault();
   });
   
   $('.btn-mode').click(function(e){
     var id = $(this).data('filter');
     $.ajax({
          type : "get",
          url: "/ajax/set-mode/"+id,
          success: function(response){
               window.location.href = '/';
          }
     });
   });


     $(".notificls").click(function(e){
          $.ajax({
               type : "get",
               url: "/ajax/notification",
               success: function(response){
                 $('.notifiresult').html(response);  
               }
          });
     });
     $('.openPopup').on('click',function(){
          var dataId = $(this).attr('data-id');
          $('#popupid').val(dataId);
     });

});	


function sendMarkRequest(id = null) {
     var csrf = document.querySelector('meta[name="csrf-token"]').content;
     return $.ajax("/ajax/mark-as-read", {
          method: 'POST',
          data: {
               "_token": csrf,
               "id": id
          }
     }); 
}

function sendComment(id, module) {
     
     var csrf = document.querySelector('meta[name="csrf-token"]').content;
     var comment = $('#comment').val();
     if(comment == "") {
       $('#comment').css('border', '1px solid red');
     } else {
       $('#comment').css('border', 'none');  
     }
}


function close() {
     $.ajax({
          type : "get",
          url: "/ajax/close-leftnav",
          success: function(response){
          }
     });
}
 $('.td-pointer').click(function(e){
     e.preventDefault();
     var id = $(this).attr('value');
     window.location = $('.tr-pointer-'+id).data('href');
     return false;
 });
 $(document).mouseup(function (e) {
     var container = $(".sidebar_right");
     if(!container.is(e.target) && 
     container.has(e.target).length === 0) {
         close();
         container.removeClass("open");
     }
 });

 $(".closee").click(function(e){ 
     close();
     $(".sidebar_right").removeClass("open");
});
$(document).ready(function(){
     $('.btn-submit').prop('disabled', true);
     $('#select_all').on('click',function(){
         if(this.checked){
             $('.checkbox').each(function(){
                 this.checked = true;
                 $('.btn-submit').prop('disabled', false);
             });
         }else{
              $('.checkbox').each(function(){
                 this.checked = false;
                 $('.btn-submit').prop('disabled', true);
             });
         }
     });
     
     $('.checkbox').on('click',function(){
         if($('.checkbox:checked').length == $('.checkbox').length){
             $('#select_all').prop('checked',true);
         }else{
             $('#select_all').prop('checked',false);
         }
         if($('.checkbox:checked').length >0){
          $('.btn-submit').prop('disabled', false);
         } else {
          $('.btn-submit').prop('disabled', true);
         }
     });
 });
function openForm() {
     $("#myForm").slideUp();
     document.getElementById("myForm").style.display = "block";
   }
   function chatMinus() {
     $id=$('#hidden-chatvalue').val();
     if($id==0)
     {
     $("#myForm").addClass("intro");
     $('#hidden-chatvalue').val(1);
     }
     else{
       $("#myForm").removeClass("intro");
     }
   }
   function chatClose() {
     document.getElementById('myForm').style.display = 'none';
   }
   function backtoChat()
   {
     document.getElementById('myForm').style.display = 'block'; 
     document.getElementById('chat-area').style.display = 'none'; 
   }
   $("#filter_date").change(function (){ 
     var days = this.value;
     if (days == 3) {
        var txt_day = 'months';
     } else {
        var txt_day = 'days';
     }
     if (days > 0) {
          var startdate = new Date();
          var to_date = moment(startdate, "DD-MM-YYYY").format('YYYY-MM-DD');
          var from_date = moment(startdate, "DD-MM-YYYY").subtract(days, txt_day).format('YYYY-MM-DD');
          $("input[name='d_from']").val(from_date);
          $("input[name='d_to']").val(to_date);
     } else {
          $("input[name='d_from']").val('');
          $("input[name='d_to']").val('');
     }
   });