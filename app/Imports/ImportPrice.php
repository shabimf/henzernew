<?php

namespace App\Imports;


use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use App\Repositories\Frontend\PriceRepository;
use App\Http\Traits\CommonTrait;

class ImportPrice implements ToModel, WithHeadingRow
{
    use CommonTrait;

    public function model(array $row)
    {
        $id = $this->FocuseCodeByID('id', $row['code']);
        $input['catalog_id'] =  $id;
        $input['price'] =  $row['price'];
        $price = new PriceRepository();
        $price->create($input);

    }
}