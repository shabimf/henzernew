<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use App\Repositories\Frontend\StocksRepository;
use App\Http\Traits\CommonTrait;

class ImportStock implements ToModel, WithHeadingRow
{
    use CommonTrait;
    
    public function model(array $row)
    {
        $id = $this->FocuseCodeByID('id', $row['code']);
        $input['catalog_id'] =  $id;
        $input['qty'] =  $row['stock'];
        $stock = new StocksRepository();
        $stock->create($input);

    }
}