<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;   
use App\Http\Traits\GetTrait;
use App\Http\Traits\CommonTrait;
use App\Repositories\Frontend\ShipmentRepository;  
use App\Http\Requests\Frontend\Shipment\SubmitShipmentRequest;
use App\Http\Resources\ShipmentResource;


class ShipmentController extends Controller
{
    use GetTrait,CommonTrait;

    protected $repository;

    public function __construct(ShipmentRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $collection = $this->repository->retrieveList($request->all());
        return ShipmentResource::collection($collection);
    }

    public function show($id)  {

        $this->data['common_details']   = $this->repository->getById($id);
        $this->data['details'] = $this->repository->getShipmentById($id);
        return [ 'data' => $this->data ];

    }

    public function update(Request $request, $id)
    {
        $this->repository->update($request->all(), $id);
        return [
            'message' => 'The Shipment was successfully Updated.'
        ];
    }

}