<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Traits\CommonTrait;
use DB;
class NotificationController extends APIController
{
    use CommonTrait;
    
    public function index(Request $request)
    {
        $this->getnotification();
    }
    
    public function read(Request $request) {
       $this->markReadNotification($request->all());
    }
   
}