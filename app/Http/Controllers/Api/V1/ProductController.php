<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Requests\Frontend\Product\ManageProductRequest;
use App\Http\Resources\ProductResource;
use App\Repositories\Frontend\ProductRepository;
use Illuminate\Http\Response;

class ProductController extends APIController
{
   
    protected $repository;

    public function __construct(ProductRepository $repository)
    {
        $this->repository = $repository;
    }
    
    public function index(Request $request)
    {
        
        $collection = $this->repository->retrieveList($request->all());
        return ProductResource::collection($collection);
    }

    public function show($id, Request $request)
    {
        $product =  $this->repository->getById($id);
        return $product;
    }

}
