<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Repositories\Frontend\OrderRepository;  
use App\Http\Requests\Frontend\Order\StoreOrderRequest;
use App\Http\Requests\Frontend\Order\SubmitOrderRequest;
use App\Http\Resources\OrderResource;
use Illuminate\Http\Response;

class OrderController extends APIController
{
   
    protected $repository;

    public function __construct(OrderRepository $repository)
    {
        $this->repository = $repository;
    }
    
    public function index(Request $request)
    {
        $collection = $this->repository->retrieveList($request->all());
        return OrderResource::collection($collection);
    }

    public function show($id)
    {
        
        $this->data['common_details']   = $this->repository->getById($id);
        $this->data['details'] = $this->repository->getOrderById($id);
        return [ 'data' => $this->data ];
    }

    public function submit(SubmitOrderRequest $request, $id)   
    {
        $orderItem = $this->repository->submit($request->validated(), $id );
        return new OrderResource($orderItem);    
    }

    public function destroy(Request $request)   
    {
        if($request->id) {
            $this->repository->delete($request->id);
            return response()->noContent();
        }
    }

    public function convert(Request $request, $id)  
    {
        $orderItem = $this->repository->update($request->except(['_token', '_method']), $id);
        if ( $request->status == "order") {
          return new OrderResource($orderItem);   
        } else if ( $request->status == "salesorder") {
          return $id; 
        } else if ( $request->status == "approve") {
            return $id; 
        } else if ( $request->status == "reject") {
            return $id; 
        }  else if ( $request->status == "invoice") {
             if (is_integer($orderItem)) {
                return $id;    
             } else {
                return $orderItem;    
             }
            
        } else {
          return $orderItem; 
        } 
    }

    public function store(StoreOrderRequest $request)
    {
        $id = $this->repository->create($request->validated());
        return $id;      
    }

    public function itemUpdate(Request $request, $id) {
        
        $id = $this->repository->itemUpdate($request->all(), $id);
        return $this->respond([
              'message' => 'Product updated successfully !',
            ]); 
    } 

    

}
