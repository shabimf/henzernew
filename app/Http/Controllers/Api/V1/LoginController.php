<?php

namespace App\Http\Controllers\Api\V1;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Http\Traits\CommonTrait;
use DB;  

class LoginController extends APIController
{
    use CommonTrait;
    
    public function login(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|min:4',
        ]);

        if ($validation->fails()) {
            return $this->throwValidation($validation->messages()->first());
        }

        $credentials = $request->only(['email', 'password']);

        try {
            if (! Auth::attempt($credentials)) {
                return $this->throwValidation(trans('api.messages.login.failed'));
            }

            $user = $request->user();

            $passportToken = $user->createToken('API Access Token');
            

            // Save generated token
            $passportToken->token->save();

            $token = $passportToken->accessToken;
            
            $user = Auth::user();
            $user->device_token = $request->input('device_token');
            $user->save();
            
            
        } catch (\Exception $e) {
            return $this->respondInternalError($e->getMessage());
        }

        return $this->respond([
            'message' => trans('api.messages.login.success'),
            'token' => $token,
            'branch_id' => $user->branch_id,
            'branch_name' => $this->FieldByID("branches", "name", $user->branch_id),
            'user_type' => $this->getRoleName(),
            'user_id' => $user->id
        ]);
    }

    public function me()
    {
        $this->data['data'] = Auth::guard()->user();
        $this->data['data']['user_type'] = $this->getRoleName();
        $this->data['data']['branch_name'] = $this->FieldByID('branches','name', $this->data['data']->branch_id);
        return response()->json($this->data);
    }

    public function logout(Request $request)
    {
        try {
            $request->user()->token()->revoke();
        } catch (\Exception $e) {
            return $this->respondInternalError($e->getMessage());
        }

        return $this->respond([
            'message' => trans('api.messages.logout.success'),
        ]);
    }
    
    public function changePassword (Request $request){

        if (!(\Hash::check($request->get('current_password'), Auth::user()->password))) {
            // The passwords matches
            return $this->respond([
                'message' => 'Your current password does not matches with the password you provided. Please try again.',
            ]);

        }

        if(strcmp($request->get('current_password'), $request->get('new_password')) == 0){
            //Current password and new password are same
            return $this->respond([
                'message' => 'New Password cannot be same as your current password. Please choose a different password.',
            ]);
        }

        $validatedData = $request->validate([
            'current_password' => 'required',
            'new_password' => 'required|string|min:6',
            'confirm_password' => 'required|same:new_password|min:6'
        ]);

        //Change Password
        $user = Auth::user();
        $user->password = bcrypt($request->get('new_password'));
        $user->save();
        return $this->respond([
            'message' => 'Password changed successfully !',
        ]);

    }

    public function update (Request $request) { 

        $validatedData = $request->validate([
            'first_name' => ['required'],
            'last_name' => ['required'],
            'email' => ['sometimes', 'required', 'email'],
        ]);

        $user = Auth::user();
        $user->first_name = $request->get('first_name');
        $user->last_name = $request->get('last_name');
        $user->email = $request->get('email');
        $user->save();
        
        return $this->respond([
            'message' => 'Profile updated successfully !',
        ]);
    }
    
    public function getUser (Request $request) { 
        $user = Auth::user();
        $branch_to = $this->getToBranchId();
        if ($this->getRoleName() == "Vendor" || $this->getRoleName() == "Sub Vendor") {
           $qry = DB::table('users')
                ->select('id','first_name','last_name')
                ->whereNotIn('id', [Auth::user()->id])
                ->where('branch_id', $branch_to)->get(); 
            
        } elseif ($this->getRoleName() == "Sourcing Team") {
           $qry = DB::table('users')
                ->leftjoin('branches', 'branches.id', '=', 'users.branch_id')
                ->select('users.id','users.first_name','users.last_name')
                ->whereNotIn('users.id', [Auth::user()->id])
                ->where('branches.branch_id', Auth::user()->branch_id)->get(); 
            
        }  else {
           $qry = DB::table('users')
                ->select('id','first_name','last_name')
                ->whereNotIn('id', [Auth::user()->id])
                ->get();  
        }
        return $qry;
    }
    
    


}
