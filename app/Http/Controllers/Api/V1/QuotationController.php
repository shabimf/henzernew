<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Requests\Frontend\Quotation\StoreQuotationRequest;
use App\Http\Requests\Frontend\Quotation\SubmitQuotationRequest;
use App\Http\Resources\QuotationResource;
use App\Repositories\Frontend\QuotationRepository;
use Illuminate\Http\Response;

class QuotationController extends APIController
{
   
    protected $repository;

    public function __construct(QuotationRepository $repository)
    {
        $this->repository = $repository;
    }
    
    public function index(Request $request)
    {
        $collection = $this->repository->retrieveList($request->all());

        return QuotationResource::collection($collection);
    }

    public function submit(SubmitQuotationRequest $request, $id)   
    {
        $enquiryItem = $this->repository->submitQuotation($request->validated(), $id );
        return new QuotationResource($enquiryItem);    
    }

    public function show($id)
    {
        $this->data['common_details'] = $this->repository->getById($id);
        $this->data['details'] = $this->repository->getQuotationById($id);
        return [ 'data' => $this->data ]; 
    }
    
    public function destroy(Request $request)   
    {
        if($request->id) {
            $this->repository->delete($request->id);
            return response()->noContent();
        }
    }

    public function convert(Request $request, $id)  
    { 
        $last_id = $this->repository->update($request->except(['_token', '_method']), $id);
        if ($request->status == "quotation") {
           return new QuotationResource($last_id);    
        } else {
            return $last_id;
        }
       
    }
    
    public function store(StoreQuotationRequest $request)
    {
        $id = $this->repository->create($request->validated());
        return $id;      
    }

    public function itemUpdate(Request $request, $id) {
        
        $id = $this->repository->itemUpdate($request->all(), $id);
        return $this->respond([
             'message' => 'Product updated successfully !',
         ]); 
     } 



}
