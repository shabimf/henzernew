<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Requests\Frontend\Price\StorePriceRequest;
use App\Http\Requests\Frontend\Price\ManagePriceRequest;
use App\Http\Resources\PriceResource;
use App\Repositories\Frontend\PriceRepository;
use Illuminate\Http\Response;

class PriceController extends APIController
{
   
    protected $repository;

    public function __construct(PriceRepository $repository)
    {
        $this->repository = $repository;
    }
    
    public function index(ManagePriceRequest $request)
    {
        $collection = $this->repository->retrieveList($request->all());

        return PriceResource::collection($collection);
    }

    public function store(StorePriceRequest $request)
    {
        $stock = $this->repository->create($request->validated());

        return (new PriceResource($stock))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

}
