<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Requests\Frontend\Invoice\StoreInvoiceRequest;
use App\Http\Requests\Frontend\Invoice\SubmitInvoiceRequest;
use App\Http\Resources\InvoiceResource;
use App\Repositories\Frontend\InvoiceRepository;
use Illuminate\Http\Response;

class InvoiceController extends APIController
{
   
    protected $repository;

    public function __construct(InvoiceRepository $repository)
    {
        $this->repository = $repository;
    }
    
    public function index(Request $request)
    {
        $collection = $this->repository->retrieveList($request->all());
        return InvoiceResource::collection($collection);
    }

    public function show($id)
    {
        
        $this->data['common_details']   = $this->repository->getById($id);
        $this->data['details'] = $this->repository->getInvoiceById($id);
        return [ 'data' => $this->data ];
    }
    
    public function store(StoreInvoiceRequest $request)
    {
        $id = $this->repository->create($request->validated());
        return $id;      
    }
    
    public function submit(SubmitInvoiceRequest $request, $id)   
    {
        $invoiceItem = $this->repository->submitInvoice($request->validated(), $id );
        return new InvoiceResource($invoiceItem);    
    }
    
    public function destroy(Request $request)   
    {
        if($request->id) {
            $this->repository->delete($request->id);
            return response()->noContent();
        }
    }
    
    public function convert(Request $request, $id)  
    {    
       $message = $this->repository->update($request->except(['_token', '_method']), $id);
        if (is_integer($message)) {
            return $id;  
        } else {
            return $message;
        }
    }

    public function itemUpdate(Request $request, $id) {
        
        $id = $this->repository->itemUpdate($request->all(), $id);
        return $this->respond([
             'message' => 'Product updated successfully !',
        ]); 
    } 

}
