<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Repositories\Frontend\ChatRepository;  
use App\Http\Resources\ChatResource;
use Illuminate\Http\Response;

class ChatController extends APIController
{
    /**
     * Repository.
     *
     * @var PagesRepository
     */
    protected $repository;

    /**
     * __construct.
     *
     * @param $repository
     */
    public function __construct(ChatRepository $repository)
    {
        $this->repository = $repository;
    }
    
    public function index(Request $request)
    {
        $collection = $this->repository->retrieveList($request->all());

        return ChatResource::collection($collection);
    }
    
    public function store(Request $request)
    {
        return (new ChatResource($this->repository->create($request->all())))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }
    
     public function show($id)
    {
        $this->repository->getById($id);
    }
}