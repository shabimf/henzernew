<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\Frontend\stock\StoreStockRequest;
use App\Http\Requests\Frontend\stock\ManageStockRequest;
use App\Http\Resources\StocksResource;
use App\Repositories\Frontend\StocksRepository;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

class StocksController extends APIController
{
   
    protected $repository;

    public function __construct(StocksRepository $repository)
    {
        $this->repository = $repository;
    }
    
    public function index(Request $request)
    {
        $collection = $this->repository->retrieveList($request->all());

        return StocksResource::collection($collection);
    }

    public function store(Request $request)
    {
        $stock = $this->repository->create($request->validated());

        return (new StocksResource($stock))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

}
