<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Traits\CommonTrait;
use App\Http\Resources\VendorResource;

class VendorController extends APIController
{

    use CommonTrait;
    
    public function index(Request $request)
    {
        if($this->getRoleName() == "Sub Vendor") {
        
           $collection = $this->VendorList($request);  
        } else {
          $collection = $this->SubVendorList($request);   
        }
        
        return VendorResource::collection($collection);
       
    }

   
}