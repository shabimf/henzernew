<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Requests\Frontend\Enquiry\ManageEnquiryRequest;
use App\Http\Requests\Frontend\Enquiry\SendEnquiryRequest;
use App\Http\Requests\Frontend\Enquiry\StoreEnquiryRequest;
use App\Http\Requests\Frontend\Enquiry\SubmitEnquiryRequest;
use App\Http\Resources\EnquiryResource;
use App\Repositories\Frontend\EnquiryRepository;  
use App\Models\Enquiry;
use Illuminate\Http\Response;

class EnquiryController extends APIController
{
   
    protected $repository;

    public function __construct(EnquiryRepository $repository)
    {
        $this->repository = $repository;
    }
    
    public function index(ManageEnquiryRequest $request)
    {
       
        $collection = $this->repository->retrieveEnquiryList($request->all());
        return EnquiryResource::collection($collection);
    }

    
    public function send(SendEnquiryRequest $request)
    {
        $enquiryItem = $this->repository->enquirySend($request->validated());
        return new EnquiryResource($enquiryItem);
    }

    public function show(EnquiryRepository $request, $id)
    {
        
        $this->data['common_details']   = $this->repository->getById($id);
        $this->data['details'] = $this->repository->getEnquiryById($id);
        return [ 'data' => $this->data ];
    }

    public function convert(Request $request, $id)
    {
        $last_id = $this->repository->updateEnquiry($request->except(['_token', '_method']), $id);
        if ($request->status == "quotation") {
            return $last_id;
        } else {
            return new EnquiryResource($last_id);
        }
    }

    public function store(StoreEnquiryRequest $request)
    {
        $id = $this->repository->createEnquiry($request->except(['_token', '_method', 'product_name']));
        return $id;       
    }

    public function submit(SubmitEnquiryRequest $request, $id)   
    {
        $enquiryItem = $this->repository->submitEnquiry($request->except(['_token', '_method','product_name']), $id);
        return new EnquiryResource($enquiryItem);    
    }

    public function destroy(Request $request)   
    {
        if($request->id) {
            $this->repository->delete($request->id);
            return response()->noContent();
        }
    }

    

}
