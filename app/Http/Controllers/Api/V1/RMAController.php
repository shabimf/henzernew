<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Repositories\Frontend\RMARepository;  
use App\Http\Requests\Frontend\RMA\StoreRMARequest;
use App\Http\Requests\Frontend\RMA\SubmitRMARequest;
use App\Http\Requests\Frontend\RMA\ManageRMARequest;
use App\Http\Resources\RMAResource;
use Illuminate\Http\Response;

class RMAController extends APIController
{
   
    protected $repository;

    public function __construct(RMARepository $repository)
    {
        $this->repository = $repository;
    }
    
    public function index(Request $request)
    {
        $collection = $this->repository->retrieveList($request->all());
        return RMAResource::collection($collection);
    }

    public function show($id)
    { 
        $this->data['common_details']   = $this->repository->getById($id);
        $this->data['details'] = $this->repository->getRMAById($id);
        return [ 'data' => $this->data ];
    }
    
    
}
