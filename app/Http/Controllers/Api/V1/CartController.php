<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Requests\Frontend\Cart\StoreCartRequest;
use App\Http\Resources\CartResource;
use App\Repositories\Frontend\CartRepository;  
use Illuminate\Http\Response;
use App\Models\CartItem;

class CartController extends APIController
{
   
    protected $repository;

    public function __construct(CartRepository $repository)
    {
        $this->repository = $repository;
    }
    
    public function index(Request $request)
    {
        $collection = $this->repository->retrieveList($request->all());

        return CartResource::collection($collection);
    }

    public function store(StoreCartRequest $request)
    {
        $stock = $this->repository->create($request->validated());

        return (new CartResource($stock))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function destroy(Request $request)
    {
        $this->repository->delete($request->id);

        return response()->noContent();
    }

    public function update(UpdateEnquiryRequest $request, CartItem $catitem)
    {
    
        $cartItem = $this->repository->update($catitem, $request->validated());

        return new CartResource($cartItem);
    }
    
    public function itemUpdate(Request $request, $id) {
      
      $cartItem = $this->repository->itemUpdate($request->all(), $id);

      return new CartResource($cartItem);
    }

}
