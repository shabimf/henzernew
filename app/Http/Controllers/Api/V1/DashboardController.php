<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Traits\CommonTrait;

class DashboardController extends APIController
{

    use CommonTrait;
    
    public function index(Request $request)
    {

        return $this->dashboardList($request);
       
    }

    public function fetchPrice()
    {
        $data = $this->get_price_calender();
        return [ 'data' => $data ];
    }
}