<?php
namespace App\Http\Controllers\Backend\Brands;

use App\Http\Controllers\Controller;
use App\Http\Traits\GetTrait;
use App\Http\Traits\CommonTrait;
use App\Http\Responses\RedirectResponse;
use App\Models\Brand;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use Auth;


class BrandsController extends Controller
{
    use GetTrait,CommonTrait;

    public function __construct()
    {
        View::share('js', ['brands']);
    }
    
    public function index()
    {
        return view('backend.brands.index');
    }

    public function create()
    {
        return view('backend.brands.create');
    }

    public function store(Request $request)
    {
        $post = new Brand();

        $validator = $this->validateBrandForm($request);
      
        if ($validator->fails()) 
            return redirect()->back()->withInput()->withErrors($validator);

        if (Brand::where('name', $request['name'])->first()) {
            return redirect()->back()->with("success",'Brand name already exist.');
        }

        $post = $this->GetCountryForm($post, $request);
        $post->created_by = Auth::id();

        if ($post->save()){
            return redirect()->back()->with("success","The brand was successfully created.");
        }

    }

    public function update(Request $request, $id)
    {

        $validator = $this->validateBrandForm($request);
        

        if ($validator->fails()) 
            return redirect()->back()->withInput()->withErrors($validator);

        if (Brand::where('name', $request['name'])->where('id', '!=', $id)->first()) {
            return redirect()->back()->with("success",'Brand name already exist.');
        }

        $post = Brand::find($id);

        $post = $this->GetCountryForm($post, $request);
        $post->updated_by = Auth::id();

        if ($post->save()){
            return redirect()->back()->with("success","The brand was successfully updated.");
        }
    }

    public function edit($id)
    {
        $this->data['edit_brand'] = Brand::find($id);
        $this->data['edit_id'] = $id;
        return view('backend.brands.create', $this->data);
    }

   

    public function destroy($id)
    {
        $brand = Brand::find($id);
        $brand->delete();
        return new RedirectResponse(route('admin.brands.index'), ['flash_success' => __('The Brand was successfully deleted.')]);
    }
    

}
