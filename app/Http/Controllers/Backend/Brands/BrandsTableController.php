<?php

namespace App\Http\Controllers\Backend\Brands;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use DB;
use Yajra\DataTables\Facades\DataTables;

class BrandsTableController extends Controller
{
    public function __invoke()
    {
        $queries = Brand::leftjoin('users', 'users.id', '=', 'brands.created_by')
                        ->select([
                            'brands.id',
                            'brands.name',
                            'brands.status',
                            'brands.created_at',
                            'users.first_name as created_by',
                        ]);
        
        
    	return Datatables::of($queries)
            ->filterColumn('status', function ($query, $keyword) {
                if (in_array(strtolower($keyword), ['active', 'inactive'])) {
                    $query->where('brands.status', (strtolower($keyword) == 'active') ? 1 : 0);
                }
            })
            ->filterColumn('created_by', function ($query, $keyword) {
                $query->whereRaw('users.first_name like ?', ["%{$keyword}%"]);
            })
            ->editColumn('status', function ($country) {
                return $country->status_label;
            })
            ->editColumn('created_at', function ($country) {
                return $country->created_at->toDateString();
            })
            ->addColumn('actions', function ($country) {
                return $country->action_buttons;
            })
            ->escapeColumns(['name'])
            ->make(true);
    }
}


