<?php
namespace App\Http\Controllers\Backend\Countries;

use App\Http\Controllers\Controller;
use App\Http\Traits\GetTrait;
use App\Http\Traits\CommonTrait;
use App\Http\Responses\RedirectResponse;
use App\Models\Country;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use Auth;


class CountriesController extends Controller
{
    use GetTrait,CommonTrait;

    public function __construct()
    {
        $this->data['list_currency'] = $this->NameAndID('currency');
        View::share('js', ['countries']);
    }
    
    public function index()
    {
        return view('backend.countries.index');
    }

    public function create()
    {
        return view('backend.countries.create', $this->data);
    }

    public function store(Request $request)
    {
        $post = new Country();

        $validator = $this->validateCountryForm($request);
      
        if ($validator->fails()) 
            return redirect()->back()->withInput()->withErrors($validator);

        if (Country::where('name', $request['name'])->first()) {
            return redirect()->back()->with("success",'Country name already exist.');
        }

        $post = $this->GetCountryForm($post, $request);
        $post->created_by = Auth::id();

        if ($post->save()){
            return redirect()->back()->with("success","The country was successfully created.");
        }

    }

    public function update(Request $request, $id)
    {

        $validator = $this->validateCountryForm($request);
        

        if ($validator->fails()) 
            return redirect()->back()->withInput()->withErrors($validator);

        if (Country::where('name', $request['name'])->where('id', '!=', $id)->first()) {
            return redirect()->back()->with("success",'Country name already exist.');
        }

        $post = Country::find($id);

        $post = $this->GetCountryForm($post, $request);
        $post->updated_by = Auth::id();

        if ($post->save()){
            return redirect()->back()->with("success","The country was successfully updated.");
        }
    }

    public function edit($id)
    {

        $this->data['edit_country'] = Country::find($id);
        $this->data['edit_id'] = $id;
        return view('backend.countries.create', $this->data);
    }

   

    public function destroy($id)
    {
        $country = Country::find($id);
        $country->delete();
        return new RedirectResponse(route('admin.countries.index'), ['flash_success' => __('alerts.backend.country.deleted')]);
    }

}
