<?php

namespace App\Http\Controllers\Backend\Common;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Traits\CommonTrait;

class CommonController extends Controller
{
    use CommonTrait;

    public function index(Request $request)
    {
        $brand_id = $request->id_brand;   
        $models = $this->getModel($brand_id);
        return response()->json($models);
    }

    public function getCity(Request $request)
    {     
        $country_id = $request->id_country;   
        $cities = $this->getCities($country_id);
        return response()->json($cities);
    }

    public function getSerie(Request $request)
    {     
        $model_id = $request->id_model;   
        $series = $this->getSeries($model_id);
        return response()->json($series);
    }

    public function getAttribute(Request $request)
    {  
        $qry = $request->input('query');     
        $attributes = $this->getAttributes($qry);
        return $attributes;
    }

    public function getBranch(Request $request)
    {  
        $type = $request->input('type');
        if($type == 2) {
            $data = $this->VendorList();
        }  else {
            $data = $this->SourcingList();
        }   
        
        return response()->json($data);
    }


    


    


    

}
