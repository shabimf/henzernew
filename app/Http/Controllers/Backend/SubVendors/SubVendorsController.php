<?php
namespace App\Http\Controllers\Backend\SubVendors;

use App\Http\Controllers\Controller;
use App\Http\Traits\GetTrait;
use App\Http\Traits\CommonTrait;
use App\Http\Responses\RedirectResponse;
use App\Models\Vendor;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use Auth;


class SubVendorsController extends Controller
{
    use GetTrait,CommonTrait;

    public function __construct()
    {
        $this->data['list_vendor'] = $this->getVendors();
        View::share('js', ['subvendors']);   
    }
    
    public function index()
    {
        return view('backend.subvendors.index');
    }

    public function create()
    {
        return view('backend.subvendors.create', $this->data);
    }

    public function store(Request $request)
    {
        $post = new Vendor();

        $validator = $this->validateSubVendorForm($request, 0);
      
        if ($validator->fails()) 
            return redirect()->back()->withInput()->withErrors($validator);

        $post = $this->GetSubVendorForm($post, $request);
        $post->created_by = Auth::id();

        if ($post->save()){
            return redirect()->back()->with("success","The subvendor was successfully created.");
        }

    }

    public function update(Request $request, $id)
    {

        $validator = $this->validateSubVendorForm($request, $id);
        

        if ($validator->fails()) 
            return redirect()->back()->withInput()->withErrors($validator);

        $post = Vendor::find($id);

        $post = $this->GetSubVendorForm($post, $request);
        $post->updated_by = Auth::id();

        if ($post->save()){
            return redirect()->back()->with("success","The subvendor was successfully updated.");
        }
    }

    public function edit($id)
    {
        $this->data['edit_vendor'] = Vendor::find($id);
        $this->data['edit_id'] = $id;
        return view('backend.subvendors.create', $this->data);
    }

   

    public function destroy($id)
    {
        $subvendor = Vendor::find($id);
        $subvendor->delete();
        return new RedirectResponse(route('admin.subvendors.index'), ['flash_success' => __('The subvendor was successfully deleted.')]);
    }
    

}
