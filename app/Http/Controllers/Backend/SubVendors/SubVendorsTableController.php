<?php

namespace App\Http\Controllers\Backend\SubVendors;

use App\Http\Controllers\Controller;
use App\Models\Vendor;
use DB;
use Yajra\DataTables\Facades\DataTables;

class SubVendorsTableController extends Controller
{
    public function __invoke()
    {
        $queries = Vendor::leftjoin('users', 'users.id', '=', 'vendors.created_by')
                       ->join('vendors as b', 'vendors.id', '=', 'b.vendor_id')
                        ->where('b.vendor_id', '>', '0')
                        ->select([
                            'b.id',
                            'b.company_name',
                            'b.contact_person',
                            'b.email',
                            'b.phone_number',
                            'b.status',
                            'b.created_at',
                            'users.first_name as created_by',
                            'vendors.company_name as vendor_name'
                        ]);
        
        
    	return Datatables::of($queries)
            ->filterColumn('status', function ($query, $keyword) {
                if (in_array(strtolower($keyword), ['active', 'inactive'])) {
                    $query->where('vendors.status', (strtolower($keyword) == 'active') ? 1 : 0);
                }
            })
            ->filterColumn('created_by', function ($query, $keyword) {
                $query->whereRaw('users.first_name like ?', ["%{$keyword}%"]);
            })
            ->filterColumn('vendor_name', function ($query, $keyword) {
                $query->whereRaw('vendors.company_name like ?', ["%{$keyword}%"]);
            })
            ->filterColumn('company_name', function ($query, $keyword) {
                $query->whereRaw('b.company_name like ?', ["%{$keyword}%"]);
            })
            ->filterColumn('company_name', function ($query, $keyword) {
                $query->whereRaw('b.company_name like ?', ["%{$keyword}%"]);
            })
            ->filterColumn('email', function ($query, $keyword) {
                $query->whereRaw('b.email like ?', ["%{$keyword}%"]);
            })
            ->filterColumn('phone_number', function ($query, $keyword) {
                $query->whereRaw('b.phone_number like ?', ["%{$keyword}%"]);
            })
           
            ->editColumn('status', function ($subvendor) {
                return $subvendor->status_label;
            })
            ->editColumn('created_at', function ($subvendor) {
                return $subvendor->created_at->toDateString();
            })
            ->addColumn('actions', function ($subvendor) {
                return '
                <div class="btn-group action-btn">
                    <a class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" href="'.route('admin.subvendors.edit', $subvendor->id).'" title="Edit">
                        <i class="fas fa-edit"></i>
                    </a>
                    <a class="btn btn-primary btn-danger btn-sm" href="'.route('admin.subvendors.destroy', $subvendor->id).'"
                    data-method="delete"
                    data-trans-button-cancel="'.trans('buttons.general.cancel').'"
                    data-trans-button-confirm="'.trans('buttons.general.crud.delete').'"
                    data-trans-title="'.trans('strings.backend.general.are_you_sure').'"><i class="fa fa-trash" data-toggle="tooltip" data-placement="top" title="'.trans('buttons.general.crud.delete').'"></i></a>
                </div>';
            })
            ->escapeColumns(['name'])
            ->make(true);
    }
}
