<?php
namespace App\Http\Controllers\Backend\Models;

use App\Http\Controllers\Controller;
use App\Http\Traits\GetTrait;
use App\Http\Traits\CommonTrait;
use App\Http\Responses\RedirectResponse;
use App\Models\Model;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use Auth;


class ModelsController extends Controller
{
    use GetTrait,CommonTrait;

    public function __construct()
    {
        $this->data['list_brand'] = $this->NameAndID('brands');
        View::share('js', ['models']);
    }
    
    public function index()
    {
        return view('backend.models.index');
    }

    public function create()
    {
        return view('backend.models.create', $this->data);
    }

    public function store(Request $request)
    {
        $post = new Model();

        $validator = $this->validateModelForm($request);
      
        if ($validator->fails()) 
            return redirect()->back()->withInput()->withErrors($validator);

        if (Model::where('name', $request['name'])->where('brand_id', $request['brand_id'])->first()) {
            return redirect()->back()->with("error",'Model name already exist.');
        }

        $post = $this->GetModelForm($post, $request);
        $post->created_by = Auth::id();

        if ($post->save()){
            return redirect()->back()->with("success","The model was successfully created.");
        }

    }

    public function update(Request $request, $id)
    {

        $validator = $this->validateModelForm($request);
        

        if ($validator->fails()) 
            return redirect()->back()->withInput()->withErrors($validator);

        if (Model::where('name', $request['name'])->where([['id', '!=', $id], ['brand_id', $request['brand_id']]])->first()) {
            return redirect()->back()->with("success",'Model name already exist.');
        }

        $post = Model::find($id);

        $post = $this->GetModelForm($post, $request);
        $post->updated_by = Auth::id();

        if ($post->save()){
            return redirect()->back()->with("success","The model was successfully updated.");
        }
    }

    public function edit($id)
    {

        $this->data['edit_model'] = Model::find($id);
        $this->data['edit_id'] = $id;
        return view('backend.models.create', $this->data);
    }

   

    public function destroy($id)
    {
        $model = Model::find($id);
        $model->delete();
        return new RedirectResponse(route('admin.models.index'), ['flash_success' => __('The model was successfully deleted.')]);
    }

}
