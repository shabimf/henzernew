<?php

namespace App\Http\Controllers\Backend\Models;

use App\Http\Controllers\Controller;
use App\Models\Model;
use DB;
use Yajra\DataTables\Facades\DataTables;

class ModelsTableController extends Controller
{
    public function __invoke()
    {
        $queries = Model::leftjoin('users', 'users.id', '=', 'models.created_by')
                        ->leftjoin('brands', 'brands.id', '=', 'models.brand_id')
                        ->select([
                            'models.id',
                            'models.name',
                            'models.status',
                            'models.created_at',
                            'users.first_name as created_by',
                            'brands.name as brand_name',
                        ]);
        
        
    	return Datatables::of($queries)
            ->filterColumn('status', function ($query, $keyword) {
                if (in_array(strtolower($keyword), ['active', 'inactive'])) {
                    $query->where('models.status', (strtolower($keyword) == 'active') ? 1 : 0);
                }
            })
            ->filterColumn('created_by', function ($query, $keyword) {
                $query->whereRaw('users.first_name like ?', ["%{$keyword}%"]);
            })
            ->filterColumn('brand_name', function ($query, $keyword) {
                $query->whereRaw('brands.name like ?', ["%{$keyword}%"]);
            })
            ->editColumn('status', function ($country) {
                return $country->status_label;
            })
            ->editColumn('created_at', function ($country) {
                return $country->created_at->toDateString();
            })
            ->addColumn('actions', function ($country) {
                return $country->action_buttons;
            })
            ->escapeColumns(['name'])
            ->make(true);
    }
}
