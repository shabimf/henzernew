<?php

namespace App\Http\Controllers\Backend\Branches;

use App\Http\Controllers\Controller;
use App\Models\Branch;
use DB;
use Yajra\DataTables\Facades\DataTables;

class BranchesTableController extends Controller
{
    public function __invoke()
    {
        $queries =  Branch::leftjoin('users', 'users.id', '=', 'branches.created_by')
                        ->leftjoin('countries', 'countries.id', '=', 'branches.country_id')
                        ->leftjoin('cities', 'cities.id', '=', 'branches.city_id')
                        ->select([
                            'branches.id',
                            'branches.name',
                            'branches.code',
                            'branches.status',
                            'branches.created_at',
                            'branches.type',
                            'users.first_name as created_by',
                            'countries.name as country_name',
                            'cities.name as city_name',
                        ]);
        
        
    	return Datatables::of($queries)
            ->filterColumn('status', function ($query, $keyword) {
                if (in_array(strtolower($keyword), ['active', 'inactive'])) {
                    $query->where('branches.status', (strtolower($keyword) == 'active') ? 1 : 0);
                }
            })
            ->filterColumn('created_by', function ($query, $keyword) {
                $query->whereRaw('users.first_name like ?', ["%{$keyword}%"]);
            })
            ->filterColumn('country_name', function ($query, $keyword) {
                $query->whereRaw('countries.name like ?', ["%{$keyword}%"]);
            })
            ->filterColumn('city_name', function ($query, $keyword) {
                $query->whereRaw('cities.name like ?', ["%{$keyword}%"]);
            })
            ->editColumn('status', function ($branch) {
                return $branch->status_label;
            })
            ->editColumn('type', function ($branch) {
                if ($branch->type == 1) return "Sourcing Team";
                elseif ($branch->type == 2) return "Vendor";
                elseif ($branch->type == 3) return "Subvendor";
            })
            ->editColumn('created_at', function ($branch) {
                return $branch->created_at->toDateString();
            })
            ->addColumn('actions', function ($branch) {
                return $branch->action_buttons;
            })
            ->escapeColumns(['name'])
            ->make(true);
    }
}
