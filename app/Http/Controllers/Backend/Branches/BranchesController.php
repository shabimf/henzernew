<?php
namespace App\Http\Controllers\Backend\Branches;

use App\Http\Controllers\Controller;
use App\Http\Traits\GetTrait;
use App\Http\Traits\CommonTrait;
use App\Http\Responses\RedirectResponse;
use App\Models\Branch;
use App\Models\BranchDetails;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use Auth;
use DB;


class BranchesController extends Controller 
{
    use GetTrait,CommonTrait;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->data['list_country'] = $this->NameAndID('countries');
            $this->data['vendors'] = $this->VendorList();
            $this->data['sources'] = $this->SourcingList();
            return $next($request);
        });
        View::share('js', ['branches']);
    }
    
    public function index()
    {
        return view('backend.branches.index');
    }

    public function create()
    {
        $this->data['list_city'] = $this->getModel(0)->toArray();
        return view('backend.branches.create', $this->data);
    }

    public function store(Request $request)
    {
        

        $validator = $this->validateBranchesForm($request, 0);
      
        if ($validator->fails()) 
            return redirect()->back()->withInput()->withErrors($validator);

        if (Branch::where('name', $request['name'])->where([['country_id', $request['country_id']], ['city_id', $request['city_id']]])->first()) {
            return redirect()->back()->with("error",'Branch name already exist.');
        }

        $post = $this->GetBranchForm($request);
        $post['created_by'] = Auth::id();
        DB::beginTransaction();
        $id = DB::table('branches')->insertGetId($post);
        $post_branch = $this->GetBranchDetailsForm($request);
        $post_branch['parent_id'] = $id;

        if ($id){
            DB::table('branch_details')->insertGetId($post_branch);
            DB::commit();
            return redirect()->back()->with("success","The branch was successfully created.");
        }

    }

    public function update(Request $request, $id)
    {

        $validator = $this->validateBranchesForm($request, $id);
        

        if ($validator->fails()) 
            return redirect()->back()->withInput()->withErrors($validator);

        if (Branch::where('name', $request['name'])->where([['id', '!=', $id], ['country_id', $request['country_id']], ['city_id', $request['city_id']]])->first()) {
            return redirect()->back()->with("error",'Branch name already exist.');
        }

        $post = $this->GetBranchForm($request);
        DB::beginTransaction();
        DB::table('branches')->where('id', $id)->update($post);
        $post_branch = $this->GetBranchDetailsForm($request);

        if ($id){
            DB::table('branch_details')->where('parent_id', $id)->update($post_branch);
            DB::commit();
            return redirect()->back()->with("success","The branch was successfully updated.");
        }
    }

    public function edit($id)
    {

        $this->data['edit_branches'] = Branch::find($id);
        $this->data['edit_id'] = $id;
        $this->data['list_city'] = $this->getCities($this->data['edit_branches']['country_id'])->toArray();
        $this->data['parent_details'] = $this->getChildDetails('branch_details', $id);
        return view('backend.branches.create', $this->data);
    }

   

    public function destroy($id)
    {
        $branches = Branch::find($id);
        $branches->delete();
        return new RedirectResponse(route('admin.branches.index'), ['flash_success' => __('The branch was successfully deleted.')]);
    }

}
