<?php

namespace App\Http\Controllers\Backend\Cities;

use App\Http\Controllers\Controller;
use App\Models\city;
use DB;
use Yajra\DataTables\Facades\DataTables;

class CitiesTableController extends Controller
{
    public function __invoke()
    {
        $queries = City::leftjoin('users', 'users.id', '=', 'cities.created_by')
                        ->leftjoin('countries', 'countries.id', '=', 'cities.country_id')
                        ->select([
                            'cities.id',
                            'cities.name',
                            'cities.status',
                            'cities.created_at',
                            'users.first_name as created_by',
                            'countries.name as country_name',
                        ]);
        
        
    	return Datatables::of($queries)
            ->filterColumn('status', function ($query, $keyword) {
                if (in_array(strtolower($keyword), ['active', 'inactive'])) {
                    $query->where('cities.status', (strtolower($keyword) == 'active') ? 1 : 0);
                }
            })
            ->filterColumn('created_by', function ($query, $keyword) {
                $query->whereRaw('users.first_name like ?', ["%{$keyword}%"]);
            })
            ->filterColumn('country_name', function ($query, $keyword) {
                $query->whereRaw('countries.name like ?', ["%{$keyword}%"]);
            })
            ->editColumn('status', function ($country) {
                return $country->status_label;
            })
            ->editColumn('created_at', function ($country) {
                return $country->created_at->toDateString();
            })
            ->addColumn('actions', function ($country) {
                return $country->action_buttons;
            })
            ->escapeColumns(['name'])
            ->make(true);
    }
}
