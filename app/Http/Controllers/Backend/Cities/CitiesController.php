<?php
namespace App\Http\Controllers\Backend\Cities;

use App\Http\Controllers\Controller;
use App\Http\Traits\GetTrait;
use App\Http\Traits\CommonTrait;
use App\Http\Responses\RedirectResponse;
use App\Models\City;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use Auth;


class CitiesController extends Controller
{
    use GetTrait,CommonTrait;

    public function __construct()
    {
        $this->data['list_country'] = $this->NameAndID('countries');
        View::share('js', ['cities']);
    }
    
    public function index()
    {
        return view('backend.cities.index');
    }

    public function create()
    {
        
        return view('backend.cities.create', $this->data);
    }

    public function store(Request $request)
    {
        $post = new City();

        $validator = $this->validateCitiesForm($request);
      
        if ($validator->fails()) 
            return redirect()->back()->withInput()->withErrors($validator);

        if (City::where('name', $request['name'])->where('country_id', $request['country_id'])->first()) {
            return redirect()->back()->with("error",'Cities name already exist.');
        }

        $post = $this->GetCityForm($post, $request);
        $post->created_by = Auth::id();

        if ($post->save()){
            return redirect()->back()->with("success","The cities was successfully created.");
        }

    }

    public function update(Request $request, $id)
    {

        $validator = $this->validateCitiesForm($request);
        

        if ($validator->fails()) 
            return redirect()->back()->withInput()->withErrors($validator);

        if (City::where('name', $request['name'])->where([['id', '!=', $id], ['country_id', $request['country_id']]])->first()) {
            return redirect()->back()->with("success",'Cities name already exist.');
        }

        $post = City::find($id);

        $post = $this->GetCityForm($post, $request);
        $post->updated_by = Auth::id();

        if ($post->save()){
            return redirect()->back()->with("success","The cities was successfully updated.");
        }
    }

    public function edit($id)
    {

        $this->data['edit_city'] = City::find($id);
        $this->data['edit_id'] = $id;
        return view('backend.cities.create', $this->data);
    }

   

    public function destroy($id)
    {
        $city = City::find($id);
        $city->delete();
        return new RedirectResponse(route('admin.cities.index'), ['flash_success' => __('The cities was successfully deleted.')]);
    }

}
