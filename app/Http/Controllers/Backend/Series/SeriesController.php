<?php
namespace App\Http\Controllers\Backend\Series;

use App\Http\Controllers\Controller;
use App\Http\Traits\GetTrait;
use App\Http\Traits\CommonTrait;
use App\Http\Responses\RedirectResponse;
use App\Models\Serie;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use Auth;


class SeriesController extends Controller
{
    use GetTrait,CommonTrait;

    public function __construct()
    {
        $this->data['list_brand'] = $this->NameAndID('brands');
        View::share('js', ['series']);
    }
    
    public function index()
    {
        return view('backend.series.index');
    }

    public function create()
    {
        $this->data['list_model'] = $this->getModel(0)->toArray();
        return view('backend.series.create', $this->data);
    }

    public function store(Request $request)
    {
        $post = new Serie();

        $validator = $this->validateSeriesForm($request);
      
        if ($validator->fails()) 
            return redirect()->back()->withInput()->withErrors($validator);

        if (Serie::where('name', $request['name'])->where([['model_id', $request['model_id']], ['brand_id', $request['brand_id']]])->first()) {
            return redirect()->back()->with("error",'Series name already exist.');
        }

        $post = $this->GetSeriesForm($post, $request);
        $post->created_by = Auth::id();

        if ($post->save()){
            return redirect()->back()->with("success","The series was successfully created.");
        }

    }

    public function update(Request $request, $id)
    {

        $validator = $this->validateSeriesForm($request);
        

        if ($validator->fails()) 
            return redirect()->back()->withInput()->withErrors($validator);

        if (Serie::where('name', $request['name'])->where([['id', '!=', $id], ['brand_id', $request['brand_id']], ['model_id', $request['model_id']]])->first()) {
            return redirect()->back()->with("success",'Series name already exist.');
        }

        $post = Serie::find($id);

        $post = $this->GetSeriesForm($post, $request);
        $post->updated_by = Auth::id();

        if ($post->save()){
            return redirect()->back()->with("success","The series was successfully updated.");
        }
    }

    public function edit($id)
    {

        $this->data['edit_series'] = Serie::find($id);
        $this->data['edit_id'] = $id;
        $this->data['list_model'] = $this->getModel($this->data['edit_series']['brand_id'])->toArray();
        return view('backend.series.create', $this->data);
    }

   

    public function destroy($id)
    {
        $series = Serie::find($id);
        $series->delete();
        return new RedirectResponse(route('admin.series.index'), ['flash_success' => __('The series was successfully deleted.')]);
    }

}
