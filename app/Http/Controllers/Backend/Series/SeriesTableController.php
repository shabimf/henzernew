<?php

namespace App\Http\Controllers\Backend\Series;

use App\Http\Controllers\Controller;
use App\Models\Serie;
use DB;
use Yajra\DataTables\Facades\DataTables;

class SeriesTableController extends Controller
{
    public function __invoke()
    {
        $queries = Serie::leftjoin('users', 'users.id', '=', 'series.created_by')
                        ->leftjoin('brands', 'brands.id', '=', 'series.brand_id')
                        ->leftjoin('models', 'models.id', '=', 'series.model_id')
                        ->select([
                            'series.id',
                            'series.name',
                            'series.status',
                            'series.created_at',
                            'users.first_name as created_by',
                            'brands.name as brand_name',
                            'models.name as model_name',
                        ]);
        
        
    	return Datatables::of($queries)
            ->filterColumn('status', function ($query, $keyword) {
                if (in_array(strtolower($keyword), ['active', 'inactive'])) {
                    $query->where('series.status', (strtolower($keyword) == 'active') ? 1 : 0);
                }
            })
            ->filterColumn('created_by', function ($query, $keyword) {
                $query->whereRaw('users.first_name like ?', ["%{$keyword}%"]);
            })
            ->filterColumn('brand_name', function ($query, $keyword) {
                $query->whereRaw('brands.name like ?', ["%{$keyword}%"]);
            })
            ->filterColumn('model_name', function ($query, $keyword) {
                $query->whereRaw('models.name like ?', ["%{$keyword}%"]);
            })
            ->editColumn('status', function ($country) {
                return $country->status_label;
            })
            ->editColumn('created_at', function ($country) {
                return $country->created_at->toDateString();
            })
            ->addColumn('actions', function ($country) {
                return $country->action_buttons;
            })
            ->escapeColumns(['name'])
            ->make(true);
    }
}
