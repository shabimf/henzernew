<?php

namespace App\Http\Controllers\Backend\Attributes;

use App\Http\Controllers\Controller;
use App\Models\Attribute;
use DB;
use Yajra\DataTables\Facades\DataTables;

class AttributesTableController extends Controller
{
    public function __invoke()
    {
        $queries = Attribute::leftjoin('users', 'users.id', '=', 'attributes.created_by')
                        ->select([
                            'attributes.id',
                            'attributes.name',
                            'attributes.status',
                            'attributes.created_at',
                            'users.first_name as created_by',
                        ]);
        
        
    	return Datatables::of($queries)
            ->filterColumn('status', function ($query, $keyword) {
                if (in_array(strtolower($keyword), ['active', 'inactive'])) {
                    $query->where('attributes.status', (strtolower($keyword) == 'active') ? 1 : 0);
                }
            })
            ->filterColumn('created_by', function ($query, $keyword) {
                $query->whereRaw('users.first_name like ?', ["%{$keyword}%"]);
            })
            ->editColumn('status', function ($attribute) {
                return $attribute->status_label;
            })
            ->editColumn('created_at', function ($attribute) {
                return $attribute->created_at->toDateString();
            })
            ->addColumn('actions', function ($attribute) {
                return $attribute->action_buttons;
            })
            ->escapeColumns(['name'])
            ->make(true);
    }
}
