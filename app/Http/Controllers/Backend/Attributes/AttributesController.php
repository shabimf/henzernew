<?php
namespace App\Http\Controllers\Backend\Attributes;

use App\Http\Controllers\Controller;
use App\Http\Traits\GetTrait;
use App\Http\Traits\CommonTrait;
use App\Http\Responses\RedirectResponse;
use App\Models\Attribute;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use Auth;


class AttributesController extends Controller
{
    use GetTrait,CommonTrait;

    public function __construct()
    {
        View::share('js', ['attributes']);
    }
    
    public function index()
    {
        return view('backend.attributes.index');
    }

    public function create()
    {
        return view('backend.attributes.create');
    }

    public function store(Request $request)
    {
        $post = new Attribute();

        $validator = $this->validateAttributeForm($request, 0);
      
        if ($validator->fails()) 
            return redirect()->back()->withInput()->withErrors($validator);

        if (Attribute::where('name', $request['name'])->first()) {
            return redirect()->back()->with("error",'Attribute name already exist.');
        }

        $post = $this->GetCountryForm($post, $request);
        $post->created_by = Auth::id();

        if ($post->save()){
            return redirect()->back()->with("success","The attribute was successfully created.");
        }

    }

    public function update(Request $request, $id)
    {

        $validator = $this->validateAttributeForm($request, $id);
        

        if ($validator->fails()) 
            return redirect()->back()->withInput()->withErrors($validator);

        if (Attribute::where('name', $request['name'])->where('id', '!=', $id)->first()) {
            return redirect()->back()->with("success",'Attribute name already exist.');
        }

        $post = Attribute::find($id);

        $post = $this->GetCountryForm($post, $request);
        $post->updated_by = Auth::id();

        if ($post->save()){
            return redirect()->back()->with("success","The attribute was successfully updated.");
        }
    }

    public function edit($id)
    {
        $this->data['edit_attribute'] = Attribute::find($id);
        $this->data['edit_id'] = $id;
        return view('backend.attributes.create', $this->data);
    }

   

    public function destroy($id)
    {
        $attribute = Attribute::find($id);
        $attribute->delete();
        return new RedirectResponse(route('admin.attributes.index'), ['flash_success' => __('The attribute was successfully deleted.')]);
    }
    

}
