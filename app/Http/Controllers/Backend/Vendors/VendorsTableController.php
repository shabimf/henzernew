<?php

namespace App\Http\Controllers\Backend\Vendors;

use App\Http\Controllers\Controller;
use App\Models\Vendor;
use DB;
use Yajra\DataTables\Facades\DataTables;

class VendorsTableController extends Controller
{
    public function __invoke()
    {
        $queries = Vendor::leftjoin('users', 'users.id', '=', 'vendors.created_by')
                        ->leftjoin('branches', 'branches.id', '=', 'vendors.branch_id')
                        ->where('vendors.vendor_id', '=', '0')
                        ->select([
                            'vendors.id',
                            'vendors.company_name',
                            'vendors.contact_person',
                            'vendors.email',
                            'vendors.phone_number',
                            'vendors.status',
                            'vendors.created_at',
                            'users.first_name as created_by',
                            'branches.name as branch_name',
                        ]);
        
        
    	return Datatables::of($queries)
            ->filterColumn('status', function ($query, $keyword) {
                if (in_array(strtolower($keyword), ['active', 'inactive'])) {
                    $query->where('vendors.status', (strtolower($keyword) == 'active') ? 1 : 0);
                }
            })
            ->filterColumn('created_by', function ($query, $keyword) {
                $query->whereRaw('users.first_name like ?', ["%{$keyword}%"]);
            })
            ->filterColumn('branch_name', function ($query, $keyword) {
                $query->whereRaw('branches.name like ?', ["%{$keyword}%"]);
            })
           
            ->editColumn('status', function ($vendor) {
                return $vendor->status_label;
            })
            ->editColumn('created_at', function ($vendor) {
                return $vendor->created_at->toDateString();
            })
            ->addColumn('actions', function ($vendor) {
                return $vendor->action_buttons;
            })
            ->escapeColumns(['name'])
            ->make(true);
    }
}
