<?php
namespace App\Http\Controllers\Backend\Vendors;

use App\Http\Controllers\Controller;
use App\Http\Traits\GetTrait;
use App\Http\Traits\CommonTrait;
use App\Http\Responses\RedirectResponse;
use App\Models\Vendor;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use Auth;


class VendorsController extends Controller
{
    use GetTrait,CommonTrait;

    public function __construct()
    {
        $this->data['list_branch'] = $this->NameAndID('branches');
        View::share('js', ['vendors']); 
    }
    
    public function index()
    {
        return view('backend.vendors.index');
    }

    public function create()
    {
        return view('backend.vendors.create', $this->data);
    }

    public function store(Request $request)
    {
        $post = new Vendor();

        $validator = $this->validateVendorForm($request);
      
        if ($validator->fails()) 
            return redirect()->back()->withInput()->withErrors($validator);

        $post = $this->GetVendorForm($post, $request);
        $post->created_by = Auth::id();

        if ($post->save()){
            return redirect()->back()->with("success","The vendor was successfully created.");
        }

    }

    public function update(Request $request, $id)
    {

        $validator = $this->validateEditVendorForm($request, $id);
        

        if ($validator->fails()) 
            return redirect()->back()->withInput()->withErrors($validator);

        $post = Vendor::find($id);

        $post = $this->GetVendorForm($post, $request);
        $post->updated_by = Auth::id();

        if ($post->save()){
            return redirect()->back()->with("success","The vendor was successfully updated.");
        }
    }

    public function edit($id)
    {
        $this->data['edit_vendor'] = Vendor::find($id);
        $this->data['edit_id'] = $id;
        return view('backend.vendors.create', $this->data);
    }

   

    public function destroy($id)
    {
        $vendor = Vendor::find($id);
        $vendor->delete();
        return new RedirectResponse(route('admin.vendors.index'), ['flash_success' => __('The vendor was successfully deleted.')]);
    }
    

}
