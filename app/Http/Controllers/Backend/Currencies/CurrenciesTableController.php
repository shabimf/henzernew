<?php

namespace App\Http\Controllers\Backend\Currencies;

use App\Http\Controllers\Controller;
use App\Models\currency;
use DB;
use Yajra\DataTables\Facades\DataTables;

class CurrenciesTableController extends Controller
{
    public function __invoke()
    {
        $queries = currency::leftjoin('users', 'users.id', '=', 'currencies.created_by')
                        ->select([
                            'currencies.id',
                            'currencies.name',
                            'currencies.symbol',
                            'currencies.status',
                            'currencies.created_at',
                            'users.first_name as created_by',
                        ]);
        
        
    	return Datatables::of($queries)
            ->filterColumn('status', function ($query, $keyword) {
                if (in_array(strtolower($keyword), ['active', 'inactive'])) {
                    $query->where('currencies.status', (strtolower($keyword) == 'active') ? 1 : 0);
                }
            })
            ->filterColumn('created_by', function ($query, $keyword) {
                $query->whereRaw('users.first_name like ?', ["%{$keyword}%"]);
            })
            ->editColumn('status', function ($currency) {
                return $currency->status_label;
            })
            ->editColumn('created_at', function ($currency) {
                return $currency->created_at->toDateString();
            })
            ->addColumn('actions', function ($currency) {
                return $currency->action_buttons;
            })
            ->escapeColumns(['name'])
            ->make(true);
    }
}
