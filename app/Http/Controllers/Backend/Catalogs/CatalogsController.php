<?php
namespace App\Http\Controllers\Backend\Catalogs;

use App\Http\Controllers\Controller;
use App\Http\Traits\GetTrait;
use App\Http\Traits\CommonTrait;
use App\Http\Responses\RedirectResponse;
use App\Models\Catalog;
use App\Models\CatalogAttribute;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use Auth;
use DB;


class CatalogsController extends Controller
{
    use GetTrait,CommonTrait;

    public function __construct(Catalog $cat)
    {
        $this->data['list_brand'] = $this->NameAndID('brands');
        $this->data['list_attribute'] = $this->NameAndID('attributes');
        $this->data['status'] = $cat->statuses;
        View::share('js', ['catalogs']);
    }
    
    public function index()
    {
        return view('backend.catalogs.index');
    }

    public function create()
    {
        $this->data['list_model'] = $this->getModel(0)->toArray();
        $this->data['list_series'] = $this->getSeries(0)->toArray();
        return view('backend.catalogs.create', $this->data);
    }

    public function store(Request $request)
    {
        $post = new Catalog(); 
        $validator = $this->validateCatalogForm($request, 0);
      
        if ($validator->fails()) 
            return redirect()->back()->withInput()->withErrors($validator);

        $post = $this->GetCatalogForm($post, $request);
        $post->created_by = Auth::id();

        if ($post->save()){

            for ($i = 0; $i < count($request->input('attr_id')); ++$i)
            {
                $postattr = new CatalogAttribute(); 
                $postattr->catalog_id = $post->id;
                $postattr->attr_id     = $request->input('attr_id')[$i];
                $postattr->attr_dec    = $request->input('attr_dec')[$i];
                $postattr->save();
               
            } 
            
            return redirect()->back()->with("success","The catalog was successfully created.");
        }

    }

    public function update(Request $request, $id)
    {

        $validator = $this->validateCatalogForm($request, $id);
        if ($validator->fails()) 
            return redirect()->back()->withInput()->withErrors($validator);

    
        $post = Catalog::find($id);
        $post = $this->GetCatalogForm($post, $request);
        $post->updated_by = Auth::id();

        if ($post->save()){
            if (count($request->input('attr_id')) > 0) {
                DB::table('catalog_attributes')->where('catalog_id', $id)->delete();
                for ($i = 0; $i < count($request->input('attr_id')); ++$i)
                {
                    $postattr = new CatalogAttribute(); 
                    $postattr->catalog_id  = $id;
                    $postattr->attr_id     = $request->input('attr_id')[$i];
                    $postattr->attr_dec    = $request->input('attr_dec')[$i];
                    $postattr->save();
                    
                } 
            }
            
            return redirect()->back()->with("success","The catalog was successfully updated.");
        }
    }

    public function edit($id)
    {

        $this->data['edit_series'] = Catalog::find($id);
        $this->data['edit_id'] = $id;
        $this->data['list_model'] = $this->getModel($this->data['edit_series']['brand_id'])->toArray();
        $this->data['list_series'] = $this->getSeries($this->data['edit_series']['model_id'])->toArray();
        $this->data['list_cat_attr'] = CatalogAttribute::where('catalog_id', $id)->get()->toArray();
       
        
        return view('backend.catalogs.create', $this->data);
    }

   

    public function destroy($id)
    {
        $catalog = Catalog::find($id);
        $catalog->delete();
        return new RedirectResponse(route('admin.catalogs.index'), ['flash_success' => __('The catalog was successfully deleted.')]);
    }

}
