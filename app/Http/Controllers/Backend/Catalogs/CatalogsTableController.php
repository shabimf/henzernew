<?php

namespace App\Http\Controllers\Backend\Catalogs;

use App\Http\Controllers\Controller;
use App\Models\Catalog;
use DB;
use Yajra\DataTables\Facades\DataTables;

class CatalogsTableController extends Controller
{
    public function __invoke()
    {
        $queries = Catalog::leftjoin('users', 'users.id', '=', 'catalogs.created_by')
                        ->leftjoin('brands', 'brands.id', '=', 'catalogs.brand_id')
                        ->leftjoin('models', 'models.id', '=', 'catalogs.model_id')
                        ->leftjoin('series', 'series.id', '=', 'catalogs.series_id')
                        ->select([
                            'catalogs.id',
                            'catalogs.name',
                            'catalogs.focus_code',
                            'catalogs.status',
                            'catalogs.created_at',
                            'users.first_name as created_by',
                            'brands.name as brand_name',
                            'models.name as model_name',
                            'series.name as series_name',
                        ]);
        
        
    	return Datatables::of($queries)
            ->filterColumn('status', function ($query, $keyword) {
                if (in_array(strtolower($keyword), ['active', 'inactive'])) {
                    $query->where('series.status', (strtolower($keyword) == 'active') ? 1 : 0);
                }
            })
            ->filterColumn('created_by', function ($query, $keyword) {
                $query->whereRaw('users.first_name like ?', ["%{$keyword}%"]);
            })
            ->filterColumn('brand_name', function ($query, $keyword) {
                $query->whereRaw('brands.name like ?', ["%{$keyword}%"]);
            })
            ->filterColumn('model_name', function ($query, $keyword) {
                $query->whereRaw('models.name like ?', ["%{$keyword}%"]);
            })
            ->filterColumn('series_name', function ($query, $keyword) {
                $query->whereRaw('series.name like ?', ["%{$keyword}%"]);
            })
            ->editColumn('status', function ($catalog) {
                return $catalog->status_label;
            })
            ->editColumn('created_at', function ($catalog) {
                return $catalog->created_at->toDateString();
            })
            ->addColumn('actions', function ($catalog) {
                return $catalog->action_buttons;
            })
            ->escapeColumns(['name'])
            ->make(true);
    }
}
