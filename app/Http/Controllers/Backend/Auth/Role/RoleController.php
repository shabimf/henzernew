<?php

namespace App\Http\Controllers\Backend\Auth\Role;

use App\Http\Controllers\Controller;
use App\Http\Traits\GetTrait;
use App\Http\Traits\CommonTrait;
use App\Http\Responses\RedirectResponse;
use App\Models\Auth\Role;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use Auth;
use DB;


class RoleController extends Controller
{
    use GetTrait,CommonTrait;

    public function __construct()
    {
        $this->data['permissions'] =  $this->NameAndID('permissions');
        $this->data['roleCount'] =  $this->getLastRecordId('roles');
        View::share('js', ['users', 'roles']);
    }
    
    public function index()
    {
        return view('backend.auth.roles.index');
    }


    public function create()
    {
        $this->data['rolePermissions'] =  [];
        return view('backend.auth.roles.create', $this->data);
    }

    public function store(Request $request)
    { 
       
        $post = new Role();

        $validator = $this->validateRoleForm($request, 0); 
        
        if ($validator->fails()) 
            return redirect()->back()->withInput()->withErrors($validator);

        if (Role::where('name', $request['name'])->first()) {
            return redirect()->back()->with("error", "That role already exists. Please choose a different name.");
        }      

        $post = $this->GetRoleForm($post, $request);
        $post->created_by = Auth::id();

        DB::transaction(function() use ($post, $request) {
            if ($post->save()){
                $input['permissions'] = $request->permissions;
                $all = $request->associated_permissions == 'all' ? true : false;
                if (! $all) {
                   
                    $permissions = [];

                    if (is_array($input['permissions']) && count($input['permissions'])) {
                        foreach ($input['permissions'] as $perm) {
                            if (is_numeric($perm)) {
                                array_push($permissions, $perm);
                            }
                        }
                    }

                    $post->attachPermissions($permissions);
                }
               return true;   
            }
            return false;   
        });
        return redirect()->back()->with("success","The role was successfully created.");

    }

    public function update(Request $request, $id)
    {
  
        $validator = $this->validateRoleForm($request, $id);
        
        if ($validator->fails()) 
            return redirect()->back()->withInput()->withErrors($validator);

        if (Role::where('name', $request['name'])->where('id', '!=', $id)->first()) {
            return redirect()->back()->with("success","That permission already exists. Please choose a different name.");
        }

        $post = Role::find($id);

        $post = $this->GetRoleForm($post, $request);
        $post->updated_by = Auth::id();
        DB::transaction(function() use ($post, $request) {
            if ($post->save()){
                $input['permissions'] = $request->permissions;
                $all = $request->associated_permissions == 'all' ? true : false;

                if ($all) {
                    $post->permissions()->sync([]);
                } else {
                    //Remove all roles first
                    $post->permissions()->sync([]);
                    $permissions = [];

                    if (is_array($input['permissions']) && count($input['permissions'])) {
                        foreach ($input['permissions'] as $perm) {
                            if (is_numeric($perm)) {
                                array_push($permissions, $perm);
                            }
                        }
                    }

                    $post->attachPermissions($permissions);
                }
               return true;   
            }
            return false;   
        });

        
        return redirect()->back()->with("success","The permission was successfully updated.");
        
    }

    public function edit($id)
    {

        $this->data['role'] = Role::find($id);
        $this->data['edit_id'] = $id;
        $this->data['rolePermissions'] =  $this->getRolePermission($id);
        return view('backend.auth.roles.create', $this->data);
    }

   

    public function destroy($id)
    {
        $role = Role::find($id);
        $role->delete();
        return redirect()->route('admin.auth.role.index')->withFlashSuccess(__('alerts.backend.access.roles.deleted'));
    }

}
