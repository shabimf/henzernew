<?php

namespace App\Http\Controllers\Backend\Auth\Permission;

use App\Http\Controllers\Controller;
use App\Http\Traits\GetTrait;
use App\Http\Traits\CommonTrait;
use App\Http\Responses\RedirectResponse;
use App\Models\Auth\Permission;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use Auth;


class PermissionsController extends Controller
{
    use GetTrait,CommonTrait;

    public function __construct()
    {
        View::share('js', ['permissions']);
    }
    
    public function index()
    {
        return view('backend.auth.permissions.index');
    }


    public function create()
    {
        
        return view('backend.auth.permissions.create');
    }

    public function store(Request $request)
    { 
       
        $post = new Permission();

        $validator = $this->validatePermissionForm($request);
       
      
        if ($validator->fails()) 
            return redirect()->back()->withInput()->withErrors($validator);

        if (Permission::where('name', $request['name'])->first()) {
            return redirect()->back()->with("error", "That permission already exists. Please choose a different name.");
        }
       

        $post = $this->GetPermissionForm($post, $request);
        $post->created_by = Auth::id();

        if ($post->save()){
            return redirect()->back()->with("success","The Permission was successfully created.");
        }

    }

    public function update(Request $request, $id)
    {

        $validator = $this->validatePermissionForm($request);
        
        if ($validator->fails()) 
            return redirect()->back()->withInput()->withErrors($validator);

        if (Permission::where('name', $request['name'])->where('id', '!=', $id)->first()) {
            return redirect()->back()->with("success","That permission already exists. Please choose a different name.");
        }

        $post = Permission::find($id);

        $post = $this->GetPermissionForm($post, $request);
        $post->updated_by = Auth::id();

        if ($post->save()){
            return redirect()->back()->with("success","The permission was successfully updated.");
        }
    }

    public function edit($id)
    {

        $this->data['permission'] = Permission::find($id);
        $this->data['edit_id'] = $id;
        return view('backend.auth.permissions.create', $this->data);
    }

   

    public function destroy($id)
    {
        $permission = Permission::find($id);
        $permission->delete();
        return new RedirectResponse(route('admin.auth.permission.index'), ['flash_success' => __('alerts.backend.access.permissions.deleted')]);
    }

}
