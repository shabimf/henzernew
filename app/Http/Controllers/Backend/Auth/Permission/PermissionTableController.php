<?php

namespace App\Http\Controllers\Backend\Auth\Permission;

use App\Http\Controllers\Controller;
use App\Models\Auth\Permission;
use Yajra\DataTables\Facades\DataTables;

class PermissionTableController extends Controller
{
    public function __invoke()
    {
        $queries = Permission::select([
            'permissions.id',
            'permissions.name',
            'permissions.display_name',
            'permissions.sort',
            'permissions.created_at',
            'permissions.updated_at',
        ]);

        return Datatables::of($queries)
            ->escapeColumns(['name', 'sort'])
            ->addColumn('permissions', function ($permission) {
                if ($permission->all) {
                    return '<span class="label label-success">'.trans('labels.general.all').'</span>';
                }
            })
            ->addColumn('actions', function ($permission) {
                return $permission->action_buttons;
            })
            ->make(true);
    }
}
