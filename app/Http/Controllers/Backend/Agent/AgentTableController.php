<?php

namespace App\Http\Controllers\Backend\Agent;

use App\Http\Controllers\Controller;
use App\Models\Agent;
use DB;
use Yajra\DataTables\Facades\DataTables;

class AgentTableController extends Controller
{
    public function __invoke()
    {
        $queries = Agent::leftjoin('users', 'users.id', '=', 'agent.created_by')
                        ->select([
                            'agent.id',
                            'agent.name',
                            'agent.phone',
                            'agent.email',
                            'agent.address',
                            'agent.status',
                            'agent.created_at',
                            'users.first_name as created_by',
                        ]);
        
        
    	return Datatables::of($queries)
            ->filterColumn('status', function ($query, $keyword) {
                if (in_array(strtolower($keyword), ['active', 'inactive'])) {
                    $query->where('agent.status', (strtolower($keyword) == 'active') ? 1 : 0);
                }
            })
            ->filterColumn('created_by', function ($query, $keyword) {
                $query->whereRaw('users.first_name like ?', ["%{$keyword}%"]);
            })
            ->editColumn('status', function ($agent) {
                return $agent->status_label;
            })
            ->editColumn('created_at', function ($agent) {
                return $agent->created_at->toDateString();
            })
            ->addColumn('actions', function ($agent) {
                return $agent->action_buttons;
            })
            ->escapeColumns(['name'])
            ->make(true);
    }
}
