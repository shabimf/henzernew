<?php
namespace App\Http\Controllers\Backend\Agent;

use App\Http\Controllers\Controller;
use App\Http\Traits\GetTrait;
use App\Http\Traits\CommonTrait;
use App\Http\Responses\RedirectResponse;
use App\Models\Agent;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use Auth;


class AgentController extends Controller
{
    use GetTrait,CommonTrait;

    public function __construct()
    {
        View::share('js', ['agent']);
    }
    
    public function index()
    {
        return view('backend.agent.index');
    }

    public function create()
    {
        return view('backend.agent.create');
    }

    public function store(Request $request)
    {
        $post = new Agent();

        $validator = $this->validateAgentForm($request);
      
        if ($validator->fails()) 
            return redirect()->back()->withInput()->withErrors($validator);

        
        $post = $this->GetAgentForm($post, $request);
        $post->created_by = Auth::id();

        if ($post->save()){
            return redirect()->back()->with("success","The agent was successfully created.");
        }

    }

    public function update(Request $request, $id)
    {

        $validator = $this->validateAgentForm($request, $id);
        

        if ($validator->fails()) 
            return redirect()->back()->withInput()->withErrors($validator);

       
        $post = Agent::find($id);

        $post = $this->GetAgentForm($post, $request);
        $post->updated_by = Auth::id();

        if ($post->save()){
            return redirect()->back()->with("success","The agent was successfully updated.");
        }
    }

    public function edit($id)
    {

        $this->data['edit_agent'] = Agent::find($id);
        $this->data['edit_id'] = $id;
        return view('backend.agent.create', $this->data);
    }

   

    public function destroy($id)
    {
        $agent = Agent::find($id);
        $agent->delete();
        return new RedirectResponse(route('admin.agent.index'), ['flash_success' => __('The agent was successfully deleted.')]);
    }

}
