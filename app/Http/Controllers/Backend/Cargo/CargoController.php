<?php
namespace App\Http\Controllers\Backend\Cargo;

use App\Http\Controllers\Controller;
use App\Http\Traits\GetTrait;
use App\Http\Traits\CommonTrait;
use App\Http\Responses\RedirectResponse;
use App\Models\Cargo;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use Auth;


class CargoController extends Controller
{
    use GetTrait,CommonTrait;

    public function __construct()
    {
        View::share('js', ['cargo']);
    }
    
    public function index()
    {
        
        return view('backend.cargo.index');
    }

    public function create()
    {
        return view('backend.cargo.create');
    }

    public function store(Request $request)
    {
        $post = new Cargo();

        $validator = $this->validateCargoForm($request);
      
        if ($validator->fails()) 
            return redirect()->back()->withInput()->withErrors($validator);

        if (Cargo::where('name', $request['name'])->first()) {
            return redirect()->back()->with("error",'Cargo mode name already exist.');
        }

        $post = $this->GetCargoForm($post, $request);
        $post->created_by = Auth::id();

        if ($post->save()){
            return redirect()->back()->with("success","The Cargo mode was successfully created.");
        }

    }

    public function update(Request $request, $id)
    {

        $validator = $this->validateCargoForm($request);
        

        if ($validator->fails()) 
            return redirect()->back()->withInput()->withErrors($validator);

        if (Cargo::where('name', $request['name'])->where('id', '!=', $id)->first()) {
            return redirect()->back()->with("error",'Cargo mode name already exist.');
        }

        $post = Cargo::find($id);

        $post = $this->GetCargoForm($post, $request);
        $post->updated_by = Auth::id();

        if ($post->save()){
            return redirect()->back()->with("success","The cargo mode was successfully updated.");
        }
    }

    public function edit($id)
    {

        $this->data['edit_cargo'] = Cargo::find($id);
        $this->data['edit_id'] = $id;
        return view('backend.cargo.create', $this->data);
    }

   

    public function destroy($id)
    {
        $cargo = Cargo::find($id);
        $cargo->delete();
        return new RedirectResponse(route('admin.cargo.index'), ['flash_success' => __('The cargo mode was successfully deleted.')]);
    }

}
