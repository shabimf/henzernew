<?php

namespace App\Http\Controllers\Backend\Cargo;

use App\Http\Controllers\Controller;
use App\Models\Cargo;
use DB;
use Yajra\DataTables\Facades\DataTables;

class CargoTableController extends Controller
{
    public function __invoke()
    {
        $queries = Cargo::leftjoin('users', 'users.id', '=', 'cargo_mode.created_by')
                        ->select([
                            'cargo_mode.id',
                            'cargo_mode.name',
                            'cargo_mode.status',
                            'cargo_mode.created_at',
                            'users.first_name as created_by',
                        ]);
        
        
    	return Datatables::of($queries)
            ->filterColumn('status', function ($query, $keyword) {
                if (in_array(strtolower($keyword), ['active', 'inactive'])) {
                    $query->where('argo_mode.status', (strtolower($keyword) == 'active') ? 1 : 0);
                }
            })
            ->filterColumn('created_by', function ($query, $keyword) {
                $query->whereRaw('users.first_name like ?', ["%{$keyword}%"]);
            })
            ->editColumn('status', function ($cargo) {
                return $cargo->status_label;
            })
            ->editColumn('created_at', function ($cargo) {
                return $cargo->created_at->toDateString();
            })
            ->addColumn('actions', function ($cargo) {
                return $cargo->action_buttons;
            })
            ->escapeColumns(['name'])
            ->make(true);
    }
}
