<?php

namespace App\Http\Controllers\Frontend\Invoice;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;   
use App\Repositories\Frontend\InvoiceRepository;  
use App\Http\Requests\Frontend\Invoice\StoreInvoiceRequest;
use App\Http\Requests\Frontend\Invoice\SubmitInvoiceRequest;
use App\Http\Traits\CommonTrait;
use Illuminate\Support\Facades\View;
use PDF;
use DB;

class InvoiceController extends Controller
{
    use CommonTrait;

    public function __construct(InvoiceRepository $repository)
    {
        $this->repository = $repository;
        $this->middleware(function ($request, $next) {
            $this->menuSetNav();
            $this->data['module']  = 'invoice';
            $this->data['price_array'] =  $this->get_price_calender();
            $this->data['vendors'] = $this->VendorList();
            $this->data['subvendors'] = $this->SubVendorList();
            $this->data['sources'] = $this->SourcingList();
            $this->data['branches'] = $this->NameAndID('branches');
            $this->data['role'] = $this->getRoleName();
            $this->data['status_array'] = [ 
                                            "0" => "Draft",
                                            "1" => "Pending",
                                            "2" => "Invoiced"
                                          ];
            return $next($request);
        });
       
        View::share('js', ['jquery-ui','autocomplete-items','invoice']);
        View::share('css', ['jquery-ui']); 
    }

    public function index(Request $request)
    {
        $this->data['branch_to']  = $this->getToBranchId();
        $this->data['active_list'] = true;
        $this->data['list'] = $this->repository->retrieveList($request->all());
        return view('frontend.invoice.index', $this->data);
    }

    public function create()  {
        return view('frontend.invoice.create', $this->data); 
    }
    
    public function store(StoreInvoiceRequest $request)
    {
        $id = $this->repository->create($request->except(['_token', '_method']));
        return redirect('invoice/add/'.$id);      
    }

    public function add($id)  {
        $this->data['details'] = $this->repository->getInvoiceById($id);
        $this->data['common_details'] = $this->repository->getById($id);
        $this->data['parent_id'] = $id;
        return view('frontend.invoice.add-invoice', $this->data); 
    }


    public function view($id)  {
        $this->data['parent_id'] = $id;
        $this->data['active_list'] = true;
        $this->data['details'] = $this->repository->getInvoiceById($id);
        $this->data['common_details'] = $this->repository->getById($id);
        return view('frontend.invoice.view', $this->data); 
    }

    public function submit(SubmitInvoiceRequest $request, $id)   
    {
        $this->repository->submitInvoice($request->except(['_token', '_method','product_name']), $id);
        return redirect()->back()->with("success","The product was successfully added.");
    }

    public function remove(Request $request)   
    {
        if($request->id) {
            $this->repository->delete($request->id);
            session()->flash('success', 'Product removed successfully');
        }
    }

    public function pdf($id){

        $this->data['common_details'] = $this->repository->getById($id);
        $this->data['details'] = $this->repository->getInvoiceById($id); 
        
        $pdf = PDF::loadView('frontend.invoice.invoice', $this->data);
        return $pdf->stream('invoice-'.$this->data['common_details']->serial_no.'.pdf');
    }

    public function update(Request $request, $id)  
    {  
        
        $message = $this->repository->update($request->except(['_token', '_method']), $id);
        if (is_integer($message)) {
            return redirect('invoice/view/'.$id)->with("success","The Invoice was successfully created.");  
        } else {
           return redirect('invoice/add/'.$id)->with("info", $message);
        }

    }

    public function rma($id)  {
        $this->data['parent_id'] = $id;
        $this->data['active_list'] = true;
        $this->data['details'] = $this->repository->getInvoiceById($id);
        $this->data['common_details'] = $this->repository->getById($id);
        return view('frontend.invoice.rma', $this->data); 
    }

}