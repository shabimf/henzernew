<?php

namespace App\Http\Controllers\Frontend\Markup;

use App\Http\Controllers\Controller;  
use App\Http\Traits\CommonTrait;
use App\Repositories\Frontend\MarkupRepository;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

class MarkupController extends Controller
{
    use CommonTrait;

    protected $repository;

    public function __construct(MarkupRepository $repository)
    {
        $this->repository = $repository;
        $this->middleware(function ($request, $next) {
            $this->data['module']  = 'markup';
            $this->menuSetNav();
            $this->data['price_array'] =  $this->get_price_calender();
            $this->data['role'] = $this->getRoleName();
            $this->data['subvendors'] = $this->SubVendorList();
            return $next($request);
        });
    }
    
    public function index(Request $request)
    {
        $this->data['list_catalog'] = $this->NameAndID('catalogs');
        $this->data['list'] = $this->repository->retrieveList($request->all());
        $this->data['page_title']  = 'MANAGE MARKUP';
        return view('frontend.markup.index', $this->data);
    }

    public function store(Request $request)
    {
        $this->repository->create($request->except(['_token', '_method']));
        return redirect()->back()->with("success","Markup was successfully added ");       
    }

  
    
}