<?php

namespace App\Http\Controllers\Frontend\Subvendor;

use App\Http\Controllers\Controller;  
use App\Http\Traits\CommonTrait;
use App\Http\Requests\Frontend\Subvendor\ManageSubvendorRequest;
use App\Repositories\Frontend\VendorRepository;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\View;


class SubvendorController extends Controller
{
    use CommonTrait;

    protected $repository;

    public function __construct(VendorRepository $repository)
    {
        $this->middleware(function ($request, $next) {
            $this->data['module']  = 'subvendor';
            $this->data['role'] = $this->getRoleName();
            $this->data['price_array'] =  $this->get_price_calender();
            return $next($request);
        });
        View::share('js', ['scroll']); 
        $this->repository = $repository;
    }
    
    public function index(ManageSubvendorRequest $request)
    {
        $this->data['list_catalog'] = $this->NameAndID('catalogs');
        $this->data['list'] = $this->repository->retrieveList($request->all());
        $this->data['page_title']  = 'MANAGE SUBVENDOR';
        return view('frontend.subvendor.index', $this->data);
    }

    
}