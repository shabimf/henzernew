<?php

namespace App\Http\Controllers\Frontend\User;

use App\Http\Controllers\Controller;
use Auth;
use App\Http\Traits\CommonTrait;
use Illuminate\Http\Request; 

class DashboardController extends Controller
{
    use CommonTrait;

    public function __construct()
    {   
        
        $this->middleware(function ($request, $next) {
            $this->data['module']  = 'dashbord';
            $this->data['role'] = $this->getRoleName();
            $this->data['subvendors'] = $this->SubVendorList();
            $this->data['price_array'] =  $this->get_price_calender();
            return $next($request);
        });
        
    }
  
    public function index()
    {
    
        $this->data['enquiry_count'] = $this->getRowCount('enquiries', 1);
        $this->data['quotation_count'] = $this->getRowCount('quotation', 1);
        $this->data['order_count'] = $this->getRowCount('order', 1);
        $this->data['invoice_count'] = $this->getRowCount('invoice', 1);  
        $this->data['page_title'] = 'Dashbord';
        return view('frontend.user.dashboard', $this->data);
    }

    function fetch_data(Request $request)
    { 
        if($request->ajax()) {
            $options['branch_from'] = $request->branch_from;
            $options['branch_to'] = $request->branch_to;
            $options['d_from'] = $request->from_date;
            $options['d_to'] = $request->to_date;
          
            $this->data['enquiry_count'] = (strlen($this->getRowCount('enquiry', 1))==1?"0".$this->getRowCount('enquiry', 1): $this->getRowCount('enquiry', 1));
            $this->data['enquiry_total_count'] =  $this->getRowCount('enquiry','-1', $options);

            $this->data['quotation_count'] = (strlen($this->getRowCount('quotation', 1))==1?"0".$this->getRowCount('quotation', 1): $this->getRowCount('quotation', 1));
            $this->data['quotation_total_count'] = $this->getRowCount('quotation','-1' , $options);

            $this->data['order_count'] = (strlen($this->getRowCount('order', 1))==1?"0".$this->getRowCount('order', 1): $this->getRowCount('order', 1));
            $this->data['order_total_count'] = $this->getRowCount('order','-1' , $options);

            $this->data['invoice_count'] = (strlen($this->getRowCount('invoice', 1))==1?"0".$this->getRowCount('invoice', 1): $this->getRowCount('invoice', 1));
            $this->data['invoice_total_count'] = $this->getRowCount('invoice','-1' , $options);

            $this->data['total_sale'] =  $this->getTotalSale('invoice', $options,'month');
            $this->data['today_sale'] =  $this->getTotalSale('invoice', $options,'day');
            $prev_sale = (str_replace(',', '', $this->getTotalSale('invoice', $options,'prevmonth'))/date("d", mktime(0, 0, 0, date("m"), 0)));
            $this->data['prev_sale'] =  number_format($prev_sale, 3);
            echo json_encode($this->data);
        }  
    }   
}
