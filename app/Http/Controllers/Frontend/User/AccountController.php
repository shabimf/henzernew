<?php

namespace App\Http\Controllers\Frontend\User;
use App\Http\Traits\CommonTrait;

use App\Http\Controllers\Controller;

/**
 * Class AccountController.
 */
class AccountController extends Controller
{
    use CommonTrait;
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->data['module']  = 'account';
            $this->data['role'] = $this->getRoleName();
            $this->data['price_array'] =  $this->get_price_calender();
            $this->data['list_notification'] = $this->NameAndID('notification_setting');
            $this->data['list_user_notification'] = $this->getUserNotiID();
            $this->data['list_price_graph_type'] = $this->getPriceGraphID();
            return $next($request);
        });
        
    }
    
    public function index()
    {
        return view('frontend.user.account', $this->data);
    }


}
