<?php

namespace App\Http\Controllers\Frontend\RMA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;   
use App\Http\Traits\GetTrait;
use App\Http\Traits\CommonTrait;
use App\Repositories\Frontend\RMARepository;  
use App\Http\Requests\Frontend\RMA\StoreRMARequest;
use App\Http\Requests\Frontend\RMA\SubmitRMARequest;
use App\Http\Requests\Frontend\RMA\ManageRMARequest;
use App\Http\Requests\Frontend\Shipment\SubmitShipmentRequest;
use Illuminate\Support\Facades\View;
use PDF;
use Mail;

class RMAController extends Controller
{
    use GetTrait,CommonTrait;

    public function __construct(RMARepository $repository)
    {
        $this->repository = $repository;
        $this->middleware(function ($request, $next) {
            $this->menuSetNav();
            $this->data['role'] = $this->getRoleName();
            $this->data['price_array'] =  $this->get_price_calender();
            $this->data['vendors'] = $this->VendorList();
            $this->data['subvendors'] = $this->SubVendorList();
            $this->data['sources'] = $this->SourcingList();
            $this->data['branches'] = $this->NameAndID('branches');
            $this->data['list_mode'] = $this->NameAndID('cargo_mode');
            $this->data['list_agent'] = $this->NameAndID('agent');
            $this->data['module']  = 'rma';
            $this->data['status_array'] =  [ 
                                                "0" => "Draft",
                                                "1" => "Pending"
                                            ];
            return $next($request);
        });

        View::share('js', ['jquery-ui','autocomplete-items','rma']); 
        View::share('css', ['jquery-ui']); 
    }
    
    public function index(ManageRMARequest $request)
    {
        $this->data['active_list'] = true;
        $this->data['branch_to']  = $this->getToBranchId();
        $this->data['list'] = $this->repository->retrieveList($request->all());
        return view('frontend.rma.index', $this->data);
    }

    public function create()
    {
        $this->data['branch_to']  = $this->getToBranchId();
        return view('frontend.rma.create', $this->data);
    }
    
    public function add($id)  {
        $this->data['details'] = $this->repository->getRMAById($id);
        $this->data['common_details'] = $this->repository->getById($id);
        $this->data['parent_id'] = $id;
        return view('frontend.rma.add-rma', $this->data); 
    }

    public function store(StoreRMARequest $request)
    {
        $id = $this->repository->create($request->except(['_token', '_method']));
        return redirect('rma/add/'.$id);       
    }

    public function remove(Request $request)   
    {
        if($request->id) {
            $this->repository->delete($request->id);
            session()->flash('success', 'Product removed successfully');
        }
    }

    public function view($id)  {
        $this->data['details'] = $this->repository->getRMAById($id);
        $this->data['common_details'] = $this->repository->getById($id);
        $this->data['parent_id'] = $id;
        $this->data['active_list'] = true;
        return view('frontend.rma.view', $this->data); 
    }

    public function submit(SubmitRMARequest $request, $id)   
    {
        $this->repository->createItem($request->except(['_token', '_method','product_name']), $id);
        return redirect()->back()->with("success","The product was successfully added.");
    }

    public function update(Request $request, $id)  
    {
        $last_id = $this->repository->update($request->except(['_token', '_method']), $id);
        return redirect('rma/view/'.$id)->with("success","The RMA was successfully created.");  
    }

    public function changeStatus(Request $request, $id)  {
        $this->repository->status($request->except(['_token']), $id);   
        if ($request->status == "3") {
          session()->flash('success', 'Item rejected successfully'); 
        } else {
          session()->flash('success', 'Item accepted successfully'); 
        }
    }

    public function updateNote(Request $request, $id)  {
        return true;
        // $this->repository->updateNote($request->except(['_token']), $id);   
        // session()->flash('success', 'Item rejected successfully'); 
    }

    public function shipmentList(Request $request) {

        $this->data['module']  = 'rma_shipment';
        $this->data['page_title'] = "RMA Shipment Manage";
        
        $this->data['list'] = $this->repository->retrieveListItems($request->all());
        var_dump($this->data['list']);
        exit;
        $rma_id = $this->data['list'][0]->parent_id??0;
        $this->data['shipping_list'] = $this->repository->retrieveShippingListItems($rma_id);
        return view('frontend.rma.shipment', $this->data);

    }

    public function storeRMAShipment(SubmitShipmentRequest $request) {   
        $this->repository->createShipment($request->except(['_token'])); 
        return redirect('rma/shipment?serial_num='.$request->serial_no)->with("success","The RMA Shipment was successfully created.");  
        
    }

    public function qtyupdate(Request $request, $id)
    {
        
        $this->repository->qtyupdate($request->except(['_token']),$id);
        return redirect('rma')->with("success","The RMA was successfully created."); 
    
        
    }


    
}