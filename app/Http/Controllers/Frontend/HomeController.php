<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Controller;
use Auth;
use App\Http\Traits\CommonTrait;
use Session; 


/**
 * Class HomeController.
 */
class HomeController extends Controller
{

    use CommonTrait;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->menuSetNav();
            $this->data['module']  = 'dashbord';
            $this->data['list_branches'] =  $this->NameAndID('branches');
            if (Session::has('is_source')) {
            } else {
                if (Auth::id() > 0){
                  $this->data['role'] = $this->getRoleName();
                  if ($this->data['role'] == "Vendor") session(['is_source' => 1,'is_source_set' => 1]);
                  else session(['is_source' => 1, 'is_source_set' => 0]); 
                }
            }
            return $next($request);
        });
    }

    public function index()
    {   
       
        $this->data['page_title'] = 'Dashbord';
        if (Auth::id() > 0){
            $this->data['enquiry_count'] = (strlen($this->getRowCount('enquiry', 1))==1?"0".$this->getRowCount('enquiry', 1): $this->getRowCount('enquiry', 1));
            $this->data['quotation_count'] = (strlen($this->getRowCount('quotation', 1))==1?"0".$this->getRowCount('quotation', 1): $this->getRowCount('quotation', 1));
            $this->data['order_count'] = (strlen($this->getRowCount('order', 1))==1?"0".$this->getRowCount('order', 1): $this->getRowCount('order', 1));
            $this->data['invoice_count'] = (strlen($this->getRowCount('invoice', 1))==1?"0".$this->getRowCount('invoice', 1): $this->getRowCount('invoice', 1));
            $this->data['role'] = $this->getRoleName();
            $this->data['price_array'] =  $this->get_price_calender();
            $this->data['subvendors'] = $this->SubVendorList();
            return view('frontend.user.dashboard', $this->data);
        } 
        return view('frontend.auth.login');
    }
}
