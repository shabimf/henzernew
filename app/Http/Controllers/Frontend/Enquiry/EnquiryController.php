<?php

namespace App\Http\Controllers\Frontend\Enquiry;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;   
use App\Http\Traits\GetTrait;
use App\Http\Traits\CommonTrait;
use App\Repositories\Frontend\EnquiryRepository;  
use App\Http\Requests\Frontend\Enquiry\StoreEnquiryRequest;
use App\Http\Requests\Frontend\Enquiry\SubmitEnquiryRequest;
use App\Http\Requests\Frontend\Enquiry\ManageEnquiryRequest;
use Illuminate\Support\Facades\View;
use PDF;
use Mail;

class EnquiryController extends Controller
{
    use GetTrait,CommonTrait;

    public function __construct(EnquiryRepository $repository)
    {
        $this->repository = $repository;
        $this->middleware(function ($request, $next) {
            $this->menuSetNav();
            $this->data['role'] = $this->getRoleName();
            $this->data['price_array'] =  $this->get_price_calender();
            $this->data['vendors'] = $this->VendorList();
            $this->data['subvendors'] = $this->SubVendorList();
            $this->data['sources'] = $this->SourcingList();
            $this->data['currency'] = $this->CurrencyList();
            $this->data['branches'] = $this->NameAndID('branches');
            $this->data['module']  = 'enquiry';
            $this->data['status_array'] =  [ 
                                                "0" => "Draft",
                                                "1" => "Pending",
                                                "2" => "Quoted"
                                            ];
            return $next($request);
        });
     
        View::share('js', ['jquery-ui','autocomplete-items','enquiry']); 
        View::share('css', ['jquery-ui']); 
    }
    
    public function index(ManageEnquiryRequest $request)
    {
       
        $this->data['active_list'] = true;
        $this->data['branch_to']  = $this->getToBranchId();
        $this->data['list'] = $this->repository->retrieveEnquiryList($request->all());
        return view('frontend.enquiry.index', $this->data);
    }

    public function create()
    {
        $this->data['branch_to']  = $this->getToBranchId();
        return view('frontend.enquiry.create', $this->data);
    }
    
    public function add($id)  {
        $this->data['details'] = $this->repository->getEnquiryById($id);
        $this->data['common_details'] = $this->repository->getById($id);
        $this->data['parent_id'] = $id;
        return view('frontend.enquiry.add-enquiry', $this->data); 
    }

    public function store(StoreEnquiryRequest $request)
    {
        $id = $this->repository->createEnquiry($request->except(['_token', '_method', 'product_name']));
        return redirect('enquiry/add/'.$id);       
    }

    public function remove(Request $request)   
    {
        if($request->id) {
            $this->repository->delete($request->id);
            session()->flash('success', 'Product removed successfully');
        }
    }

    public function view($id)  {
        $this->data['details'] = $this->repository->getEnquiryById($id);
        $this->data['common_details'] = $this->repository->getById($id);
        $this->data['quotation_items'] = $this->getIteamArray('enquiry','quotation', $id);
        $this->data['parent_id'] = $id;
        $this->data['active_list'] = true;
        return view('frontend.enquiry.view', $this->data); 
    }

    public function submit(SubmitEnquiryRequest $request, $id)   
    {
        $this->repository->submitEnquiry($request->except(['_token', '_method','product_name']), $id);
        return redirect()->back()->with("success","The product was successfully added.");
    }

    public function update(Request $request, $id)  
    {
       
        $last_id = $this->repository->updateEnquiry($request->except(['_token', '_method']), $id);
        if ($request->status == "quotation") {
            return $last_id;
        } else {
           return redirect('enquiry/view/'.$id)->with("success","The enquiry was successfully created.");     
            
        }
       
    }
}