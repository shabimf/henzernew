<?php

namespace App\Http\Controllers\Frontend\Quotation;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;   
use App\Repositories\Frontend\QuotationRepository;  
use App\Http\Requests\Frontend\Quotation\StoreQuotationRequest;
use App\Http\Requests\Frontend\Quotation\SubmitQuotationRequest;
use App\Http\Traits\CommonTrait;
use Illuminate\Support\Facades\View;
use PDF;
use Mail;

class QuotationController extends Controller
{
    use CommonTrait;
    public function __construct(QuotationRepository $repository)
    {
       
        $this->repository = $repository;
        $this->middleware(function ($request, $next) {
            $this->menuSetNav();
            $this->data['role'] = $this->getRoleName();
            $this->data['price_array'] =  $this->get_price_calender();
            $this->data['module']  = 'quotation';
            $this->data['vendors'] = $this->VendorList();
            $this->data['subvendors'] = $this->SubVendorList();
            $this->data['sources'] = $this->SourcingList();
            $this->data['branches'] = $this->NameAndID('branches');
            $this->data['status_array'] = [ 
                                            "0" => "Draft",
                                            "1" => "Pending",
                                            "2" => "Ordered"
                                          ];
            return $next($request);
        });
        View::share('js', ['jquery-ui','autocomplete-items','quotation']); 
        View::share('css', ['jquery-ui']); 
    }

    public function index(Request $request)
    {
        View::share('js', ['scroll']);
        $this->data['active_list'] = true;
        $this->data['branch_to']  = $this->getToBranchId();
        $this->data['list'] = $this->repository->retrieveList($request->all());
        return view('frontend.quotation.index', $this->data);
    }

    public function create()  {
        return view('frontend.quotation.create', $this->data); 
    }
    
    public function store(StoreQuotationRequest $request)
    {
        $id = $this->repository->create($request->except(['_token', '_method']));
        return redirect('quotation/add/'.$id);      
    }

    public function add($id)  {
       
        $this->data['details'] = $this->repository->getQuotationById($id);
        $this->data['common_details'] = $this->repository->getById($id);
        $this->data['parent_id'] = $id;
        return view('frontend.quotation.add-quotation', $this->data); 
    }


    public function view($id)  {
        $this->data['parent_id'] = $id;
        $this->data['active_list'] = true;
        $this->data['details'] = $this->repository->getQuotationById($id);
        $this->data['common_details'] = $this->repository->getById($id);
        $this->data['quotation_items'] = $this->getIteamArray('quotation','order', $id);
        return view('frontend.quotation.view', $this->data); 
    }

    public function submit(SubmitQuotationRequest $request, $id)   
    {
        $this->repository->submitQuotation($request->except(['_token', '_method','product_name']), $id);
        return redirect()->back()->with("success","The product was successfully added.");
    }

    public function remove(Request $request)   
    {
        if($request->id) {
            $this->repository->delete($request->id);
            session()->flash('success', 'Product removed successfully');
        }
    }

    public function update(Request $request, $id)  
    {
        $last_id = $this->repository->update($request->except(['_token', '_method']), $id);
        if ($request->status == "quotation") {
            return redirect('quotation/view/'.$id)->with("success","The quotation was successfully created.");  
        } else {
            //session()->flash('success', 'The order was successfully created.');
            return $last_id;
        }
       
    }

}