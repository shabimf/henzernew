<?php

namespace App\Http\Controllers\Frontend\Price;

use App\Http\Controllers\Controller;  
use App\Http\Traits\CommonTrait;
use App\Http\Requests\Frontend\Price\StorePriceRequest;
use App\Http\Requests\Frontend\Price\ManagePriceRequest;
use App\Repositories\Frontend\PriceRepository;
use Illuminate\Http\Response;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\ImportPrice;
use App\Exports\ExportPrice;



class PriceController extends Controller
{
    use CommonTrait;

    protected $repository;

    public function __construct(PriceRepository $repository)
    {
        $this->repository = $repository;
        $this->middleware(function ($request, $next) {
            $this->data['module']  = 'price';
            $this->menuSetNav();
            $this->data['price_array'] =  $this->get_price_calender();
            $this->data['role'] = $this->getRoleName();
            return $next($request);
        });
    }
    
    public function index(ManagePriceRequest $request)
    {
        $this->data['list_catalog'] = $this->NameAndID('catalogs');
        $this->data['list'] = $this->repository->retrieveList($request->all());
        $this->data['page_title']  = 'MANAGE PRICE';
        return view('frontend.price.index', $this->data);
    }

    public function store(StorePriceRequest $request)
    {
        $this->repository->create($request->except(['_token', '_method']));
        return redirect()->back()->with("success","Price was successfully added ");       
    }

    public function import() 
    {
        Excel::import(new ImportPrice, request()->file('file')); 
        return redirect()->back()->with("success","The file important successfully!...");      ;
    }

    public function export() 
    {
        return Excel::download(new ExportPrice, 'price.xlsx');
    }

    
}