<?php

namespace App\Http\Controllers\Frontend\Order;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;   
use App\Repositories\Frontend\OrderRepository;  
use App\Http\Requests\Frontend\Order\StoreOrderRequest;
use App\Http\Requests\Frontend\Order\SubmitOrderRequest;
use App\Http\Traits\CommonTrait;
use Illuminate\Support\Facades\View;

class OrderController extends Controller
{
    use CommonTrait;

    public function __construct(OrderRepository $repository)
    {
        $this->repository = $repository;
        $this->middleware(function ($request, $next) {
            $this->menuSetNav();
            $this->data['role'] = $this->getRoleName();
            $this->data['price_array'] =  $this->get_price_calender();
            $this->data['vendors'] = $this->VendorList();
            $this->data['subvendors'] = $this->SubVendorList();
            $this->data['sources'] = $this->SourcingList();
            $this->data['branches'] = $this->NameAndID('branches');
            $this->data['module']  = 'order';
            $this->data['status_array'] = [ 
                                            "0" => "Draft",
                                            "1" => "Pending",
                                            "2" => "Invoiced"
                                        ];
            return $next($request);
        });
        View::share('js', ['jquery-ui','autocomplete-items','order']); 
        View::share('css', ['jquery-ui']);
        
    }

    public function index(Request $request)
    {
        $this->data['active_list'] = true;
        $this->data['branch_to']  = $this->getToBranchId();
        $this->data['list'] = $this->repository->retrieveList($request->all());
        return view('frontend.order.index', $this->data);
    }

    public function create()
    {
        return view('frontend.order.create', $this->data);
    }

    public function store(StoreOrderRequest $request)
    {
        $id = $this->repository->create($request->except(['_token', '_method']));
        return redirect('order/add/'.$id);       
    }

    public function add($id)  {

        $this->data['details'] = $this->repository->getOrderById($id);
        $this->data['common_details'] = $this->repository->getById($id);
        $this->data['parent_id'] = $id;
        return view('frontend.order.add-order', $this->data); 
    }

    public function view($id)  {
        $this->data['parent_id'] = $id;
        $this->data['active_list'] = true;
        $this->data['details'] = $this->repository->getOrderById($id);
        $this->data['common_details'] = $this->repository->getById($id);
        return view('frontend.order.view', $this->data); 
    }

    public function submit(SubmitOrderRequest $request, $id)   
    {
        $this->repository->submit($request->except(['_token', '_method','product_name']), $id);
        return redirect()->back()->with("success","The product was successfully added.");
    }

    public function remove(Request $request)   
    {
        if($request->id) {
            $this->repository->delete($request->id);
            session()->flash('success', 'Product removed successfully');
        }
    }

    public function update(Request $request, $id)  
    {
        
        $last_id = $this->repository->update($request->except(['_token', '_method']), $id);
        if ($request->status == "invoice") {
            if (is_integer($last_id)) {
               return redirect('order/view/'.$id)->with("success","The invoice was successfully created.");  
            } else {
               return redirect('order/view/'.$id)->with("info", $last_id);  
            }
        } else {
            return redirect('order/view/'.$id)->with("success","The order was successfully created.");  
        }
       
    }
    

}