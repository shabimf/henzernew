<?php

namespace App\Http\Controllers\Frontend\Shipment;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;   
use App\Http\Traits\GetTrait;
use App\Http\Traits\CommonTrait;
use App\Repositories\Frontend\ShipmentRepository;  
use App\Http\Requests\Frontend\Shipment\SubmitShipmentRequest;
use Illuminate\Support\Facades\View;
use App\Models\Document;
use App\Models\Invoice;
use PDF;
use Mail;
use Auth;

class ShipmentController extends Controller
{
    use GetTrait,CommonTrait;

    public function __construct(ShipmentRepository $repository)
    {
        $this->repository = $repository;
        $this->middleware(function ($request, $next) {
            $this->menuSetNav();
            $this->data['role'] = $this->getRoleName();
            $this->data['price_array'] =  $this->get_price_calender();
            $this->data['vendors'] = $this->VendorList();
            $this->data['subvendors'] = $this->SubVendorList();
            $this->data['sources'] = $this->SourcingList();
            $this->data['branches'] = $this->NameAndID('branches');
            $this->data['list_agent'] = $this->NameAndID('agent');
            $this->data['module']  = 'shipment';
            $this->data['page_title'] = 'Shipment Management' ;
            $this->data['list_mode'] = $this->NameAndID('cargo_mode');
            $this->data['status_array'] =  [ 
                "1" => "TRANSIT",
                "2" => "RECEIVED"
            ];
            View::share('js', ['shipment']);
            return $next($request);
        });
    }
    
    public function index(Request $request)
    {
        $this->data['active_list'] = true;
        $this->data['branch_to']  = $this->getToBranchId();
        $this->data['list'] = $this->repository->retrieveList($request->all());

        return view('frontend.shipment.index', $this->data);
    }

    public function create(Request $request)
    {       
        $this->data['branch_to']  = $this->getToBranchId();
        // $this->data['list'] = $this->repository->retrieveListItems($request->all());
        // $invoice_id = $this->data['list'][0]->parent_id??0;    
        // $this->data['shipping_list'] = $this->repository->retrieveShippingListItems($invoice_id);
        return view('frontend.shipment.create', $this->data);
    }

    public function search(Request $request)
    {
          
        $this->data['branch_to']  = $this->getToBranchId();
        $this->data['list'] = $this->repository->retrieveListItems($request->all());  
        return view('frontend.shipment.search', $this->data);
    }

    public function store(SubmitShipmentRequest $request) {
        $this->repository->create($request->except(['_token']));
        return redirect('shipment')->with("success","The invoice shipment was successfully created."); 
        
    }

    public function view($id)  {
        $this->data['active_list'] = true;
        $this->data['parent_id'] = $id;
        $this->data['details'] = $this->repository->getShipmentById($id);
        $this->data['enquiry_details'] = $this->repository->getEnquiryById($id);
        $this->data['quotation_details'] = $this->repository->getQuotationById($id);
        $this->data['order_details'] = $this->repository->getOrderById($id);
        $this->data['invoice_details'] = $this->repository->getInvoiceById($id);
        $this->data['common_details'] = $this->repository->getById($id);
        return view('frontend.shipment.view', $this->data); 
    }

    public function upload_data($id)  {
        $this->data['active_list'] = true;
        $this->data['parent_id'] = $id;
        $this->data['package_list'] =  $this->get_shipment_document($id, 1);
        $this->data['clearance_list'] =  $this->get_shipment_document($id, 2);
        $this->data['invoice_list'] =  $this->get_shipment_document($id, 3);
       
        return view('frontend.shipment.Form_upload', $this->data); 
    }

  
    public function save_upload_data($id, Request $request)  {
        $this->validate($request,[
            'filename' =>'required',
            'filename.*' => 'mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);
        if ($request->hasFile('filename')) {
            $image = $request->file('filename');
            foreach ($image as $files) {
                $destinationPath = public_path().'/files/';
                $file_name = time() . "." . $files->getClientOriginalExtension();
                $files->move($destinationPath, $file_name);
                //$data[] = $file_name;
                $file= new Document();
                $file->filename= $file_name;
                $file->parent_id=$id;
                $file->type_id=$request->type_id;
                $file->save();
            }
        }
        
        return back()->withSuccess('Great! Image has been successfully uploaded.');
    }

    
    public function update(Request $request, $id)  
    {  
        $message = $this->repository->update($request->except(['_token', '_method']), $id);
        session()->flash('success', "The Shipment was successfully Updated.");
        return $id;  
    }

    public function dataAjax(Request $request)
    {
       
    	$data = [];
        if($request->has('q')){
            $search = $request->q;
            $data =Invoice::select("id","serial_no as text")
            		->where('serial_no','LIKE',"%$search%")
                    ->where('branch_from', Auth::user()->branch_id)
            		->get();
        }
        return response()->json($data);
    }

    


}