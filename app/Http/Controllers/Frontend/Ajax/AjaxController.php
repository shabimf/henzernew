<?php

namespace App\Http\Controllers\Frontend\Ajax;
use App\Models\Catalog;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Traits\CommonTrait;
use App\Repositories\Frontend\ChatRepository;
use App\Models\Message;
use DB;
use Auth;
use Session;

class AjaxController extends Controller
{
    use CommonTrait;
    
    public function __construct(ChatRepository $repository)
    {      
        $this->repository = $repository;
        
    }

    public function autocompleteSearch(Request $request)
    {
        $qry = $request->input('query');
        $data = Catalog::select("name","id")
                ->where("name","LIKE","%".$qry."%")
                ->orWhere('focus_code',"LIKE","%".$qry."%")                
                ->get();
        return response()->json($data);
    
    }
    
    public function stockAvailableCheck(Request $request)
    {
        if($request->input('catalog_id'))
        {
            $catalog_id = $request->input('catalog_id');
            return $this->checkStock($catalog_id);
        }
    }

    public function getNotification() {

        return view('frontend.includes.notifications');

    }

    public function markNotification(Request $request)
    {
        auth()->user()
            ->unreadNotifications
            ->when($request->input('id'), function ($query) use ($request) {
                return $query->where('id', $request->input('id'));
            })
            ->markAsRead();

        return response()->noContent();
    }

    public function getUser(Request $request) {
        return $this->getUsers($request->all());
    }

    public function searchUser(Request $request) {
        return $this->getUsers($request->all());
    }

    public function getUserInfo(Request $request) {

        $query  = $this->GetByID('users', $request->id);
        $output = "";
        $output .= '<header>
                        <a class="back-icon" id="chat-back" onclick="backtoChat()"><i class="fas fa-arrow-left"></i></a>
                        <img src="/images/user.png" alt="">
                        <div class="details">
                            <span>'.$query->first_name . " " . $query->last_name .'</span>
                            <p>'.$query->chat_status.'</p>
                        </div>
                    </header>
                    <div class="chat-box">

                    </div>
                    <form action="#" class="typing-area">
                        <input type="text" class="incoming_id" name="incoming_id" value="'.$request->id.'" hidden>
                        <input type="text" name="message" class="input-field" id="message" placeholder="Type a message here..." autocomplete="off" onkeypress="clickPress(event)">
                        <button class="btn-chat" type="button" onclick="insertChat()"><i class="fab fa-telegram-plane"></i></button>
                    </form>';
        return $output;
    }
    
    public function getChat(Request $request) {

        $outgoing_id = Auth::user()->id;
        $incoming_id = $request->incoming_id;

        $query = DB::select( DB::raw("SELECT messages.* FROM messages LEFT JOIN users ON users.id = messages.outgoing_msg_id
        WHERE (outgoing_msg_id = $outgoing_id AND incoming_msg_id = $incoming_id)
        OR (outgoing_msg_id = $incoming_id AND incoming_msg_id = $outgoing_id) ORDER BY messages.id"));
       
        $output = "";
        if (count($query) > 0) {
            foreach ($query as $row) {
                
                if($row->outgoing_msg_id === $outgoing_id) {
                    $output .= '<div class="chat outgoing">
                                    <div class="details">
                                        <p>'. $row->msg .'</p>
                                    </div>
                                </div>';
                } else {
                    DB::table('messages')->where('id', $row->id)->update(['read_status' => 1]);
                    $output .= '<div class="chat incoming">
                                    <img src="/images/user.png" alt="">
                                        <div class="details">
                                            <p>'. $row->msg .'</p>
                                        </div>
                                </div>';
                }
            }

        } else {
            $output .= '<div class="text">No messages are available. Once you send message they will appear here.</div>';
        }
        echo $output;
    }

    public function insertChat(Request $request) {
        $message =  $this->repository->create($request->except(['_token', '_method']));
        return $message->id;
    }
    
    public function getGraphData(Request $request) {
        
        $from = date('Y-m-01');
        $to = date('Y-m-t');
       
        $qry = DB::table($request->tbl)
                ->whereBetween('created_at', [$from, $to]);
                
        if($this->getRoleName() == "Vendor") {
            if ($request->tbl == "enquiry") {
                if(Session::get('is_source') == 1) $qry->where('branch_from', Auth::user()->branch_id);
                else $qry->where('branch_to', Auth::user()->branch_id);
            } else {
                if(Session::get('is_source') == 1) $qry->where('branch_to', Auth::user()->branch_id);
                else $qry->where('branch_from', Auth::user()->branch_id); 
            }
        }
        if($this->getRoleName() == "Sub Vendor" ) {
            if ($request->tbl == "enquiry")  {
                $qry->where('branch_from', Auth::user()->branch_id);
            }  else {
               $qry->where('branch_to', Auth::user()->branch_id);  
            }    
        } 
        if($this->getRoleName() == "Sourcing Team") { 
          if ($request->tbl == "enquiry")  {
                $qry->where('branch_to', Auth::user()->branch_id);
            }  else {
               $qry->where('branch_from', Auth::user()->branch_id);  
            }    
        }
        $qry = $qry->get(); 
       
        $date_Ar = [];
        $qty = 1;
        foreach($qry as $key => $val) {
            if(isset($date_Ar[date('Y-m-d', strtotime($val->created_at))])) {
              $date_Ar[date('Y-m-d', strtotime($val->created_at))] = $date_Ar[date('Y-m-d', strtotime($val->created_at))] + $qty;
            } else {
              $date_Ar[date('Y-m-d', strtotime($val->created_at))] = $qty;  
            }
        }
        
        $encode = array();
        if (count($date_Ar) > 0) {
            foreach($date_Ar as $u => $v) {
               $new = array(
                            'date' => $u,
                            'count' => $v
                        );
                $encode[] = $new;
            }
        }
       return json_encode($encode);
        
    }
    


    


    
}
