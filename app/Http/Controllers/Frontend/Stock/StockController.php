<?php

namespace App\Http\Controllers\Frontend\Stock;

use App\Http\Controllers\Controller;  
use App\Http\Traits\CommonTrait;
use Illuminate\Http\Request;
use App\Http\Requests\Frontend\Stock\StoreStockRequest;
use App\Http\Requests\Frontend\Stock\ManageStockRequest;
use App\Repositories\Frontend\StocksRepository;
use Illuminate\Http\Response;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\ImportStock;
use App\Exports\ExportStock;


class StockController extends Controller
{
    use CommonTrait;

    protected $repository;

    public function __construct(StocksRepository $repository)
    {      
        $this->repository = $repository;
        $this->middleware(function ($request, $next) {
            $this->data['price_array'] =  $this->get_price_calender();
            $this->data['module']  = 'stock';  
            $this->data['role'] = $this->getRoleName();
            $this->menuSetNav();
            return $next($request);
        });
    }
    
    public function index(ManageStockRequest $request)
    {
        $this->data['list_catalog'] = $this->NameAndID('catalogs');
        $this->data['list'] = $this->repository->retrieveList($request->all());
        $this->data['page_title']  = 'MANAGE Stock';
        return view('frontend.stock.index', $this->data);
    }

    public function store(StoreStockRequest $request)
    {
        $this->repository->create($request->except(['_token', '_method']));
        return redirect()->back()->with("success","The stock was successfully created.");       
    }

    public function import() 
    {
        Excel::import(new ImportStock, request()->file('file'));    
        return redirect()->back()->with("success","The file important successfully!...");      ;
    }

    public function export() 
    {
        return Excel::download(new ExportStock, 'stock.xlsx');
    }

    
}