<?php

namespace App\Http\Traits;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use App\Models\Auth\User;
use Notification;
use App\Notifications\Frontend\EnquiryNotification;
use DB;  
use Auth;
use Session;
use App\Models\Message;

trait CommonTrait
{

    public function NameAndID($table)
    {
        return DB::table($table)->select('id', 'name')->whereNull('deleted_at')->pluck('name', 'id')->all();
    }

    public function VendorList($options = '')
    {
        $api = isset($options['api']) ? (int) $options['api']  : 0;
        if ($api == 1) {
          return DB::table("branches")->select('*')->where("type", 2)->whereNull('deleted_at')->get();  
        } else {
        $type = 2;
        if($this->getRoleName() == "Sub Vendor") $type = 3;
        return DB::table("branches")->select('id', 'name')->where("type", $type)->whereNull('deleted_at')->pluck('name', 'id')->all();
        }
    }

    public function CurrencyList()
    {
        return DB::table('countries')
                ->select('countries.name','countries.exchange_rate as rate','currency.name as currency')
                ->leftjoin('currency', 'countries.currency_id', '=', 'currency.id')
                ->whereNull('countries.deleted_at')
                ->get();
    }
    
    public function SourcingList()
    {
        $type = 1;
        if($this->getRoleName() == "Sub Vendor") $type = 2;
        return DB::table("branches")->select('id', 'name')->where("type", $type)->whereNull('deleted_at')->pluck('name', 'id')->all();
    }
   
    public function SubVendorList($options = '')
    {
        $api = isset($options['api']) ? (int) $options['api']  : 0;
        if(isset(Auth::user()->branch_id) && Auth::user()->branch_id > 0) {
            if ($api == 1) {
              return DB::table("branches")->select('*')->where("branch_id", Auth::user()->branch_id)->whereNull('deleted_at')->get();
            } else {
              return DB::table("branches")->select('id', 'name')->where("branch_id", Auth::user()->branch_id)->whereNull('deleted_at')->pluck('name', 'id')->all();
            }
        } else {
            return DB::table("branches")->select('id', 'name')->where("type", 3)->whereNull('deleted_at')->pluck('name', 'id')->all();
        }
    }

    public function getChildDetails($table, $id)
    {
        return DB::table($table)->where("parent_id", $id)->first();
    }   

    public function FieldByID($table, $field, $id)
    {
        $this->result = DB::table($table)->select($field)->where('id', '=', $id)->first();
        return $this->result->$field;
    }

    public function GetByID($table, $id)
    {
        $this->result = DB::table($table)->where('id', '=', $id)->first();
        return $this->result;
    }

    public function getLastRecordId($table)
    {
        return DB::table($table)->orderBy('id', 'desc')->first()->id;
    }

    public function FocuseCodeByID($field, $focus_code)
    {
        $this->result = DB::table('catalogs')->select($field)->where('focus_code', '=', $focus_code)->first();
        return $this->result->$field;
    }

    public function getRoleID()
    {
        $role_id = DB::table('role_user')->where('user_id', app('auth')->user()->id)->pluck('role_id')->toArray();
        return $role_id[0];
    }


    public function getRoleName()
    {
        $role = Auth::user()->roles()->get();
        return $role[0]['name'];
    }


    public function getToBranchId()
    {
        if($this->getRoleName() == "Vendor" || $this->getRoleName() == "Sub Vendor") {
            $this->result = DB::table('branches')->select('branch_id')->where('id', '=', Auth::user()->branch_id)->first();
            return $this->result->branch_id ? $this->result->branch_id:'';
        } else {
            return false;
        }
       
    }

    public function getRowCount($table, $status, $options = '') {

        $api = isset($options['api']) ? (int) $options['api']  : 0;
        $qry = DB::table($table);
       
        if ($status!="-1") { 
            $qry->where($table.'.status', $status);
        }
        if (isset($options['branch_from'])) { 
            $qry->where($table.'.branch_from', $options['branch_from']);
        }
        if (isset($options['branch_to'])) {
            $qry->where($table.'.branch_to', $options['branch_to']);
        }
        if (isset($options['d_from'])) {
            $qry->where($table.'.created_at', '>=', date('Y-m-d', strtotime($options['d_from']))." 00:00:00");
        }
        if (isset($options['d_to'])) {
            $qry->where($table.'.created_at', '<=', date('Y-m-d', strtotime($options['d_to']))." 23:59:59");
        }
        if($this->getRoleName() == "Vendor") {
            if ($table == "enquiry") {
                if(Session::get('is_source') == 1 || $api == 1) $qry->where($table.'.branch_from', Auth::user()->branch_id);
                else $qry->where($table.'.branch_to', Auth::user()->branch_id);
            } else {
                if(Session::get('is_source') == 1 || $api == 1) $qry->where($table.'.branch_to', Auth::user()->branch_id);
                else $qry->where($table.'.branch_from', Auth::user()->branch_id); 
            }
        }
        if($this->getRoleName() == "Sub Vendor") {
            if ($table == "enquiry") {
               $qry->where($table.'.branch_from', Auth::user()->branch_id);
            } else {
                $qry->where($table.'.branch_to', Auth::user()->branch_id); 
            }
        }
        if($this->getRoleName() == "Sourcing Team") {
            if ($table == "enquiry") {
               $qry->where($table.'.branch_to', Auth::user()->branch_id);
            } else {
               $qry->where($table.'.branch_from', Auth::user()->branch_id); 
            }
        }
        return $qry->count();
    }

    public function getTotalSale($table, $options = '', $type) {

        $qry = DB::table($table . ' as parent')->where('parent.status', 2)
                ->leftjoin($table . '_item as item','item.parent_id', '=', 'parent.id')
                ->select(DB::raw('SUM(item.price*item.qty) as total_sale'));
                
        if ($type == "month") {
            $from = date('Y-m-01')." 00:00:00";
            $to = date('Y-m-t')." 23:59:59";
            $qry->whereBetween('created_at', [$from, $to]);   
        } 

        if ($type == "day") {
            $qry->whereDate('created_at', DB::raw('CURDATE()'));
        }

        if ($type == "prevmonth") {
            $from = date("Y-m-d", mktime(0, 0, 0, date("m")-1, 1))." 00:00:00";
            $to = date("Y-m-d", mktime(0, 0, 0, date("m"), 0))." 23:59:59";
            $qry->whereBetween('created_at', [$from, $to]);   
        } 

        if (isset($options['branch_from'])) { 
            $qry->where('parent.branch_from', $options['branch_from']);
        }

        if (isset($options['branch_to'])) {
            $qry->where('parent.branch_to', $options['branch_to']);
        }
        
        // if (isset($options['d_from'])) {
        //     $qry->where('parent.created_at', '>=', date('Y-m-d', strtotime($options['d_from']))." 00:00:00");
        // }
        // if (isset($options['d_to'])) {
        //     $qry->where('parent.created_at', '<=', date('Y-m-d', strtotime($options['d_to']))." 23:59:59");
        // }

        if($this->getRoleName() == "Sourcing Team" || $this->getRoleName() == "Sub Vendor" || $this->getRoleName() == "Vendor") {
            $qry->where('parent.branch_from', Auth::user()->branch_id);  
        }

        $total_sale = "0.00";
        $result = $qry->first();
        if($result->total_sale>0)$total_sale = number_format($result->total_sale, 2);
        return $total_sale;   
    }

    public function getModel($brand_id) {
        $model = DB::table('brands')->leftJoin('models', 'models.brand_id', '=', 'brands.id')
                    ->where('models.brand_id', $brand_id)
                    ->whereNull('models.deleted_at')
                    ->orderBy('models.name', 'asc')
                    ->pluck('models.name','models.id');
        return $model;
    }

    public function getAttributes($qry) {
        $data = DB::table('attributes')->select("name","id")
                ->where("name","LIKE","%".$qry."%")             
                ->get();
        return response()->json($data);

    }
    
    public function getCities($country_id) {
        $city = DB::table('countries')->leftJoin('cities', 'cities.country_id', '=', 'countries.id')
                    ->where('cities.country_id', $country_id)
                    ->whereNull('cities.deleted_at')
                    ->orderBy('cities.name', 'asc')
                    ->pluck('cities.name','cities.id');
        return $city;
    }

    public function getSeries($model_id) {
        $city = DB::table('models')->leftJoin('series', 'series.model_id', '=', 'models.id')
                    ->where('series.model_id', $model_id)
                    ->whereNull('series.deleted_at')
                    ->orderBy('series.name', 'asc')
                    ->pluck('series.name','series.id');
        return $city;
    }

    public function getRolePermission($role_id)
    {
        $rolePermission = DB::table('roles')->leftJoin('permission_role', 'permission_role.role_id', '=', 'roles.id')
                            ->leftJoin('permissions', 'permission_role.permission_id', '=', 'permissions.id')->where('roles.id', $role_id)
                            ->pluck('permissions.id')->toArray();
        return $rolePermission;
    }

    

    public function getVendors()
    {
        return DB::table("vendors")->select('id', 'company_name')->where('vendor_id','0')->whereNull('deleted_at')->pluck('company_name', 'id')->all();
    }

    public function is_in_stock($catalog_id, $branch_id)
    {
        $stock =  DB::table('catalog_stock')->where('catalog_id', $catalog_id)->where('branch_id', $branch_id)->pluck('qty');
        return $stock[0]??0;
    }

    public function updateStock($catalog_id, $branch_from, $branch_to, $qty)
    {
        DB::table('catalog_stock')
                ->where('catalog_id', $catalog_id)
                ->where('branch_id', $branch_from)
                ->decrement('qty', $qty);
        $check = DB::table('catalog_stock')->where('catalog_id', $catalog_id)->where('branch_id', $branch_to)->count();
        if($check > 0)
        {
            DB::table('catalog_stock')
                    ->where('catalog_id', $catalog_id)
                    ->where('branch_id', $branch_to)
                    ->increment('qty', $qty);
        } else {
            
            $input['qty'] = $qty;
            $input['catalog_id'] = $catalog_id;
            $input['branch_id'] = $branch_to;
            $input['created_by'] = auth::id();
            DB::table('catalog_stock')->insert($input);  
        }

    }

    public function stock_availability($table, $request, $id) {

        $query = DB::table($table . ' as parent')->find($id); 
        $item_qry = DB::table($table . '_item as item')->where('parent_id', $id)->get();
        $errors = array();
        if ($table ==  "enquiry") $branch_to = $query->branch_to;
        else $branch_to = $query->branch_from;
        
        foreach ($item_qry as $key => $val) {
            if (is_numeric($val->id)) {
                $stock = $this->is_in_stock($val->catalog_id, $branch_to);       
                $qty = isset($request['qty'])?$request['qty'][$val->id]:$val->qty;
                if($qty > $stock) {
                    
                    if ($stock > 0 ) {
                        $msg = $this->FieldByID('catalogs','name',$val->catalog_id). ' '.($qty-$stock).' out of stock, '.$stock.' is available';
                    } else {
                        $msg = $this->FieldByID('catalogs','name',$val->catalog_id).' is Out of stock';
                    }
                    $errors[] = $msg;    
                }  
            }
        } 

       
        return $errors;
        
    }

    public function OrderByInvoiceID($id)
    {
        $this->result = DB::table('invoice')->select('id')->where('order_id', '=', $id)->first();
        return $this->result->id;
    }

    public function menuSetNav() {
        if(session('csh_nav') == "" && session('set_nav') == "") { 
            session(['csh_nav' => 1]);
        }
        session(['nav' => " ",'container' => " "]);
        if(session('set_nav') == 1 ){
            session(['nav' => "shrinked_nav",'container' => "shriked_container"]);
        } 
    }

    public function ListItems($table, $options){
       
        $perPage = isset($options['per_page']) ? (int) $options['per_page'] : 18;
        $api = isset($options['api']) ? (int) $options['api']  : 0;
        $orderBy = isset($options['order_by']) && in_array($options['order_by'], $this->sortable) ? $options['order_by'] : 'created_at';
        $order = isset($options['order']) && in_array($options['order'], ['asc', 'desc']) ? $options['order'] : 'desc';
       
        $qry = DB::table($table . ' as parent')
                        ->leftjoin('users', 'users.id', '=', 'parent.created_by')
                        ->leftjoin('branches as from','parent.branch_from', '=', 'from.id')
                        ->leftjoin('branches as to','parent.branch_to', '=', 'to.id')  
                        ->leftjoin($table . '_item as item','item.parent_id', '=', 'parent.id')              
                        ->select([       
                          \DB::raw('
                            parent.*, 
                            users.first_name as created_by,
                            from.name as branch_from_name,
                            to.name as branch_to_name,
                            SUM(item.price*item.qty) as amount'
                          )
                        ]);

                       
        if (isset($options['serial_num'])) {
            $qry->where('parent.serial_no', $options['serial_num']);
        }
        if($this->getRoleName() == "Vendor") {
            if ($table == "enquiry") {
                if(Session::get('is_source') == 1 || $api == 1) $qry->where('parent.branch_from', Auth::user()->branch_id);
                else $qry->where('parent.branch_to', Auth::user()->branch_id);
            } else if ($table == "rma") {
                $qry->where('parent.branch_from', Auth::user()->branch_id);
            }  else if ($table == "order") {
                if(Session::get('is_source') == 1 || $api == 1)  $qry->where('parent.branch_to', Auth::user()->branch_id);
                else $qry->where('parent.branch_from', Auth::user()->branch_id);

                $qry->whereIn('flag', [1, 2]);
            } else {
                if(Session::get('is_source') == 1 || $api == 1) $qry->where('parent.branch_to', Auth::user()->branch_id)->where('parent.status', '!=', 0);
                else $qry->where('parent.branch_from', Auth::user()->branch_id); 
            }
        }
        if($this->getRoleName() == "Sub Vendor") {
            if ($table == "enquiry") {
               $qry->where('parent.branch_from', Auth::user()->branch_id);
            } else if ($table == "rma") {
                $qry->where('parent.branch_from', Auth::user()->branch_id);
            } else {
                $qry->where('parent.branch_to', Auth::user()->branch_id); 
                $qry->where('parent.status', '!=', 0);
            }
        }
        if($this->getRoleName() == "Sourcing Team") {
            if ($table == "enquiry") {
               $qry->where('parent.branch_to', Auth::user()->branch_id);
               $qry->where('parent.status', '!=', 0);
            } else if ($table == "order") {
                  $qry->where('parent.branch_from', Auth::user()->branch_id);
                  $qry->whereIn('flag', [3, 2]);;
            } else if ($table == "rma") {
                $qry->where('parent.branch_to', Auth::user()->branch_id);
            } else {
                $qry->where('parent.branch_from', Auth::user()->branch_id);
            } 
        }
        if (isset($options['branch_from'])) { 
            $qry->where('parent.branch_from', $options['branch_from']);
        }
        if (isset($options['branch_to'])) {
            $qry->where('parent.branch_to', $options['branch_to']);
        }
        if (isset($options['d_from'])) {
            $qry->where('parent.created_at', '>=', date('Y-m-d', strtotime($options['d_from']))." 00:00:00");
        }
        if (isset($options['d_to'])) {
            $qry->where('parent.created_at', '<=', date('Y-m-d', strtotime($options['d_to']))." 23:59:59");
        }
        if (isset($options['status'])) {
            $qry->where('parent.status', $options['status']);
        }
        $qry->orderBy($orderBy, $order)->groupBy('parent.id');
        
        $query = $qry;
        if ($perPage == -1) {
            return $query->get();
        }
        return $query->paginate($perPage);

    }

    public function viewItems($table, $id) {
       
        $query = DB::table($table . '_item as item')
                    ->leftJoin($table.' as parent', 'item.parent_id', '=', 'parent.id')
                    ->leftjoin('catalogs', 'item.catalog_id', '=', 'catalogs.id')
                    ->leftjoin('series', 'series.id', '=', 'catalogs.series_id')
                    ->leftjoin('models', 'models.id', '=', 'series.model_id')
                    ->leftjoin('brands', 'brands.id', '=', 'models.brand_id')
                    ->leftjoin('users', 'users.id', '=', 'parent.created_by')
                    ->leftjoin('branches as from','parent.branch_from', '=', 'from.id')
                    ->leftjoin('branches as to','parent.branch_to', '=', 'to.id')
                    ->select([
                        'catalogs.name as product_name',
                        'series.name as series_name',
                        'models.name as model_name',
                        'brands.name as brand_name',                       
                        'item.*'
                    ])
                    ->where('item.parent_id', $id)->get();
        return $query;
    
            
    }
    public function viewRMAItems($table, $id) {
       
        $query = DB::table($table . '_item as item')
                    ->leftJoin($table.' as parent', 'item.parent_id', '=', 'parent.id')
                    ->leftjoin('catalogs', 'item.catalog_id', '=', 'catalogs.id')
                    ->leftjoin('series', 'series.id', '=', 'catalogs.series_id')
                    ->leftjoin('models', 'models.id', '=', 'series.model_id')
                    ->leftjoin('brands', 'brands.id', '=', 'models.brand_id')
                    ->leftjoin('users', 'users.id', '=', 'parent.created_by')
                    ->leftjoin('branches as from','parent.branch_from', '=', 'from.id')
                    ->leftjoin('branches as to','parent.branch_to', '=', 'to.id')
                    ->select([
                        'catalogs.name as product_name',
                        'series.name as series_name',
                        'models.name as model_name',
                        'brands.name as brand_name',                       
                        'item.*',
                        DB::raw("SUM(item.qty) as qty")
                    ])
                    ->where('item.parent_id', $id)->groupBy(DB::raw("item.catalog_id"))->get();
        return $query;
    
            
    }

    public function viewItemsID($table, $id) { 
        $query = DB::table($table.' as parent')
                    ->leftjoin('branches as from','parent.branch_from', '=', 'from.id')
                    ->leftjoin('branches as to','parent.branch_to', '=', 'to.id')
                    ->leftjoin('branch_details as from_details','from.id', '=', 'from_details.parent_id')
                    ->leftjoin('branch_details as to_details','to.id', '=', 'to_details.parent_id')
                    ->select([
                        'parent.*',
                        'from.name as branch_from_name',
                        'to.name as branch_to_name',
                        'from_details.email as branch_from_email',
                        'to_details.email as branch_to_email'
                    ])
                    ->where('parent.id', $id)->get();
        return $query[0];
    }

    public function getPrice($catalog_id, $branch_id) {
        $query =  DB::table('catalog_price')
                        ->select('price')
                        ->where('catalog_id', $catalog_id)
                        ->where('branch_id', $branch_id)
                        ->orderBy('id', 'desc')->first();
        return $query->price?? 0.00;  
    }

    public function insertItems($table, $input, $id) {
        $input['parent_id'] = $id;
        $input['status'] = 0;
        $query = DB::table($table . ' as parent')->find($id);

        if( $table == "enquiry" ){
            $branch_to = $query->branch_to;
        } else {
            $branch_to = $query->branch_from;
        }
        
        $input['price'] = $this->getPrice($input['catalog_id'], $branch_to);
        $item_query = DB::table($table . '_item as item')
                            ->where('catalog_id', $input['catalog_id'])
                            ->where('parent_id', $id)
                            ->first();
        if ($item_query === null ) {
            if ($item = DB::table($table . '_item')->insert($input)) {
                return $query;
            }
        } else {
           
            $input['qty'] = $item_query->qty + $input['qty'];
            if (DB::table($table . '_item')->where('id', $item_query->id)->update($input)) {  
                return $query;
            }
        }
    }

    public function update_serial_number($module, $table, $id)
    {
        $branch_id = $this->FieldByID($table, 'branch_to', $id);
        $totalCount = DB::table($table)
            ->where([
                        ['branch_to', '=', $branch_id],
                        ['serial_no', '!=', null],
                        ['serial_no', '!=', ''],
                        ['status', '=', 1],
                    ])
            ->get()->count();

          

        if ($module == 'enquiry') {
            $pre = "EN";
        } 
        else if ($module == 'quotation') {
            $pre = "QT";
        } else if ($module == 'order') {
            $pre = "SO";
        } else if ($module == 'invoice') {
            $pre = "IV";
        } else if ($module == 'rma') {
            $pre = "RMA";
        } else {
            dd("Check Sales Controller, No Table");
        }

        $branch_prefix = $this->FieldByID("branches", 'code', $branch_id);

        $serial_no = $pre . $branch_prefix . ++$totalCount;

        while (DB::table($table)->where("serial_no", "=", $serial_no)->count() > 0) {
            $serial_no = $pre . $branch_prefix . ++$totalCount;
        }
        DB::table($table)->where('id', $id)->update(['serial_no' => $serial_no]);


    }

    public function get_price_calender()
    {
        if($this->getRoleName() == "Administrator" || $this->getRoleName() == "Sourcing Team") {
            $branch_id = Auth::user()->branch_id;
        } else {
            $branch_id = $this->getToBranchId();
        } 
        $query = DB::table('catalog_price')
                            ->select('catalogs.name','catalog_price.price')
                            ->where('branch_id', $branch_id)
                            ->leftjoin('catalogs', 'catalog_price.catalog_id', '=', 'catalogs.id')
                            ->orderBy('catalog_price.id', 'desc')
                            ->get();
        $price_array = $data = array(); 
        foreach ($query as $key => $val) {
            
            if(isset($price_array[$val->name])){ 
               $price_array[$val->name] = $price_array[$val->name].",".$val->price;
            } else {
               $price_array[$val->name] = $val->price;
            }
        }
        $count = 0;
        $end = 25;
        foreach ($price_array as $key => $value) {
            $price = explode(',', $value);
            $P1 = $price[0]??0;
            $P2 = $price[1]??0;
           
            $difference = 0;
            if ($P2 > 0) {
                $difference = (($P1 - $P2) / $P2) * 100;
            }
            $data[$count]['name'] = $key;
            $data[$count]['price'] = $price[0];
            $data[$count]['old_price'] = $P2;
            $data[$count]['difference'] = round($difference, 0);
            $count++;
            if ($count == $end) break;
        }
        return $data;
    }

    function dashboardList($request){
        $options['branch_from'] = $request->branch_from;
        $options['branch_to'] = $request->branch_to;
        $options['d_from'] = $request->from_date;
        $options['d_to'] = $request->to_date;
        
        $this->data['enquiry_count'] = $this->getRowCount('enquiry', 1 , $options);
        $this->data['enquiry_total_count'] =  $this->getRowCount('enquiry','-1', $options);

        $this->data['quotation_count'] = $this->getRowCount('quotation', 1 , $options);
        $this->data['quotation_total_count'] = $this->getRowCount('quotation','-1' , $options);

        $this->data['order_count'] = $this->getRowCount('order', 1 , $options);
        $this->data['order_total_count'] = $this->getRowCount('order','-1' , $options);

        $this->data['invoice_count'] = $this->getRowCount('invoice', 1, $options);
        $this->data['invoice_total_count'] = $this->getRowCount('invoice','-1' , $options);
        $this->data['total_sale'] =  $this->getTotalSale('invoice', $options, 'month');
        
        header('Content-type: application/json');
        
        echo json_encode($this->data, JSON_PRETTY_PRINT);
          
        
    }

    public function get_branch_users($branch_id) {

      return DB::table('users')->where('branch_id', $branch_id)->whereNull('deleted_at')->pluck('first_name', 'id')->all();
    
    }

    public function insertRMAItems($table, $input, $id) {

        $input['parent_id'] = $id;
        $query = DB::table($table . ' as parent')->find($id);
        $branch_to = $query->branch_from;
        $input['price'] = $this->getPrice($input['catalog_id'], $branch_to);
        $qty = $input['qty'];
        for ($i=1;$i <= $qty;$i++) {
            $input['qty'] = 1;
            $item = DB::table($table . '_item')->insert($input);
        }
        return $query;
        
    }

    public function get_shipment_document($parent_id, $type) {

        return DB::table('shipment_document')->where('parent_id', $parent_id)->where('type_id', $type)->pluck('filename')->all();
      
    }

    public function getUserNotiID()
    {
        return DB::table('notification_assigned_user')->select('notification_id')->where("user_id", Auth::user()->id)->first();
    } 

    public function getPriceGraphID()
    {
        return DB::table('price_graph_settings')->select('type_id')->where("user_id", Auth::user()->id)->first();
    }
    
    public function markReadNotification(array $input) {
        if(isset($input['id'])) {
        auth()->user()
            ->unreadNotifications
            ->when($input['id'], function ($query) use ($input) {
                return $query->where('id', $input['id']);
            })
            ->markAsRead();
        } else {
          auth()->user()->unreadNotifications->markAsRead();
        }

        return response()->noContent();
    }
    
    public function getIteamArray($table, $jointbl, $id)
    {
        return  DB::table($table)
                        ->leftjoin($jointbl, $jointbl.'.'.$table.'_id', '=', $table.'.id')
                        ->leftjoin($jointbl.'_item', $jointbl.'_item.parent_id', '=', $jointbl.'.id')
                        ->where($jointbl.".".$table.'_id', $id)
                        ->pluck($jointbl.'_item.catalog_id')->toArray();
    }

    public function getUsers(array $input)
    {
        $outgoing_id = Auth::user()->id;
        $branch_to = $this->getToBranchId();
        if(isset($input['searchTerm'])) {
            $searchTerm = $input['searchTerm'];
            if ($this->getRoleName() == "Vendor" || $this->getRoleName() == "Sub Vendor") {
               $query = DB::table('users')
                        ->select('id','first_name','last_name','chat_status')
                        ->where('branch_id', $branch_to)
                        ->whereNotIn('id', [Auth::user()->id])
                        ->where('first_name', 'LIKE', "%{$searchTerm}%")
                        ->orWhere('last_name', 'LIKE', "%{$searchTerm}%")
                        ->get(); 
                
            } elseif ($this->getRoleName() == "Sourcing Team") {
               $query = DB::table('users')
                        ->leftjoin('branches', 'branches.id', '=', 'users.branch_id')
                        ->select('users.id','users.first_name','users.last_name','users.chat_status')
                        ->where('branches.branch_id', Auth::user()->branch_id)
                        ->whereNotIn('users.id', [Auth::user()->id])
                        ->where('users.first_name', 'LIKE', "%{$searchTerm}%")
                        ->orWhere('users.last_name', 'LIKE', "%{$searchTerm}%")
                        ->get(); 
                
            }  else {
                 $query = DB::table('users')
                            ->whereNotIn('id', [Auth::user()->id])
                            ->where('first_name', 'LIKE', "%{$searchTerm}%")
                            ->orWhere('last_name', 'LIKE', "%{$searchTerm}%")
                            ->get();
            }
        } else {
            
            if ($this->getRoleName() == "Vendor" || $this->getRoleName() == "Sub Vendor") {
                $query = DB::table('users')
                        ->leftjoin('messages', 'messages.outgoing_msg_id', '=', 'users.id')
                        ->select('users.id','users.first_name','users.last_name','users.chat_status')
                        ->whereNotIn('users.id', [Auth::user()->id])
                        ->where('users.branch_id', $branch_to)->groupBy('users.id')->orderBy('messages.id')->get(); 
                
            } elseif ($this->getRoleName() == "Sourcing Team") {
               $query = DB::table('users')
                        ->leftjoin('branches', 'branches.id', '=', 'users.branch_id')
                        ->select('users.id','users.first_name','users.last_name','users.chat_status')
                        ->whereNotIn('users.id', [Auth::user()->id])
                        ->where('branches.branch_id', Auth::user()->branch_id)->get(); 
                
            }  else {
               $query = DB::table('users')
                        ->select('id','first_name','last_name','chat_status')
                        ->whereNotIn('id', [Auth::user()->id])
                        ->get();  
            }
        }
        
        $output = "";

        if(count($query) == 0){
          $output .= "No users are available to chat";
        } else {
           foreach ($query as $row) {
            $uid = $row->id; 
           
            $query1 = Message::where(function ($query) use ($uid) {
                        $query->where('incoming_msg_id', '=', $uid)
                            ->orWhere('outgoing_msg_id', '=', $uid);
                    })->where(function ($query) use ($outgoing_id) {
                        $query->where('outgoing_msg_id', '=', $outgoing_id)
                            ->orWhere('incoming_msg_id', '=', $outgoing_id);
                    })->orderByDesc('messages.id')
                        ->limit(1)
                        ->get();
            (count($query1) > 0) ? $result = $query1[0]->msg : $result ="No message available";
            (strlen($result) > 28) ? $msg =  substr($result, 0, 28) . '...' : $msg = $result;
            if(isset($query1[0]->outgoing_msg_id)){
                ($outgoing_id == $query1[0]->outgoing_msg_id) ? $you = "You: " : $you = "";
            }else{
                $you = "";
            }
            ($row->chat_status == "Offline now") ? $offline = "offline" : $offline = "";
            $output .=  '<a href="javascript:void(0)" class="chatid" data-id="'.$row->id.'">
                    <div class="content">
                    <img src="images/user.png" alt=""> <div class="status-dot '. $offline .'"><i class="fas fa-circle chat-circle "></i>
                    </div>
                    <div class="details">
                        <span>'. $row->first_name. " " . $row->last_name .'</span>
                        <p>'. $you . $msg .'</p>
                    </div>
                    </div>

                    <div class="status-dot"><span class="chat-date">'.\Carbon\Carbon::parse($query1[0]->created_at)->diffForHumans().'</span>';
                    if (getChatCount($outgoing_id,$uid) > 0) $output .=  '<br><span class="badgechat" style="float:right">'.getChatCount($outgoing_id,$uid).'</span>';
                   
                    
                    $output .=  '</div></a>';
           }
        }
        return $output;  
    }
    
    function getnotification() {
        $encode = array();
        foreach(UnreadNotificationFetch() as $notification) {
            $module = $notification->data['module'];
            $serial_no = DB::table($module)->select('serial_no')->where('id',$notification->data['parent_id'])->first()->serial_no;  
            $new = array(
                            'id' => $notification->id,
                            'serial_no' => $serial_no,
                            'comment' => \Illuminate\Support\Str::limit($notification->data['comment'], 40, $end='...'),
                            'user_name' => \App\Models\Auth\User::where('id',$notification->data['created_by'])->first()->first_name,
                            'time'=> \Carbon\Carbon::parse($notification->created_at)->diffForHumans()
                        );
                $encode[] = $new;
        }
         header('Content-type: application/json');
        echo json_encode($encode, JSON_PRETTY_PRINT);
    } 
    
    function sendFirebaseNotification($fb_key_array, $title, $body){
        $url = "https://fcm.googleapis.com/fcm/send";
        //replace serverkey with your firebase console project server key
        $serverKey = "AAAABlaqKiM:APA91bFyT3O7aB4n7Jrk1TXxKr0kHkUSLx-S1lp44cuDcwTNfC-51RBcgmySTxbk4HMiB6LhNwNagFVhTmNMgjSekmB3uDth7JZJ-VN20HWXzR6I5K-HoaBPkdyr2t-uvzP-O2yJ5teb";
        
        $notification = array('title' =>$title , 'body' => $body, 'vibrate' => 1, 'sound' => 1, 'badge' => '1');
        $arrayToSend = array(
            'registration_ids' => $fb_key_array, 'notification' => $notification,'priority'=>'high');
        $json = json_encode($arrayToSend);
        $headers = array();
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Authorization: key='. $serverKey;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST,"POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);
        $response = curl_exec($ch);
        curl_close ($ch);
    }
    
}

