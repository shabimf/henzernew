<?php 
namespace App\Http\Traits;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

trait GetTrait
{
    // USER FORM
    public function ValidateUserForm($request){

        $validator =  Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
            'user_type' => 'required',
        ]);

        return $validator;
    }

    // COUNTRY FORM 


    public function validateCountryForm($request){

        $rules = [
            'name' => ['required', 'max:191'],
            'status' => ['boolean'],
            'currency_id' => ['required'],
            'warranty' => ['required'],
            'exchange_rate' => ['required'],
        ];

        $messsages = [
            'name.required' => 'Country name must required',
            'name.max' => 'Country may not be greater than 191 characters.',
            'currency_id.required' => 'Currency must required',
            'warranty.required' => 'Warranty must required',
            'exchange_rate.required' => 'Exchange rate must required',
        ];
        
        $validator = Validator::make($request->all(), $rules, $messsages);
        return $validator;


    }

    public function GetCountryForm($post, $request){

        $post->name = $request->input('name');
        $post->currency_id = $request->input('currency_id'); 
        $post->warranty = $request->input('warranty');
        $post->exchange_rate = $request->input('exchange_rate');
        $post->status = 0;
        if($request->has('status'))
        $post->status = 1;

        return $post;
    }

    //Brand Form

    public function validateBrandForm($request){

        $rules = [
            'name' => ['required', 'max:191'],
            'status' => ['boolean'],
        ];

        $messsages = [
            'name.required' => 'Brand name must required',
            'name.max' => 'Brand may not be greater than 191 characters.',
        ];
        
        $validator = Validator::make($request->all(), $rules, $messsages);
        return $validator;


    }

    // Model Form


    public function validateModelForm($request){

        $rules = [
            'brand_id' => ['required'],
            'name' => ['required', 'max:191'],
            'status' => ['boolean'],
        ];

        $messsages = [
            'brand_id.required' => 'Brand name must required',
            'name.required' => 'Model name must required',
            'name.max' => 'Model may not be greater than 191 characters.',
        ];
        
        $validator = Validator::make($request->all(), $rules, $messsages);
        return $validator;


    }

    public function GetModelForm($post, $request){

        $post->name = $request->input('name');
        $post->brand_id = $request->input('brand_id');

        $post->status = 0;
        if($request->has('status'))
        $post->status = 1;

        return $post;
    }

    //Cities Form


    public function validateCitiesForm($request){

        $rules = [
            'country_id' => ['required'],
            'name' => ['required', 'max:191'],
            'status' => ['boolean'],
        ];

        $messsages = [
            'country_id.required' => 'Country name must required',
            'name.required' => 'City name must required',
            'name.max' => 'City may not be greater than 191 characters.',
        ];
        
        $validator = Validator::make($request->all(), $rules, $messsages);
        return $validator;


    }

    public function GetCityForm($post, $request){

        $post->name = $request->input('name');
        $post->country_id = $request->input('country_id');

        $post->status = 0;
        if($request->has('status'))
        $post->status = 1;

        return $post;
    }

    //Permission Form

    public function validatePermissionForm($request){

        $rules = [
            'name' => ['required', 'max:191'],
            'display_name' => 'required|max:191',
        ];

        $messsages = [
            'name.required' => 'Permission name must required',
            'name.max' => 'Permission name may not be greater than 191 characters.',
            'display_name.required' => 'Display name must required',
            'name.max' => 'Display name may not be greater than 191 characters.',
        ];
        
        $validator = Validator::make($request->all(), $rules, $messsages);
        return $validator;


    }

    public function GetPermissionForm($post, $request){
        $input['sort'] = $request->input('sort');
        $post->name = $request->input('name');
        $post->display_name = $request->input('display_name');
        $post->sort = isset($input['sort']) && strlen($input['sort']) > 0 && is_numeric($input['sort']) ? (int) $input['sort'] : 0;
        $post->status = 1;

        return $post;
    }

    //Role Form

    public function validateRoleForm($request, $id){

        $permissions = '';
        
        if ($request->associated_permissions != 'all') {
            $permissions = 'required';
        }
        
        if($id > 0 ){
            $rules = [
               'name' => 'required|max:191|unique:roles,name,'.$id,
               'permissions' => $permissions,
            ];
        } else {
            $rules = [
                'name' => 'required|max:191|unique:roles,name',
                'permissions' => $permissions,
            ];
        }
        

        $messsages = [
            'permissions.required' => 'You must select at least one permission for this role.',
        ];
        
        $validator = Validator::make($request->all(), $rules, $messsages);
        return $validator;


    }

    public function GetRoleForm($post, $request){

        $input['status'] = $request->input('status');
        $all = $request->input('associated_permissions') == 'all' ? true : false;

        $post->name = $request->input('name');
        $post->all = $all;
        $post->sort = $request->input('sort');
        $post->status = (isset($input['status']) && $input['status'] == 1) ? 1 : 0;

        return $post;
    }

    //Series Form


    public function validateSeriesForm($request){

        $rules = [
            'brand_id' => ['required'],
            'model_id' => ['required'],
            'name' => ['required', 'max:191'],
            'status' => ['boolean'],
        ];

        $messsages = [
            'brand_id.required' => 'Brand name must required',
            'model_id.required' => 'Model name must required',
            'name.required' => 'Series name must required',
            'name.max' => 'Series may not be greater than 191 characters.',
        ];
        
        $validator = Validator::make($request->all(), $rules, $messsages);
        return $validator;


    }


    public function GetSeriesForm($post, $request){

        $post->name = $request->input('name');
        $post->brand_id = $request->input('brand_id');
        $post->model_id = $request->input('model_id');
        $post->status = 0;
        if($request->has('status'))
        $post->status = 1;

        return $post;
    }

    // Branch Form

    public function validateBranchesForm($request, $id){

        
        if ($id > 0) {
            $rules = [
                'country_id' => ['required'],
                'city_id' => ['required'],
                'type' => ['required'],
                'name' => ['required', 'max:191'],
                'code' => ['required', 'max:191'],
                'company_name' => ['required', 'max:191'],
                'contact_person' => ['required', 'max:191'],
                'email' => ['required','email','max:255','unique:vendors,email,'.$id],
                'phone_number' => ['required','regex:/^([0-9\s\-\+\(\)]*)$/','min:10','unique:vendors,phone_number,'.$id],
                'address' => ['required'],
                'status' => ['boolean'],
            ];
        } else {
            $rules = [
                'country_id' => ['required'],
                'city_id' => ['required'],
                'type' => ['required'],
                'name' => ['required', 'max:191'],
                'code' => ['required', 'max:191'],
                'status' => ['boolean'],
                'company_name' => ['required', 'max:191'],
                'contact_person' => ['required', 'max:191'],
                'email' => ['required','email','max:255','unique:vendors,email'],
                'phone_number' => ['required','regex:/^([0-9\s\-\+\(\)]*)$/','min:10','unique:vendors,phone_number'],
                'address' => ['required'],
            ];
        }

        

        $messsages = [
            'country_id.required' => 'Country name must required',
            'city_id.required' => 'City name must required',
            'type.required' => 'Type must required',
            'name.required' => 'Branch name must required',
            'name.max' => 'Branch may not be greater than 191 characters.',
            'code.required' => 'Branch code must required',
            'code.max' => 'Branch may not be greater than 191 characters.',
            'company_name.required' => 'Company name must required',
            'company_name.max' => 'Company name may not be greater than 191 characters.',
            'contact_person.required' => 'Contact person must required',
            'contact_person.max' => 'Contact person may not be greater than 191 characters.',
            'email.required' => 'Email must required',
            'email.unique' => 'The email has already been taken.',
            'email.max' => 'Email may not be greater than 255 characters.',
            'phone_number.required' => 'Phone no must required',
            'address.required' => 'Address must required',
        ];
        
        
        $validator = Validator::make($request->all(), $rules, $messsages);
        return $validator;


    }


    public function GetBranchForm($request){

        $post['name'] = $request->input('name');
        $post['country_id'] = $request->input('country_id');
        $post['city_id'] = $request->input('city_id');
        $post['code'] = $request->input('code');
        $post['type'] = $request->input('type');
        $post['branch_id'] = $request->input('branch_id');
        if($request->input('ctype') == 2) $post['type'] = 3;
        $post['status'] = 0;
        if($request->has('status'))
        $post['status'] = 1;

        return $post;
    }

    public function GetBranchDetailsForm($request){

        $post['company_name'] = $request->input('company_name');
        $post['contact_person'] = $request->input('contact_person');
        $post['email'] = $request->input('email');
        $post['phone_number'] = $request->input('phone_number');
        $post['address'] = $request->input('address');
        $post['branch_id'] = $request->input('branch_id');

        return $post;
    }

    


    // vendor Form

    public function validateVendorForm($request){

        $rules = [
            'branch_id' => ['required'],
            'company_name' => ['required', 'max:191'],
            'contact_person' => ['required', 'max:191'],
            'email' => ['required','email','max:255','unique:vendors,email'],
            'phone_number' => ['required','regex:/^([0-9\s\-\+\(\)]*)$/','min:10','unique:vendors,phone_number'],
            'address' => ['required'],
            'status' => ['boolean'],
        ];

        $messsages = [
            'branch_id.required' => 'Branch name must required',
            'company_name.required' => 'Company name must required',
            'company_name.max' => 'Company name may not be greater than 191 characters.',
            'contact_person.required' => 'Contact person must required',
            'contact_person.max' => 'Contact person may not be greater than 191 characters.',
            'email.required' => 'Email must required',
            'email.unique' => 'The email has already been taken.',
            'email.max' => 'Email may not be greater than 255 characters.',
            'phone_number.required' => 'Phone no must required',
            'address.required' => 'Address must required',
        ];
        
        $validator = Validator::make($request->all(), $rules, $messsages);
        return $validator;

    }

    public function GetVendorForm($post, $request){

        $post->company_name = $request->input('company_name');
        $post->branch_id = $request->input('branch_id');
        $post->contact_person = $request->input('contact_person');
        $post->email = $request->input('email');
        $post->phone_number = $request->input('phone_number');
        $post->address = $request->input('address');
        $post->vendor_id = 0;
        $post->status = 0;
        if($request->has('status'))
        $post->status = 1;

        return $post;
    }

    public function validateEditVendorForm($request, $id){

        $rules = [
            'branch_id' => ['required'],
            'company_name' => ['required', 'max:191'],
            'contact_person' => ['required', 'max:191'],
            'email' => ['required','email','max:255','unique:vendors,email,'.$id],
            'phone_number' => ['required','regex:/^([0-9\s\-\+\(\)]*)$/','min:10','unique:vendors,phone_number,'.$id],
            'address' => ['required'],
            'status' => ['boolean'],
        ];

        $messsages = [
            'branch_id.required' => 'Branch name must required',
            'company_name.required' => 'Company name must required',
            'company_name.max' => 'Company name may not be greater than 191 characters.',
            'contact_person.required' => 'Contact person must required',
            'contact_person.max' => 'Contact person may not be greater than 191 characters.',
            'email.required' => 'Email must required',
            'email.unique' => 'The email has already been taken.',
            'email.max' => 'Email may not be greater than 255 characters.',
            'phone_number.required' => 'Phone no must required',
            'address.required' => 'Address must required',
        ];
        
        $validator = Validator::make($request->all(), $rules, $messsages);
        return $validator;

    }


    public function GetSubVendorForm($post, $request){

        $post->company_name = $request->input('company_name');
        $post->vendor_id = $request->input('vendor_id');
        $post->contact_person = $request->input('contact_person');
        $post->email = $request->input('email');
        $post->phone_number = $request->input('phone_number');
        $post->address = $request->input('address');
        $post->branch_id = 0;
        $post->status = 0;
        if($request->has('status'))
        $post->status = 1;

        return $post;
    }

    public function validateSubVendorForm($request, $id){

        
        if ($id > 0) {
            $rules = [
                'vendor_id' => ['required'],
                'company_name' => ['required', 'max:191'],
                'contact_person' => ['required', 'max:191'],
                'email' => ['required','email','max:255','unique:vendors,email,'.$id],
                'phone_number' => ['required','regex:/^([0-9\s\-\+\(\)]*)$/','min:10','unique:vendors,phone_number,'.$id],
                'address' => ['required'],
                'status' => ['boolean'],
            ];
        } else {
            $rules = [
                'vendor_id' => ['required'],
                'company_name' => ['required', 'max:191'],
                'contact_person' => ['required', 'max:191'],
                'email' => ['required','email','max:255','unique:vendors,email'],
                'phone_number' => ['required','regex:/^([0-9\s\-\+\(\)]*)$/','min:10','unique:vendors,phone_number'],
                'address' => ['required'],
                'status' => ['boolean'],
            ];
        }
        

        $messsages = [
            'vendor_id.required' => 'Vendor name must required',
            'company_name.required' => 'Company name must required',
            'company_name.max' => 'Company name may not be greater than 191 characters.',
            'contact_person.required' => 'Contact person must required',
            'contact_person.max' => 'Contact person may not be greater than 191 characters.',
            'email.required' => 'Email must required',
            'email.unique' => 'The email has already been taken.',
            'email.max' => 'Email may not be greater than 255 characters.',
            'phone_number.required' => 'Phone no must required',
            'address.required' => 'Address must required',
        ];
        
        $validator = Validator::make($request->all(), $rules, $messsages);
        return $validator;

    }


    public function validateCatalogForm($request, $id){

        
        if ($id > 0) {
            $rules = [
                'brand_id' => ['required'],
                'model_id' => ['required'],
                'series_id' => ['required'],
                'name' => ['required', 'max:191'],
                'focus_code' => ['required','max:255','unique:catalogs,focus_code,'.$id],
                'part_no' => ['required'],
                'description' => ['required'],
                
            ];
        } else {
            $rules = [
                'brand_id' => ['required'],
                'model_id' => ['required'],
                'series_id' => ['required'],
                'name' => ['required', 'max:191'],
                'focus_code' => ['required','max:255','unique:catalogs,focus_code'],
                'part_no' => ['required'],
                'description' => ['required'],
                'image' => ['required', 'image','max:1999'],
            ];
        }
        

        $messsages = [
            'brand_id.required' => 'Brand name must required',
            'model_id.required' => 'Model name must required',
            'series_id.required' => 'Series name must required',
            'name.required' => 'Product name must required',
            'name.max' => 'Product name may not be greater than 191 characters.',
            'focus_code.required' => 'Focus code must required',
            'focus_code.unique' => 'The Focus code has already been taken.',
            'focus_code.max' => 'Focus code may not be greater than 255 characters.',
            'part_no.required' => 'Part no must required',
            'description.required' => 'Description must required',
            'image.required' => 'The : Product image field can not be empty value.',
            'image.max'      => 'The maximun Size of The Product Image must not exceed :max',
        ];
        
        $validator = Validator::make($request->all(), $rules, $messsages);
        return $validator;

    }


    public function GetCatalogForm($post, $request){

        $post->brand_id = $request->input('brand_id');
        $post->model_id = $request->input('model_id');
        $post->series_id = $request->input('series_id');
        $post->name = $request->input('name');
        $post->focus_code = $request->input('focus_code');
        $post->part_no = $request->input('part_no');
        $post->description = $request->input('description');
        //$post->image = $this->uploadImage($request->file('image'));
        $post->status = $request->input('status');
        if ($request->has('image')) {
            // Get image file
            $image = $request->file('image');
            // Make a image name based on user name and current timestamp
            $name = time();
            // Define folder path
            $folder = public_path('images');
            // Make a file path where image will be stored [ folder path + file name + file extension]
            $filePath = $folder . $name. '.' . $image->getClientOriginalExtension();
            // Upload image
            $this->uploadOne($image, $folder, 'public', $name);
            // Set user profile image path in database to filePath
            $post->image = $filePath;
        }

        return $post;
    }  

    public function uploadOne(UploadedFile $uploadedFile, $folder = null, $disk = 'public', $filename = null)
    {
        $name = !is_null($filename) ? $filename : Str::random(25);

        $file = $uploadedFile->storeAs($folder, $name.'.'.$uploadedFile->getClientOriginalExtension(), $disk);

        return $file;
    }

    //Enquiry form

    public function validateEnquiryForm($request){
  
        $rules = [
            'product_id' => ['required'],
            'quantity' => ['required','numeric']
            
        ];

        $messsages = [
            'product_id.required' => 'Product name must required',
            'quantity.required' => 'Quantity must required'
            
        ];
        
        $validator = Validator::make($request->all(), $rules, $messsages);
        return $validator;


    }


    //Attribute Form

    public function validateAttributeForm($request, $id){

        if ($id > 0) {
            $rules = [
                'name' => ['required', 'max:191','unique:attributes,name,'.$id],
                'status' => ['boolean'],
            ];
        }  else {
            $rules = [
                'name' => ['required', 'max:191','unique:attributes,name'],
                'status' => ['boolean'],
            ];
        }
        

        $messsages = [
            'name.required' => 'Attribute name must required',
            'name.max' => 'Attribute may not be greater than 191 characters.',
            'name.unique' => 'The attribute name has already been taken.',
        ];
        
        $validator = Validator::make($request->all(), $rules, $messsages);
        return $validator;


    }


    public function validateStockForm($request){
  
        $rules = [
            'product_id' => ['required'],
            'quantity' => ['required','numeric']
            
        ];

        $messsages = [
            'product_id.required' => 'Product name must required',
            'quantity.required' => 'Quantity must required'
            
        ];
        
        $validator = Validator::make($request->all(), $rules, $messsages);
        return $validator;


    }

    public function GetStockForm($post, $request){

        $post->catalog_id = $request->product_id;
        $post->qty = $request->quantity;
        return $post;
    }
    
    public function validateCargoForm($request){

        $rules = [
            'name' => ['required', 'max:191'],
            'status' => ['boolean']
        ];

        $messsages = [
            'name.required' => 'Cargo Mode name must required',
            'name.max' => 'Cargo Mode may not be greater than 191 characters.'
        ];
        
        $validator = Validator::make($request->all(), $rules, $messsages);
        return $validator;


    }

    public function GetCargoForm($post, $request){

        $post->name = $request->input('name');
        $post->status = 0;
        if($request->has('status'))
        $post->status = 1;

        return $post;
    }

    public function validateAgentForm($request, $id = NULL){
        if ($id > 0) {
            $rules = [
                'name' => ['required', 'max:191'],
                'email' => ['required','string','email','max:255','unique:agent,email,'.$id],
                'phone' => ['required', 'digits:10','unique:agent,phone,'.$id]
                
            ];
        } else {
            $rules = [
                'name' => ['required', 'max:191'],
                'email' => ['required', 'string','email','max:255','unique:agent'],
                'phone' => ['required', 'digits:10','unique:agent'],
                'status' => ['boolean']
    
            ];
        }
       

        $messsages = [
            'name.required' => 'Agent name must required',
            'name.max' => 'Agent may not be greater than 191 characters.',
            'email' => 'Email ID must required',
            'email.max' => 'Email ID may not be greater than 255 characters.',
            'email.unique' => 'The email id has already been taken.',
            'phone' => 'Phone no must required',
            'phone.unique' => 'The phone no has already been taken.'

        ];
        
        $validator = Validator::make($request->all(), $rules, $messsages);
        return $validator;


    }

    public function GetAgentForm($post, $request){

        $post->name = $request->input('name');
        $post->phone = $request->input('phone');
        $post->email = $request->input('email');
        $post->address = $request->input('address');
        $post->status = 0;
        if($request->has('status'))
        $post->status = 1;

        return $post;
    }
    



    
    


    



    

    



    


    



    


    



    



}