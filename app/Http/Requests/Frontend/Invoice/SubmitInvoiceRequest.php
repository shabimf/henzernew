<?php

namespace App\Http\Requests\Frontend\Invoice;

use Illuminate\Foundation\Http\FormRequest;

class SubmitInvoiceRequest extends FormRequest
{
    public function rules()
    {
        return [
            'catalog_id' => ['required'],
            'qty' => ['required','numeric'] 
        ];
    }
    public function messages()
    {
        return [
            'catalog_id.required' => 'Product name is required.',
            'qty.required' => 'Quantity field is required.',
            'qty.numeric' => 'The Quantity must be a number.',
        ];
    }
}
