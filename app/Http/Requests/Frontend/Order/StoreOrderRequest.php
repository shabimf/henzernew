<?php

namespace App\Http\Requests\Frontend\Order;

use Illuminate\Foundation\Http\FormRequest;

class StoreOrderRequest extends FormRequest
{

    public function rules()
    {
        return [
            'branch_from' => ['required'],
            'branch_to' => ['required'] 
        ];
    }
    public function messages()
    {
        return [
            'branch_from.required' => 'Branch from is required.',
            'branch_to.required' => 'Branch to is required.'
        ];
    }
}
