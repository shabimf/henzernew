<?php

namespace App\Http\Requests\Frontend\Quotation;

use Illuminate\Foundation\Http\FormRequest;

class SendQuotationRequest extends FormRequest
{

    public function rules()
    {
        return [
            'catalog_id' => ['required'],
        ];
    }
    
    public function messages()
    {
        return [
            'catalog_id.required' => 'You must select at least one product for this quotation.',
        ];
    }
}
