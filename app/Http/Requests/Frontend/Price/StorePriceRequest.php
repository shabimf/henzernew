<?php

namespace App\Http\Requests\Frontend\Price;

use Illuminate\Foundation\Http\FormRequest;

class StorePriceRequest extends FormRequest
{
    public function authorize()
    {
        return access()->allow('create-price');
    }

    public function rules()
    {
        return [
            'catalog_id' => ['required'],
            'price' => ['required','regex:/^(\d+(\.\d*)?)|(\.\d+)$/'] 
        ];
    }
    public function messages()
    {
        return [
            'catalog_id.required' => 'Product name is required.',
            'price.required' => 'Price field is required.'
        ];
    }
}
