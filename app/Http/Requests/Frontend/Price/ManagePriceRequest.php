<?php

namespace App\Http\Requests\Frontend\Price;

use Illuminate\Foundation\Http\FormRequest;

class ManagePriceRequest extends FormRequest
{
    public function authorize()
    {
        return access()->allow('view-price-management');
    }
    
    public function rules()
    {
        return [
            //
        ];
    }
}
