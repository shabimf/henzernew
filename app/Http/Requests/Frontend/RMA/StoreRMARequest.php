<?php

namespace App\Http\Requests\Frontend\RMA;

use Illuminate\Foundation\Http\FormRequest;

class StoreRMARequest extends FormRequest
{

    public function authorize()
    {
        return access()->allow('create-enquiry');
    }
    
    public function rules()
    {
        return [
            'branch_from' => ['required'],
            'branch_to' => ['required'] 
        ];
    }
    public function messages()
    {
        return [
            'branch_from.required' => 'Branch from is required.',
            'branch_to.required' => 'Branch to is required.'
        ];
    }
}
