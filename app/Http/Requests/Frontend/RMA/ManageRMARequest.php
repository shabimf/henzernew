<?php

namespace App\Http\Requests\Frontend\RMA;

use Illuminate\Foundation\Http\FormRequest;

class ManageRMARequest extends FormRequest
{
    public function authorize()
    {
        return access()->allow('view-enquiry');
    }
    
    public function rules()
    {
        return [
            //
        ];
    }
}
