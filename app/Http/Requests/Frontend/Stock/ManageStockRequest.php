<?php

namespace App\Http\Requests\Frontend\Stock;

use Illuminate\Foundation\Http\FormRequest;

class ManageStockRequest extends FormRequest
{
    public function authorize()
    {
        return access()->allow('view-stock-management');
    }
    
    public function rules()
    {
        return [
            //
        ];
    }
}
