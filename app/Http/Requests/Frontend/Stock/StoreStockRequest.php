<?php

namespace App\Http\Requests\Frontend\Stock;

use Illuminate\Foundation\Http\FormRequest;

class StoreStockRequest extends FormRequest
{
    public function authorize()
    {
        return access()->allow('create-stock');
    }

    public function rules()
    {
        return [
            'catalog_id' => ['required'],
            'qty' => ['required','numeric'] 
        ];
    }
    public function messages()
    {
        return [
            'catalog_id.required' => 'Product name is required.',
            'qty.required' => 'Quantity field is required.',
            'qty.numeric' => 'The Quantity must be a number.',
        ];
    }
}
