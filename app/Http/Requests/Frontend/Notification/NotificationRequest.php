<?php

namespace App\Http\Requests\Frontend\Notification;

use Illuminate\Foundation\Http\FormRequest;

class NotificationRequest extends FormRequest
{

    public function rules()
    {
        return [
            'chk_noti' => ['required'],
          
        ];
    }
    public function messages()
    {
        return [
           
            'chk_noti.required' => 'Notification is required.'
        ];
    }
}
