<?php

namespace App\Http\Requests\Frontend\Subvendor;

use Illuminate\Foundation\Http\FormRequest;

class ManageSubvendorRequest extends FormRequest
{
    public function authorize()
    {
        return access()->allow('view-subvendor-management');
    }
    
    public function rules()
    {
        return [
            //
        ];
    }
}
