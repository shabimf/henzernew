<?php

namespace App\Http\Requests\Frontend\Shipment;

use Illuminate\Foundation\Http\FormRequest;

class SubmitShipmentRequest extends FormRequest
{

    public function rules()
    {
        return [
            'track_no' => ['required','unique:shipment'],
            'mode_id' => ['required'],
            'agent_id' => ['required'],
            'track_link' => ['required'],
            'catalog_id'=> ['required'],
            'carton' => ['required','numeric'],
        ];
    }
    public function messages()
    {
        return [
            'track_no.required' => 'Track No is required.',
            'catalog_id.required' => 'Product name is required.',
            'mode_id.required' => 'Cargo mode is required.',
            'agent_id.required' => 'Agent is required.',
            'track_link.required' => 'Track link is required.',
            'carton.required' => 'Carton is required.',
            'carton.numeric' => 'The carton must be a number.'
        ];
    }
}
