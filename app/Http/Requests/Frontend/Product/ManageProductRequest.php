<?php

namespace App\Http\Requests\Frontend\Product;

use Illuminate\Foundation\Http\FormRequest;

class ManageProductRequest extends FormRequest
{
    public function authorize()
    {
        return access()->allow('view-catalog');
    }
    
    public function rules()
    {
        return [
            //
        ];
    }
}
