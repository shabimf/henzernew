<?php

namespace App\Http\Requests\Frontend\Enquiry;

use Illuminate\Foundation\Http\FormRequest;

class SendEnquiryRequest extends FormRequest
{
    public function rules()
    {
        return [
            'cart_id' => ['required'],
        ];
    }
    
    public function messages()
    {
        return [
            'cart_id.required' => 'You must select at least one product for this enquiry.',
        ];
    }
}
