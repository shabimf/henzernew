<?php

namespace App\Http\Requests\Frontend\Enquiry;

use Illuminate\Foundation\Http\FormRequest;

class ManageEnquiryRequest extends FormRequest
{
    public function authorize()
    {
        return access()->allow('view-enquiry');
    }
    
    public function rules()
    {
        return [
            //
        ];
    }
}
