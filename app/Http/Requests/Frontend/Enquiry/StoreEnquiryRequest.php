<?php

namespace App\Http\Requests\Frontend\Enquiry;

use Illuminate\Foundation\Http\FormRequest;

class StoreEnquiryRequest extends FormRequest
{

    public function authorize()
    {
        return access()->allow('create-enquiry');
    }
    
    public function rules()
    {
        return [
            'branch_from' => ['required'],
            'branch_to' => ['required'] 
        ];
    }
    public function messages()
    {
        return [
            'branch_from.required' => 'Branch from is required.',
            'branch_to.required' => 'Branch to is required.'
        ];
    }
}
