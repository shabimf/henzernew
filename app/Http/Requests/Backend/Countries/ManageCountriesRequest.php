<?php

namespace App\Http\Requests\Backend\Countries;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ManageBlogCategoriesRequest.
 */
class ManageCountriesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->allow('view-country');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
