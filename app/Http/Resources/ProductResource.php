<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class ProductResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'image' => $this->image,
            'focus_code'=> $this->focus_code,
            'part_no'=> $this->part_no,
            'description'=> $this->description,
            'status'=> $this->status,
            'brand_name'=> $this->brand_name,
            'model_name'=> $this->model_name,
            'series_name'=> $this->series_name,
        ];
    }
}
