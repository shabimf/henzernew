<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class OrderResource extends Resource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'serial_no' => $this->serial_no,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'status' => $this->status,
            'branch_from_name'  => isset($this->branch_from_name) ? ($this->branch_from_name) : null,
            'branch_to_name'  => isset($this->branch_to_name) ? ($this->branch_to_name) : null,
            'amount'  => isset($this->amount) ? ($this->amount) : null
        ];
    }
}