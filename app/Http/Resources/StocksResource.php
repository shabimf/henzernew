<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class StocksResource extends Resource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'catalog_id' => $this->catalog_id,
            'qty' => $this->qty,
            'created_at' => $this->created_at->toDateTimeString(),
            'updated_at' => $this->updated_at->toDateTimeString(),
            'created_by' => $this->created_by
        ];
    }
}
