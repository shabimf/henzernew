<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class ShipmentResource extends Resource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'track_no' => $this->track_no,
            'mode_name'=> $this->cargo_name,
            'agent' => $this->agent_name,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'status' => $this->status,
            'branch_from_name'  => isset($this->branch_from_name) ? ($this->branch_from_name) : null,
            'branch_to_name'  => isset($this->branch_to_name) ? ($this->branch_to_name) : null,
            'track_link' => $this->track_link
        ];
    }
}