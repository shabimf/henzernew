<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class ChatResource extends Resource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'incoming_msg_id'=> $this->incoming_msg_id,
            'outgoing_msg_id'=> $this->outgoing_msg_id,
            'msg'=> $this->msg,
            'created_at' => optional($this->created_at)->format('d/m/Y h:i A'),
        ];
    }
}
