<?php

namespace App\Http\Middleware;
use Closure;
use App\Http\Traits\CommonTrait;
use Spatie\Permission\Exceptions\UnauthorizedException;

class CheckPermission
{
    use CommonTrait;
    public function handle($request, Closure $next, $permission)
    {
        if (app('auth')->guest()) {
            throw UnauthorizedException::notLoggedIn();
        }
        $role_id = $this->getRoleID();
        if($role_id >1) {
            $userPermission = $this->getRolePermission($role_id);
            $permissions = is_array($permission)
                ? $permission
                : explode('|', $permission);
            foreach ($permissions as $permission) {
                if (in_array(trim($permission), $userPermission)) {
                    return $next($request);
                }
            }
            throw UnauthorizedException::forPermissions($permissions);   
        }

        return $next($request);        
    }
}
