<?php

namespace App\Exports;

use App\Models\Catalog;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Auth;

class ExportPrice implements FromQuery,WithHeadings
{
    public function headings(): array
    {
        return [
            'code',
            'price'
        ];
    }
    public function query()
    {
        $qry =  Catalog::query()
                ->leftjoin('catalog_price', 'catalogs.id', '=', 'catalog_price.catalog_id') 
                ->select('catalogs.focus_code','catalog_price.price');
        if(Auth::user()->branch_id > 0) {
            $qry->where('catalog_price.branch_id', Auth::user()->branch_id);
        } 
        $qry->groupBy('catalog_price.catalog_id')->orderBy('catalog_price.created_at', 'desc');
        return $qry;
    }
    public function map($bulk): array
    {
        return [
            $bulk->focus_code,
            $bulk->price
        ];
    }

}