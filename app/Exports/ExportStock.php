<?php

namespace App\Exports;

use App\Models\Catalog;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Auth;

class ExportStock implements FromQuery,WithHeadings
{
    public function headings(): array
    {
        return [
            'code',
            'stock'
        ];
    }
    public function query()
    {
        $qry =  Catalog::query()
                ->leftjoin('catalog_stock', 'catalogs.id', '=', 'catalog_stock.catalog_id') 
                ->select('catalogs.focus_code','catalog_stock.qty');
        if(Auth::user()->branch_id > 0) {
            $qry->where('catalog_stock.branch_id', Auth::user()->branch_id);
        } 
        return $qry;
    }
    public function map($bulk): array
    {
        return [
            $bulk->focus_code,
            $bulk->qty
        ];
    }

}