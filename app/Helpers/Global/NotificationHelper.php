<?php
if (! function_exists('UnreadNotificationFetch')) {
    function UnreadNotificationFetch() {
        return auth()->user()->unreadNotifications;
    }
}
if (! function_exists('UnreadNotificationCount')) {
    function UnreadNotificationCount(){
        return auth()->user()->unreadNotifications
                ->count();
    }
}