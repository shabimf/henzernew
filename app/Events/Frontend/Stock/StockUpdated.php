<?php

namespace App\Events\Frontend\Stock;

use Illuminate\Queue\SerializesModels;

/**
 * Class StockUpdated.
 */
class StockUpdated
{
    use SerializesModels;

    /**
     * @var
     */
    public $stock;

    /**
     * @param $page
     */
    public function __construct($stock)
    {
        $this->stock = $stock;
    }
}
