<?php

namespace App\Events\Frontend\Stock;

use Illuminate\Queue\SerializesModels;

/**
 * Class StockCreated.
 */
class StockCreated
{
    use SerializesModels;

    /**
     * @var
     */
    public $stock;

    /**
     * @param $stock
     */
    public function __construct($stock)
    {
        $this->stock = $stock;
    }
}
