<?php

namespace App\Models;

use App\Models\Traits\Attributes\CityAttributes;
use App\Models\Traits\ModelAttributes;
use Illuminate\Database\Eloquent\SoftDeletes;

class City extends BaseModel
{
    use ModelAttributes, SoftDeletes,CityAttributes;
    protected $fillable = [
        'country_id',
        'name',
        'status',
        'created_by',
        'updated_by',
    ];

    protected $casts = [
        'status' => 'boolean',
    ];

    protected $appends = [
        'display_status',
    ];
}
