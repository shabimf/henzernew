<?php 
namespace App\Models;


class RMAItem extends BaseModel
{
    protected $table = 'rma_item';
    public $timestamps = false;
    protected $guarded = array();
}