<?php 
namespace App\Models;


class RMAShipmentItem extends BaseModel
{
    protected $table = 'rma_shipment_item';
    public $timestamps = false;
    protected $guarded = array();
}