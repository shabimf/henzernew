<?php

namespace App\Models;

use App\Models\Traits\Attributes\ModelsAttributes;
use App\Models\Traits\ModelAttributes;
use Illuminate\Database\Eloquent\SoftDeletes;

class Model extends BaseModel
{
    use ModelAttributes, SoftDeletes,ModelsAttributes;
    protected $fillable = [
        'brand_id',
        'name',
        'status',
        'created_by',
        'updated_by',
    ];

    protected $casts = [
        'status' => 'boolean',
    ];

    protected $appends = [
        'display_status',
    ];
}
