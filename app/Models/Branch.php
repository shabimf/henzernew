<?php

namespace App\Models;

use App\Models\Traits\Attributes\BranchesAttributes;
use App\Models\Traits\ModelAttributes;
use Illuminate\Database\Eloquent\SoftDeletes;

class Branch extends BaseModel
{
    use ModelAttributes, SoftDeletes,BranchesAttributes;
    protected $fillable = [
        'country_id',
        'city_id',
        'name',
        'status',
        'created_by',
        'updated_by',
    ];

    protected $casts = [
        'status' => 'boolean',
    ];

    protected $appends = [
        'display_status',
    ];
}
