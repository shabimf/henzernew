<?php 
namespace App\Models;

use App\Models\Traits\Attributes\AgentAttributes;
use App\Models\Traits\ModelAttributes;
use Illuminate\Database\Eloquent\SoftDeletes;

class Agent extends BaseModel
{
    use SoftDeletes,ModelAttributes,AgentAttributes;
    protected $table = 'agent';
    protected $guarded = array();
    protected $casts = [
        'status' => 'boolean',
    ];

    protected $appends = [
        'display_status',
    ];
}