<?php 
namespace App\Models;


class InvoiceItem extends BaseModel
{
    protected $table = 'invoice_item';
    public $timestamps = false;
    protected $guarded = array();
}