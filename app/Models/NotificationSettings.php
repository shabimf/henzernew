<?php 
namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class NotificationSettings extends BaseModel
{
    use SoftDeletes;
    protected $table = 'notification_assigned_user';
    protected $guarded = array();

}