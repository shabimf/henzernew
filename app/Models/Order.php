<?php 
namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends BaseModel
{
    use SoftDeletes;
    protected $table = 'order';
    protected $guarded = array();
}