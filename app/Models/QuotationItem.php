<?php 
namespace App\Models;


class QuotationItem extends BaseModel
{
    protected $table = 'quotation_item';
    public $timestamps = false;
    protected $guarded = array();
}