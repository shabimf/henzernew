<?php 
namespace App\Models;
use App\Models\Traits\ModelAttributes;
use App\Models\Traits\Attributes\CargoAttributes;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cargo extends BaseModel
{
    use SoftDeletes,ModelAttributes,CargoAttributes;
    protected $table = 'cargo_mode';
    protected $guarded = array();
    protected $casts = [
        'status' => 'boolean',
    ];

    protected $appends = [
        'display_status',
    ];
}