<?php 
namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class BranchDetails extends BaseModel
{
    use SoftDeletes;
    protected $table = 'branch_details';
    protected $guarded = array();

}