<?php

namespace App\Models;

use App\Models\Traits\Attributes\VendorsAttributes;
use App\Models\Traits\ModelAttributes;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vendor extends BaseModel
{
    use ModelAttributes, SoftDeletes,VendorsAttributes;
    protected $fillable = [
        'company_name',
        'contact_person',
        'email',
        'phone_number',
        'address',
        'status',
        'created_by',
        'updated_by',
    ];

    protected $casts = [
        'status' => 'boolean',
    ];

    protected $appends = [
        'display_status',
    ];
}
