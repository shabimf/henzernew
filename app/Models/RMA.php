<?php 
namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class RMA extends BaseModel
{
    use SoftDeletes;
    protected $table = 'rma';
    protected $guarded = array();
}