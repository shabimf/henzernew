<?php 
namespace App\Models;


class ShipmentItem extends BaseModel
{
    protected $table = 'shipment_item';
    public $timestamps = false;
    protected $guarded = array();
}