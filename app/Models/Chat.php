<?php 
namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class Chat extends BaseModel
{
    protected $table = 'messages';
    public $timestamps = false;
    protected $guarded = array();

}