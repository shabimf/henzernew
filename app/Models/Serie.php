<?php

namespace App\Models;

use App\Models\Traits\Attributes\SeriesAttributes;
use App\Models\Traits\ModelAttributes;
use Illuminate\Database\Eloquent\SoftDeletes;

class Serie extends BaseModel
{
    use ModelAttributes, SoftDeletes,SeriesAttributes;
    protected $fillable = [
        'brand_id',
        'model_id',
        'name',
        'status',
        'created_by',
        'updated_by',
    ];

    protected $casts = [
        'status' => 'boolean',
    ];

    protected $appends = [
        'display_status',
    ];
}
