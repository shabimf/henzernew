<?php 
namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class Invoice extends BaseModel
{
    use SoftDeletes;
    protected $table = 'invoice';
    protected $guarded = array();

}