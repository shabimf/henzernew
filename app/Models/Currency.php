<?php

namespace App\Models;

use App\Models\Traits\Attributes\CurrencyAttributes;
use App\Models\Traits\ModelAttributes;
use Illuminate\Database\Eloquent\SoftDeletes;

class Currency extends BaseModel
{
    use ModelAttributes, SoftDeletes,CurrencyAttributes;
    protected $fillable = [
        'name',
        'symbol',
        'currency_id',
        'status',
        'created_by',
        'updated_by',
    ];

    protected $casts = [
        'status' => 'boolean',
    ];

    protected $appends = [
        'display_status',
    ];
}
