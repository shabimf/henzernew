<?php 
namespace App\Models;


class CatalogAttribute extends BaseModel
{
    protected $table = 'catalog_attributes';
    public $timestamps = false;
    protected $fillable = ['catalog_id', 'attr_id', 'attr_dec'];
}