<?php

namespace App\Models;

use App\Models\Traits\Attributes\CatalogsAttributes;
use App\Models\Traits\ModelAttributes;
use Illuminate\Database\Eloquent\SoftDeletes;

class Catalog extends BaseModel
{
    use ModelAttributes, SoftDeletes,CatalogsAttributes;
    protected $fillable = [
        'brand_id',
        'model_id',
        'series_id',
        'name',
        'focus_code',
        'status',
        'created_by',
        'updated_by',
    ];

    protected $casts = [
        'status' => 'boolean',
    ];
    
    // protected $appends = [
    //     'display_status',
    // ];
}
