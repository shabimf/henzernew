<?php

namespace App\Models\Traits\Attributes;

trait AgentAttributes
{
    public function getActionButtonsAttribute()
    {
        return '<div class="btn-group action-btn">
                    '.$this->getEditButtonAttribute('edit-agent', 'admin.agent.edit').'
                    '.$this->getDeleteButtonAttribute('delete-agent', 'admin.agent.destroy').'
                </div>';
    }

    public function getStatusLabelAttribute()
    {
        if ($this->isActive()) {
            return "<label class='label label-success'>".trans('labels.general.active').'</label>';
        }

        return "<label class='label label-danger'>".trans('labels.general.inactive').'</label>';
    }

    public function isActive()
    {
        return $this->status == 1;
    }

    public function getDisplayStatusAttribute(): string
    {
        return $this->isActive() ? 'Active' : 'InActive';
    }
}
