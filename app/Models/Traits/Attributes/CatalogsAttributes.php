<?php

namespace App\Models\Traits\Attributes;

trait CatalogsAttributes
{
    public function getActionButtonsAttribute()
    {
        return '<div class="btn-group action-btn">
                    '.$this->getEditButtonAttribute('edit-country', 'admin.catalogs.edit').'
                    '.$this->getDeleteButtonAttribute('delete-country', 'admin.catalogs.destroy').'
                </div>';
    }

    public function getStatusLabelAttribute()
    {
        if ($this->isActive()) {
            return "<label class='label label-success'>".trans('labels.general.active').'</label>';
        }

        return "<label class='label label-danger'>".trans('labels.general.inactive').'</label>';
    }

    public function isActive()
    {
        return $this->status == 1;
    }

    public function getDisplayStatusAttribute(): string
    {
        return $this->isActive() ? 'Published' : 'InActive';
    }
}
