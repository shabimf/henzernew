<?php 
namespace App\Models;


class Document extends BaseModel
{
    protected $table = 'shipment_document';
    public $timestamps = false;
    protected $guarded = array();
}