<?php

namespace App\Models;

use App\Models\Traits\Attributes\Attributes;
use App\Models\Traits\ModelAttributes;
use Illuminate\Database\Eloquent\SoftDeletes;

class Attribute extends BaseModel
{
    use ModelAttributes, SoftDeletes,Attributes;
    protected $fillable = [
        'name',
        'status',
        'created_by',
        'updated_by',
    ];

    protected $casts = [
        'status' => 'boolean',
    ];

    protected $appends = [
        'display_status',
    ];
}