<?php 
namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class Enquiry extends BaseModel
{
    use SoftDeletes;
    protected $table = 'enquiry';
    protected $guarded = array();
}