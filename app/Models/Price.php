<?php 
namespace App\Models;


class Price extends BaseModel
{
    protected $table = 'catalog_price';
    protected $guarded = array();
}