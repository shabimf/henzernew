<?php 
namespace App\Models;


class EnquiryItem extends BaseModel
{
    protected $table = 'enquiry_item';
    public $timestamps = false;
    protected $fillable = ['parent_id', 'qty', 'catalog_id'];
}