<?php

namespace App\Models;

use App\Models\Traits\Attributes\BrandAttributes;
use App\Models\Traits\ModelAttributes;
use Illuminate\Database\Eloquent\SoftDeletes;

class Brand extends BaseModel
{
    use ModelAttributes, SoftDeletes,BrandAttributes;
    protected $fillable = [
        'name',
        'status',
        'created_by',
        'updated_by',
    ];

    protected $casts = [
        'status' => 'boolean',
    ];

    protected $appends = [
        'display_status',
    ];
}