<?php 
namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class RMAShipment extends BaseModel
{
    use SoftDeletes;
    protected $table = 'rma_shipment';
    protected $guarded = array();
}