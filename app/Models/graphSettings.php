<?php 
namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class graphSettings extends BaseModel
{
    use SoftDeletes;
    protected $table = 'price_graph_settings';
    protected $guarded = array();

}