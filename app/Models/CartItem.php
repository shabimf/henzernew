<?php 
namespace App\Models;


class CartItem extends BaseModel
{
    protected $table = 'cart_items';
    protected $fillable = [
        'catalog_id',
        'status',
        'qty',
        'price',
        'created_by',
        'updated_by',
    ];

    protected $casts = [
        'status' => 'boolean',
    ];
}