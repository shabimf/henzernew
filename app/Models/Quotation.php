<?php 
namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class Quotation extends BaseModel
{
    use SoftDeletes;
    protected $table = 'quotation';
    protected $guarded = array();
}