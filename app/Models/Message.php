<?php 
namespace App\Models;


class Message extends BaseModel
{
    protected $table = 'messages';
    public $timestamps = false;
    protected $guarded = array();
}