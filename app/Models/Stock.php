<?php 
namespace App\Models;


class Stock extends BaseModel
{
    protected $table = 'catalog_stock';
    protected $guarded = array();
}