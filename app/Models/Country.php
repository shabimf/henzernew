<?php

namespace App\Models;

use App\Models\Traits\Attributes\CountryAttributes;
use App\Models\Traits\ModelAttributes;
use Illuminate\Database\Eloquent\SoftDeletes;

class Country extends BaseModel
{
    use ModelAttributes, SoftDeletes,CountryAttributes;
    protected $fillable = [
        'name',
        'status',
        'created_by',
        'updated_by',
    ];

    protected $casts = [
        'status' => 'boolean',
    ];

    protected $appends = [
        'display_status',
    ];
}
