<?php

namespace App\Notifications\Frontend;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class EnquiryNotification extends Notification
{
    use Queueable;
    private $enquiryData;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($enquiryData)
    {
        $this->enquiryData = $enquiryData;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'comment' => $this->enquiryData['comment'],
            'parent_id' => $this->enquiryData['parent_id'],
            'created_by' => $this->enquiryData['created_by'],
            'module'=> $this->enquiryData['module']
        ];
    }
}