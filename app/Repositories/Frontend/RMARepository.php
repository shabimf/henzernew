<?php

namespace App\Repositories\Frontend;
use App\Exceptions\GeneralException;
use App\Models\RMA;
use App\Models\RMAItem;
use App\Models\RMAShipment;
use App\Models\RMAShipmentItem;
use App\Repositories\BaseRepository;
use App\Http\Traits\CommonTrait;
use DB;
use Auth;
use PDF;
use Mail;

class RMARepository extends BaseRepository
{
    use CommonTrait;

    public function retrieveList(array $options = [])
    { 
        return $this->ListItems("rma", $options);
    }

    public function getRMAById($id)
    {   
       $rma = $this->viewItemsID('rma', $id);

       if(Auth::user()->branch_id ==  $rma->branch_to) {
        return  $this->viewItems('rma', $id); 
       } else {
        return  $this->viewRMAItems('rma', $id);      
       }
             
    }

    public function getById($id)
    {   
       return  $this->viewItemsID('rma', $id);             
    }

    public function create(array $input) {

        $input['created_by'] = auth::id();
        $rma = RMA::create($input);
        return $rma->id;
    }

    public function createItem(array $input, $id)
    {
        return $this->insertItems('rma', $input, $id); 

    }
    
    public function update(array $input, $id)
    {
      
        DB::beginTransaction();
        if ($id) {
            $this->update_serial_number('rma','rma', $id);
            $query = DB::table('rma')->find($id); 
            DB::table('rma')->where('id', $id)->update(['status' => 1]);
            $item_qry = DB::table('rma_item')->where('parent_id', $id)->get();
            foreach ($item_qry as $key => $val) {
                if (is_numeric($val->id)) {
                    $qty = $input['qty'][$val->id];
                    for ($i=1;$i <= $qty;$i++) {
                        $item['qty'] = 1;
                        $item['status'] = 1;
                        $item['parent_id'] = $id;
                        $item['catalog_id'] = $val->catalog_id;
                        $item['price'] = $this->getPrice($val->catalog_id, $query->branch_from);
                        DB::table('rma_item')->insert($item);
                    }
                }
            }
            DB::table('rma_item')->where('parent_id', '=', $id)->where('status', '=', 0)->delete();
            DB::commit();
            
            
            $data["title"] = "Send RMA";
            $data["body"]  = "RMA";
            $data['common_details'] = $this->getById($id);
            $data['details'] = $this->getRMAById($id); 
            $data["to_email"] = $data['common_details']->branch_to_email;
            $data["to_name"] = $data['common_details']->branch_to_name;
            $data["from_email"] = $data['common_details']->branch_from_email;
            $data["from_name"] = $data['common_details']->branch_from_name;
            $this->sendEmail($data, $id);
            
            return $query;

        }
        throw new GeneralException(__('There was a problem creating this enquiry. Please try again.'));
    }
    
    public function status(array $input, $id)
    {
        RMAItem::where('id', $id)
                ->update([
                    'status' => $input['status']
                ]);
        return true;

    }

    public function updateNote(array $input, $id)
    {
        RMA::where('id', $id)
                ->update([
                    'note' => $input['note']
                ]);
        return true;

    }


    

    public function sendEmail($data, $id) {
        // $pdf = PDF::loadView('frontend.mail.pdf', $data);
        // Mail::send('frontend.mail.mail', $data, function($message)use($data, $pdf) {
        //     $message->to($data["to_email"], $data["to_name"])
        //             ->subject($data["title"])
        //             ->attachData($pdf->output(), $data["body"].".pdf");
        // });
    }
        
    public function delete($id)
    {
        $item = RMAItem::find($id);
        if ($item->delete()) {
            return true;
        }
        throw new GeneralException(__('exceptions.backend.blog-category.delete_error'));
    }

    public function retrieveListItems(array $options = [])
    {
        
        $serial_num = isset($options['serial_num']) ?  $options['serial_num'] : '';
        $query = DB::table('rma_item as item')
                ->leftJoin('rma as parent', 'item.parent_id', '=', 'parent.id')
                ->leftjoin('catalogs', 'item.catalog_id', '=', 'catalogs.id')
                ->leftjoin('series', 'series.id', '=', 'catalogs.series_id')
                ->leftjoin('models', 'models.id', '=', 'series.model_id')
                ->leftjoin('brands', 'brands.id', '=', 'models.brand_id')
                ->leftjoin('users', 'users.id', '=', 'parent.created_by')
                ->leftjoin('branches as from','parent.branch_from', '=', 'from.id')
                ->leftjoin('branches as to','parent.branch_to', '=', 'to.id')
                ->select([
                    'catalogs.name as product_name',
                    'series.name as series_name',
                    'models.name as model_name',
                    'brands.name as brand_name',                       
                    'item.*',
                    DB::raw("SUM(item.qty) as qty")
                ])
                ->where('parent.serial_no', $serial_num)->groupBy(DB::raw("item.catalog_id"))->get();
        return $query;
    }

    public function retrieveShippingListItems ($id) {

        $query = DB::table('rma_shipment_item as item')
        ->leftJoin('rma_shipment as parent', 'item.parent_id', '=', 'parent.id')
        ->leftjoin('catalogs', 'item.catalog_id', '=', 'catalogs.id')
        ->leftjoin('series', 'series.id', '=', 'catalogs.series_id')
        ->leftjoin('models', 'models.id', '=', 'series.model_id')
        ->leftjoin('brands', 'brands.id', '=', 'models.brand_id')
        ->leftjoin('users', 'users.id', '=', 'parent.created_by')
        ->leftjoin('cargo_mode', 'cargo_mode.id', '=', 'parent.mode_id')
        ->leftjoin('agent', 'agent.id', '=', 'parent.agent_id')
        ->select([
            'catalogs.name as product_name',
            'series.name as series_name',
            'models.name as model_name',
            'brands.name as brand_name',    
            'cargo_mode.name as cargo_name',   
            'agent.name as agent_name',                
            'item.*',
            'parent.*'
        ])
        ->where('parent.rma_id', $id)->get();
       return $query;

    }


    public function createShipment(array $input)
    {
        $input_arr['created_by'] = auth::id();
        $input_arr['rma_id'] = $input['invoice_id'];
        $input_arr['track_no'] = $input['track_no'];
        $input_arr['mode_id'] = $input['mode_id'];
        $input_arr['agent_id'] = $input['agent_id'];
        $input_arr['track_link'] = $input['track_link'];
        $result = RMAShipment::create($input_arr);
        $item_arr['parent_id'] =   $result->id;
        if (count($input['catalog_id']) > 0) {
            foreach ($input['catalog_id'] as $v) {
                $item_arr['catalog_id'] =   $v;
                $item_arr['qty'] =  $input['qty'][$v];
                $item_arr['status'] =  1;
                RMAShipmentItem::create($item_arr);
            }
        }
        return $result->id;

    }

    Public function qtyupdate(array $input, $id) {
        
        DB::beginTransaction();
        $qty =  $input['returnqty'];
        $query = DB::table('invoice')->find($id); 
        $rma['status'] = 1;
        $rma['invoice_id'] = $id;
        $rma['branch_from'] = $query->branch_from;
        $rma['branch_to'] = $query->branch_to;
        $rma['created_by'] = auth()->user()->id;
        $rma_id = DB::table('rma')->insertGetId($rma);
            
        foreach ($input['check'] as $key => $value) {
            $qty = $input['returnqty'][$value];
            for ($i=1;$i <= $qty;$i++) {
                $item['qty'] = 1;
                $item['price'] = $this->getPrice($value, $rma['branch_to']);
                $item['status'] = 1;
                $item['parent_id'] = $rma_id;
                $item['catalog_id'] = $value;
                DB::table('rma_item')->insertGetId($item);
            }
        }
        $this->update_serial_number('rma','rma', $rma_id);
        $query = DB::table('rma')->find($rma_id); 
        DB::commit();     
        return $query;
    }
    
}
