<?php

namespace App\Repositories\Frontend;
use App\Exceptions\GeneralException;
use App\Models\Invoice;
use App\Models\InvoiceItem;
use App\Repositories\BaseRepository;
use App\Http\Traits\CommonTrait;
use App\Models\Auth\User;
use Notification;
use App\Notifications\Frontend\InvoiceNotification;
use DB;
use Auth;
use PDF;
use Mail;


class InvoiceRepository extends BaseRepository   
{
    use CommonTrait;
    const MODEL = Invoice::class;

    public function retrieveList(array $options = [])
    {
        return $this->ListItems("invoice", $options);
    }

    public function getInvoiceById($id)
    {   
        return  $this->viewItems('invoice', $id); 
    }

    public function getById($id)
    {   
       return  $this->viewItemsID('invoice', $id);             
    }

    public function create(array $input)
    {
        $input['created_by'] = auth::id();
        $invoice = Invoice::create($input);
        return $invoice->id;
    }
    
    public function submitInvoice(array $input, $id)
    {
        return $this->insertItems('invoice', $input, $id); 
    }

    public function delete($id)
    {
        $item = InvoiceItem::find($id);
        if ($item->delete()) {
            return true;
        }

        throw new GeneralException(__('exceptions.backend.blog-category.delete_error'));
    }

    public function update(array $input, $id)
    { 
       
        DB::beginTransaction();
        if ($id) {
            $query = DB::table('invoice')->find($id); 
            $item_qry = DB::table('invoice_item')->where('parent_id', $id)->get();
        
            $stock_availability = $this->stock_availability('invoice', $input, $id);
            
            if (count($stock_availability) > 0) {
                return $stock_availability;
            } else {
               
                foreach ($item_qry as $key => $val) {
                    $is_warranty = 0; 
                  
                    if (is_numeric($val->id)) {
                        
                        $stock = $this->is_in_stock($val->catalog_id, $query->branch_from); 
                        if(isset($input['is_warranty'][$val->id])) { $is_warranty = 1; }
                        $item['qty'] = $input['qty'][$val->id];
                        $item['is_warranty'] = $is_warranty;
                        $item['price'] = $input['price'][$val->id];
                        $item['status'] = 1;

                        //$availability = $stock - $item['qty'];
                        
                        DB::table('invoice_item')
                                   ->where('id', $val->id)
                                   ->update($item);
                        $this->update_serial_number('invoice','invoice', $id);
                        $this->updateStock($val->catalog_id, $query->branch_from, $query->branch_to, $item['qty']);  
                    } 
                }
                DB::table('invoice')->where('id', $id)->update(['status' => 2]);
                DB::commit();
                
                $data["title"] = "Send Invoice";
                $data["body"]  = "Invoice";
                $data['common_details'] = $this->getById($id);
                $data['details'] = $this->getInvoiceById($id); 
                $data["to_email"] = $data['common_details']->branch_to_email;
                $data["to_name"] = $data['common_details']->branch_to_name;
                $data["from_email"] = $data['common_details']->branch_from_email;
                $data["from_name"] = $data['common_details']->branch_from_name;
                $this->sendEmail($data, $id);
                $invoiceData = [
                    'parent_id' => $id,
                    'comment' => '('.$data['common_details']->serial_no.') Invoice created',
                    'created_by' => $query->created_by,
                    'module' => 'invoice'
                ];
                $this->sendInvoiceNotification($query->branch_to, $invoiceData);
                return 1;
            }
            
        }
        throw new GeneralException(__('There was a problem creating this enquiry. Please try again.'));
    }

    public function sendEmail($data, $id) {
        
        // $pdf = PDF::loadView('frontend.mail.pdf', $data);
        // Mail::send('frontend.mail.mail', $data, function($message)use($data, $pdf) {
        //     $message->to($data["to_email"], $data["to_name"])
        //             ->subject($data["title"])
        //             ->attachData($pdf->output(), $data["body"].".pdf");
        // });
    }

    public function sendInvoiceNotification($branch_id, $data) {

        $sales_users = $this->get_branch_users($branch_id);
        foreach($sales_users as $key => $value) {
            Notification::send(User::find($key), new InvoiceNotification($data));
        }
        return true;
      
    }

    public function itemUpdate(array $input, $id) {
        DB::table('invoice_item')->where('id', $id)->update(['qty' => $input['qty'],'price' => $input['price'],'is_warranty' => $input['is_warranty']]);
        return $id;
    }

}
