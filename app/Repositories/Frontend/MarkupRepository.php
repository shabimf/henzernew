<?php

namespace App\Repositories\Frontend;
use App\Exceptions\GeneralException;
use App\Models\Markup;
use App\Repositories\BaseRepository;
use Auth;

class MarkupRepository extends BaseRepository
{
    
    const MODEL = Markup::class;

    public function retrieveList(array $options = [])
    {
        $perPage = isset($options['per_page']) ? (int) $options['per_page'] : 20;
        $orderBy = isset($options['order_by']) && in_array($options['order_by'], $this->sortable) ? $options['order_by'] : 'created_at';
        $order = isset($options['order']) && in_array($options['order'], ['asc', 'desc']) ? $options['order'] : 'desc';
        $qry = $this->query()->leftjoin('users', 'users.id', '=', 'branch_markup_price.created_by')
                        ->leftjoin('branches', 'branches.id', '=', 'branch_markup_price.branch_id') 
                        ->select([
                        'branch_markup_price.*',
                        'users.first_name as created_by',
                        'branches.name as branch_name'
                        ]);

              
        $qry->orderBy($orderBy, $order);
        $query = $qry;
        if ($perPage == -1) {
            return $query->get();
        }

        return $query->paginate($perPage);
    }


    public function create(array $input)
    {
        $input['created_by'] =  auth()->user()->id;
        $qry = Markup::where('branch_id', $input['branch_id'])->first();
        if($qry === null ) {
            if ($markup = Markup::create($input)) {
                return $markup;
            }
        } else {
            $input['updated_by'] = auth()->user()->id;  
            $result = Markup::find($qry->id); 
            if ($result->update($input)) {    
                return $result;
            }
        }
        throw new GeneralException(__('There was a problem creating this markup value. Please try again.'));
    }
    
}
