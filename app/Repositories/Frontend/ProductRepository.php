<?php

namespace App\Repositories\Frontend;

use App\Exceptions\GeneralException;
use App\Models\Catalog;
use App\Models\CatalogAttribute;
use App\Repositories\BaseRepository;
use Illuminate\Http\Response;

class ProductRepository extends BaseRepository
{
   
    const MODEL = Catalog::class;

    public function retrieveList(array $options = [])
    {
        
        $perPage = isset($options['per_page']) ? (int) $options['per_page'] : 20;
        $orderBy = isset($options['order_by']) && in_array($options['order_by'], $this->sortable) ? $options['order_by'] : 'created_at';
        $order = isset($options['order']) && in_array($options['order'], ['asc', 'desc']) ? $options['order'] : 'desc';
        $query = $this->query()
                    ->leftjoin('series', 'series.id', '=', 'catalogs.series_id')
                    ->leftjoin('models', 'models.id', '=', 'series.model_id')
                    ->leftjoin('brands', 'brands.id', '=', 'models.brand_id')
                    ->select([
                        'catalogs.id',
                        'catalogs.name',
                        'catalogs.image',
                        'catalogs.focus_code',
                        'catalogs.part_no',
                        'catalogs.description',
                        'catalogs.status',
                        'series.name as series_name',
                        'models.name as model_name',
                        'brands.name as brand_name'
                    ])
                    ->orderBy('catalogs.'.$orderBy, $order);
        
        if(isset($options['name'])) {
            $query->Where('catalogs.name', 'like', "%{$options['name']}%");
        }
        if ($perPage == -1) {
            return $query->get();
        }

        return $query->paginate($perPage);
    }

    public function getById($id)
    {   
        $catalog =  Catalog::leftjoin('series', 'series.id', '=', 'catalogs.series_id')
                                ->leftjoin('models', 'models.id', '=', 'series.model_id')
                                ->leftjoin('brands', 'brands.id', '=', 'models.brand_id')
                                ->select([
                                    'catalogs.id',
                                    'catalogs.name',
                                    'catalogs.image',
                                    'catalogs.focus_code',
                                    'catalogs.part_no',
                                    'catalogs.description',
                                    'catalogs.status',
                                    'series.name as series_name',
                                    'models.name as model_name',
                                    'brands.name as brand_name'
                                ])
                                ->where('catalogs.id', $id)->get()->toArray();

        $catalog_attribute  = CatalogAttribute::leftjoin('attributes', 'attributes.id', '=', 'catalog_attributes.attr_id')
                                                    ->leftjoin('catalogs', 'catalogs.id', '=', 'catalog_attributes.catalog_id')
                                                    ->select([
                                                        'attributes.name',
                                                        'catalog_attributes.attr_dec as description'
                                                    ])
                                                    ->where('catalogs.id', $id)->get()->toArray();
        $encode = [];
        foreach($catalog as $k=>$v ) {
            $encode['product'] =  [
                        'id' => $v['id'],
                        'name' => $v['name'],
                        'image' => $v['image'],
                        'focus_code' => $v['focus_code'],
                        'part_no' => $v['part_no'],
                        'description'=> filter_var(str_replace("\r\n", '', $v['description'] ), FILTER_SANITIZE_STRING),
                        'status' => $v['status'],
                        'series_name' => $v['series_name'],
                        'model_name' => $v['model_name'],
                        'brand_name' => $v['brand_name'],
                        
            ];
        }

        foreach($catalog_attribute as $k=>$v ) {
            $encode['product']['attribute'][] = array(
                'name' => $v['name'],
                'description' => $v['description']
            );
        };
   
        return [ $encode  ];
                         
    }
}
