<?php

namespace App\Repositories\Frontend;

use App\Exceptions\GeneralException;
use App\Models\Stock;
use App\Repositories\BaseRepository;
use Auth;

class StocksRepository extends BaseRepository
{
    
    const MODEL = Stock::class;

    public function retrieveList(array $options = [])
    {
        $perPage = isset($options['per_page']) ? (int) $options['per_page'] : 20;
        $orderBy = isset($options['order_by']) && in_array($options['order_by'], $this->sortable) ? $options['order_by'] : 'created_at';
        $order = isset($options['order']) && in_array($options['order'], ['asc', 'desc']) ? $options['order'] : 'desc';
        $qry = $this->query()->leftjoin('users', 'users.id', '=', 'catalog_stock.created_by')
                        ->leftjoin('catalogs', 'catalogs.id', '=', 'catalog_stock.catalog_id') 
                        ->select([
                        'catalog_stock.id',
                        'catalogs.name as catalog_id',
                        'catalog_stock.qty',
                        'users.first_name as created_by',
                        'catalog_stock.updated_by',
                        'catalog_stock.created_at',
                        'catalog_stock.updated_at'
                        ]);
        if(Auth::user()->branch_id > 0) {
            $qry->where('catalog_stock.branch_id', Auth::user()->branch_id);
        }      
                  
        $qry->orderBy($orderBy, $order);
        $query = $qry;
        if ($perPage == -1) {
            return $query->get();
        }

        return $query->paginate($perPage);
    }


    public function create(array $input)
    {
       
        $input['created_by'] =  auth()->user()->id;
        $input['branch_id'] =  Auth::user()->branch_id;

        $stock_qry = Stock::where('catalog_id', $input['catalog_id'])
                        ->where('branch_id', Auth::user()->branch_id)
                        ->first();

        if($stock_qry === null ) {
            if ($stock = Stock::create($input)) {
                return $stock;
            }
        } else {

            $input['qty'] = $stock_qry->qty + $input['qty'];
            $input['updated_by'] = auth()->user()->id;  
            $stock = Stock::find($stock_qry->id); 
            if ($stock->update($input)) {
                return $stock;
            }
        }

        throw new GeneralException(__('There was a problem creating this Stock. Please try again.'));
    }
    
}
