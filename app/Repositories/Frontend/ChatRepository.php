<?php
namespace App\Repositories\Frontend;
use App\Exceptions\GeneralException;
use App\Models\Chat;
use App\Repositories\BaseRepository;
use DB;
use Auth;
use App\Http\Traits\CommonTrait;

class ChatRepository extends BaseRepository
{
    use CommonTrait;
    const MODEL = Chat::class;

    public function retrieveList(array $options = [])
    {

        $perPage = isset($options['per_page']) ? (int)$options['per_page'] : 10;
        $orderBy = isset($options['order_by']) && in_array($options['order_by'], $this->sortable) ? $options['order_by'] : 'created_at';
        $order = isset($options['order']) && in_array($options['order'], ['asc', 'desc']) ? $options['order'] : 'asc';
        $query = $this->query()
            ->orderBy($orderBy, $order);
        if ($perPage == - 1)
        {
            return $query->get();
        }

        return $query->paginate($perPage);
    }

    public function create(array $input)
    {
        $input['outgoing_msg_id'] = Auth::user()->id;
        $input['read_status'] = 0;
        $chat = Chat::create($input);
        return $chat;
    }

    public function getById($id)
    {
        $outgoing_id = Auth::user()->id;
        $incoming_id = $id;
        $encode = array();
        $query = DB::select(DB::raw("SELECT messages.*,users.id as user_id,users.first_name,users.last_name FROM messages LEFT JOIN users ON users.id = messages.outgoing_msg_id
        WHERE (outgoing_msg_id = $outgoing_id AND incoming_msg_id = $incoming_id)
        OR (outgoing_msg_id = $incoming_id AND incoming_msg_id = $outgoing_id) ORDER BY messages.id DESC"));
        $arr = [];
        foreach ($query as $row)
        {
           if($row->outgoing_msg_id != $outgoing_id) {
               DB::table('messages')->where('id', $row->id)->update(['read_status' => 1]);
           } 
            $new = array(
                '_id' => $row->id,
                'text' => $row->msg,
                'user_id' => $outgoing_id,
                'createdAt' => date("M d,Y", strtotime($row->created_at))." ". date("h:i a" ,strtotime($row->created_at)) ,
                'user' => array(
                    '_id' => $row->user_id,
                    'name' => $row->first_name . " " . $row->last_name
                )
            );
            $encode[] = $new;
        }
        header('Content-type: application/json');

        echo json_encode($encode, JSON_PRETTY_PRINT);
        exit;
    }

}

