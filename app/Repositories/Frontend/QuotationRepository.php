<?php

namespace App\Repositories\Frontend;
use App\Exceptions\GeneralException;
use App\Models\Quotation;
use App\Models\QuotationItem;
use App\Repositories\BaseRepository;
use App\Http\Traits\CommonTrait;
use App\Models\Auth\User;
use Notification;
use App\Notifications\Frontend\QuotaionNotification;
use DB;
use Auth;
use PDF;
use Mail;

class QuotationRepository extends BaseRepository   
{
    use CommonTrait;
    const MODEL = Quotation::class;

    public function retrieveList(array $options = [])
    {
        return $this->ListItems("quotation", $options);
    }
    
    public function getQuotationById($id)
    {   
        return  $this->viewItems('quotation', $id); 
    }

    public function getById($id)
    {   
       return  $this->viewItemsID('quotation', $id);             
    }

    public function create(array $input)
    {
        $input['created_by'] = auth::id();
        $quotation = Quotation::create($input);
        return $quotation->id;
    }
    
    public function submitQuotation(array $input, $id)
    {
        return $this->insertItems('quotation', $input, $id); 
    }

    public function delete($id)
    {
        $item = QuotationItem::find($id);
        if ($item->delete()) {
            return true;
        }

        throw new GeneralException(__('exceptions.backend.blog-category.delete_error'));
    }
    public function update(array $input, $id)
    { 
      
        DB::beginTransaction();
        if ($id) {
            if($input['status'] == "quotation") {
                $qty =  $input['qty'];
                $is_warranty =  $input['is_warranty'];
                $price =  $input['price'];
                 
                $this->update_serial_number('quotation','quotation', $id);
                DB::table('quotation')->where('id', $id)->update(['status' => 1]);           
                $item_qry = DB::table('quotation_item')->where('parent_id', $id)->get();  
               
                foreach ($item_qry as $key => $val) {
                    $is_warranty = 0;
                   
                    if (is_numeric($val->id)) {
                        if(isset($input['is_warranty'][$val->id])) { $is_warranty = 1; }
                        $item['qty'] = $input['qty'][$val->id];
                        $item['is_warranty'] = $is_warranty;
                        $item['price'] = $input['price'][$val->id];
                        $item['status'] = 1; 
                        
                        DB::table('quotation_item')
                            ->where('id', $val->id)
                            ->update($item);
                    }
                }
                DB::commit();
                $data["title"] = "Send Quotation";
                $data["body"]  = "Quotation";
                $data['common_details'] = $this->getById($id);
                $data['details'] = $this->getQuotationById($id); 
                $data["to_email"] = $data['common_details']->branch_to_email;
                $data["to_name"] = $data['common_details']->branch_to_name;
                $data["from_email"] = $data['common_details']->branch_from_email;
                $data["from_name"] = $data['common_details']->branch_from_name;

                $query = DB::table('quotation')->find($id); 
                $this->sendEmail($data, $id);
                $quotationData = [
                    'parent_id' => $id,
                    'comment' => '('.$query->serial_no.') Quotation created',
                    'created_by' => $query->created_by,
                    'module' => 'quotation'
                ];
                $this->sendNotification($query->branch_to, $quotationData);
                return $query;

            }
            else {
            
                $query = DB::table('quotation')->find($id); 
                $quotation['status'] = 1;
                $quotation['quotation_id'] = $id;
                $quotation['branch_from'] = $query->branch_from;
                $quotation['branch_to'] = $query->branch_to;
                $quotation['created_by'] = auth()->user()->id;
                $quotation['flag'] = 2;
                //$item_qry = DB::table('quotation_item')->where('parent_id', $id)->get();
                $order_id = DB::table('order')->insertGetId($quotation);
                $item_ex =  explode("," , $input['chk']);
                foreach ($item_ex as $k => $v) {
                    if (is_numeric($v)) {
                        $val = DB::table('quotation_item')->where('id', $v)->first();
                        $item['qty'] = $val->qty;
                        $item['price'] = $val->price;
                        $item['status'] = 0;
                        $item['parent_id'] = $order_id;
                        $item['catalog_id'] = $val->catalog_id;
                        $item['is_warranty'] = $val->is_warranty;
                       
                        DB::table('order_item')->insertGetId($item);
                    }
                }
                DB::table('quotation')->where('id', $id)->update(['status' => 2]);
                DB::commit();
                $data["title"] = "Send Order";
                $data["body"]  = "Order";
                $data['common_details'] = $this->getById($id);
                $data['details'] = $this->getQuotationById($id); 
                $data["from_email"] = $data['common_details']->branch_to_email;
                $data["from_name"] = $data['common_details']->branch_to_name;
                $data["to_email"] = $data['common_details']->branch_from_email;
                $data["to_name"] = $data['common_details']->branch_from_name;
                //$this->sendEmail($data, $id);
                return $order_id;
            }
            
        }
        throw new GeneralException(__('There was a problem creating this enquiry. Please try again.'));
    }

    public function sendEmail($data, $id) {
        
        // $pdf = PDF::loadView('frontend.mail.pdf', $data);
        // Mail::send('frontend.mail.mail', $data, function($message)use($data, $pdf) {
        //     $message->to($data["to_email"], $data["to_name"])
        //             ->subject($data["title"])
        //             ->attachData($pdf->output(), $data["body"].".pdf");
        // });
    }

    public function sendNotification($branch_id, $data) {

        $sales_users = $this->get_branch_users($branch_id);
        foreach($sales_users as $key => $value) {
            Notification::send(User::find($key), new QuotaionNotification($data));
        }
        return true;
      
    }

    public function itemUpdate(array $input, $id) {
        DB::table('quotation_item')->where('id', $id)->update(['qty' => $input['qty'],'price' => $input['price'],'is_warranty' => $input['is_warranty']]);
        return $id;
    }
    
}
