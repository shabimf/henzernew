<?php

namespace App\Repositories\Frontend;
use App\Exceptions\GeneralException;
use App\Models\CartItem;
use App\Repositories\BaseRepository;
use DB;
use Auth;
use App\Http\Traits\CommonTrait;

class CartRepository extends BaseRepository
{
    use CommonTrait;
    const MODEL = CartItem::class;

    public function retrieveList(array $options = [])
    {
        
        $perPage = isset($options['per_page']) ? (int) $options['per_page'] : 10;
        $orderBy = isset($options['order_by']) && in_array($options['order_by'], $this->sortable) ? $options['order_by'] : 'created_at';
        $order = isset($options['order']) && in_array($options['order'], ['asc', 'desc']) ? $options['order'] : 'desc';
        $query = $this->query()->leftjoin('users', 'users.id', '=', 'cart_items.created_by')
                        ->leftjoin('catalogs', 'catalogs.id', '=', 'cart_items.catalog_id') 
                        ->select([
                        'cart_items.id',
                        'catalogs.name as catalog_id',
                        'cart_items.qty',
                        'cart_items.price',
                        'users.first_name as created_by',
                        'cart_items.updated_by',
                        'cart_items.created_at',
                        'cart_items.updated_at'
                    ])

                    ->where('cart_items.created_by', auth()->user()->id)
                    ->orderBy($orderBy, $order);

        if ($perPage == -1) {
            return $query->get();
        }

        return $query->paginate($perPage);
    }

    public function create(array $input)
    {
        $input['created_by'] =  auth()->user()->id;
        $input['status'] = 1;
        $input['price'] = $this->getPrice($input['catalog_id'], Auth::user()->branch_id);

        $cart_qry = CartItem::where('catalog_id', $input['catalog_id'])
                ->where('created_by', $input['created_by'])
                ->first();

        if($cart_qry === null ) {
            if ($cart = CartItem::create($input)) {
                return $cart;
            }
        } else {

            $input['qty'] = $cart_qry->qty + $input['qty'];
            $input['updated_by'] = auth()->user()->id;  
            $cart = CartItem::find($cart_qry->id); 
            if ($cart->update($input)) {    
                return $cart;
            }
        }

        throw new GeneralException(__('There was a problem creating this enquiry. Please try again.'));
    }

    public function delete($id)
    {
        $item = CartItem::find($id);
        if ($item->delete()) {
            return true;
        }

        throw new GeneralException(__('There was a problem deleting this cart. Please try again.'));
    }
    
    public function itemUpdate(array $input, $id)
    {
        $input['qty'] = $input['qty'];
        $input['updated_by'] = auth()->user()->id;  
        $cart = CartItem::find($id); 
        if ($cart->update($input)) {    
            return $cart;
        }
        
        throw new GeneralException(__('There was a problem creating this cart. Please try again.'));
    }
}