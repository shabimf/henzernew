<?php

namespace App\Repositories\Frontend;
use App\Exceptions\GeneralException;
use App\Models\CartItem;
use App\Models\Enquiry;
use App\Models\EnquiryItem;
use App\Repositories\BaseRepository;
use App\Http\Traits\CommonTrait;
use App\Models\Auth\User;
use Notification;
use App\Notifications\Frontend\EnquiryNotification;
use DB;
use Auth;
use PDF;
use Mail;

class EnquiryRepository extends BaseRepository
{
    use CommonTrait;
    const MODEL = CartItem::class;

    public function retrieveList(array $options = [])
    {
        
        $perPage = isset($options['per_page']) ? (int) $options['per_page'] : 10;
        $orderBy = isset($options['order_by']) && in_array($options['order_by'], $this->sortable) ? $options['order_by'] : 'created_at';
        $order = isset($options['order']) && in_array($options['order'], ['asc', 'desc']) ? $options['order'] : 'desc';
        $query = $this->query()->leftjoin('users', 'users.id', '=', 'cart_items.created_by')
                        ->leftjoin('catalogs', 'catalogs.id', '=', 'cart_items.catalog_id') 
                        ->select([
                        'cart_items.id',
                        'catalogs.name as catalog_id',
                        'cart_items.qty',
                        'cart_items.price',
                        'users.first_name as created_by',
                        'cart_items.updated_by',
                        'cart_items.created_at',
                        'cart_items.updated_at'
                    ])

                    ->where('cart_items.created_by', auth()->user()->id)
                    ->orderBy($orderBy, $order);

        if ($perPage == -1) {
            return $query->get();
        }

        return $query->paginate($perPage);
    }

    public function retrieveEnquiryList(array $options = [])
    {
        return $this->ListItems("enquiry", $options);
    }
    
    public function getEnquiryById($id)
    {   
       return  $this->viewItems('enquiry', $id);             
    }

    public function getById($id)
    {   
       return  $this->viewItemsID('enquiry', $id);             
    }

    public function create(array $input)
    {
        $input['created_by'] =  auth()->user()->id;
        $input['status'] = 1;
        $input['price'] = 500;

        $cart_qry = CartItem::where('catalog_id', $input['catalog_id'])
                ->where('created_by', $input['created_by'])
                ->first();

        if($cart_qry === null ) {
            if ($cart = CartItem::create($input)) {
                return $cart;
            }
        } else {

            $input['qty'] = $cart_qry->qty + $input['qty'];
            $input['updated_by'] = auth()->user()->id;  
            $cart = CartItem::find($cart_qry->id); 
            if ($cart->update($input)) {    
                return $cart;
            }
        }

        throw new GeneralException(__('There was a problem creating this enquiry. Please try again.'));
    }

    public function createEnquiry(array $input) {

        $input['created_by'] = auth::id();
        $enquiry = Enquiry::create($input);
        return $enquiry->id;

    }

    public function enquirySend(array $input)
    {
        
        if (is_array($input['cart_id']) && count($input['cart_id'])) {
            
           
            $enquiry['status'] = 1;
            $enquiry['created_by'] = auth()->user()->id;
            $enquiry['branch_from'] = Auth::user()->branch_id;
            $enquiry['branch_to'] = $this->getToBranchId();
            DB::beginTransaction();
            $id = DB::table('enquiry')->insertGetId($enquiry);
            if ($id > 0) {
                $this->update_serial_number('enquiry','enquiry', $id);
                $list_enquiry = DB::table('enquiry')->find($id); 
                foreach ($input['cart_id'] as $cart_id) {
                    if (is_numeric($cart_id)) {
                        $cart_items = DB::table('cart_items')->find($cart_id); 
                        $enquiry_item['parent_id'] = $id;
                        $enquiry_item['qty'] = $cart_items->qty;
                        $enquiry_item['catalog_id'] = $cart_items->catalog_id;
                        $enquiry_item['price'] = $cart_items->price;
                        $enquiry_item['status'] = 1;
                        DB::table('enquiry_item')->insertGetId($enquiry_item);
                        DB::table('cart_items')->where('id',$cart_id)->delete();
                    }
                }
                DB::commit();
                return $list_enquiry;
            }
            
            throw new GeneralException(__('There was a problem sending this enquiry. Please try again.'));
           
        }

    }

    public function submitEnquiry(array $input, $id)
    {
        return $this->insertItems('enquiry', $input, $id); 
    }
    
    public function updateEnquiry(array $input, $id)
    {
       
        DB::beginTransaction();
        if ($id) {
            if($input['status'] == "enquiry") {
                $qty =  $input['qty'];
                $this->update_serial_number('enquiry','enquiry', $id);
                DB::table('enquiry')->where('id', $id)->update(['status' => 1]);
                $item_qry = DB::table('enquiry_item')->where('parent_id', $id)->get();
                foreach ($item_qry as $key => $val) {
                    if (is_numeric($val->id)) {
                        $item['qty'] = $qty[$val->id];
                        $item['status'] = 1;
                        DB::table('enquiry_item')
                            ->where('id', $val->id)
                            ->update($item);
                    }
                }
                DB::commit();
                
               
                $data["title"] = "Send Enquiry";
                $data["body"]  = "Enquiry";
                $data['common_details'] = $this->getById($id);
                $data['details'] = $this->getEnquiryById($id); 
                $data["to_email"] = $data['common_details']->branch_to_email;
                $data["to_name"] = $data['common_details']->branch_to_name;
                $data["from_email"] = $data['common_details']->branch_from_email;
                $data["from_name"] = $data['common_details']->branch_from_name;
                //$this->sendEmail($data, $id);
                $query = DB::table('enquiry')->find($id);                 
                $enquiryData = [
                        'parent_id' => $id,
                        'comment' => '('.$query->serial_no.') Enquiry created',
                        'created_by' => $query->created_by,
                        'module' => 'enquiry'
                    ];
                $this->sendNotification($query->branch_to, $enquiryData);
                return $query;
               
            }
            else {
               
                $query = DB::table('enquiry')->find($id); 
               
                $quotation['status'] = 0;
                $quotation['enquiry_id'] = $id;
                $quotation['branch_from'] = $query->branch_to;
                $quotation['branch_to'] = $query->branch_from;              
                $quotation['created_by'] = auth()->user()->id;
         
                $quote_id = DB::table('quotation')->insertGetId($quotation);
                $item_ex =  explode("," , $input['chk']);
                foreach ($item_ex as $k => $v) {
                    if (is_numeric($v)) {
                        $val = DB::table('enquiry_item')->where('id', $v)->first();
                        $item['qty'] = $val->qty;
                        $item['price'] = $val->price;
                        $item['status'] = 0;
                        $item['parent_id'] = $quote_id;
                        $item['catalog_id'] = $val->catalog_id;
                        DB::table('quotation_item')->insertGetId($item);
                    }
                }

                DB::table('enquiry')->where('id', $id)->update(['status' => 2]);
                DB::commit();

                $data["title"] = "Send Quotation";
                $data["body"]  = "Quotation";
                $data['common_details'] = $this->getById($id);
                $data['details'] = $this->getEnquiryById($id); 
                $data["from_email"] = $data['common_details']->branch_to_email;
                $data["from_name"] = $data['common_details']->branch_to_name;
                $data["to_email"] = $data['common_details']->branch_from_email;
                $data["to_name"] = $data['common_details']->branch_from_name;
                //$this->sendEmail($data, $id);
                return $quote_id;
            }
            
        }
        throw new GeneralException(__('There was a problem creating this enquiry. Please try again.'));
    }
    
    public function sendEmail($data, $id) {
        $pdf = PDF::loadView('frontend.mail.pdf', $data);
        Mail::send('frontend.mail.mail', $data, function($message)use($data, $pdf) {
            $message->to($data["to_email"], $data["to_name"])
                    ->subject($data["title"])
                    ->attachData($pdf->output(), $data["body"].".pdf");
        });
    }
    
    
    public function delete($id)
    {
        $item = EnquiryItem::find($id);
        if ($item->delete()) {
            return true;
        }

        throw new GeneralException(__('exceptions.backend.blog-category.delete_error'));
    }

    public function sendNotification($branch_id, $data) {

        $sales_users = $this->get_branch_users($branch_id);
        foreach($sales_users as $key => $value) {
            Notification::send(User::find($key), new EnquiryNotification($data));
        }
        return true;
      
    }
   
}
