<?php

namespace App\Repositories\Frontend;
use App\Exceptions\GeneralException;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Invoice;
use App\Repositories\BaseRepository;
use App\Http\Traits\CommonTrait;
use App\Models\Auth\User;
use Notification;
use App\Notifications\Frontend\OrderNotification;
use App\Notifications\Frontend\InvoiceNotification;
use DB;
use Auth;
use PDF;
use Mail;

class OrderRepository extends BaseRepository   
{
    use CommonTrait;

    const MODEL = Order::class;

    public function retrieveList(array $options = [])
    {
        return $this->ListItems("order", $options);
    }

    public function getOrderById($id)
    {   
        return  $this->viewItems('order', $id);
    }

    public function getById($id)
    {   
       return  $this->viewItemsID('order', $id);             
    }

    public function create(array $input)
    {
        $input['created_by'] = auth::id();
        $input['flag'] = 3;
        $quotation = Order::create($input);
        return $quotation->id;
    }

    public function delete($id)
    {
        $item = OrderItem::find($id);
        if ($item->delete()) {
            return true;
        }

        throw new GeneralException(__('exceptions.backend.blog-category.delete_error'));
    }

    public function submit(array $input, $id)
    {
        return $this->insertItems('order', $input, $id); 
    }

    public function update(array $input, $id)
    { 
      
        DB::beginTransaction();
        if ($id) {
            if($input['status'] == "order") {
                $qty =  $input['qty'];
                $is_warranty =  $input['is_warranty'];
                $price =  $input['price']; 
                $this->update_serial_number('order','order', $id);
                DB::table('order')->where('id', $id)->update(['status' => 1,'flag' => 2]);               
                $item_qry = DB::table('order_item')->where('parent_id', $id)->get();

                foreach ($item_qry as $key => $val) {
                    $is_warranty = 0;
                    if (is_numeric($val->id)) {
                        if(isset($input['is_warranty'][$val->id])) { $is_warranty = 1; }
                        $item['qty'] = $qty[$val->id];
                        $item['is_warranty'] = $is_warranty;
                        $item['price'] = $input['price'][$val->id];
                        $item['status'] = 1;
                        DB::table('order_item')
                            ->where('id', $val->id)
                            ->update($item);
                    }
                }
                DB::commit();

                $data["title"] = "Send Order";
                $data["body"]  = "Order";
                $data['common_details'] = $this->getById($id);
                $data['details'] = $this->getOrderById($id); 
                $data["to_email"] = $data['common_details']->branch_to_email;
                $data["to_name"] = $data['common_details']->branch_to_name;
                $data["from_email"] = $data['common_details']->branch_from_email;
                $data["from_name"] = $data['common_details']->branch_from_name;
                $this->sendEmail($data, $id);
                $query = DB::table('order')->find($id); 
                $orderData = [
                    'parent_id' => $id,
                    'comment' => '('.$query->serial_no.') Order created',
                    'created_by' => $query->created_by,
                    'module' => 'order'
                ];
                $this->sendOrderNotification($query->branch_to, $orderData);
                return $query;
            }
            elseif($input['status'] == "salesorder") {
                $api = (isset($input['api'])?1:0);
                DB::table('order')->where('id', $id)->update(['status' => 3]);
                if( $api == 0 ) {
                    $item_qry = DB::table('order_item')->where('parent_id', $id)->get();
                    foreach ($item_qry as $key => $val) {
                        if (is_numeric($val->id)) {
                            $item['status'] = 2;
                            if (count($input['selector']) > 0) {
                                foreach ($input['selector'] as $k => $v) {
                                    if ($val->id == $v) {
                                        DB::table('order_item')
                                            ->where('id', $val->id)
                                            ->update($item);
                                    }
                                }
                            }
                        }
                    }
                    DB::commit();
                }
                return true;
            } 
            // else if($input['status'] == "approve") {
            //     DB::table('order')->where('id', $id)->update(['status' => 4]);
            //     DB::commit();
            //     return true;
            // } else if($input['status'] == "reject") {
            //     DB::table('order')->where('id', $id)->update(['status' => 5]);
            //     DB::commit();
            //     return true;
            // } 
            else { 
              $errors = [];
              $api = (isset($input['api'])?1:0);
              $query = DB::table('order')->find($id); 
              $order['status'] = 2;
              $order['order_id'] = $id;
              $order['branch_from'] = $query->branch_from;
              $order['branch_to'] = $query->branch_to;
              $order['created_by'] = auth()->user()->id;
              $invoice_id = DB::table('invoice')->insertGetId($order);
              $this->update_serial_number('invoice','invoice', $invoice_id);
              $total_qty = 0;
              if($api == 1) {
               if (is_array($input['item_id']) && count($input['item_id'])) {
                   
                   foreach ($input['item_id'] as $val) {
                       $query_item = DB::table('order_item')->find($val); 
                       if (is_numeric($val)) {
                        $stock = $this->is_in_stock($query_item->catalog_id, $query->branch_from);  
                        $item['qty'] = $query_item->qty;
                        if($item['qty'] > $stock) {
                            if ($stock > 0 ) {
                                $msg = $this->FieldByID('catalogs','name',$query_item->catalog_id). ' '.($item['qty']-$stock).' out of stock, '.$stock.' is available';
                            } else {
                                $msg = $this->FieldByID('catalogs','name',$query_item->catalog_id).' is Out of stock';
                            }
                            $errors[] = $msg;    
                        }
                        $item['price'] = $query_item->price;   
                        $item['status'] = 1;
                        $item['parent_id'] = $invoice_id;
                        $item['catalog_id'] = $query_item->catalog_id;
                        $item['is_warranty'] = $query_item->is_warranty;
                        DB::table('invoice_item')->insertGetId($item);
                        $total_qty += checkInvoiceItemCount($id, $item['catalog_id']);
                        $this->updateStock($query_item->catalog_id, $query->branch_from, $query->branch_to, $item['qty']);      
                       }
                   }
                 DB::table('order')->where('id', $id)->update(['status' => 2]);
                 if (count($errors) > 0) {
                       return $errors;
                    } else {
                        DB::commit();
                        return 1;
                    } 
               }
              } else {
             
              if ( count($input['selector']) > 0 ) {
              
                foreach ($input['selector'] as $key => $val ) {
                    $query_item = DB::table('order_item')->find($val); 
                    if (is_numeric($val)) {
                        $stock = $this->is_in_stock($query_item->catalog_id, $query->branch_from);       
                        $item['qty'] = $input['qty_'.$val];
                        if($item['qty'] > $stock) {
                            if ($stock > 0 ) {
                                $msg = $this->FieldByID('catalogs','name',$query_item->catalog_id). ' '.($item['qty']-$stock).' out of stock, '.$stock.' is available';
                            } else {
                                $msg = $this->FieldByID('catalogs','name',$query_item->catalog_id).' is Out of stock';
                            }
                            $errors[] = $msg;    
                        }
                        $item['price'] = $query_item->price;   
                        $item['status'] = 1;
                        $item['parent_id'] = $invoice_id;
                        $item['catalog_id'] = $query_item->catalog_id;
                        $item['is_warranty'] = $query_item->is_warranty;
                        DB::table('invoice_item')->insertGetId($item);
                        $total_qty += checkInvoiceItemCount($id, $item['catalog_id']);
                        $this->updateStock($query_item->catalog_id, $query->branch_from, $query->branch_to, $item['qty']);    
                    }
                } 
                DB::table('order')->where('id', $id)->update(['status' => 2]);  
                if (count($errors) > 0) {
                    return $errors;
                } else {   
                     
                    DB::commit();
                } 
              }
            }
        }
        }
        throw new GeneralException(__('There was a problem creating this enquiry. Please try again.'));
    }

    public function sendEmail($data, $id) {
        
        // $pdf = PDF::loadView('frontend.mail.pdf', $data);
        // Mail::send('frontend.mail.mail', $data, function($message)use($data, $pdf) {
        //     $message->to($data["to_email"], $data["to_name"])
        //             ->subject($data["title"])
        //             ->attachData($pdf->output(), $data["body"].".pdf");
        // });
    }

    public function sendOrderNotification($branch_id, $data) {

        $sales_users = $this->get_branch_users($branch_id);
        foreach($sales_users as $key => $value) {
            Notification::send(User::find($key), new OrderNotification($data));
        }
        return true;
      
    }

    public function sendInvoiceNotification($branch_id, $data) {

        $sales_users = $this->get_branch_users($branch_id);
        foreach($sales_users as $key => $value) {
            Notification::send(User::find($key), new InvoiceNotification($data));
        }
        return true;
      
    }

    public function itemUpdate(array $input, $id) {
        DB::table('order_item')->where('id', $id)->update(['qty' => $input['qty'],'price' => $input['price'],'is_warranty' => $input['is_warranty']]);
        return $id;
    }
}
