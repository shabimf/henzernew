<?php

namespace App\Repositories\Frontend;
use App\Exceptions\GeneralException;
use App\Models\Branch;
use App\Repositories\BaseRepository;
use Auth;
use App\Http\Traits\CommonTrait;

class VendorRepository extends BaseRepository
{
    use CommonTrait;
    const MODEL = Branch::class;

    public function retrieveList(array $options = [])
    {
        $perPage = isset($options['per_page']) ? (int) $options['per_page'] : 20;
        $orderBy = isset($options['order_by']) && in_array($options['order_by'], $this->sortable) ? $options['order_by'] : 'created_at';
        $order = isset($options['order']) && in_array($options['order'], ['asc', 'desc']) ? $options['order'] : 'desc';
        $qry =  $this->query()->leftjoin('users', 'users.id', '=', 'branches.created_by')
                        ->leftjoin('branch_details', 'branches.id', '=', 'branch_details.parent_id')
                        ->select([
                        'branches.*',
                        'branch_details.*',
                        'users.first_name as created_by'
                        ]);   
        if ( Auth::user()->branch_id > 0) {
            $qry->where('branches.branch_id', Auth::user()->branch_id);
        }   else {
            $qry->where('branches.branch_id', '!=', 0); 
        }
        $qry->where('branches.type','=', 3)
            ->orderBy($orderBy, $order);

        $query = $qry;

        if ($perPage == -1) {
            return $query->get();
        }
        return $query->paginate($perPage);
    }

}
