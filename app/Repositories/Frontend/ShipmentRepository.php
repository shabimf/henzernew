<?php

namespace App\Repositories\Frontend;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use App\Models\Shipment;
use App\Models\ShipmentItem;
use App\Http\Traits\CommonTrait;
use App\Models\Auth\User;
use DB;
use Auth;
use Session;

class ShipmentRepository extends BaseRepository
{
    use CommonTrait;

    Public function retrieveList(array $options = []) {
        $perPage = isset($options['per_page']) ? (int) $options['per_page'] : 10;
        $api = isset($options['api']) ? (int) $options['api']  : 0;
        $orderBy = isset($options['order_by']) && in_array($options['order_by'], $this->sortable) ? $options['order_by'] : 'created_at';
        $order = isset($options['order']) && in_array($options['order'], ['asc', 'desc']) ? $options['order'] : 'desc';
        DB::enableQueryLog(); 
        $qry = DB::table('shipment as parent')
                ->leftjoin('users', 'users.id', '=', 'parent.created_by')
                ->leftjoin('shipment_item as item','item.parent_id', '=', 'parent.id')  
                ->leftjoin('invoice','item.invoice_id', '=', 'invoice.id')  
                ->leftjoin('cargo_mode', 'cargo_mode.id', '=', 'parent.mode_id')
                ->leftjoin('agent', 'agent.id', '=', 'parent.agent_id')           
                ->select([       
                \DB::raw('
                    parent.*, 
                    users.first_name as created_by,
                    cargo_mode.name as cargo_name,  
                    agent.name as agent_name,
                    invoice.serial_no'             
                )
                ]);

        if(!isset($options['branch_to']) && $this->getRoleName() == "Vendor" || $this->getRoleName() == "Sub Vendor") {
            if(Session::get('is_source') == 1 || $api == 1)  $qry->where('invoice.branch_to', Auth::user()->branch_id);
            else $qry->where('invoice.branch_from', Auth::user()->branch_id); 
        } 
        
         if(!isset($options['branch_from']) && $this->getRoleName() == "Sourcing Team") {
            $qry->where('invoice.branch_from', Auth::user()->branch_id);
        }  
        if (isset($options['serial_num'])) {
            $qry->where('parent.track_no', 'LIKE', "%{$options['serial_num']}%");  
        }
        if (isset($options['branch_from'])) { 
            $qry->where('invoice.branch_from', $options['branch_from']);
        }
        if (isset($options['branch_to'])) {
            $qry->where('invoice.branch_to', $options['branch_to']);
        } 
        if (isset($options['d_from'])) {
            $qry->where('parent.created_at', '>=', date('Y-m-d', strtotime($options['d_from']))." 00:00:00");
        }
        if (isset($options['d_to'])) {
            $qry->where('parent.created_at', '<=', date('Y-m-d', strtotime($options['d_to']))." 23:59:59");
        }
        if (isset($options['status'])) {
            $qry->where('parent.status', $options['status']);
        }
        $qry->orderBy($orderBy, $order)->groupBy('parent.id');
        $query = $qry;

        if ($perPage == -1) {
            return $query->get();
        }
        return $query->paginate($perPage);
    }

    public function retrieveListItems(array $options = [])
    {
        
        $serial_num = isset($options['serial_num']) ?  $options['serial_num'] : '';
        $type = isset($options['type']) ?  $options['type'] : '';
        if ($type == 2) {
            $query = DB::table('rma_item as item')
                    ->leftJoin('rma as parent', 'item.parent_id', '=', 'parent.id')
                    ->leftjoin('catalogs', 'item.catalog_id', '=', 'catalogs.id')
                    ->leftjoin('series', 'series.id', '=', 'catalogs.series_id')
                    ->leftjoin('models', 'models.id', '=', 'series.model_id')
                    ->leftjoin('brands', 'brands.id', '=', 'models.brand_id')
                    ->leftjoin('users', 'users.id', '=', 'parent.created_by')
                    ->leftjoin('branches as from','parent.branch_from', '=', 'from.id')
                    ->leftjoin('branches as to','parent.branch_to', '=', 'to.id')
                    ->select([
                        'catalogs.name as product_name',
                        'series.name as series_name',
                        'models.name as model_name',
                        'brands.name as brand_name',                       
                        'item.*'
                    ])
                    ->where('parent.serial_no', $output)
                    ->groupBy(DB::raw("item.catalog_id"))->get();
        } else  {
            
            $query = DB::table('invoice_item as item')
                    ->leftJoin('invoice as parent', 'item.parent_id', '=', 'parent.id')
                    ->leftjoin('catalogs', 'item.catalog_id', '=', 'catalogs.id')
                    ->leftjoin('series', 'series.id', '=', 'catalogs.series_id')
                    ->leftjoin('models', 'models.id', '=', 'series.model_id')
                    ->leftjoin('brands', 'brands.id', '=', 'models.brand_id')
                    ->leftjoin('users', 'users.id', '=', 'parent.created_by')
                    ->leftjoin('branches as from','parent.branch_from', '=', 'from.id')
                    ->leftjoin('branches as to','parent.branch_to', '=', 'to.id')
                    ->select([
                        'catalogs.name as product_name',
                        'series.name as series_name',
                        'models.name as model_name',
                        'brands.name as brand_name',                       
                        'item.*',
                        'parent.serial_no'
                    ])
                    ->whereIn('parent.id', $serial_num)->get();    
         }
            
        return $query;
    }

    // public function retrieveShippingListItems ($id) {
    //     $query = DB::table('shipment_item as item')
    //     ->leftJoin('shipment as parent', 'item.parent_id', '=', 'parent.id')
    //     ->leftjoin('catalogs', 'item.catalog_id', '=', 'catalogs.id')
    //     ->leftjoin('series', 'series.id', '=', 'catalogs.series_id')
    //     ->leftjoin('models', 'models.id', '=', 'series.model_id')
    //     ->leftjoin('brands', 'brands.id', '=', 'models.brand_id')
    //     ->leftjoin('users', 'users.id', '=', 'parent.created_by')
    //     ->leftjoin('cargo_mode', 'cargo_mode.id', '=', 'parent.mode_id')
    //     ->leftjoin('agent', 'agent.id', '=', 'parent.agent_id')
    //     ->select([
    //         'catalogs.name as product_name',
    //         'series.name as series_name',
    //         'models.name as model_name',
    //         'brands.name as brand_name',    
    //         'cargo_mode.name as cargo_name',   
    //         'agent.name as agent_name',                
    //         'item.*',
    //         'parent.*'
    //     ])
    //     ->where('parent.invoice_id', $id)->get();
    //    return $query;
    // }

    public function create(array $input)
    {
        $table = "invoice";
        $query = DB::table($table)->where('id', 5)->first();
        $input_arr['created_by'] = auth::id();
        $input_arr['track_no'] = $input['track_no'];
        $input_arr['mode_id'] = $input['mode_id'];
        $input_arr['agent_id'] = $input['agent_id'];
        $input_arr['track_link'] = $input['track_link'];
        $input_arr['carton'] = $input['carton'];
        
        $result = Shipment::create($input_arr);
        $item_arr['parent_id'] =   $result->id;
       
        if (count($input['item_id']) > 0) {
            foreach ($input['item_id'] as $v) {
                $item_arr['catalog_id'] =  $input['catalog_id'][$v];
                $item_arr['qty'] =  $input['qty'][$v];
                $item_arr['invoice_id'] =  $input['invoice_id'][$v];
                $item_arr['status'] =  1;
                ShipmentItem::create($item_arr);
            }
        }
        return $result->id;

    }

    public function getShipmentById($id) {

        $query = DB::table('shipment_item as item')
        ->leftJoin('shipment as parent', 'item.parent_id', '=', 'parent.id')
        ->leftjoin('catalogs', 'item.catalog_id', '=', 'catalogs.id')
        ->leftjoin('invoice', 'item.invoice_id', '=', 'invoice.id')
        ->leftjoin('series', 'series.id', '=', 'catalogs.series_id')
        ->leftjoin('models', 'models.id', '=', 'series.model_id')
        ->leftjoin('brands', 'brands.id', '=', 'models.brand_id')
        ->leftjoin('users', 'users.id', '=', 'parent.created_by')
        ->select([
            'catalogs.name as product_name',
            'invoice.serial_no as invoice_number',
            'invoice.id as invoice_id',
            'series.name as series_name',
            'models.name as model_name',
            'brands.name as brand_name',                       
            'item.*'
        ])
        ->where('item.parent_id', $id)->get();
       return $query;
    }

    public function getEnquiryById($id) {
      
        $query = DB::table('shipment_item as item')
        ->leftJoin('shipment as parent', 'item.parent_id', '=', 'parent.id')
        ->leftjoin('invoice', 'item.invoice_id', '=', 'invoice.id')
        ->leftjoin('order', 'invoice.order_id', '=', 'order.id')
        ->leftjoin('quotation', 'order.quotation_id', '=', 'quotation.id')
        ->leftjoin('enquiry', 'quotation.enquiry_id', '=', 'enquiry.id')
        ->leftjoin('enquiry_item', 'enquiry_item.parent_id', '=', 'enquiry.id')
        ->leftjoin('catalogs', 'enquiry_item.catalog_id', '=', 'catalogs.id')
        ->leftjoin('branches as branch_from', 'branch_from.id', '=', 'enquiry.branch_from')
        ->leftjoin('branches as branch_to', 'branch_to.id', '=', 'enquiry.branch_to')
        ->leftjoin('series', 'series.id', '=', 'catalogs.series_id')
        ->leftjoin('models', 'models.id', '=', 'series.model_id')
        ->leftjoin('brands', 'brands.id', '=', 'models.brand_id')
        ->leftjoin('users', 'users.id', '=', 'parent.created_by')
        ->select([
            'catalogs.name as product_name',
            'enquiry.created_at as date',
            'enquiry.id as enquiry_id',
            'enquiry.serial_no as enquiry_number',
            'enquiry_item.price as price',
            'enquiry_item.qty as enquiry_qty',
            'enquiry.status as enquiry_status',
            'branch_from.name as brnch_from',
            'branch_to.name as brnch_to',
            'series.name as series_name',
            'models.name as model_name',
            'brands.name as brand_name',
            'item.*'
        ])
        ->where('item.parent_id', $id)
        ->where('quotation.enquiry_id','>', 0)
        ->get();
         return $query;

    }
    public function getQuotationById($id) {
      
        $query = DB::table('shipment_item as item')
        ->leftJoin('shipment as parent', 'item.parent_id', '=', 'parent.id')
        ->leftjoin('invoice', 'item.invoice_id', '=', 'invoice.id')
        ->leftjoin('order', 'invoice.order_id', '=', 'order.id')
        ->leftjoin('quotation', 'order.quotation_id', '=', 'quotation.id')
        ->leftjoin('quotation_item', 'quotation_item.parent_id', '=', 'quotation.id')
        ->leftjoin('catalogs', 'quotation_item.catalog_id', '=', 'catalogs.id')
        ->leftjoin('branches as branch_from', 'branch_from.id', '=', 'quotation.branch_from')
        ->leftjoin('branches as branch_to', 'branch_to.id', '=', 'quotation.branch_to')
        ->leftjoin('series', 'series.id', '=', 'catalogs.series_id')
        ->leftjoin('models', 'models.id', '=', 'series.model_id')
        ->leftjoin('brands', 'brands.id', '=', 'models.brand_id')
        ->leftjoin('users', 'users.id', '=', 'parent.created_by')
        ->select([
            'catalogs.name as product_name',
            'quotation.created_at as date',
            'quotation.id as quotation_id',
            'quotation.serial_no as quotation_number',
            'quotation_item.price as price',
            'quotation_item.qty as quotation_qty',
            'quotation.status as quotation_status',
            'branch_from.name as brnch_from',
            'branch_to.name as brnch_to',
            'series.name as series_name',
            'models.name as model_name',
            'brands.name as brand_name',
            'item.*'
        ])
        ->where('item.parent_id', $id)
         ->where('order.quotation_id','>', 0)
        ->get();
         return $query;
    }
    
    public function getOrderById($id) {
        // DB::enableQueryLog();
        $query = DB::table('shipment_item as item')
        ->leftJoin('shipment as parent', 'item.parent_id', '=', 'parent.id')
        ->leftjoin('invoice', 'item.invoice_id', '=', 'invoice.id')
        ->leftjoin('order', 'invoice.order_id', '=', 'order.id')
        ->leftjoin('order_item', 'order_item.parent_id', '=', 'order.id')
        ->leftjoin('catalogs', 'order_item.catalog_id', '=', 'catalogs.id')
        ->leftjoin('branches as branch_from', 'branch_from.id', '=', 'order.branch_from')
        ->leftjoin('branches as branch_to', 'branch_to.id', '=', 'order.branch_to')
        ->leftjoin('series', 'series.id', '=', 'catalogs.series_id')
        ->leftjoin('models', 'models.id', '=', 'series.model_id')
        ->leftjoin('brands', 'brands.id', '=', 'models.brand_id')
        ->leftjoin('users', 'users.id', '=', 'parent.created_by')
        ->select([
            'catalogs.name as product_name',
            'order.created_at as date',
            'order.id as order_id',
            'order.serial_no as order_number',
            'order_item.price as price',
            'order_item.qty as order_qty',
            'order.status as order_status',
            'branch_from.name as brnch_from',
            'branch_to.name as brnch_to',
            'series.name as series_name',
            'models.name as model_name',
            'brands.name as brand_name',
            'item.*'
        ])
        ->where('item.parent_id', $id)
        ->where('invoice.order_id','>', 0)
        ->get();
        // dd(DB::getQueryLog());
        // exit;
         return $query;
    }

    public function getInvoiceById($id) {
        
        $query = DB::table('shipment_item as item')
        ->leftJoin('shipment as parent', 'item.parent_id', '=', 'parent.id')
        ->leftjoin('invoice', 'item.invoice_id', '=', 'invoice.id')
        ->leftjoin('invoice_item', 'invoice_item.parent_id', '=', 'invoice.id')
        ->leftjoin('catalogs', 'invoice_item.catalog_id', '=', 'catalogs.id')
        ->leftjoin('branches as branch_from', 'branch_from.id', '=', 'invoice.branch_from')
        ->leftjoin('branches as branch_to', 'branch_to.id', '=', 'invoice.branch_to')
        ->leftjoin('series', 'series.id', '=', 'catalogs.series_id')
        ->leftjoin('models', 'models.id', '=', 'series.model_id')
        ->leftjoin('brands', 'brands.id', '=', 'models.brand_id')
        ->leftjoin('users', 'users.id', '=', 'parent.created_by')
        ->select([
            'catalogs.name as product_name',
            'invoice.created_at as date',
            'invoice.id as invoice_id',
            'invoice.serial_no as invoice_number',
            'invoice_item.price as price',
            'invoice_item.qty as qty',
            'invoice.status as status',
            'branch_from.name as brnch_from',
            'branch_to.name as brnch_to',
            'series.name as series_name',
            'models.name as model_name',
            'brands.name as brand_name',
            'item.*'
        ])
        ->where('item.parent_id', $id)
        ->get();
        return $query;
    } 

    public function getById($id)
    {
        $query = DB::table('shipment as parent')
        ->leftjoin('cargo_mode', 'cargo_mode.id', '=', 'parent.mode_id')
        ->leftjoin('agent', 'agent.id', '=', 'parent.agent_id')
        ->select([
            'parent.*',
            'cargo_mode.name as cargo_name',   
            'agent.name as agent_name',         
        ])
        ->where('parent.id', $id)->get();
        return $query[0];
    }

    public function update(array $input, $id)
    {       
        DB::beginTransaction();
        if ($id) {
            DB::beginTransaction();
            DB::table('shipment')->where('id', $id)->update(['status' => $input['status']]);
            DB::commit();
            return $id;
        }       
    }

}