<?php

namespace App\Repositories\Frontend;
use App\Exceptions\GeneralException;
use App\Models\Price;
use App\Repositories\BaseRepository;
use Auth;

class PriceRepository extends BaseRepository
{
    
    const MODEL = Price::class;

    public function retrieveList(array $options = [])
    {
        $perPage = isset($options['per_page']) ? (int) $options['per_page'] : 20;
        $orderBy = isset($options['order_by']) && in_array($options['order_by'], $this->sortable) ? $options['order_by'] : 'created_at';
        $order = isset($options['order']) && in_array($options['order'], ['asc', 'desc']) ? $options['order'] : 'desc';
        $qry = $this->query()->leftjoin('users', 'users.id', '=', 'catalog_price.created_by')
                        ->leftjoin('catalogs', 'catalogs.id', '=', 'catalog_price.catalog_id') 
                        ->select([
                        'catalog_price.id',
                        'catalogs.name as catalog_id',
                        'catalog_price.price',
                        'users.first_name as created_by',
                        'catalog_price.updated_by',
                        'catalog_price.created_at',
                        'catalog_price.updated_at'
                        ]);

        if(Auth::user()->branch_id > 0) {
            $qry->where('catalog_price.branch_id', Auth::user()->branch_id);
        }      
                    
        $qry->orderBy($orderBy, $order);
        $query = $qry;
        if ($perPage == -1) {
            return $query->get();
        }

        return $query->paginate($perPage);
    }


    public function create(array $input)
    {
       
        $input['created_by'] =  auth()->user()->id;
        $input['branch_id'] =  Auth::user()->branch_id;
        if ($stock = Price::create($input)) {
            return $stock;
        }
        throw new GeneralException(__('There was a problem creating this price. Please try again.'));
    }
    
}
